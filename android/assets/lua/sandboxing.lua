function CVM_RunInSandbox(sandbox_name, code, name)
    if _G[sandbox_name] == nil then
        _G[sandbox_name] = {}
        local metatable = {__index=_G}
        setmetatable(_G[sandbox_name], metatable)
    end
    sandbox = _G[sandbox_name]
    chunk, chunk_error = _G.load(code, name, "t")
    sandbox._G = sandbox
    if chunk == nil then
        return chunk_error
    end
    debug.setupvalue(chunk, 1, sandbox)
    chunk()
end
