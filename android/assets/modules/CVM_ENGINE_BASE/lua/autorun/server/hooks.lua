hook.DefineEvent("WorldCreated") -- Runs when a fresh world is created (NOT LOADED)
hook.DefineEvent("WorldLoaded") -- Runs when an existing world is loaded (NOT CREATED)
hook.DefineEvent("WorldStarted") -- Runs right after the above two events
hook.DefineEvent("Init") -- Runs right after the world has loaded, before the simulation begins UNIMPLEMENTED