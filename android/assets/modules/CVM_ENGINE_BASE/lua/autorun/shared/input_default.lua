hook.Add("DefineInputs", "DefaultInputs", function()
    for key_name, key_id in pairs(input) do
        if string.sub(key_name,1, 3) == "KEY" then
            input.Define(key_name, "Bool", false)
            if CLIENT then
                input.Bind(key_name, "Key", key_id)
            end
        end
    end
end)
