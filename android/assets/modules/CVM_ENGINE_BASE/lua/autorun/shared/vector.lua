local _vector_meta = {}

_vector_meta._cvm_type = "CVM_Vector"

function _vector_meta:Add(vec2)
    self.x = self.x + vec2.x
    self.y = self.y + vec2.y
end

function _vector_meta:Sub(vec2)
    self.x = self.x - vec2.x
    self.y = self.y - vec2.y
end

function _vector_meta:Mul(num)
    self.x = self.x * num
    self.y = self.y * num
end

function _vector_meta:Square()
    if self.x > 0 then
        self.x = self.x^2
    else
        self.x = -self.x^2
    end

    if self.y > 0 then
        self.y = self.y^2
    else
        self.y = -self.y^2
    end
end

function _vector_meta:Length()
    return math.sqrt(self.x*self.x + self.y*self.y)
end

function _vector_meta:Normalize()
    return self/self:Length()
end

function _vector_meta:Distance(other)
    return (other - self):Length()
end

function _vector_meta:Dot(other)
    return self.x*other.x + self.y*other.y
end

local metaTable = {
    __add = function (lhs, rhs)
        return Vector(lhs.x + rhs.x, lhs.y + rhs.y)
    end,
    __sub = function (lhs, rhs)
        return Vector(lhs.x - rhs.x, lhs.y - rhs.y)
    end,
    __mul = function (lhs, rhs)
        return Vector(lhs.x*rhs, lhs.y*rhs)
    end,
    __unm = function (vec)
        return Vector(-vec.x, -vec.y)
    end,
    __div = function (lhs, rhs)
        return Vector(lhs.x/rhs, lhs.y/rhs)
    end,
    __lt = function (lhs, rhs)
        return lhs.x < rhs.x and lhs.y < rhs.y
    end,
    __gt = function (lhs, rhs)
        return lhs.x > rhs.x and lhs.y > rhs.y
    end,
    __tostring = function(vec)
        return "Vector("..vec.x..", "..vec.y..")"
    end,
    __index = _vector_meta
}

function Vector(x, y)
    vec = {}
    vec.x = x
    vec.y = y
    setmetatable(vec, metaTable)
    return vec
end


lua.SetGlobal("Vector", Vector)
