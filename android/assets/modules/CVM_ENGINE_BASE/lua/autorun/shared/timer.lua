local timer_table = {}
local timers = {}
local unique_id = 0

timer_table.Create = function(id, delay, repetitions, func)
    timers[#timers + 1] = {
        id=id,
        delay=delay,
        repetitions=repetitions,
        next_run=CurTime()+delay,
        func=func
    }
end

timer_table.Simple = function(delay, func)
    timer_table.Create("CVM_SIMPLETIMER"..unique_id, delay, 1, func)
    unique_id = unique_id + 1
end

timer_table.Remove = function(id)
    for k, timer in ipairs(timers) do
        if timer.id == id then
            table.remove(timers, k)
            break
        end
    end
end

lua.SetGlobal("timer", timer_table)

hook.Add("Tick", "UpdateTimers", function()
    local curTime = CurTime()

    local dead_timers = {}
    for k, timer in ipairs(timers) do
        if curTime > timer.next_run then
            timer.func()
            timer.repetitions = timer.repetitions - 1

            if timer.repetitions == 0 then
                dead_timers[#dead_timers + 1] = k
            else
                timer.next_run = timer.next_run + timer.delay
                if timer.repetitions == -1 then
                    timer.repetitions = 0
                end
            end
        end
    end

    local id_shift = 0
    for k, dead_timer_id in ipairs(dead_timers) do
        table.remove(timers, dead_timer_id - id_shift)
        id_shift = id_shift + 1
    end
end)
