local r1, r2 =  0               ,  1.0
local g1, g2 = -math.sqrt(3) / 2, -0.5
local b1, b2 =  math.sqrt(3) / 2, -0.5

function color(red, green, blue, alpha)
    alpha = alpha or 255
    return {r=red, g=green, b=blue, a=alpha, _cvm_type="CVM_Color"}
end

function colorHSV(h, s, v, a)
  h = h + math.pi / 2
  local r, g, b = 1, 1, 1
  local h1, h2 = math.cos(h), math.sin(h)
  
  r = h1 * r1 + h2 * r2
  g = h1 * g1 + h2 * g2
  b = h1 * b1 + h2 * b2

  r = r + (1 - r) * s
  g = g + (1 - g) * s
  b = b + (1 - b) * s
  
  r, g, b = r * v, g * v, b * v
  
  return color(r * 255, g * 255, b * 255, a)
end
lua.SetGlobal("Color", color)
lua.SetGlobal("ColorHSV", colorHSV)
