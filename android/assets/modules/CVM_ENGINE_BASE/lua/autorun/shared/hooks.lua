-- These two only run if a world is loaded
hook.DefineEvent("Think") -- Is called every update or so if performance is good enough, but will slow down as the game needs more resources
hook.DefineEvent("Tick") -- Is called every update, use for animations, position updates, etc

hook.DefineEvent("ModulesLoaded") -- Runs after the module list is confirmed and the autorun files have been run
hook.DefineEvent("EntityTypesLoaded") -- Runs after all entity types have loaded, right after ModulesLoaded

hook.DefineEvent("PlayerConnecting")
hook.DefineEvent("PlayerConnected") -- Runs right after the player finishes loading
hook.DefineEvent("NewPlayerConnected") -- Runs right after a player is registered in the world, player as parameter
hook.DefineEvent("PlayerDisconnected") -- Runs right before the player is set to OFFLINE, player as parameter

hook.DefineEvent("SetupPlayerDataTable")

hook.DefineEvent("DefineInputs") -- Define and bind your inputs here

hook.Add("ModulesLoaded", "DefineInputs", function() hook.CallEvent("DefineInputs") end)
