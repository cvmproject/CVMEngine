hook.DefineEvent("UIDraw") -- Called every frame to draw the HUD
hook.DefineEvent("CameraSetup") -- Called before drawing the world
hook.DefineEvent("GameStart") -- This is the event that should start the game menu. Or maybe load a world right away or something.
hook.DefineEvent("LoadFinished") -- This is called every time the engine switches away from the loading screen
hook.DefineEvent("UIThink") -- Unlike Think, this gets called even if no world is loaded
hook.DefineEvent("UITick") -- Unlike Tick, this gets called even if no world is loaded

hook.DefineEvent("Connected") -- This is called after the connection to the server is made and the game begins
