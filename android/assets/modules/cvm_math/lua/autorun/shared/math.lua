function clamp(value, value_min, value_max)
    if value > value_max then
        return value_max
    elseif value < value_min then
        return value_min
    else
        return value
    end
end
