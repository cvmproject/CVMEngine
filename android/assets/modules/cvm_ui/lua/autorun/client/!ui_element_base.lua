UI_ELEMENT_BASE = {
    pos=Vector(0, 0),
    color=Color(200, 200, 200),
    size=Vector(1, 1),
    children=nil,
    parent=nil,
    enabled=true
}

function UI_ELEMENT_BASE:SetPos(x, y)
    if type(x) == "table" then
        self.pos = x
    else
        self.pos = Vector(x, y)
    end
end

function UI_ELEMENT_BASE:SetSize(width, height)
    if type(width) == "table" then
        self.size = width
    else
        self.size = Vector(width, height)
    end
end

function UI_ELEMENT_BASE:GetDrawPos()
    if self.parent == nil then
        return self.pos
    else
        return self.pos + parent.pos
    end
end

function UI_ELEMENT_BASE:SetColor(r, g, b, a)
    if type(r) == "table" then
        self.color = r
    else
        self.color = Color(r, g, b, a)
    end
end

function UI_ELEMENT_BASE:DrawRaw()
    if self.children ~= nil then
        for id, child in ipairs(self.children) do
            child:DrawRaw()
        end
    end
    self:Draw()
end

function UI_ELEMENT_BASE:TickRaw()
    if self.children ~= nil then
        for id, child in ipairs(self.children) do
            if child.is_dead then
                table.remove(self.children, id)
            else
                child:TickRaw()
            end
        end
    end
    self:Tick()
end

function UI_ELEMENT_BASE:Init() end
function UI_ELEMENT_BASE:Draw() end
function UI_ELEMENT_BASE:Tick() end
function UI_ELEMENT_BASE:Enable() self.enabled = true end
function UI_ELEMENT_BASE:Disable() self.enabled = false end
function UI_ELEMENT_BASE:Remove() self.is_dead = true end
function UI_ELEMENT_BASE:Pack() end
function UI_ELEMENT_BASE:Add(child)
    if self.children == nil then
        self.children = {}
    end
    self.children[#self.children + 1] = child
end

UI_ELEMENT_TYPES["Panel"] = UI_ELEMENT_BASE
