UI_ELEMENTS = {}
UI_ELEMENT_TYPES = {}

function CreateInstance(parent)
    local new_element = {}
    local metatable = {__index=parent}
    setmetatable(new_element, metatable)
    return new_element
end

hook.Add("UIDraw", "CVM_UI_DRAW", function()
    for id, element in ipairs(UI_ELEMENTS) do
        if element.enabled then
            element:DrawRaw()
        end
    end
end)

hook.Add("UITick", "CVM_UI_TICK", function()
    for id, element in ipairs(UI_ELEMENTS) do
        if element.is_dead then
            table.remove(UI_ELEMENTS, id)
        elseif element.enabled then
            element:TickRaw()
        end
    end
end)

function CreateElement(type)
    local acquired_type = UI_ELEMENT_TYPES[type]
    if acquired_type == nil then error("Invalid UI Element Type "..type) end
    local new_element = CreateInstance(acquired_type)
    new_element:Init()
    return new_element
end

function AddToRoot(element)
    UI_ELEMENTS[#UI_ELEMENTS + 1] = element
end
