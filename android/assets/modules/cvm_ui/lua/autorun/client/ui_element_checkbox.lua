UI_ELEMENT_CHECKBOX = CreateInstance(UI_ELEMENT_BUTTON)

UI_ELEMENT_CHECKBOX.checked = false
UI_ELEMENT_CHECKBOX.text_color = Color(255, 255, 255)

function UI_ELEMENT_CHECKBOX:Draw()
    if self.is_pressed then
        draw.SetColor(self.pressed_color)
    elseif self.is_hovered then
        draw.SetColor(self.highlight_color)
    else
        draw.SetColor(self.color)
    end
    local pos = self:GetDrawPos()
    draw.Rect(pos.x, pos.y, 4, self.size.y)
    draw.Rect(pos.x, pos.y, self.size.x, 4)
    draw.Rect(pos.x+self.size.x-4, pos.y, 4, self.size.y)
    draw.Rect(pos.x, pos.y+self.size.y-4, self.size.x, 2)
    if self.checked then
        draw.Rect(pos.x+10, pos.y+10, self.size.x-20, self.size.y-20)
    end

    draw.Text(self.text, "TitilliumWeb", self.size.y - 4, pos.x + self.size.x + 8, pos.y+38, self.text_color.r, self.text_color.g, self.text_color.b, self.text_color.a, false)
end

function UI_ELEMENT_CHECKBOX:Init()
    self.on_confirm = function()
        self.checked = not self.checked
    end
end

UI_ELEMENT_TYPES["Checkbox"] = UI_ELEMENT_CHECKBOX
