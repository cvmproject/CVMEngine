UI_ELEMENT_BUTTON = CreateInstance(UI_ELEMENT_BASE)

UI_ELEMENT_BUTTON.highlight_color = Color(255, 255, 255)
UI_ELEMENT_BUTTON.text = ""
UI_ELEMENT_BUTTON.text_color = Color(0, 0, 0)

function UI_ELEMENT_BUTTON:SetHighlightColor(r, g, b, a)
    if type(r) == "table" then
        self.highlight_color = r
    else
        self.highlight_color = Color(r, g, b, a)
    end
end

UI_ELEMENT_BUTTON.pressed_color = Color(155, 155, 155)

function UI_ELEMENT_BUTTON:SetPressedColor(r, g, b, a)
    if type(r) == "table" then
        self.pressed_color = r
    else
        self.pressed_color = Color(r, g, b, a)
    end
end

function UI_ELEMENT_BUTTON:SetTextColor(r, g, b, a)
    if type(r) == "table" then
        self.text_color = r
    else
        self.text_color = Color(r, g, b, a)
    end
end

function UI_ELEMENT_BUTTON:SetText(text)
    self.text = text
end

UI_ELEMENT_BUTTON.on_press = function() end
UI_ELEMENT_BUTTON.on_release = function() end
UI_ELEMENT_BUTTON.on_confirm = function() end
UI_ELEMENT_BUTTON.is_pressed = false
UI_ELEMENT_BUTTON.is_pressed_by = -1

function UI_ELEMENT_BUTTON:Draw()
    if self.is_pressed then
        draw.SetColor(self.pressed_color)
    elseif self.is_hovered then
        draw.SetColor(self.highlight_color)
    else
        draw.SetColor(self.color)
    end
    local pos = self:GetDrawPos()
    draw.Rect(pos.x, pos.y, self.size.x, self.size.y)
    draw.Text(self.text, "TitilliumWeb", self.size.y - 4, pos.x + self.size.x/2, pos.y + self.size.y/2, self.text_color.r, self.text_color.g, self.text_color.b, self.text_color.a)
end

function UI_ELEMENT_BUTTON:ContainsPoint(point)
    return point.x > self.pos.x
            and point.x < self.pos.x + self.size.x
            and point.y > self.pos.y
            and point.y < self.pos.y + self.size.y
end

function UI_ELEMENT_BUTTON:Tick()
    if not self.is_pressed then
        for input_id = 0, 5 do
            if input_id == 0 or input.IsTouched(input_id) then
                local pointer_pos = input.UIPointerPos(input_id)
                if input_id == 0 then
                    self.is_hovered = self:ContainsPoint(pointer_pos)
                end

                if self:ContainsPoint(pointer_pos) and input.IsTouched(input_id) then
                    self.is_hovered = true
                    self.on_press()
                    self.is_pressed_by = input_id
                    self.is_pressed = true
                    break
                end
            end
        end
    else
        if not input.IsTouched(self.is_pressed_by) or not self:ContainsPoint(input.UIPointerPos(self.is_pressed_by)) then
            if self:ContainsPoint(input.UIPointerPos(self.is_pressed_by)) then
                self.on_confirm()
            end
            self.on_release()
            self.is_pressed = false
        end
    end
end

UI_ELEMENT_TYPES["Button"] = UI_ELEMENT_BUTTON
