UI_ELEMENT_SWITCHSCREEN = CreateInstance(UI_ELEMENT_BASE)

UI_ELEMENT_SWITCHSCREEN.tabs = {}
UI_ELEMENT_SWITCHSCREEN.active_tab = nil


function UI_ELEMENT_SWITCHSCREEN:AddTab(tab_name, tab_panel)
    if self.tabs[tab_panel] ~= nil then error("Tried to add duplicate tab "..tab_name) end
    self.tabs[tab_name] = tab_panel
end

function UI_ELEMENT_SWITCHSCREEN:Switch(tab_name)
    if self.tabs[tab_name] == nil then error("Attempted to switch to nonexistent tab "..tab_name) end
    self.active_tab = tab_name
end

function UI_ELEMENT_SWITCHSCREEN:Tick()
    if self.active_tab ~= nil then
        self.tabs[self.active_tab]:TickRaw()
    end
end

function UI_ELEMENT_SWITCHSCREEN:Draw()
    if self.active_tab ~= nil then
        self.tabs[self.active_tab]:DrawRaw()
    end
end

UI_ELEMENT_TYPES["SwitchScreen"] = UI_ELEMENT_SWITCHSCREEN
