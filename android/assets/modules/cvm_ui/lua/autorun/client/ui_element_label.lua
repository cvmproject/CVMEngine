UI_ELEMENT_LABEL = CreateInstance(UI_ELEMENT_BASE)

UI_ELEMENT_LABEL.text = ""
UI_ELEMENT_LABEL.text_color = Color(0, 0, 0)

function UI_ELEMENT_LABEL:SetTextColor(r, g, b, a)
    if type(r) == "table" then
        self.text_color = r
    else
        self.text_color = Color(r, g, b, a)
    end
end

function UI_ELEMENT_LABEL:SetText(text)
    self.text = text
end

function UI_ELEMENT_LABEL:Draw()
    if self.is_pressed then
        draw.SetColor(self.pressed_color)
    elseif self.is_hovered then
        draw.SetColor(self.highlight_color)
    else
        draw.SetColor(self.color)
    end
    local pos = self:GetDrawPos()
    draw.Rect(pos.x, pos.y, self.size.x, self.size.y)
    draw.Text(self.text, "TitilliumWeb", self.size.y - 4, pos.x + self.size.x/2, pos.y + self.size.y/2, self.text_color.r, self.text_color.g, self.text_color.b, self.text_color.a)
end

UI_ELEMENT_TYPES["Label"] = UI_ELEMENT_LABEL
