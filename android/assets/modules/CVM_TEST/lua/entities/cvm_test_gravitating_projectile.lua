function ENT:Init()
    self.target = Vector(0, 0)
end

function ENT:Tick(deltaT)
    local vel = self:GetVel()
    self:SetVel(vel + (self.target - self:GetPos())*deltaT*5)
end

function ENT:Draw()
    pos = self:GetPos()
    draw.Sprite("cvm_test/testcross", pos.x-5 , pos.y-5, 10, 10)
end
