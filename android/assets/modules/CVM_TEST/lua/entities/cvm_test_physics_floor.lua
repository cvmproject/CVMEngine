function ENT:Init()
    self.target = Vector(0, 0)
end

function ENT:Draw()
    pos = self:GetPos()
    draw.SetColor(Color(255, 255, 255, 255))
    draw.Rect(pos.x-50 , pos.y-2.5, 100, 5)
end

function ENT:InitPhysics()
    self:PhysicsInitBody("KINEMATIC")
    self:PhysicsAddCollider({
        type="Rect",
        density=1,
        width=100,
        height=5
    })
end
