function ENT:Init()
    self.target = Vector(0, 0)
end

function ENT:Draw()
    pos = self:GetPos()
    draw.Sprite("cvm_test/testcross", pos.x-0.5 , pos.y-0.5, 1, 1, self:GetAngle())
end

function ENT:InitPhysics()
    self:PhysicsInitBody("DYNAMIC")
    self:PhysicsAddCollider({
        type="Rect",
        density=1,
        width=1,
        height=1
    })
end
