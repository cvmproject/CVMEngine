local COLORS = {
    [1]=Color(255, 0, 0),
    [2]=Color(0, 255, 0),
    [3]=Color(0, 0, 255)
}

function ENT:SetupDataTable()
    self:AddDataTableEntry("Layer", "Int", 1)
end
function ENT:Init()

end

function ENT:Tick(delta)
    if SERVER then return end
    self:SetDrawLayer(self:GetLayer())
end

function ENT:Draw()
    pos = self:GetPos()
    draw.SetColor(COLORS[self:GetLayer()])
    draw.Rect(pos.x-32 , pos.y-32, 64, 64 )
end