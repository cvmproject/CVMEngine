local rectangleColor = Color(0, 0, 0)
local targetColor = Color(255, 255, 255)

console.AddCommand("cvm_test_draw", function()
    hook.Add("UIDraw", "CVM_TEST_DRAW_DRAW", function()
        local WIDTH = draw.UIScreenWidth()
        local HEIGHT = draw.UIScreenHeight()

        draw.Text("Colorful rectangle spanning the entire screen -5px:", "TitilliumWeb", 20, 30, 100, 1, 1, 1, 1)
        draw.SetColor(rectangleColor)
        draw.Rect(5, 20, WIDTH-10, 10)

        local aimPos = input.UIPointerPos()
        draw.Circle(aimPos.x, aimPos.y, 10)
        draw.Rect(aimPos.x - 2, 0, 4, HEIGHT)
        draw.Rect(0, aimPos.y - 2, 0, 4)
    end)

    hook.Add("Tick", "CVM_TEST_DRAW_TICK", function()
        if math.abs(rectangleColor.r - targetColor.r) < 1 then
            targetColor.r = math.random()*255
            targetColor.g = math.random()*255
            targetColor.b = math.random()*255
        end
        if rectangleColor.r < targetColor.r then
            rectangleColor.r = rectangleColor.r + 1
        else
            rectangleColor.r = rectangleColor.r - 1
        end
        if rectangleColor.g < targetColor.g then
            rectangleColor.g = rectangleColor.g + 1
        else
            rectangleColor.g = rectangleColor.g - 1
        end
        if rectangleColor.b < targetColor.b then
            rectangleColor.b = rectangleColor.b + 1
        else
            rectangleColor.b = rectangleColor.b - 1
        end
    end)
end)

console.AddCommand("cvm_test_draw_end", function()
    hook.Remove("CVM_TEST_DRAW_DRAW")
    hook.Remove("CVM_TEST_DRAW_TICK")
end)
