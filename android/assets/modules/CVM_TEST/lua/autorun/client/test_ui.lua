console.AddCommand("cvm_test_ui", function()
    local button = cvm_ui.CreateElement("Button")
    button:SetColor(Color(200, 50, 50))
    button:SetPos(0, 100)
    button:SetSize(100, 50)
    button.on_press = function()
        print("on_press called")
    end
    button.on_release = function()
        print("on_release called")
    end
end)
