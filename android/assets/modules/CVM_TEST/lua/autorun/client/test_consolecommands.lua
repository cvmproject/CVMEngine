-- Let's test how well console commands work
console.AddCommand("cvm_test_console", function()
    print("Test command 1, just prints stuff")
end)
console.AddCommand("cvm_test_console_loop", function()
    for i = 1, 10 do
        print("Test command 2, prints in a loop, iteration "..i)
    end
end)
console.AddCommand("cvm_test_console_suicide", function()
    print("Removing myself!")
    console.RemoveCommand("cl_test_suicide")
end)
console.AddCommand("cvm_test_console_params", function(param1)
    print("Look, we are testing params:"..param1)
end)
console.AddCommand("cvm_test_console_runcommand", function (param1)
    if param1 == nil then
        print("Use 'cvm_test_console_runcommand <command name>' to test running commands programmatically")
        return
    end
    print("Running command "..param1)
    console.RunCommand(param1)
    print("Running cl_test_params with params")
    console.RunCommand("cvm_test_console_params test_param")
end)
