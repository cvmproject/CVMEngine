-- Let's test the hook functionality
hook.DefineEvent("TestHook")
function testHooks()
    print("Testing, testing... this should come up")
end
hook.Add("TestHook", "TestingHooks", testHooks)

hook.Add("TestHook", "TestingHooks2", function() print("This is the second test!") end)
print("Let's test the hook system!")
hook.CallEvent("TestHook")
print("Let's remove the first test")
hook.Remove("TestingHooks")
hook.CallEvent("TestHook")
