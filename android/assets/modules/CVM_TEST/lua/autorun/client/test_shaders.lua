console.AddCommand("cvm_test_shaders", function()
    hook.Add("UIDraw", "CVM_TEST_SHADERS", function()
        draw.Sprite("cvm_test/testcross", 150, 300, 300, 300)
        draw.SetShader("stretchy")
        local pointer_pos = input.UIPointerPos()
--         if pointer_pos.x < 500 or pointer_pos.x > 800 or pointer_pos.y < 300 or pointer_pos.y > 600 then
--             draw.SetShaderUniform("u_center", 0.5, 0.5)
--         else
            local relative_pos = pointer_pos - Vector(500, 600)
            relative_pos.y = -relative_pos.y
            local scaled_pos = relative_pos/300
            draw.SetShaderUniform("u_center", scaled_pos.x, scaled_pos.y)
--         end
        draw.Sprite("cvm_test/testcross", 500, 300, 300, 300)
        draw.ResetShader()
        draw.Sprite("cvm_test/testcross", 850, 300, 300, 300)
    end)
end)

console.AddCommand("cvm_test_shaders_end", function()
    hook.Remove("CVM_TEST_SHADERS")
end)