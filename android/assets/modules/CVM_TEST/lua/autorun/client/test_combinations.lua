console.AddCommand("cvm_test_character", function()
    console.RunCommand("host 25565")
    console.RunCommand("cl_lua print('HOSTED')")
    console.RunCommand("cl_test_camera")
    console.RunCommand("sv_test_character")
    console.RunCommand("cl_lua print('HOOKED')")
    console.RunCommand("connect localhost 25565")
    console.RunCommand("cl_lua print('CONNECTED')")
end)

console.AddCommand("cvm_test_gravity", function()
    console.RunCommand("host 25565")
    console.RunCommand("cl_lua print('HOSTED')")
    console.RunCommand("cl_test_camera")
    console.RunCommand("sv_cvm_test_gravity")
    console.RunCommand("cl_lua print('HOOKED')")
    console.RunCommand("connect localhost 25565")
    console.RunCommand("cl_lua print('CONNECTED')")
end)

console.AddCommand("cvm_test_physics", function()
    console.RunCommand("host 25565")
    console.RunCommand("sv_cvm_test_physics")
    console.RunCommand("connect localhost 25565")
    console.RunCommand("cl_lua print('CONNECTED')")
end)