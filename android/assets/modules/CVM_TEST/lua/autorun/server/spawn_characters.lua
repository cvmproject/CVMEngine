console.AddCommand("sv_test_character", function()
    print("Hooking up character spawn")
    hook.Add("PlayerConnected", "SpawnCharacters", function(ply)
        print("Player connected, spawning character")
        character = ents.Create("test_character")
        character:SetPos(Vector(0, 0))
        character:SetNetworkedPlayer("Owner", ply)
        ents.Spawn(character)
        ply:GetDataEntity():SetNetworkedEnt("Character", character)
    end)

    hook.Add("PlayerDisconnected", "DespawnCharacters", function(ply)
        print("Player disconnected, removing character")
        ply:GetDataEntity():GetNetworkedEnt("Character"):Remove()
    end)
end)
