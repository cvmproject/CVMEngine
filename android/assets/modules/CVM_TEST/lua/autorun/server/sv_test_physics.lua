console.AddCommand("sv_cvm_test_physics", function()
    local floor = ents.Create("cvm_test_physics_floor")
    floor:SetPos(Vector(0, -5))
    ents.Spawn(floor)

    for i = 1, 50 do
        local cross = ents.Create("cvm_test_physics_cross")
        cross:SetPos(Vector(math.random()*10 - 5, math.random()*10 + 5))
        ents.Spawn(cross)
    end

    physics.SetGravity(Vector(0, -9.8))
end)
