console.AddCommand("sv_cvm_test_input_default", function()
    print("Using default input mappings (keys by name directly)")
    hook.Add("Tick", "RespondWithInputs", function()
        for _, ply in ipairs(player.GetAll()) do
            if ply:GetInput("KEY_W") then print("Player pressed W!") end
            if ply:GetInput("KEY_A") then print("Player pressed A!") end
            if ply:GetInput("KEY_S") then print("Player pressed S!") end
            if ply:GetInput("KEY_D") then print("Player pressed D!") end
        end
    end)
end)
