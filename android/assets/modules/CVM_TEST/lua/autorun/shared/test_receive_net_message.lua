if SERVER then
    net.AddNetworkString("CVMTestMessage")
end

net.Receive("CVMTestMessage", function(ply)
    if ply == nil then
        print("Just received a server message!")
    else
        print("Just received a test message! From player "..ply:GetUsername())
    end
    print("Received some secret data:"..net.Read(1))
    print("And some more data:"..net.Read(2))
end)
