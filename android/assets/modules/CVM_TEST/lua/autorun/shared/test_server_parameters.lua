if CLIENT then
    console.AddCommand("cvm_test_server_parameters", function()
        print("Starting server, passing a secret parameter 'secret' with value 'secret string'")
        client.StartListenServer(25555, {secret="secret string"})
    end)
end

if SERVER then
    local params = server.GetServerParams()
    if params.secret ~= nil then
        print("Received secret parameter from client:"..server.GetServerParams().secret)
    end
end
