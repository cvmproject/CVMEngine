#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;
uniform vec2 u_center;

void main()
{
    float stretchFactor = sin(u_time) + 0.5;
    float distFromCenter = length(v_texCoords - u_center);
    float effectStrength = 1.0 - distFromCenter;
    if (effectStrength < 0.0) {
        effectStrength = 0.0;
    }
    vec2 modifiedTexCoords = v_texCoords - (v_texCoords - u_center)*effectStrength*0.2;
    if (modifiedTexCoords.x > 1.0 || modifiedTexCoords.x < 0.0 || modifiedTexCoords.y > 1.0 || modifiedTexCoords.y < 0.0) {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    } else {
        gl_FragColor = v_color * texture2D(u_texture, modifiedTexCoords) * effectStrength;
    }
}
