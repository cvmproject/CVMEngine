package com.complover116.cvmproject.cvmengine.android;

import android.content.Context;
import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.shared.ModuleManager;
import com.complover116.cvmproject.cvmengine.shared.ISteamAPIServiceProvider;
import com.complover116.cvmproject.cvmengine.shared.SteamAPIServiceDummy;
import com.complover116.cvmproject.cvmengine.shared.ISteamAPIProxy;

import java.util.LinkedList;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Context context = getApplicationContext();
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.numSamples = 2;
		initialize(new CVMControl(new ModuleManager.ModuleManagerParams(new String[]{}, false, new LinkedList<>(), new LinkedList<>()), false, new ISteamAPIServiceProvider() {
			public ISteamAPIProxy initSteamService(CVMControl engine) {
				return new SteamAPIServiceDummy();
			}
		}), config);
	}
}
