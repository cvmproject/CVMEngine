# CVMEngine Global API
In CVMEngine, there are several global tables (referred to as "libraries" in the engine code and further) that provide you with access to CVMEngine's features.

For example, where applicable, you can use the [player](/engine/api/core/player) library to get the list of all players connected to the server:

```lua
local players = player.GetAllActive()
```

This section contains the API reference to all the available CVMEngine libraries. Note that not all functionality (and, in fact, not all libraries!) are available on both the client and the server. Read the [Concepts: Networking](/engine/concepts/networking) page to understand what this means.


### Available libraries:
#### <span style="color:cyan">Sha</span><span style="color:yellow">red:</span>
${sharedLibs}
#### <span style="color:yellow">Client only</span>:
${clientLibs}
#### <span style="color:cyan">Server only</span>:
${serverLibs}
