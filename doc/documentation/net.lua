net.description = [[
This library lets the developer send custom (reliable) messages between the client and server.
It's very useful for when the data that CVMEngine sends automatically isn't enough, or for one-off events.
For example, it's used in TSIG and The Void Dogma to send inventory data to clients and to receive inventory move commands from players.

Every network message works like this: one of the parties (client or server) calls net.Start, then optionally calls
net.Write one or several times to add data to the network message, then calls net.Broadcast/net.Send
]]
