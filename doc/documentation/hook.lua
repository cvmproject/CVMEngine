hook.description = [[
This is one of the most important libraries in CVMEngine as it allows your code to be run when you want it to.
You can add hooks to events, as well as create and call your own events.

CVMEngine uses an event system to trigger game code. It's possible to add hooks to events defined by CVMEngine itself or in game code.
Here is a demonstration of how the hook system works:
```Lua
hook.DefineEvent("MyEvent") -- Defines an event named MyEvent
hook.Add("MyEvent", "MyHook", function()
    print("Hello!")
end) -- Adds a hook called MyHook to the event
hook.CallEvent("MyEvent") -- Calls the event MyEvent, MyHook will be called as well as any other added hooks, prints "Hello!"
hook.Remove("MyHook") -- Removes the hook
hook.CallEvent("MyEvent") -- Will no longer print anything
```

There are many events that CVMEngine provides, check the Events section on the left panel to get a list of them all
Many games and other modules will also define their own events that you can add hooks to.

**NOTE**: Despite the hook library being available on both server and client, the realms are still separate -
if you add a hook on an event on the client, firing this event on the server will **not** trigger the hook. You also can't
add hooks to events only defined on one realm. Refer to [Networking](/engine/concepts/networking) for more info
]]

hook.DefineEvent.arguments = "String event_name"
hook.DefineEvent.returns = ""
hook.DefineEvent.documentation = [[
Defines an event named `event_name`
This must be called before you can use `hook.CallEvent` and `hook.Add` with this event.
]]

hook.CallEvent.arguments = "String event_name, ... ..."
hook.CallEvent.returns = ""
hook.CallEvent.documentation = [[
Calls the event named `event_name`.
All parameters after the event_name will be passed to the hooks that exist on this event.
]]

hook.Add.arguments = "String event_name, String hook_name, Function callback"
hook.Add.returns = ""
hook.Add.documentation = [[
Adds a hook to the event called `event_name`.
`hook_name` **must be unique!** It's used to prevent hooks being added multiple times when a file is reloaded,
and also to allow removing created hooks via `hook.Remove`
]]

hook.Remove.arguments = "String hook_name"
hook.Remove.returns = ""
hook.Remove.documentation = [[
Removes a hook previously added with `hook.Add`
Notice that this function requires the **hook name**, not the event name!
]]
