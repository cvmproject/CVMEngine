draw.description = [[
This library is the main way to draw stuff on screen, both in the world and on the UI.

### Where can I use this?
This library is a little special, since where the resulting images are going to end up depends on where you use this library.
You need to call these functions every frame to keep seeing the image on the screen.
You should probably only use this library in methods and hooks that have "Draw" in their name. For example:
- [ENT:Draw()](/engine/api/entities/draw.md)
- [UIDraw hook](/engine/api/events/UIDraw.md)
- [Draw hook](/engine/api/events/Draw.md)
]]

draw.SetColor.arguments = "Color color"
draw.SetColor.returns = ""
draw.SetColor.documentation = [[
Sets the color that will be used to draw the following shapes. You don't have to call it every time you want to draw a
shape, only when the color needs to be changed

```lua
draw.SetColor(Color(255, 0, 0)) -- Sets the shape drawer color to red
draw.Rect(-5, -5, 10, 10) -- Draws a 10x10 red rectangle in the middle of the world
draw.Circle(5, 0, 10) -- Adds a circle shape to the rectangle, the circle is also red because the color did not change
```
]]

draw.Rect.arguments = "Number x, Number y, Number width, Number height"
draw.Rect.returns = ""
draw.Rect.documentation = [[
Draws an axis-aligned filled rectangle, see example in `draw.SetColor`
]]

draw.Circle.arguments = "Number x, Number y, Number radius"
draw.Circle.returns = ""
draw.Circle.documentation = [[
Draws a filled circle, see example in `draw.SetColor`
]]

draw.Line.arguments = "Number x, Number y, Number x2, Number y2, Number line_width"
draw.Line.returns = ""
draw.Line.documentation = [[
Draws a line from ```Vector(x, y)``` to ```Vector(x2, y2)``` which is `line_width` pixels thick
]]

draw.Sprite.arguments = "String asset_name, Number x, Number y, Number width, Number height, Number rotation=0, Number originX=width/2, Number originY=height/2, Number alpha=1"
draw.Sprite.returns = ""
draw.Sprite.documentation = [[
Draws a sprite on the screen.
Only `asset_name`, `x`, `y`, `width` and `height` are required parameters. The rest are optional.
**This method will likely change soon**
]]

draw.Text.arguments = "String text, String font, Number size, Number x, Number y, Number r, Number g, Number b, Number a"
draw.Text.returns = ""
draw.Text.documentation = [[
Draws some text on the screen.
**This method will likely change soon**
]]

