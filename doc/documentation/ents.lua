ents.description = [[
This library allows for interaction with the entity system.
The "Get***" methods can be used for selecting entities that exist in the world,
while the "Create***" and "Spawn" methods are used to create new entities.
]]

ents.GetAllActive.arguments = ""
ents.GetAllActive.returns = "List"
ents.GetAllActive.documentation = [[
Returns a list of all active entities
Does *NOT* include entities in unloaded sectors, only in active ones. If you want to access entities in unloaded sectors, you have to use other selection methods
```lua
-- Prints ids of all active entities
for entity in ents.GetAllActive() do
    print(entity:ID())
end
```
]]

ents.GetByID.arguments = "Number id"
ents.GetByID.returns = "List"
ents.GetByID.documentation = [[
Returns the entity which has the corresponding id.
This function is efficient, it does not search through all entities and looks up the entity in the entity map instead.
This function works on entities in unloaded sectors
]]

ents.GetInRadius.arguments = "Vector pos, Number radius"
ents.GetInRadius.returns = "List"
ents.GetInRadius.documentation = [[
Returns a list of entities in a radius around a certain position
This function is efficient, it does not search through all entities in the world, only looking in sectors around the provided position.
This function includes entities in unloaded sectors
]]

ents.GetByType.arguments = "String type"
ents.GetByType.returns = "List"
ents.GetByType.documentation = [[
Returns a list of all entities of the specified type.
This function is efficient, it does not search through all entities and uses a type map instead.
This function includes entities in unloaded sectors.
]]

ents.Create.arguments = "String type"
ents.Create.returns = "Entity"
ents.Create.documentation = [[
Creates and returns an entity with the specified type.
After this method returns, the entity has been initialized, but *has not been spawned yet*.
After setting all the required parameters, you *must* spawn the entity, or it will not appear.
]]

ents.Spawn.arguments = "Entity entity"
ents.Spawn.returns = ""
ents.Spawn.client = false
ents.Spawn.documentation = [[
Spawns the entity in the world.
The entity will automatically appear on the connected clients and will begin updating in the next tick.
```lua
-- Spawn a platform from qar5 at 100, 100. Notice that BOTH ents.Create AND ents.Spawn are required
local platform = ents.Create("qar5_platform")
platform:SetPos(Vector(100, 100))
ents.Spawn(platform)
```
]]

ents.SpawnClientside.arguments = "Entity entity"
ents.SpawnClientside.returns = ""
ents.SpawnClientside.documentation = [[
Spawns the entity in the world on this client only.
The entity will begin updating in the next tick, but will NOT exist on the server or any other clients.
```lua
-- Spawn a particle from qar5 at 100, 100
-- We use SpawnClientside because the server does not need to know about particles or network them
local particle = ents.Create("qar5_particle")
particle:SetPos(Vector(100, 100))
ents.SpawnClientside(particle)
```
]]

ents.LoadArea.arguments = "Vector center, Number radius"
ents.LoadArea.returns = ""
ents.LoadArea.documentation = [[
Forcibly loads an area of the world. Unless you know what you are doing, you probably don't need this.
<span style="color:orange">WARNING: Using this function to load a huge area can lead to dramatically reduced performance!</span>
]]