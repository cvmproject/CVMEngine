console.description = [[
This library lets you interact with the Developer Console
]]

console.AddCommand.arguments = "String command, Function callback"
console.AddCommand.returns = ""
console.AddCommand.documentation = [[
Registers a command called `command`. When received, CVMEngine will call `callback` and pass the arguments to the command.

**NOTE**: Commands that you want to run on the server HAVE to be prefixed with `sv_`,
otherwise they won't be passed to the listen server correctly.
]]

console.RemoveCommand.arguments = "String command"
console.RemoveCommand.returns = ""
console.RemoveCommand.documentation = [[
Unregisters an existing command previously added with `console.AddCommand`.
]]

console.RunCommand.arguments = "String command_line"
console.RunCommand.returns = ""
console.RunCommand.documentation = [[
Runs the command specified. You must specify the whole command line, including the parameters you want in `command_line`

**NOTE**: If you want to run a command on a listen server from the client, the command HAS to be prefixed with `sv_`, otherwise it won't be sent to the listen server.
]]