cam.description = [[
This library lets you control the camera
]]

cam.SetPos.arguments = "Vector pos"
cam.SetPos.returns = ""
cam.SetPos.documentation = [[
Makes the **center** of the camera point at `pos`
]]

cam.SetViewSize.arguments = "Vector size"
cam.SetViewSize.returns = ""
cam.SetViewSize.documentation = [[
Sets the size of the minimum area the camera is to show to `size`.
**NOTE**: If the aspect ratio of the game window does not match the aspect ratio of the camera `ViewSize`,
the camera will show **MORE** of the world in the longer axis to avoid stretching the view. Keep that in mind if you want to keep some areas of the world hidden.
]]

cam.GetViewSize.arguments = ""
cam.GetViewSize.returns = "Vector"
cam.GetViewSize.documentation = [[
Returns the view size set with `cam.SetViewSize`. Useful if you aren't the one setting it.
]]

cam.GetActualViewSize.arguments = ""
cam.GetActualViewSize.returns = "Vector"
cam.GetActualViewSize.documentation = [[
Returns the size of the actual area being shown. This can be larger than the view size set with `cam.SetViewSize`.
]]

cam.GetPos.arguments = ""
cam.GetPos.returns = "Vector"
cam.GetPos.documentation = [[
Returns the camera position set with `cam.SetPos`. Useful if you aren't the one setting it.
]]