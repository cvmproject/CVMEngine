client.description = [[
This library lets you manage the game client
]]

client.StartListenServer.arguments = "Number port, Table server_params"
client.StartListenServer.returns = ""
client.StartListenServer.documentation = [[
Starts (and connects to) a local game server on port `port`, passing the `server_params` as the server parameter table that can later be retrieved by [server.GetServerParams()](server)

```lua
-- Starts a QAR-5 deathmatch game on a map called dm_reload_arena
client.StartListenServer(
        20480,
        {
            map="dm_reload_arena",
            gamemode="dm"
        }
)
```
]]

client.ConnectUDP.arguments = "String address, Number port"
client.ConnectUDP.returns = ""
client.ConnectUDP.documentation = [[
Connects to a game server at ```address``` on port ```port```
**NOTE:** This function executes immediately, so the code following the call to `client.Connect` may not be run as the client needs to reload LUA to connect!
]]

