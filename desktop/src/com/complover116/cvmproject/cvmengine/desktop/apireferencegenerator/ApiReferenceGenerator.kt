package com.complover116.cvmproject.cvmengine.desktop.apireferencegenerator

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.*
import org.apache.commons.text.StringSubstitutor
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.ast.Str
import org.luaj.vm2.compiler.LuaC
import org.luaj.vm2.lib.OneArgFunction
import java.io.File
import java.util.LinkedList

/***
 * This is supposed to autogenerate WikiJS pages for all the API functions in CVMEngine
 */

fun getTemplate(name: String, args: HashMap<String, Any>): String {
    return StringSubstitutor.replace(
        File("../../doc/templates").resolve("$name.md").readText(),
        args,
        "\${",
        "}"
    )
}

fun checkString(table: LuaTable, string: String, default: String): String {
    return if (table[string].isstring()) table[string].checkjstring() else default
}

fun color(string: String, color: String): String {
    return "<span style=\"color:$color\">$string</span>"
}

fun main() {
    println("Extracting symbols from the CVMEngine API...")

    val config = Lwjgl3ApplicationConfiguration()
    val engine = CVMControl(ModuleManager.ModuleManagerParams(), false, object : ISteamAPIServiceProvider {
        override fun initSteamService(engine: CVMControl): ISteamAPIProxy {
            return SteamAPIServiceDummy()
        }
    })
    engine.commandProcessor.process("cvm_extract_api_symbols")
    Lwjgl3Application(engine, config)

    println("Done extracting symbols")
    println("Preparing LUA...")
    val globals = Globals()
    LuaC.install(globals)
    globals.set("print", object: OneArgFunction() {
        override fun call(arg: LuaValue): LuaValue {
            println("LUA: ${arg.checkjstring()}")
            return NIL
        }
    })
    println("Loading symbol definitions...")
    File("../../doc/extracted_symbols").listFiles()?.forEach {
        globals.load(it.readText()).call()
    }
    println("Loading documentation...")
    File("../../doc/documentation").listFiles()?.forEach {
        globals.load(it.readText()).call()
    }

    println("Generating WikiJS pages...")
    val outputDir = File("../../doc/output")
    outputDir.deleteRecursively()
    outputDir.mkdirs()
    val coreDirectory = outputDir.resolve("core")
    coreDirectory.mkdirs()

    val sharedLibs = LinkedList<String>()
    val clientLibs = LinkedList<String>()
    val serverLibs = LinkedList<String>()

    librariesToDocument.forEach {libraryName ->
        val library = globals[libraryName].checktable()
        val description = checkString(library, "description", color("??? NO DESCRIPTION ???", "red"))

        var isAvailableOnClient = false
        var isAvailableOnServer = false
        val symbols = library.keys().filter { it.checkjstring() != "description" }.sortedBy { library[it]["type"].checkjstring() + it.checkjstring() }.map {
            val symbol = library[it].checktable()
            val clientSide = symbol["client"].checkboolean()
            val serverSide = symbol["server"].checkboolean()
            isAvailableOnClient = isAvailableOnClient || clientSide
            isAvailableOnServer = isAvailableOnServer || serverSide

            val realm = if (clientSide && serverSide) {
                color("Sha", "cyan") + color("red", "yellow")
            } else if (clientSide) {
                color("Client", "yellow")
            } else {
                color("Server", "cyan")
            }
            if (symbol["type"].checkjstring() == "function") {
                var arguments = checkString(symbol, "arguments", "???")
                if (arguments == "???") {
                    arguments = color(arguments, "red")
                } else if (arguments != "") {
                    arguments = arguments.split(", ").map {
                        val split = it.split(" ")
                        color(split[0], "#3c94e7") + " " + split[1]
                    }.joinToString(", ")
                }
                var returnType = checkString(symbol, "returns", "??? ")
                if (returnType == "??? ") {
                    returnType = color(returnType, "red")
                } else {
                    returnType = color(returnType, "#3c94e7") + " "
                }
                getTemplate(
                    "function_entry", hashMapOf(
                        "libraryName" to libraryName,
                        "name" to it.checkjstring(),
                        "realm" to realm,
                        "arguments" to arguments,
                        "returnType" to returnType,
                        "documentation" to checkString(
                            symbol,
                            "documentation",
                            color("??? NO DOCUMENTATION ???", "red")
                        )
                    )
                )
            } else {
                getTemplate(
                    "constant_entry", hashMapOf(
                        "libraryName" to libraryName,
                        "name" to it.checkjstring(),
                        "documentation" to checkString(
                            symbol,
                            "documentation",
                            color("??? NO DOCUMENTATION ???", "red")
                        )
                    )
                )
            }
        }

        if (isAvailableOnClient && isAvailableOnServer) {
            sharedLibs.add(libraryName)
        } else if (isAvailableOnClient) {
            clientLibs.add(libraryName)
        } else if (isAvailableOnServer) {
            serverLibs.add(libraryName)
        }

        coreDirectory.resolve("$libraryName.md").outputStream().use {
            it.write("""
                |# $libraryName
                |$description
                |
                |${symbols.joinToString("\n")}
            """.trimMargin().toByteArray())
            it.write("\n".toByteArray())
        }
    }

    outputDir.resolve("core.md").outputStream().use { stream ->
        stream.write(getTemplate("core_page_header", hashMapOf(
            "sharedLibs" to sharedLibs.map { "- [$it](/engine/api/core/$it)" }.joinToString("\n"),
            "clientLibs" to clientLibs.map { "- [$it](/engine/api/core/$it)" }.joinToString("\n"),
            "serverLibs" to serverLibs.map { "- [$it](/engine/api/core/$it)" }.joinToString("\n")
        )).toByteArray())
    }

    println("Done")
}
