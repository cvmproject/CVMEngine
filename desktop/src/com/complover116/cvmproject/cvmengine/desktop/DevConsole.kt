package com.complover116.cvmproject.cvmengine.desktop

import com.badlogic.gdx.Gdx
import com.complover116.cvmproject.cvmengine.desktop.worldmaster.EntityEntry
import com.complover116.cvmproject.cvmengine.desktop.worldmaster.WorldMaster
import java.awt.*
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import javax.swing.*
import javax.swing.text.Style
import javax.swing.text.StyleConstants


class DevConsole(val remoteAddress: String = "localhost", val remotePort: Int = 20483): JFrame("CVMEngine Developer Console") {
    private val commandBox : SuggestTextBox = SuggestTextBox()
    private val logField : JTextPane = JTextPane()
    private var lastLogLine : String = ""
    private var lastLineRepeatCount : Int = 1
    private val normalStyle: Style = logField.addStyle("Normal", null)
    private val clientStyle: Style = logField.addStyle("Client", normalStyle)
    private val serverStyle: Style = logField.addStyle("Server", normalStyle)
    private val warnStyle: Style = logField.addStyle("Warning", normalStyle)
    private val errorStyle: Style = logField.addStyle("Error", normalStyle)
    private val debugStyle: Style = logField.addStyle("Debug", normalStyle)
    private val interactStyle: Style = logField.addStyle("Interact", normalStyle)
    private val scrollPane = JScrollPane(logField)
    private val statusBar = JProgressBar()

    val bgColor = Color(40, 40, 40)

    private val messageRegex: Regex = Regex("""\[([A-Z_]+)](.+)""")
    private val reverseCommandRegex: Regex = Regex("""<([A-Za-z_]+)>--->(.*)""")

    val reverseCommands: HashMap<String, (String) -> Unit> = HashMap()

    private val outboundCommandQueue: ConcurrentLinkedQueue<String> = ConcurrentLinkedQueue()

    private var lastSeen: Long = System.nanoTime()
    private var lastKeepAlive: Long = System.nanoTime()

    private val KEEPALIVE_TIMEOUT = 5000000000
    private val KEEPALIVE = 1000000000

    private var isConnected = true

    private var toRunOnClientLoad: LinkedList<String> = LinkedList();

    private val entityListButton = JButton("Open WorldMaster")

    private val worldMaster: WorldMaster = WorldMaster(this)

    private var onPointerClick: ((Float, Float) -> Unit)? = null

    var entityTypeList = LinkedList<String>()

    var outboundWorker: SwingWorker<Unit, Unit>? = null
    var inboundWorker: SwingWorker<Unit, Unit>? = null

    init {
        entityListButton.isEnabled = false
        statusBar.isStringPainted = true
        StyleConstants.setFontSize(normalStyle, 16)
        StyleConstants.setForeground(normalStyle, Color.WHITE)
        StyleConstants.setForeground(warnStyle, Color(200, 100, 0))
        StyleConstants.setBold(warnStyle, true)
        StyleConstants.setForeground(errorStyle, Color.RED)
        StyleConstants.setBold(errorStyle, true)
        StyleConstants.setForeground(debugStyle, Color(100, 100, 255))
        StyleConstants.setBold(interactStyle, true)
        StyleConstants.setForeground(clientStyle, Color.YELLOW)
        StyleConstants.setForeground(serverStyle, Color.CYAN)
//        StyleConstants.setItalic(interactStyle, true)
        this.layout = GridBagLayout()
        this.logField.background = bgColor
        this.background = bgColor
        this.commandBox.background = bgColor
        this.commandBox.foreground = Color.WHITE
        this.commandBox.caretColor = Color.WHITE
        this.commandBox.font = Font(this.commandBox.font.fontName, Font.BOLD, 20)
        this.add(entityListButton, GridBagConstraints(
            0,0,
            1,1,
            1.0,0.1,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            Insets(0,0,0,0),
            10,10
        ))

        entityListButton.addActionListener {
            worldMaster.open()
        }

        this.add(scrollPane, GridBagConstraints(
            0,1,
            1,1,
            1.0,15.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            Insets(0,0,0,0),
            10,10
        ))
        logField.isEditable = false
        logField.preferredSize = Dimension(900, 800)
        logField.autoscrolls = true
        this.add(commandBox, GridBagConstraints(
            0,2,
            1,1,
            1.0,0.1,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            Insets(0,0,0,0),
            10,10
        ))
        this.pack()
//        this.iconImage = ImageIcon(Gdx.files.internal("img/cvm_icon.png").readBytes()).image
        this.isVisible = true
        logField.isFocusable = false
        scrollPane.isFocusable = false
        commandBox.requestFocusInWindow()
        this.defaultCloseOperation = DISPOSE_ON_CLOSE
        this.commandBox.isEnabled = false
//        this.commandBox.text = "<---NOT CONNECTED--->"
        reverseCommands["COMMAND_LIST_CLEAR"] = {
            commandBox.suggestedValues.clear()
        }
        reverseCommands["COMMAND_LIST_ADD"] = {
            commandBox.suggestedValues.add(it)
        }
        reverseCommands["KEEPALIVE"] = {}
        reverseCommands["ENTITY_LIST"] = {
            worldMaster.entityEntries.clear()
        }
        reverseCommands["ENTITY_LIST_ELEMENT"] = {
            val entityData = it.split(":")
            worldMaster.entityEntries.add(EntityEntry(entityData[0].toInt(), entityData[1]))
        }
        reverseCommands["ENTITY_LIST_END"] = {
            worldMaster.entitesLoaded();
        }
        reverseCommands["ENTITY_TYPE_LIST"] = {
            entityTypeList.clear()
            worldMaster.createTypeEntry.suggestedValues.clear()
        }
        reverseCommands["ENTITY_TYPE_LIST_ELEMENT"] = {
            entityTypeList.add(it)
        }
        reverseCommands["ENTITY_TYPE_LIST_END"] = {
            entityTypeList.forEach {
                worldMaster.createTypeEntry.suggestedValues.add(it)
            }
        }
        reverseCommands["ENGINE_SHUTDOWN"] = {
            err("Engine shut down, console disconnecting")
            isConnected = false
        }
        reverseCommands["POINTER_CLICK"] = {
            val positionData = it.split(":")
            if (onPointerClick != null) {
                onPointerClick!!(positionData[0].toFloat(), positionData[1].toFloat())
                onPointerClick = null
            } else {
                interact("Received pointer coords (${positionData[0]}, ${positionData[1]})")
            }
        }
        reverseCommands["LOADING_STATUS_UPDATE"] = {
            if (it == "MODULES_LOADED") {
                outboundCommandQueue.add("command_list")
            }
            if (it == "RESOURCES_LOADED") {
                toRunOnClientLoad.forEach { it ->
                    outboundCommandQueue.add(it)
                }
                toRunOnClientLoad.clear()
            }
            if (it == "WORLD_STARTED") {
                outboundCommandQueue.add("sv_ent_types")
                if (worldMaster.isVisible)
                    worldMaster.reloadEntities()
                entityListButton.isEnabled = true
            }
            interact("Loading status update:$it")
        }
        startListening()
    }

    fun queueCommand(command: String) {
        outboundCommandQueue.add(command)
    }

    fun setOnPointerClick(value: (Float, Float) -> Unit) {
        onPointerClick = value
    }

    fun startListening() {
       inboundWorker = object: SwingWorker<Unit, Unit>() {
            override fun doInBackground() {
                val socket = Socket()
                val remote: SocketAddress = InetSocketAddress(remoteAddress, remotePort)
                SwingUtilities.invokeAndWait {
                    warn("Connecting to $remote...")
                    commandBox.text = "<---CONNECTING--->"
                }
                Thread.sleep(500)
                socket.connect(remote)
                SwingUtilities.invokeAndWait {
                    warn("Connected to CVMEngine")
                    commandBox.text = ""
                    commandBox.isEnabled = true
                }
                val reader = socket.getInputStream().bufferedReader()
                SwingUtilities.invokeAndWait {
                    warn("Receiving data from CVMEngine")
                    commandBox.addKeyListener(object: KeyAdapter() {
                        override fun keyPressed(e: KeyEvent?) {
                            if (e!!.keyCode == KeyEvent.VK_ENTER) {
                                interact("<--- ${commandBox.text}")
                                outboundCommandQueue.add(commandBox.text)
                                commandBox.recordHistory()
                            }
                        }
                    })
                }

                outboundWorker = object: SwingWorker<Unit, Unit>() {
                    override fun doInBackground() {
                        val writer = socket.getOutputStream().bufferedWriter()
                        warn("Ready to send data to CVMEngine")
                        while (isConnected) {
                            val message = outboundCommandQueue.poll()
                            if (message == null) {
                                try {
                                    Thread.sleep(100)
                                } catch (e: InterruptedException) {
                                    break
                                }
                                continue
                            } else {
                                writer.write(message)
                                writer.newLine()
                                writer.flush()
                            }
                        }
                        err("Stopped write thread")
                    }
                }

                outboundWorker!!.execute()

                while (isConnected) {
                    while (!reader.ready())
                        try {
//                            if (System.nanoTime() > lastSeen + KEEPALIVE_TIMEOUT) {
//                                println("KeepAlive timeout, disconnecting")
//                                socket.close()
//                                isConnected = false
//                                break
//                            }
                            if (System.nanoTime() > lastKeepAlive + KEEPALIVE) {
                                outboundCommandQueue.add("<---keepalive")
                                lastKeepAlive = System.nanoTime()
                            }
                            Thread.sleep(100)
                        } catch (e: InterruptedException) {
                            throw InterruptedException()
                        }
                    if (!isConnected) break
                    while (reader.ready()) {
                        val logMessage = reader.readLine()
                        lastKeepAlive = System.nanoTime()
                        lastSeen = System.nanoTime()
                        val match = messageRegex.matchEntire(logMessage)
                        val messageType = match?.groups?.get(1)?.value
                        val toPrint = match?.groups?.get(2)?.value ?: "MATCH ERROR"
                        SwingUtilities.invokeLater {
                            when (messageType) {
                                "INFO" -> info(toPrint)
                                "WARN" -> warn(toPrint)
                                "ERROR" -> err(toPrint)
                                "DEBUG" -> debug(toPrint)
                                "INTERACT" -> {
                                    val match = reverseCommandRegex.matchEntire(toPrint)
                                    if (match != null) {
                                        val reverseCommand = match.groups[1]!!.value
                                        val reverseCommandParams = match.groups[2]!!.value
                                        if (reverseCommands.containsKey(reverseCommand)) {
                                            reverseCommands[reverseCommand]!!(reverseCommandParams)
                                        } else {
                                            warn("Received unknown reverse command <$reverseCommand>")
                                        }
                                    } else {
                                        interact(toPrint)
                                    }
                                }
                                "LUA_CL" -> client(toPrint)
                                "LUA_SV" -> server(toPrint)
                                else -> info(logMessage)
                            }
                        }
                    }
                }
                SwingUtilities.invokeAndWait {
                    err("Connection to CVMEngine lost")
                    commandBox.isEnabled = false
                    commandBox.text = "<---CONNECTION LOST--->"
                }
            }
        }
        inboundWorker!!.execute()
    }

    fun runOnLoad(command: String) {
        toRunOnClientLoad.add(command)
    }

    override fun dispose() {
        Gdx.app.exit()
        inboundWorker!!.cancel(true)
        super.dispose()
        Frame.getFrames().filter { it != this }.forEach { it.dispose() }
    }

    private fun addText(text: String, style: Style) {
        if (text == lastLogLine) {
            val length = if (lastLineRepeatCount == 1) {
                text.length
            } else {
                val lastEditedText = "\n$text (x${lastLineRepeatCount})"
                lastEditedText.length - 1
            }
            lastLineRepeatCount ++
            val editedText = "$text (x${lastLineRepeatCount})"
            logField.styledDocument.remove(logField.styledDocument.length - length, length)
            logField.styledDocument.insertString(logField.styledDocument.length, editedText, style)
        } else {
            logField.styledDocument.insertString(logField.styledDocument.length, "\n$text", style)
            lastLineRepeatCount = 1
            lastLogLine = text
        }

        logField.caretPosition = logField.styledDocument.length - 1
    }
    fun info(text: String) {
        addText(text, normalStyle)
    }
    fun warn(text: String) {
        addText(text, warnStyle)
    }
    fun err(text: String) {
        addText(text, errorStyle)
    }
    fun debug(text: String) {
        addText(text, debugStyle)
    }
    fun interact(text: String) {
        addText(text, interactStyle)
    }
    fun client(text: String) {
        addText(text, clientStyle)
    }
    fun server(text: String) {
        addText(text, serverStyle)
    }
}
