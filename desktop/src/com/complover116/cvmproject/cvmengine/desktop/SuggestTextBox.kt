package com.complover116.cvmproject.cvmengine.desktop

import java.awt.Dimension
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.util.*
import javax.swing.JMenuItem
import javax.swing.JPopupMenu
import javax.swing.JTextField
import javax.swing.MenuSelectionManager
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener


class SuggestTextBox : JTextField() {
    val suggestionMenu: JPopupMenu = JPopupMenu()
    val suggestedValues: LinkedList<String> = LinkedList()
    private val history: LinkedList<String> = LinkedList()

    init {

        this.preferredSize = Dimension(200, 30)
        this.document.addDocumentListener(object: DocumentListener {
            override fun insertUpdate(e: DocumentEvent?) {
                updateMenu()
            }

            override fun removeUpdate(e: DocumentEvent?) {
                updateMenu()
            }

            override fun changedUpdate(e: DocumentEvent?) {
                updateMenu()
            }

        })
        this.focusTraversalKeysEnabled = false

        this.addKeyListener(object: KeyAdapter() {
            override fun keyPressed(e: KeyEvent?) {
                if (e!!.keyCode == KeyEvent.VK_TAB) {
                    MenuSelectionManager.defaultManager().selectedPath.forEach {
                        if (it is JMenuItem) {
                            text = it.text
                            suggestionMenu.isVisible = false
                        }
                    }
                } else if (e.keyCode == KeyEvent.VK_UP) {
                    popupHistoryMenu();
                }
            }
        })
    }

    fun popupHistoryMenu() {
        if (!suggestionMenu.isVisible) {
            history.forEach {
                val item = JMenuItem(it)
                item.addActionListener {
                    text = item.text
                    suggestionMenu.isVisible = false
                }
                suggestionMenu.add(item)
            }
            suggestionMenu.isVisible = true
            suggestionMenu.show(this, 0, 40)
            this.grabFocus()
        }
    }

    fun updateMenu() {
        suggestionMenu.isVisible = false
        if (this.text != "") {
            suggestionMenu.removeAll()
            suggestedValues.filter { it.startsWith(this.text) }.forEach {
                val item = JMenuItem(it)
                item.addActionListener {
                    text = item.text
                    suggestionMenu.isVisible = false
                }
                suggestionMenu.add(item)
            }


            suggestionMenu.isVisible = true
            suggestionMenu.show(this, 0, 40)
            this.grabFocus()
        }
    }

    fun recordHistory() {
        history.add(text)
        if (history.size > 5) {
            history.pop()
        }
        text = ""
    }
}
