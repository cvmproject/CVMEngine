package com.complover116.cvmproject.cvmengine.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Files;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3NativesLoader;
import com.badlogic.gdx.files.FileHandle;
import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.server.Server;
import com.complover116.cvmproject.cvmengine.shared.*;
import com.complover116.cvmproject.cvmengine.shared.net.NetConstants;
import org.apache.commons.cli.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DesktopLauncher {
    public static void main(String[] args) throws IOException{
        Options options = new Options();

        Option externalModuleDirsOption = new Option("external_modules", true, "Also load modules from supplied path");
        externalModuleDirsOption.setArgs(Option.UNLIMITED_VALUES);
        Option additionalModulesOption = new Option("additional_modules", true, "Load specified modules directly");
        additionalModulesOption.setArgs(Option.UNLIMITED_VALUES);

        options.addOption(externalModuleDirsOption);
        options.addOption(additionalModulesOption);
        options.addOption(new Option("no_workdir_modules", "Do not load modules from ./modules"));
        options.addOption(new Option("console_remote", true,"Start development console and connect to a remote CVMEngine instance"));
        options.addOption(new Option("console", "Start with the development console open"));
        options.addOption(new Option("console_remote_port", true, "Override default console port"));
        options.addOption(new Option("concommand", true, "Run a console command immediately on startup"));
        options.addOption(new Option("dedicated", "Launch as a dedicated server"));
        options.addOption(new Option("fullscreen", "Launch in fullscreen"));
        options.addOption(new Option("max_packet_size", "Max packet size CVMEngine will use, decrease if packets drop and file transfers fail"));
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Failed to parse command line arguments:"+e.getMessage());
            System.exit(-1);
        }

        if (cmd.hasOption("max_packet_size")) {
            NetConstants.MAX_PACKET_SIZE = Integer.parseInt(cmd.getOptionValue("max_packet_size"));
        }

        String port = cmd.getOptionValue("console_remote_port");
        if (cmd.hasOption("console") || cmd.hasOption("console_remote")) {
            CommandLine finalCmd = cmd;
            new Thread(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                DevConsole console = new DevConsole(
                        finalCmd.hasOption("console_remote") ? finalCmd.getOptionValue("console_remote") : "localhost",
                        finalCmd.hasOption("console_remote_port") ? Integer.parseInt(port) : 20483
                );
                if (finalCmd.hasOption("concommand")) {
                    console.runOnLoad(finalCmd.getOptionValue("concommand"));
                }
            }, "DevConsole Starter Thread").start();
        }

        if (!cmd.hasOption("console_remote")) {
            String[] moduleDirs = new String[]{};

            if (cmd.hasOption("external_modules")) {
                moduleDirs = cmd.getOptionValues("external_modules");
            }

            List<String> additionalModules = new LinkedList<>();

            if (cmd.hasOption("additional_modules")) {
                additionalModules.addAll(Arrays.asList(cmd.getOptionValues("additional_modules")));
//                Logger.log(Logger.LogType.INFO, "Added " + additionalModules.size() + " modules added with a command argument");
            }

            ModuleManager.ModuleManagerParams moduleManagerParams = new ModuleManager.ModuleManagerParams(
                    moduleDirs,
                    cmd.hasOption("no_workdir_modules"),
                    new LinkedList<>(),
                    additionalModules
            );


            if (!cmd.hasOption("dedicated")) {
                Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
                config.setTitle("CVMEngine <LOADING>");
                config.setWindowedMode(800, 480);
//                TODO: Set AA samples
//                config.setBackBufferConfig();
                config.useVsync(false);
                config.setWindowIcon("img/cvm_icon.png");
                CVMControl engine = new CVMControl(moduleManagerParams, cmd.hasOption("fullscreen"), new ISteamAPIServiceProvider() {
                    @NotNull
                    @Override
                    public ISteamAPIProxy initSteamService(CVMControl engineInstance) {
                        return new SteamAPIService(engineInstance);
                    }
                });
                new Lwjgl3Application(engine, config);
            } else {
                Lwjgl3NativesLoader.load();
                Gdx.files = new Lwjgl3Files();
                Logger.disableGDXLogging();
                Server server = new Server(20480, moduleManagerParams, null, null);
                server.allowConnections = true;
                server.start();
                Logger.hook(new DevConsoleConnector(20483, server.commandProcessor));
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            }
        }
    }
}
