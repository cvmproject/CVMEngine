package com.complover116.cvmproject.cvmengine.desktop

import com.badlogic.gdx.Gdx
import com.codedisaster.steamworks.*
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.ISteamAPIProxy
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.net.NetReceiver
import com.complover116.cvmproject.cvmengine.shared.net.NetRemoteDestination

class SteamAPIService(val engine: CVMControl): ISteamAPIProxy {
    val steamAPIAvailable: Boolean = try {
        Logger.log(Logger.LogType.INFO, "Initializing Steam API")
        SteamAPI.loadLibraries(SteamLibraryLoaderGdx())
//        if (SteamAPI.restartAppIfNecessary(2368420)) {
//            Logger.log(Logger.LogType.WARN, "Not launched through Steam, relaunching")
//            Gdx.app.exit()
//        }
        SteamAPI.init()
    } catch (e: SteamException) {
        Logger.log(Logger.LogType.ERROR, "Failed to initialize Steam native libraries")
        false
    }

    private val steamFriends = if (steamAPIAvailable) SteamFriends(object : SteamFriendsCallback {
        override fun onGameRichPresenceJoinRequested(steamIDFriend: SteamID?, connect: String?) {
            engine.connect(NetRemoteDestination.SteamIdDestination(SteamNativeHandle.getNativeHandle(steamIDFriend), engine.steamAPI, NetReceiver.SERVER_CHANNEL))
        }
    }) else null

    private val steamUser = if (steamAPIAvailable) SteamUser(object : SteamUserCallback {
    }) else null

    private val steamApps = if (steamAPIAvailable) SteamApps() else null

    private val steamNetworkingUtils = if (steamAPIAvailable) SteamNetworkingUtils(object : SteamNetworkingUtilsCallback {
        override fun onSteamRelayNetworkStatusChanged() {
            Logger.log(Logger.LogType.INFO, "Steam relay network status changed!")
        }
    }) else null

    private val steamNetworkingMessages = if (steamAPIAvailable) SteamNetworkingMessages(object : SteamNetworkingMessagesCallback {
        override fun onSessionRequest(steamID: Long) {
            Logger.log(Logger.LogType.WARN, "Received a session request from $steamID")
            val acceptResult = acceptSessionWithUser(steamID)
            Logger.log(Logger.LogType.WARN, "Accept result was $acceptResult")
        }
    }) else null


    init {
        if (steamFriends != null) {
            Logger.log(Logger.LogType.INFO, "Connected to Steam as ${steamFriends.personaName}")
        } else {
            Logger.log(Logger.LogType.INFO, "Did not connect to Steam. Steam features disabled")
        }
    }

    override fun dispose() {
        if (steamAPIAvailable) {
            Logger.log(Logger.LogType.INFO, "Shutting down Steam API")
            steamFriends!!.dispose()
            steamNetworkingUtils!!.dispose()
            steamNetworkingMessages!!.dispose()
            steamUser!!.dispose()
            steamApps!!.dispose()
            SteamAPI.shutdown()
        }
    }

    override fun setRichPresenceKey(key: String, value: String) {
        Logger.log(Logger.LogType.DEBUG, "Setting Steam Rich Presence key $key=$value")
        steamFriends?.setRichPresence(key, value)
    }

    override fun clearRichPresence() {
        steamFriends?.clearRichPresence()
    }

    override fun runCallbacks() {
        if (SteamAPI.isSteamRunning()) {
            SteamAPI.runCallbacks()
        }
    }

    override fun enableSteamJoins() {
        if (SteamAPI.isSteamRunning()) {
            setRichPresenceKey("connect", "${SteamNativeHandle.getNativeHandle(steamUser!!.steamID)}")
        }
    }

    override fun disableSteamJoins() {
        if (SteamAPI.isSteamRunning()) {
            steamFriends?.setRichPresence("connect", null)
        }
    }

    override fun getLaunchCommandLine(): String? {
        return steamApps?.launchCommandLine
    }

    override fun initRelayNetworkAccess() {
        if (SteamAPI.isSteamRunning()) {
            Logger.log(Logger.LogType.INFO, "Initializing relay network access")
            steamNetworkingUtils!!.initRelayNetworkAccess()
        }
    }

    fun acceptSessionWithUser(steamID: Long): Boolean {
        return steamNetworkingMessages!!.acceptSessionWithUser(steamID)
    }

    override fun getRelayNetworkStatus(): Boolean {
        if (SteamAPI.isSteamRunning()) {
            return steamNetworkingUtils!!.status == SteamNetworkingUtils.NetworkingAvailability.CONNECTED
        } else {
            return false
        }
    }

    override fun sendMessageToUser(steamID: Long, data: ByteArray, channel: Int) {
        if (SteamAPI.isSteamRunning()) {
            steamNetworkingMessages!!.sendMessageToUser(steamID, data, channel)
        }
    }

    override fun receiveMessage(channel: Int): Pair<Long, ByteArray>?  {
        val messages = steamNetworkingMessages?.receiveMessages(1, channel)
        messages ?: return null
        if (messages[0] != null) {
            return Pair(messages[0].remoteSteamID, messages[0].data)
        }
        return null
    }

    override val steamAvailable: Boolean
        get() = SteamAPI.isSteamRunning()

}
