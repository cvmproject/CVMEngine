package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Dimension
import javax.swing.*

class ProgressDialog(parent: JFrame): JDialog(parent, true) {
    val mainProgressBar = JProgressBar()
    val statusBar = JLabel("Loading...")
    private val rootPanel = JPanel()

    init {
        this.defaultCloseOperation = DO_NOTHING_ON_CLOSE
        mainProgressBar.preferredSize = Dimension(200, 20)
        mainProgressBar.isStringPainted = true

        rootPanel.add(statusBar)
        rootPanel.add(mainProgressBar)
        this.contentPane = rootPanel
        this.isResizable = false
        this.title = "Progress"
        this.setLocationRelativeTo(parent)
        this.pack()
    }

    fun popup() {
        this.pack()
        this.setLocationRelativeTo(parent)
        this.isVisible = true
    }
}
