package com.complover116.cvmproject.cvmengine.desktop.worldmaster

data class EntityEntry(val id: Int, val type: String)