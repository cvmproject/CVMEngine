package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import com.complover116.cvmproject.cvmengine.desktop.DevConsole
import javax.swing.JButton
import javax.swing.JCheckBox

class WorldMasterEntityMoveDialog(parent: WorldMaster, devConsole: DevConsole, private val entitiesToMove: List<Int>): DevEntityListPopup(parent, devConsole) {
    private val resetVelocityCheckbox = JCheckBox("Reset velocity")
    private val moveButton = JButton("Move")

    init {
        rootPanel.add(resetVelocityCheckbox)
        rootPanel.add(moveButton)
        this.title = "Move ${entitiesToMove.size} entities..."

        this.pack()
        this.moveButton.addActionListener {
            doMove()
        }
    }

    fun doMove() {
        devConsole.queueCommand("debug_pointer Move ${entitiesToMove.size} entities...")
        moveButton.isEnabled = false
        resetVelocityCheckbox.isEnabled = false
        devConsole.setOnPointerClick { x, y ->
            entitiesToMove.forEach {
                val velocityResetCommand = if (resetVelocityCheckbox.isSelected) """ e:SetVel(Vector(0, 0))""" else ""
                devConsole.queueCommand("""sv_lua local e = ents.GetByID($it) e:SetPos(Vector($x, $y))$velocityResetCommand""")
            }
            returnToEntityList()
        }
    }
}