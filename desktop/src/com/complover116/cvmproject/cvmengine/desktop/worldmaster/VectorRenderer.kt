package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Color
import java.awt.Component
import java.awt.GridLayout
import javax.swing.*
import javax.swing.table.TableCellRenderer

class VectorRenderer: JPanel(GridLayout(0,2)), TableCellRenderer {
    init {
        val emptyBorder = BorderFactory.createEmptyBorder()
        add(JSpinner().apply { border = emptyBorder })
        add(JSpinner().apply { border = emptyBorder })
    }

    override fun getTableCellRendererComponent(table: JTable?, value: Any?, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
        value as Pair<Double, Double>
        setValue(value)

        return this
    }

    fun setValue(value: Pair<Double, Double>) {
        (components[0] as JSpinner).value = value.first
        (components[1] as JSpinner).value = value.second
    }
}