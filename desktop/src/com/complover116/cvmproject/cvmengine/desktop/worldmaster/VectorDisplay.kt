package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Dimension
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JSpinner

open class VectorDisplay: JPanel() {
    val labelX = JLabel("X:")
    val labelY = JLabel("Y:")
    val fieldX = JSpinner()
    val fieldY = JSpinner()

    var isDisabled: Boolean = true
        set(value) {
            field = value
            fieldX.isEnabled = !value
            fieldY.isEnabled = !value
        }

    init {
        fieldX.preferredSize = Dimension(50, 30)
        fieldY.preferredSize = Dimension(50, 30)
        fieldX.alignmentY = CENTER_ALIGNMENT
        fieldY.alignmentY = CENTER_ALIGNMENT
        labelX.alignmentY = CENTER_ALIGNMENT
        labelY.alignmentY = CENTER_ALIGNMENT
        add(labelX)
        add(fieldX)
        add(labelY)
        add(fieldY)

        fieldY.addChangeListener { onChanged() }
        fieldX.addChangeListener { onChanged() }
    }

    open fun onChanged(){}
}