package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Color
import java.util.*
import javax.swing.JTable
import javax.swing.table.AbstractTableModel
import javax.swing.table.TableCellEditor
import javax.swing.table.TableCellRenderer

class WorldMasterDataTableTable(val worldMaster: WorldMaster) : JTable() {
    private var selectedTableEntry: Class<*>? = null
    val dataTableEntries = LinkedList<WorldMasterEntityDetails.DataTableEntrySimple>()
    var selectedEntityID: Int? = null

    init {
        // Color
        this.setDefaultRenderer(Color::class.java, ColorRenderer())
        this.setDefaultEditor(Color::class.java, ColorEditor())

        // Pair<*, *>
        this.setDefaultRenderer(Pair::class.java, VectorRenderer())
        this.setDefaultEditor(Pair::class.java, VectorEditor())

        this.model = object: AbstractTableModel() {
            override fun getRowCount(): Int {
                return dataTableEntries.size
            }

            override fun getColumnCount(): Int {
                return 3
            }

            override fun getColumnName(column: Int): String {
                return when(column) {
                    0 -> "Name"
                    1 -> "Type"
                    2 -> "Value"
                    else -> "!!INVALID COLUMN!!"
                }
            }

            override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
                return when(columnIndex) {
                    0 -> dataTableEntries[rowIndex].name
                    1 -> dataTableEntries[rowIndex].type
                    2 -> dataTableEntries[rowIndex].value
                    else -> "!!INVALID COLUMN!!"
                }
            }

            override fun isCellEditable(rowIndex: Int, columnIndex: Int): Boolean {
                return columnIndex == 2
            }

            override fun setValueAt(aValue: Any?, rowIndex: Int, columnIndex: Int) {
                if (columnIndex != 2) throw Exception("Attempted to set a value of non-editable column!")
                dataTableEntries[rowIndex].value = when(dataTableEntries[rowIndex].type) {
                    "Int" -> {
                        try {
                            val result = (aValue as String).toInt()
                            worldMaster.devConsole.queueCommand("sv_lua ents.GetByID($selectedEntityID):Set${dataTableEntries[rowIndex].name}($result)")
                            result
                        } catch (e: NumberFormatException) {
                            dataTableEntries[rowIndex].value
                        }
                    }
                    "String" -> {
                        val result = aValue as String
                        worldMaster.devConsole.queueCommand("sv_lua ents.GetByID($selectedEntityID):Set${dataTableEntries[rowIndex].name}('$result')")
                        result
                    }
                    "Float" -> {
                        try {
                            val result = (aValue as String).toFloat()
                            worldMaster.devConsole.queueCommand("sv_lua ents.GetByID($selectedEntityID):Set${dataTableEntries[rowIndex].name}($result)")
                            result
                        } catch (e: NumberFormatException) {
                            dataTableEntries[rowIndex].value
                        }
                    }
                    "Bool" -> {
                        try {
                            val result = aValue as Boolean
                            worldMaster.devConsole.queueCommand("sv_lua ents.GetByID($selectedEntityID):Set${dataTableEntries[rowIndex].name}($result)")
                            result
                        } catch (e: NumberFormatException) {
                            dataTableEntries[rowIndex].value
                        }
                    }
                    "Vector" -> {
                        try {
                            val result = aValue as Pair<Double, Double>
                            worldMaster.devConsole.queueCommand("sv_lua ents.GetByID($selectedEntityID):Set${dataTableEntries[rowIndex].name}(Vector$result)")
                            result
                        } catch (e: NumberFormatException) {
                            dataTableEntries[rowIndex].value
                        }
                    }
                    "Color" -> {
                        try {
                            val result = aValue as Color
                            worldMaster.devConsole.queueCommand("sv_lua ents.GetByID($selectedEntityID):Set${dataTableEntries[rowIndex].name}(Color(${result.red}, ${result.green}, ${result.blue}, ${result.alpha}))")
                            result
                        } catch (e: NumberFormatException) {
                            dataTableEntries[rowIndex].value
                        }
                    }
                    else -> dataTableEntries[rowIndex].value
                }
            }
        }
    }

    override fun getCellEditor(rowIndex: Int, columnIndex: Int): TableCellEditor {
        selectedTableEntry = null
        return if (columnIndex == 2)  {
            getDefaultEditor(dataTableEntries[rowIndex].value.javaClass)
        } else getDefaultEditor(String::class.java)
    }

    override fun getCellRenderer(rowIndex: Int, columnIndex: Int): TableCellRenderer {
        selectedTableEntry = null
        return if (columnIndex == 2)  {
            val `class` = dataTableEntries[rowIndex].value.javaClass
            selectedTableEntry = `class`
            getDefaultRenderer(`class`)
        } else getDefaultRenderer(String::class.java)
    }

    override fun getColumnClass(column: Int): Class<*> {
        return selectedTableEntry?: super.getColumnClass(column)
    }
}