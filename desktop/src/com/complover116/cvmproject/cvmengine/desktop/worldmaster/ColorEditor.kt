package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Color
import java.awt.Component
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.table.TableCellEditor
import javax.swing.table.TableCellRenderer

class ColorEditor: AbstractCellEditor(), TableCellEditor, ActionListener {
    companion object {
        const val ACTION = "edit"
    }

    private lateinit var color: Color
    private val colorChooser = JColorChooser()
    private val button       = ColorRenderer().apply {
        actionCommand   = ACTION
        addActionListener(this@ColorEditor)
    }

    private val dialog = JColorChooser.createDialog(button, "Pick a color", true, colorChooser, this, null)

    override fun getCellEditorValue(): Color = color
    override fun getTableCellEditorComponent(table: JTable?, value: Any?, isSelected: Boolean, row: Int, column: Int): Component {
        color = value as Color
        return button
    }

    override fun actionPerformed(e: ActionEvent) {
        if (e.actionCommand == ACTION) {
            button.setBackground(color)
            button.setColor(color)
            colorChooser.color = color
            dialog.isVisible   = true

            fireEditingStopped()
        } else color = colorChooser.color
    }
}