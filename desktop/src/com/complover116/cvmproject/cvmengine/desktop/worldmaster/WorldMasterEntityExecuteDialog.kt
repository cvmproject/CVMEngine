package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import com.complover116.cvmproject.cvmengine.desktop.DevConsole
import java.awt.Dimension
import javax.swing.BoxLayout
import javax.swing.JButton
import javax.swing.JLabel
import javax.swing.JTextField

class DevEntityExecuteDialog(parent: WorldMaster, devConsole: DevConsole, private val entitiesToProcess: List<Int>): DevEntityListPopup(parent, devConsole) {
    private val codeEntry = JTextField()
    private val label = JLabel("Use '$ENTITY_NAME_IN_CODE' to refer to the current entity")
    private val executeButton = JButton("Execute")
    init {
        rootPanel.layout = BoxLayout(rootPanel, BoxLayout.Y_AXIS)
        codeEntry.preferredSize = Dimension(500, 30)
        rootPanel.add(label)
        rootPanel.add(codeEntry)
        rootPanel.add(executeButton)
        this.title = "Execute for ${entitiesToProcess.size} entities..."
        this.pack()
        this.executeButton.addActionListener {
            doExecute()
        }
    }

    fun doExecute() {
        entitiesToProcess.forEach {
            devConsole.queueCommand("""sv_lua local $ENTITY_NAME_IN_CODE = ents.GetByID($it) """+codeEntry.text)
        }

        returnToEntityList()
    }

    companion object {
        val ENTITY_NAME_IN_CODE = "ent"
    }
}