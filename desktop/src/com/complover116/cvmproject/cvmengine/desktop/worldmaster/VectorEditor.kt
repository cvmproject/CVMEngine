package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Component
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.table.TableCellEditor

class VectorEditor: AbstractCellEditor(), TableCellEditor, ActionListener {
    private val editorComponent = VectorRenderer()

    override fun actionPerformed(e: ActionEvent) {}
    override fun getCellEditorValue(): Pair<*, *> = editorComponent.components.map { (it as JSpinner).value }.zipWithNext().first()
    override fun getTableCellEditorComponent(table: JTable?, value: Any?, isSelected: Boolean, row: Int, column: Int): Component {
        return editorComponent.apply { setValue(value as Pair<Double, Double>) }
    }
}