package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_BOOL
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_COLOR
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_ENTITY
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_INT
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_FLOAT
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_PLAYER
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_STRING
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.TYPE_VECTOR
import java.awt.*
import java.util.*
import javax.swing.*
import javax.swing.table.AbstractTableModel
import kotlin.math.pow


class WorldMasterEntityDetails(val worldMaster: WorldMaster): JPanel(GridBagLayout()) {
    val selectedLabel = JLabel("Selected Entity:")
    val entityNameLabel = JLabel("<NONE>")
    val entityIDLabel = JLabel("<NONE>")
    val entityPosLabel = JLabel("Pos:")
    val entityPos = object: VectorDisplay() {
        override fun onChanged() {
            worldMaster.devConsole.queueCommand("sv_ent_move $selectedEntityID ${fieldX.value} ${fieldY.value}")
        }
    }
    val entityVelLabel = JLabel("Vel:")
    val entityVel = object: VectorDisplay() {
        override fun onChanged() {
            worldMaster.devConsole.queueCommand("sv_ent_set_vel $selectedEntityID ${fieldX.value} ${fieldY.value}")
        }
    }

    data class DataTableEntrySimple(val name: String, val type: String, var value: Any)


    var selectedEntityID: Int? = null
        set(value) { field = value; dataTable.selectedEntityID = value; }
    val dataTable = WorldMasterDataTableTable(worldMaster)

    init {
        worldMaster.devConsole.reverseCommands["ENTITY_TYPE"] = {
            val entityData = it.split(":")
            if (worldMaster.entityDetails.selectedEntityID == entityData[0].toInt()) {
                worldMaster.entityDetails.entityNameLabel.text = entityData[1]
            }
        }
        worldMaster.devConsole.reverseCommands["ENTITY_POS"] = {
            val entityData = it.split(":")
            if (worldMaster.entityDetails.selectedEntityID == entityData[0].toInt()) {
                worldMaster.entityDetails.entityPos.fieldX.value = entityData[1].toFloat()
                worldMaster.entityDetails.entityPos.fieldY.value = entityData[2].toFloat()
                worldMaster.entityDetails.entityPos.isDisabled = false
            }
        }
        worldMaster.devConsole.reverseCommands["ENTITY_VEL"] = {
            val entityData = it.split(":")
            if (worldMaster.entityDetails.selectedEntityID == entityData[0].toInt()) {
                worldMaster.entityDetails.entityVel.fieldX.value = entityData[1].toFloat()
                worldMaster.entityDetails.entityVel.fieldY.value = entityData[2].toFloat()
                worldMaster.entityDetails.entityVel.isDisabled = false
            }
        }
        worldMaster.devConsole.reverseCommands["ENTITY_DATATABLE_ENTRY"] = {
            dataTable.isEnabled = true
            val entityData = it.split(":")
            if (worldMaster.entityDetails.selectedEntityID == entityData[0].toInt()) {
                val entryName = entityData[1]
                val entryType = entityData[2].toByte()
                val entryValue = when(entryType) {
                    TYPE_INT,
                    TYPE_PLAYER,
                    TYPE_ENTITY -> entityData[3].toInt()
                    TYPE_BOOL   -> entityData[3].toBoolean()
                    TYPE_FLOAT  -> entityData[3].toFloat()
                    TYPE_VECTOR -> entityData[3].split(',').map { it.toDouble() }.zipWithNext().first()
                    TYPE_COLOR  -> {
                        val v = entityData[3]
                            .split(',')
                            .map { it.toDouble().toInt() }
                            .toTypedArray()

                        Color(v[0], v[1], v[2], v[3])
                    }
                    TYPE_STRING -> entityData[3]
                    else        -> "ERROR: UNKNOWN TYPE"
                }
                dataTable.dataTableEntries.add(DataTableEntrySimple(entryName, DataTableEntry.getTypeFromByte(entryType), entryValue))
                (dataTable.model as AbstractTableModel).fireTableDataChanged()
            }
        }


        val snapSlider = JSlider(JSlider.HORIZONTAL, 0, 6, 0)
        snapSlider.minorTickSpacing = 1
        snapSlider.paintTicks = true
        snapSlider.paintLabels = true
        snapSlider.paintTrack = true
        val labelTable = Hashtable<Any, Any>()
        labelTable[0] = JLabel("Off")
        for (i in 1..6) {
            labelTable[i] = JLabel(2.0.pow(i - 1.0).toInt().toString())
        }
        snapSlider.labelTable = labelTable
        snapSlider.addChangeListener {
            worldMaster.devConsole.queueCommand("debug_pointer_snap ${2.0.pow((it.source as JSlider).value - 1.0).toInt()}")
        }
        this.add(snapSlider, GridBagConstraints(
            0,0,
            2,1,
            2.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,0
        ))

        selectedLabel.font = Font(selectedLabel.font.fontName, Font.BOLD, 20)
        this.add(selectedLabel, GridBagConstraints(
            0,1,
            2,1,
            1.0,0.0,
            GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,0
        )
        )
        entityIDLabel.font = Font(selectedLabel.font.fontName, Font.BOLD, 20)
        entityNameLabel.preferredSize = Dimension(200, 20)
        this.add(entityNameLabel, GridBagConstraints(
            0,2,
            2,1,
            1.0,0.0,
            GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,5
        )
        )
        entityNameLabel.font = Font(selectedLabel.font.fontName, Font.BOLD, 20)
        this.add(entityIDLabel, GridBagConstraints(
            0,3,
            2,1,
            1.0,0.0,
            GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,5
        )
        )
        entityPosLabel.font = Font(selectedLabel.font.fontName, Font.BOLD, 20)
        this.add(entityPosLabel, GridBagConstraints(
            0,4,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,5
        )
        )
        this.add(entityPos, GridBagConstraints(
            1,4,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,0
        )
        )
        entityVelLabel.font = Font(selectedLabel.font.fontName, Font.BOLD, 20)
        this.add(entityVelLabel, GridBagConstraints(
            0,5,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,5
        )
        )
        this.add(entityVel, GridBagConstraints(
            1,5,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            10,0
        )
        )
        this.add(JScrollPane(dataTable), GridBagConstraints(
            0,6,
            2,1,
            1.0,1.0,
            GridBagConstraints.NORTH,
            GridBagConstraints.BOTH,
            Insets(0,0,0,0),
            10,10
        )
        )
    }

    fun onEntitySelect(entID: Int) {
        selectedEntityID = entID
        entityIDLabel.text = "ID:$entID"
        worldMaster.devConsole.queueCommand("sv_ent_dump_state $entID")
        entityNameLabel.text = "<LOADING>"
        entityVel.isDisabled = true
        dataTable.isEnabled = false
        dataTable.dataTableEntries.clear()
    }

    fun onEntityDeselect(message: String) {
        entityIDLabel.text = "<$message>"
        entityNameLabel.text = "<$message>"
        entityPos.isDisabled = true
        entityVel.isDisabled = true
        dataTable.isEnabled = false
    }
}
