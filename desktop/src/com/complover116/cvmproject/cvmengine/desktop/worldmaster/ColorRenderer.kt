package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import java.awt.Color
import java.awt.Component
import javax.swing.*
import javax.swing.table.TableCellRenderer

class ColorRenderer: JButton(), TableCellRenderer {
    override fun getTableCellRendererComponent(table: JTable?, value: Any?, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
        isBorderPainted = false
        background      = value as Color
        setColor(value)

        return this
    }

    fun setColor(value: Color) {
        text = "${value.red}, ${value.blue}, ${value.green}, ${value.alpha}"
    }
}