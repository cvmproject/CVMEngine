package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import com.complover116.cvmproject.cvmengine.desktop.DevConsole
import javax.swing.BoxLayout
import javax.swing.JDialog
import javax.swing.JPanel

open class DevEntityListPopup(val parent: WorldMaster, val devConsole: DevConsole): JDialog(parent, true) {
    val rootPanel = JPanel()

    init {
        rootPanel.layout = BoxLayout(rootPanel, BoxLayout.Y_AXIS)
        this.defaultCloseOperation = DISPOSE_ON_CLOSE
        this.contentPane = rootPanel
        this.isResizable = false
        this.setLocationRelativeTo(parent)
    }
    fun returnToEntityList() {
        dispose()
        parent.reloadEntities()
    }

    fun popup() {
        this.pack()
        this.setLocationRelativeTo(parent)
        this.isVisible = true
    }
}