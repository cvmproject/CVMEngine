package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import com.complover116.cvmproject.cvmengine.desktop.DevConsole
import java.awt.Dimension
import javax.swing.JButton
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JTextField

class WorldMasterTypeRenameDialog(parent: WorldMaster, devConsole: DevConsole): DevEntityListPopup(parent, devConsole) {
    private val originalTypeLabel = JLabel("Type to rename:")
    private val newTypeLabel = JLabel("Rename to:")

    private val originalType = JTextField()
    private val newType = JTextField()
    private val confirmButton = JButton("Confirm")

    init {
        rootPanel.add(originalTypeLabel)
        rootPanel.add(originalType)
        rootPanel.add(newTypeLabel)
        rootPanel.add(newType)
        rootPanel.add(confirmButton)
        originalType.preferredSize = Dimension(400, 20)
        this.title = "Rename entity types..."

        this.pack()
        this.confirmButton.addActionListener {
            doRewrite()
        }
    }

    fun doRewrite() {
        devConsole.queueCommand("sv_ents_change_type ${originalType.text} ${newType.text}")
        confirmButton.isEnabled = false
        confirmButton.text = "Rewriting..."
        originalType.isEnabled = false
        newType.isEnabled = false
        devConsole.reverseCommands["WORLD_ENTITY_TYPE_CHANGED"] = {
            dispose()
            if (it.toInt() == 0) {
                JOptionPane.showMessageDialog(this, "No entities changed", "Type rename complete", JOptionPane.INFORMATION_MESSAGE)
            } else {
                JOptionPane.showMessageDialog(this, "Rewrote $it entities' types", "Type rename complete", JOptionPane.INFORMATION_MESSAGE)
                parent.reloadEntities()
            }
        }
    }
}