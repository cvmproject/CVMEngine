package com.complover116.cvmproject.cvmengine.desktop.worldmaster

import com.complover116.cvmproject.cvmengine.desktop.DevConsole
import com.complover116.cvmproject.cvmengine.desktop.SuggestTextBox
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import java.awt.event.KeyEvent
import java.util.*
import javax.swing.*
import javax.swing.table.AbstractTableModel


class WorldMaster(val devConsole: DevConsole): JFrame("CVMEngine WorldMaster") {
    val progressDialog = ProgressDialog(this)

    var lastLoadedMap: String? = null

    fun open() {
        isVisible = true
        reloadEntities()
    }

    fun reloadEntities() {
        val worker = object: SwingWorker<Unit, Pair<String, Int>>() {
            override fun doInBackground() {
                Thread.sleep(200)
                devConsole.queueCommand("sv_all_ent_list")
            }

            override fun done() {
                super.done()
                progressDialog.isVisible = false
                createTypeEntry.isEnabled = true
                createButton.isEnabled = true
            }
        }
        progressDialog.mainProgressBar.isIndeterminate = true
        progressDialog.statusBar.text = "Retrieving entity list..."
        worker.execute()
        if (!progressDialog.isVisible)
            progressDialog.popup()
    }

    fun waitForReload(message: String) {
        progressDialog.mainProgressBar.isIndeterminate = true
        progressDialog.statusBar.text = message
        progressDialog.popup()
    }

    fun entitesLoaded() {
        (entityTable.model as AbstractTableModel).fireTableDataChanged()
        pack()
        progressDialog.isVisible = false
    }

    val entityEntries = LinkedList<EntityEntry>()
    val entityTable = JTable(object: AbstractTableModel() {
        override fun getRowCount(): Int {
            return entityEntries.size
        }

        override fun getColumnCount(): Int {
            return 2
        }

        override fun getColumnName(column: Int): String {
            return when(column) {
                0 -> "ID"
                1 -> "Type"
                else -> "!!INVALID COLUMN!!"
            }
        }

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
            return when(columnIndex) {
                0 -> entityEntries[rowIndex].id
                1 -> entityEntries[rowIndex].type
                else -> "!!INVALID COLUMN!!"
            }
        }
    })

    init {
        entityTable.autoCreateRowSorter = true
    }

    val tableScrollPane = JScrollPane(entityTable)

    val deleteButton = JButton("DELETE Selected")
    val moveButton = JButton("Move Selected")
    val createTypeEntry = SuggestTextBox()
    val createButton = JButton("Create")
    val executeButton = JButton("Run code for each entity...")

    val entityDetails = WorldMasterEntityDetails(this)

    init {
        this.jMenuBar = JMenuBar()
        val worldMenu = JMenu("World")
        worldMenu.mnemonic = KeyEvent.VK_W
        worldMenu.accessibleContext.accessibleDescription = "World manipulation menu"
        jMenuBar.add(worldMenu)

        val loadLastMap = JMenuItem("Load last opened map")
        loadLastMap.isEnabled = false
        worldMenu.add(loadLastMap)

        val testMap = JMenuItem("Commit and playtest")
        testMap.addActionListener {
            devConsole.queueCommand("sv_save")
            devConsole.queueCommand("sv_load_map $lastLoadedMap")
            waitForReload("Restarting server in play mode")
            loadLastMap.isEnabled = true
            testMap.isEnabled = false
        }
        testMap.isEnabled = false

        loadLastMap.addActionListener {
            devConsole.queueCommand("sv_edit_map $lastLoadedMap")
            waitForReload("Loading $lastLoadedMap")
            testMap.isEnabled = true
            loadLastMap.isEnabled = false
        }

        val loadMenuItem = JMenuItem("Load map for editing...")
        loadMenuItem.addActionListener {
            val mapName = JOptionPane.showInputDialog("Type in the map name (you can use [A-Za-z0-9_/])")
            devConsole.queueCommand("sv_edit_map $mapName")
            waitForReload("Loading $mapName")
            lastLoadedMap = mapName
            testMap.isEnabled = true
        }
        worldMenu.add(loadMenuItem)

        val commitMenuItem = JMenuItem("Commit changes")
        commitMenuItem.addActionListener {
            devConsole.queueCommand("sv_save")
        }
        worldMenu.add(commitMenuItem)

        val discardChangesMenuItem = JMenuItem("Discard changes")
        discardChangesMenuItem.addActionListener {
            val result = JOptionPane.showConfirmDialog(this, "All the changes you made since the last commit will be lost permanently!", "Are you sure?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE)
            if (result == JOptionPane.OK_OPTION) {
                devConsole.queueCommand("sv_reload")
                waitForReload("Discarding changes and reloading...")
            }
        }
        worldMenu.add(discardChangesMenuItem)

        worldMenu.add(testMap)

        val operationsMenu = JMenu("Operations")
        jMenuBar.add(operationsMenu)

        val advancedOperationsMenu = JMenu("Advanced...")
        operationsMenu.add(advancedOperationsMenu)

        val changeEntityTypeMenuItem = JMenuItem("Rename entity type")
        changeEntityTypeMenuItem.addActionListener {
            val result = JOptionPane.showConfirmDialog(this, "Rewriting entity types can CORRUPT YOUR WORLD! Are you sure know what you are doing?", "Are you sure?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE)
            if (result == JOptionPane.OK_OPTION) {
                WorldMasterTypeRenameDialog(this, this.devConsole).popup()
            }
        }

        advancedOperationsMenu.add(changeEntityTypeMenuItem)

        this.background = devConsole.bgColor
        entityEntries.add(EntityEntry(-1, "Test Type"))
        this.layout = GridBagLayout()
        this.add(tableScrollPane, GridBagConstraints(
            0,0,
            2,1,
            1.0,1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            Insets(0,0,0,0),
            0,10
        )
        )
        this.add(deleteButton, GridBagConstraints(
            0,1,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            0,10
        ))
        this.add(moveButton, GridBagConstraints(
            1,1,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            0,10
        ))
        this.add(createTypeEntry, GridBagConstraints(
            0,2,
            1,1,
            1.0,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            0,10
        ))
        this.add(createButton, GridBagConstraints(
            1,2,
            1,1,
            0.1,0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            0,10
        ))
        this.add(executeButton, GridBagConstraints(
            0,3,
            2,1,
            1.0, 0.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL,
            Insets(0,0,0,0),
            0,10
        ))
        this.add(entityDetails, GridBagConstraints(
            2,0,
            1,4,
            2.0,1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            Insets(0,0,0,0),
            0,10
        ))
        deleteButton.addActionListener {
            val worker = object: SwingWorker<Unit, Pair<String, Int>>() {
                override fun doInBackground() {
                    val entitiesToDelete = entityTable.selectedRows
                    entitiesToDelete.forEach {
                        val entityID = entityEntries[it].id
                        devConsole.queueCommand("sv_ent_remove $entityID")
                    }
                    Thread.sleep(1000)
                }

                override fun done() {
                    super.done()
                    open()
                }
            }
            progressDialog.mainProgressBar.isIndeterminate = true
            progressDialog.statusBar.text = "Deleting ${entityTable.selectedRows.size} entities..."
            worker.execute()
            progressDialog.popup()
            devConsole.queueCommand("sv_all_ent_list")
        }
        moveButton.addActionListener {
            val worldMasterEntityMoveDialog = WorldMasterEntityMoveDialog(this, devConsole, entityTable.selectedRows.map { entityEntries[it].id })
            worldMasterEntityMoveDialog.popup()
        }
        createButton.addActionListener {
            devConsole.queueCommand("debug_pointer Creating ${createTypeEntry.text}...")
            createButton.isEnabled = false
            createTypeEntry.isEnabled = false
            devConsole.setOnPointerClick { x, y ->
                devConsole.queueCommand("""sv_lua local e = ents.Create("${createTypeEntry.text}") e:SetPos(Vector($x, $y)) ents.Spawn(e)""")
                reloadEntities()
            }
        }
        executeButton.addActionListener {
            val devEntityExecuteDialog = DevEntityExecuteDialog(this, devConsole, entityTable.selectedRows.map { entityEntries[it].id })
            devEntityExecuteDialog.popup()
        }
        entityTable.selectionModel.addListSelectionListener {
            if(it.valueIsAdjusting) return@addListSelectionListener
            devConsole.queueCommand("cl_ent_deselect")
            entityTable.selectedRows.forEach { row_id ->
                val entID = entityTable.getValueAt(row_id, 0)
                devConsole.queueCommand("cl_ent_select $entID")
            }
            if (entityTable.selectedRows.size == 1) {
                entityDetails.onEntitySelect(entityTable.getValueAt(entityTable.selectedRow, 0) as Int)
            } else if (entityTable.selectedRows.size > 1) {
                entityDetails.onEntityDeselect("MULTIPLE")
            } else {
                entityDetails.onEntityDeselect("NONE")
            }
        }
        this.pack()
        this.requestFocus()

        devConsole.reverseCommands["ENTITY_SELECTION_CLEAR"] = {
            entityTable.clearSelection()
        }
        devConsole.reverseCommands["ENTITY_SELECTION_ADD"] = {
            var row = 0
            entityEntries.forEach { entityEntry ->
                if (entityEntry.id == it.toInt()) {
                    entityTable.addRowSelectionInterval(row, row)
                }
                row ++
            }
        }
    }
}