package com.complover116.cvmproject.cvmengine.server

import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.SoundEvent
import com.complover116.cvmproject.cvmengine.shared.SoundEventReceiver
import java.util.*
import kotlin.collections.HashMap

class ServerSoundDispatch(val server: Server): SoundEventReceiver {
    val queuedSoundEvents = LinkedList<SoundEvent>()
    val activeSounds = HashMap<Int, SoundEvent>()
    override fun receiveSoundEvent(soundEvent: SoundEvent) {
        if (soundEvent.targetEntityID != null && soundEvent.soundPos == null) {
            soundEvent.soundPos = server.world.getEntityByID(soundEvent.targetEntityID!!).pos
        }
//        Logger.log(Logger.LogType.DEBUG, "Received sound event ${soundEvent.persistentID}, ${soundEvent.isDeleting}")
        if (soundEvent.persistence && !queuedSoundEvents.isEmpty() && queuedSoundEvents.last.persistentID == soundEvent.persistentID && !soundEvent.isDeleting) {
            queuedSoundEvents.last!!.assimilate(soundEvent)
        } else {
            queuedSoundEvents.add(soundEvent)
        }
        if (soundEvent.persistence) {
            if (soundEvent.isDeleting) {
                activeSounds.remove(soundEvent.persistentID)
            } else {
                if (activeSounds[soundEvent.persistentID] != null) {
                    activeSounds[soundEvent.persistentID]!!.assimilate(soundEvent)
                } else {
                    activeSounds[soundEvent.persistentID!!] = soundEvent
                }
            }
        }
    }
}
