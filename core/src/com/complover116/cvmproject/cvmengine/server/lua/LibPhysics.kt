package com.complover116.cvmproject.cvmengine.server.lua

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
import com.complover116.cvmproject.cvmengine.server.world.EntityServer
import com.complover116.cvmproject.cvmengine.server.world.WorldServer
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType
import com.complover116.cvmproject.cvmengine.shared.lua.LuaLib
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.Varargs
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.ThreeArgFunction
import org.luaj.vm2.lib.jse.CoerceLuaToJava
import kotlin.experimental.and

class LibPhysics(system: LuaSystem, val world: WorldServer) : LuaLib(system) {
    override fun setUp(table: LuaTable): String {
        table["SetGravity"] = object: OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                world.physicsWorld.gravity = Vector2(arg["x"].checkdouble().toFloat(), arg["y"].checkdouble().toFloat())
                val bodies: com.badlogic.gdx.utils.Array<Body> = com.badlogic.gdx.utils.Array<Body>()
                world.physicsWorld.getBodies(bodies)
                bodies.forEach {
                    it.isAwake = true
                }
                return NIL
            }
        }

        table["CreateFrictionJoint"] = object: OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val entA = CoerceLuaToJava.coerce(arg["entityA"]["ENT"], EntityServer::class.java) as EntityServer
                val entB = CoerceLuaToJava.coerce(arg["entityB"]["ENT"], EntityServer::class.java) as EntityServer

                if (entA.physicsBody == null || entB.physicsBody == null) error("Error creating joint: Both entities MUST have physics initialized!")

                val jointDef = FrictionJointDef()
                jointDef.initialize(entA.physicsBody, entB.physicsBody, entA.pos)
                jointDef.maxForce = arg["friction"].checkdouble().toFloat()

                world.physicsWorld.createJoint(jointDef)
                return NIL
            }
        }

        table["RayCast"] = object: OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val startPos = Vector2(arg["start_pos"]["x"].checkdouble().toFloat(), arg["start_pos"]["y"].checkdouble().toFloat())
                val endPos = Vector2(arg["end_pos"]["x"].checkdouble().toFloat(), arg["end_pos"]["y"].checkdouble().toFloat())


                val collisionMask = arg["mask"].checkint()

                val result = LuaTable()

                result["hit"] = LuaValue.valueOf(false)

                var bestFraction = 1f
                Logger.log(LogType.DEBUG, "Startpos X:${startPos.x} Y:${startPos.y}, Endpos X:${endPos.x} Y:${endPos.y}")

                if (startPos.x.isNaN() || startPos.y.isNaN() || endPos.x.isNaN() || endPos.y.isNaN()) {
                    Logger.log(LogType.ERROR, "SOMETHING IS CRITICALLY WRONG")
                    error("SOMETHING IS CRITICALLY WRONG, startPos/endPos is NaN!!")
                }
                if (startPos.dst2(endPos) < 0.2) {
                    return result
                }
                world.physicsWorld.rayCast({ fixture: Fixture, point: Vector2, normal: Vector2, fraction: Float ->
                    if (fixture.filterData.categoryBits.and(collisionMask.toShort()) == 0.toShort()) {
                        return@rayCast -1f
                    }
                    val entity = (fixture.body.userData as EntityServer).repr
                    if (!arg["exclude_entities"].isnil()) {
                        arg["exclude_entities"].checktable().keys().forEach {
                            if (arg["exclude_entities"][it] == entity) {
                                return@rayCast -1f
                            }
                        }
                    }
                    if (fraction < bestFraction) {
                        bestFraction = fraction
                        result["hit"] = LuaValue.valueOf(true)
                        result["entity"] = entity
                        result["point"] = system.luaTypes.createVector(point.x.toDouble(), point.y.toDouble())
                        result["normal"] = system.luaTypes.createVector(normal.x.toDouble(), normal.y.toDouble())
                    }
                    return@rayCast fraction
                }, startPos, endPos)
                return result
            }
        }

        table["QueryAABB"] = object: OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val pos1 = Vector2(arg["pos1"]["x"].checkdouble().toFloat(), arg["pos1"]["y"].checkdouble().toFloat())
                val pos2 = Vector2(arg["pos2"]["x"].checkdouble().toFloat(), arg["pos2"]["y"].checkdouble().toFloat())
                val collisionMask = arg["mask"].checkint()

                val result = LuaTable()

                var counter = 1
//                world.loadArea(pos1.x, pos1.y, 32.0f)
                world.physicsWorld.QueryAABB({
                    if (it.filterData.categoryBits.and(collisionMask.toShort()) == 0.toShort()) {
                        return@QueryAABB true
                    }
                    val entity = (it.body.userData as EntityServer).repr
                    result[counter] = entity
                    counter++
                    return@QueryAABB true
                }, pos1.x, pos1.y, pos2.x, pos2.y)
                return result
            }
        }
        return "physics"
    }
}
