package com.complover116.cvmproject.cvmengine.server.lua;

import com.complover116.cvmproject.cvmengine.server.Server;
import com.complover116.cvmproject.cvmengine.server.world.EntitySystemServer;
import com.complover116.cvmproject.cvmengine.shared.lua.*;
import org.luaj.vm2.LuaValue;

public class LuaSystemServer extends LuaSystem {

    public LibNetServer libNetServer;
    public LibInputServer libInputServer;
    public LibServer libServer;

    public LibPhysics libPhysics;

    public LuaSystemServer() {
        super();
        globals.set("ANDROID", LuaValue.valueOf(false));
        globals.set("PC", LuaValue.valueOf(true));
        entitySystem = new EntitySystemServer(this);
    }

    @Override
    public boolean isServer() {
        return true;
    }

    @Override
    public boolean isClient() {
        return false;
    }

    public void initServerLibs(Server server, boolean worldOnly) {
        if (!worldOnly) {
            libNetServer = new LibNetServer(this, server);
            libConsole = new LibConsole(this, server.commandProcessor);
            libInputServer = new LibInputServer(this, server.netServer);
            libServer = new LibServer(this, server);
        }
        libEnts = new LibEnts(this, server.world);
        libBase = new LibBase(this, server.world);
        libPlayer = new LibPlayerServer(this, server.world, server);
        libPhysics = new LibPhysics(this, server.world);
        libSound = new LibSound(this, server.world, server.serverSoundDispatch);
    }
}
