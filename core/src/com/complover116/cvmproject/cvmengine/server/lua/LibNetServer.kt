package com.complover116.cvmproject.cvmengine.server.lua

import com.complover116.cvmproject.cvmengine.server.PlayerServer
import com.complover116.cvmproject.cvmengine.server.Server
import com.complover116.cvmproject.cvmengine.shared.Player
import com.complover116.cvmproject.cvmengine.shared.lua.LibNetCommon
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk
import com.complover116.cvmproject.cvmengine.shared.net.NetStringRegistry
import com.complover116.cvmproject.cvmengine.shared.world.DataTable
import com.complover116.cvmproject.cvmengine.shared.world.World
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.ZeroArgFunction
import org.luaj.vm2.lib.jse.CoerceLuaToJava
import java.util.*

internal class LibNetServer(system: LuaSystem, var server: Server) : LibNetCommon(system) {
    private val inboundQueue: LinkedList<NetMessage> = LinkedList()

    override fun setUp(lib: LuaTable): String {
        super.setUp(lib)
        lib["AddNetworkString"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val string = arg.checkjstring()
                return valueOf(server.netServer.addNetString(string))
            }
        }

        lib["Start"] = object : OneArgFunction() {
            override fun call(name: LuaValue): LuaValue {
                if (outgoingMessage != null) {
                    LuaValue.error("Tried to start a new network message before the old one got sent!")
                    return NIL
                }
                outgoingMessage = NetMessage(name.checkjstring(), -1, DataTable())
                return NIL
            }
        }

        lib["Send"] = object : OneArgFunction() {
            override fun call(argPlayer: LuaValue): LuaValue {
                if (outgoingMessage == null) {
                    LuaValue.error("Tried to send a message before starting one!")
                    return NIL
                }
                val player = (CoerceLuaToJava.coerce(argPlayer.get("PLY"), PlayerServer::class.java) as PlayerServer)
                if (player.connection == null) {
                    LuaValue.error("Tried to send a message to an offline player ${player.username}")
                }
                val connection = player.connection
                NetDataChunk.ChunkNetMessage(connection, outgoingMessage, getNetStringRegistry()).enqueue(connection, true)
                outgoingMessage = null
                return NIL
            }
        }
        lib["Broadcast"] = object : ZeroArgFunction() {
            override fun call(): LuaValue {
                if (outgoingMessage == null) {
                    LuaValue.error("Tried to send a message before starting one!")
                    return NIL
                }
                server.world.allActivePlayers.forEach {
                    val ply = it as PlayerServer
                    if (ply.connection == null) {
                        LuaValue.error("Tried to send a message to an offline player ${ply.username}")
                    }
                    val connection = ply.connection
                    NetDataChunk.ChunkNetMessage(connection, outgoingMessage, getNetStringRegistry())
                        .enqueue(connection, true)
                }
                outgoingMessage = null
                return NIL
            }
        }
        return "net"
    }

    override fun getWorld(): World = server.world

    override fun getNetStringRegistry(): NetStringRegistry = server.netServer.netStringRegistry

    override fun getPlayerReprByID(playerID: Int): LuaValue {
        return server.world.getPlayerByID(playerID).repr
    }

    fun queueInboundMessage(message: NetMessage) {
        synchronized(inboundQueue) {
            inboundQueue.add(message)
        }
    }

    fun processInboundQueue() {
        synchronized(inboundQueue) {
            inboundQueue.forEach {
                handle(it)
            }
            inboundQueue.clear()
        }
    }
}
