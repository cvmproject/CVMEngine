package com.complover116.cvmproject.cvmengine.server.lua

import com.complover116.cvmproject.cvmengine.server.net.NetServer
import com.complover116.cvmproject.cvmengine.shared.lua.LibInput

class LibInputServer(systemServer: LuaSystemServer, val netServer: NetServer): LibInput(systemServer) {
    override fun makeSureStringIsRegistered(s: String) {
        netServer.addNetString(s)
    }
}