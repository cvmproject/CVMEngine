package com.complover116.cvmproject.cvmengine.server.lua

import com.complover116.cvmproject.cvmengine.server.Server
import com.complover116.cvmproject.cvmengine.server.world.WorldServer
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.lua.LibPlayer
import com.complover116.cvmproject.cvmengine.shared.world.World
import org.luaj.vm2.LuaError
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.TwoArgFunction

class LibPlayerServer(system: LuaSystemServer, world: WorldServer, val server: Server): LibPlayer(system, world) {
    override fun setUp(lib: LuaTable): String {
        lib["CreateBot"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                val botName = arg1.checkjstring()
                val botAI = arg2.checktable()
                val aiInstance = LuaTable()
                val metatable = LuaTable()
                metatable["__index"] = botAI
                aiInstance.setmetatable(metatable)
                val player = server.connect(null, botName, botName, aiInstance)
                try {
                    aiInstance.get("Init").call(aiInstance, player.repr)
                    player.repr["AI"] = aiInstance
                } catch (e: LuaError) {
                    Logger.log(Logger.LogType.ERROR, "Error while initializing AI for "+player.username+":"+e.message);
                    e.printStackTrace();
                }
                Logger.log(Logger.LogType.INFO, "Created bot $botName")
                return player.repr
            }
        }
        return super.setUp(lib)
    }
}
