package com.complover116.cvmproject.cvmengine.server.lua

import com.complover116.cvmproject.cvmengine.server.Server
import com.complover116.cvmproject.cvmengine.shared.lua.LuaLib
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.TwoArgFunction
import org.luaj.vm2.lib.ZeroArgFunction

class LibServer(system: LuaSystem, val server: Server) : LuaLib(system) {
    override fun setUp(table: LuaTable): String {
        table["GetServerParams"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                return server.serverParams
            }
        }
        table["AllowExternalConnections"] = object: OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                server.allowConnections = arg.checkboolean()
                return NIL
            }
        }
        table["Reload"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                server.restart()
                return NIL
            }
        }
        table["Exit"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                server.stopAndWait()
                return NIL
            }
        }
        table["WriteServerData"] = object: TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                return LuaValue.valueOf(server.serverDataProvider.writeLuaValue(arg1.checkjstring(), arg2))
            }
        }
        table["CommitServerData"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                server.serverDataProvider.commit()
                return NIL
            }
        }
        table["SaveWorld"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                server.world.save()
                return NIL
            }
        }
        table["ReadServerData"] = object : OneArgFunction() {
            override fun call(arg1: LuaValue): LuaValue {
                return server.serverDataProvider.readLuaValue(arg1.checkjstring())
            }
        }
        return "server"
    }
}
