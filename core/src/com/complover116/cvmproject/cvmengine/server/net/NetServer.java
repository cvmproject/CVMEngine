package com.complover116.cvmproject.cvmengine.server.net;

import com.complover116.cvmproject.cvmengine.server.PlayerServer;
import com.complover116.cvmproject.cvmengine.server.Server;
import com.complover116.cvmproject.cvmengine.shared.*;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.net.*;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.*;
import java.util.stream.Collectors;

public class NetServer implements Runnable {
    NetReceiver receiver;
    volatile boolean shouldRun = true;
    // public ArrayList<NetConnection> connections = new ArrayList<>();
    Thread netServerThread;
    volatile boolean ready = false;
    public NetStringRegistry netStringRegistry = new NetStringRegistry(this);
    final Server server;
    public NetServer(Server server, int port, ISteamAPIProxy steamAPI) throws IOException {
        this.receiver = new NetReceiver(port, steamAPI, NetReceiver.SERVER_CHANNEL);
        this.server = server;
    }
    public boolean isReady() {
        return ready;
    }
    public void start() {
        shouldRun = true;
        netServerThread = new Thread(this, "Server Network Thread");
        netServerThread.setDaemon(true);
        netServerThread.start();
    }
    public void stop() {
        ready = false;
        shouldRun = false;
    }

    @Override
    public void run() {
        Thread.currentThread().setUncaughtExceptionHandler(Logger.globalExceptionHandler);
        Logger.log(LogType.INFO, "Server Network Thread started");

        float packetTime = 1/NetConstants.PACKET_RATE;
        float time = 0;
        long ctime = System.nanoTime();
        ready = true;
        while (shouldRun) {
            time += (System.nanoTime() - ctime)/1000000000f;
            ctime = System.nanoTime();
            try {
                if (time > packetTime) {
                    time = 0;
                    LinkedList<Player> players = server.world.getAllConnectedPlayers();
                    for (int i = 0;  i < players.size(); i ++) {
                        PlayerServer playerServer = (PlayerServer) players.get(i);
                        if (playerServer.connection == null) continue;
                        if (playerServer.state == Player.STATE_ACTIVE || playerServer.state == Player.STATE_CONNECTING || playerServer.state == Player.STATE_RECONNECTING)
                            if (!playerServer.connection.update()) {
                                synchronized (server) {
                                    server.disconnect(players.get(i).id);
                                }
                                i--;
                            }
                    }
                }
                NetIncomingMessage message = receiver.receive();
                if (message == null) {
                    Thread.sleep(0, 100000);
                    continue;
                }
//                buffer.flip();
                synchronized (server) {
                    boolean processed = false;
                    for (Player player : server.world.getAllConnectedPlayers()) {
                        PlayerServer playerServer = (PlayerServer) player;
                        if (playerServer.connection == null) continue;
                        if (playerServer.connection.remote.equals(message.getRemote())) {
                            NetPacket packet = playerServer.connection.receivePacket(message.getData(), netStringRegistry);
                            for (NetDataChunk chunk : packet.dataChunks) {
                                server.netServerHandler.queueChunk(chunk, playerServer);
                            }
                            processed = true;
                            break;
                        }
                    }
                    if (!processed) {
                        // Ignore all connections but the host itself
                        if (message.getRemote() instanceof NetRemoteDestination.SocketAddressDestination) {
                            if (!server.allowConnections && !((NetRemoteDestination.SocketAddressDestination) message.getRemote()).getAddress().getAddress().isLoopbackAddress()) {
                                continue;
                            }
                        }


                        NetPacket packet = new NetPacket(message.getData(), netStringRegistry);
                        packet.dataChunks.forEach((chunk) -> {
                            if (chunk instanceof NetDataChunk.ChunkServerRequest) {
                                NetDataChunk.ChunkServerRequest chunkServerRequest = (NetDataChunk.ChunkServerRequest) chunk;
                                if (chunkServerRequest.requestType == NetDataChunk.ChunkServerRequest.STATE_INFO) {
                                    List<NetDataChunk.ChunkServerModuleInfo.ModuleInfo> moduleInfos = server.moduleManager.modules.stream().map((module -> new NetDataChunk.ChunkServerModuleInfo.ModuleInfo(module.id, module.hash))).collect(Collectors.toList());
                                    NetPacket replyPacket = new NetPacket();
                                    replyPacket.addDataChunk(new NetDataChunk.ChunkServerModuleInfo(moduleInfos));
                                    ByteBuffer data = replyPacket.toBytes(0, 0, 0);
                                    message.getRemote().send(data);
                                }
                            } else if (chunk instanceof NetDataChunk.ChunkConnectionRequest) {
                                NetDataChunk.ChunkConnectionRequest chunkConnectionRequest = (NetDataChunk.ChunkConnectionRequest) chunk;
                                Logger.log(LogType.INFO, "Server: New connection from "+message.getRemote());
                                NetConnection client = new NetConnection(message.getRemote());
                                new NetDataChunk.ChunkServerConnectionResponse(client, NetDataChunk.ChunkServerConnectionResponse.STATE_CONNECTION_BEGUN, netStringRegistry.count(), server.world.getAllLoadedEntities().size()).enqueue(client, true);
                                server.connect(client, chunkConnectionRequest.permanentName, chunkConnectionRequest.username, null);
                            }
                        });
                    }
                }
            } catch (SocketTimeoutException ee) {
                // pass
            } catch (IOException e) {
                Logger.log(LogType.ERROR, "Failed receiving data from server socket");
                e.printStackTrace();
                break;
			} catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        ready = false;
        receiver.dispose();
        Logger.log(LogType.INFO, "Server Network Thread stopped");
    }
	public void join() throws InterruptedException {
        Logger.log(LogType.INFO, "Waiting for Server Network Thread to stop...");
        netServerThread.join();
    }
    
    public int addNetString(String string) {
        int id = netStringRegistry.register(string);
//        Logger.log(LogType.DEBUG, "Registered string '"+ string + "' with id " + id);
        for (Player player : server.world.getAllConnectedPlayers()) {
            PlayerServer playerServer = (PlayerServer) player;
            new NetDataChunk.ChunkNetString(playerServer.connection, id, string).enqueue(playerServer.connection, true); // Notify all clients
        }
        return id;
    }

}