package com.complover116.cvmproject.cvmengine.server.net

import com.complover116.cvmproject.cvmengine.server.PlayerServer
import com.complover116.cvmproject.cvmengine.server.Server
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType
import com.complover116.cvmproject.cvmengine.shared.ModuleManager
import com.complover116.cvmproject.cvmengine.shared.Player
import com.complover116.cvmproject.cvmengine.shared.VirtualFS.NamedResource
import com.complover116.cvmproject.cvmengine.shared.net.NetConstants
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk.*
import com.complover116.cvmproject.cvmengine.shared.world.Sector
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Consumer

class NetServerHandler(val server: Server) {
    private val chunkQueue = ConcurrentLinkedQueue<Pair<NetDataChunk, PlayerServer>>()

    fun queueChunk(chunk: NetDataChunk, player: PlayerServer) {
        chunkQueue.add(Pair(chunk, player))
    }

    fun processPendingChunks() {
        while(chunkQueue.isNotEmpty()) {
            val entry = chunkQueue.poll()
            handleChunk(entry.first, entry.second)
        }
    }

    private fun handleChunk(chunk: NetDataChunk, player: PlayerServer) {
        if (chunk is ChunkPlayerInput) {
            synchronized(server.world) {
                if (chunk.playerInput == null) return
                player.input.applyDelta(chunk.playerInput)
            }
        } else if (chunk is ChunkSectorRequest) {
            val sectorID = Sector.posToID(chunk.sectorX, chunk.sectorY)
            synchronized(player.requestedSectors) {
                player.requestedSectors.entries.removeIf { (_, value): Map.Entry<Long?, Long> -> value < System.nanoTime() - NetConstants.SECTOR_REQUEST_TIMEOUT }
                if (chunk.shouldTransmit) {
//                                    Do transmit sector
                    player.requestedSectors.put(sectorID, System.nanoTime())
                } else player.requestedSectors.remove(sectorID)
            }
        } else if (chunk is ChunkPlayerOptions) {
            player.requestedInterpolationDelay = chunk.requestedInterpolationDelay
//                                Logger.log(LogType.INFO, "Setting interpolation delay for "+player.connection.remoteAddress.toString()+" to "+chunkPlayerOptions.requestedInterpolationDelay+ " by remote request");
        } else if (chunk is ChunkNetMessage) {
            if (chunk.message == null) {
                Logger.log(
                    LogType.ERROR,
                    "Failed to receive NetMessage from client, did you add the message type with net.AddNetworkString?"
                )
                return
            }
            server.luaSystem.libNetServer.queueInboundMessage(chunk.message)
        } else if (chunk is ChunkFileRequest) {
            if (chunk.requestedFilePath.contains("..")) {
                Logger.log(
                    LogType.WARN,
                    "Client attempted to trick us into sending something we shouldn't! Not sending " + chunk.requestedFilePath
                )
                ChunkServerFileData(
                    player.connection,
                    chunk.requestedFilePath,
                    -1,
                    -1,
                    ByteArray(0)
                ).enqueue(player.connection, false)
                return
            }
            val targetModule = server.moduleManager.modules.stream()
                .filter { module: ModuleManager.Module -> module.id == chunk.targetModule }
                .findAny()
            if (!targetModule.isPresent) {
                Logger.log(
                    LogType.WARN,
                    "Client requested a file from a non-existent module:" + chunk.targetModule
                )
                ChunkServerFileData(
                    player.connection,
                    chunk.requestedFilePath,
                    -1,
                    -1,
                    ByteArray(0)
                ).enqueue(player.connection, false)
                return
            }
            val foundFile = targetModule.get().resources[chunk.requestedFilePath]
            if (foundFile!!.file != null) {
                val chunkSize = 300
                val fileData = foundFile.file.readBytes()
                val result: MutableList<ByteArray> = ArrayList()
                if (fileData.isNotEmpty()) {
                    var start = 0
                    while (start < fileData.size) {
                        val end = fileData.size.coerceAtMost(start + chunkSize)
                        result.add(Arrays.copyOfRange(fileData, start, end))
                        start += chunkSize
                    }
                } else {
                    result.add(ByteArray(0))
                }
                Logger.log(
                    LogType.INFO,
                    "Sending " + chunk.requestedFilePath + " in " + result.size + " chunks"
                )
                val chunkID = AtomicInteger()
                result.forEach(Consumer { fileChunk: ByteArray? ->
                    ChunkServerFileData(
                        player.connection,
                        chunk.requestedFilePath,
                        chunkID.getAndIncrement(),
                        result.size,
                        fileChunk
                    ).enqueue(player.connection, false)
                })
            } else {
                Logger.log(LogType.WARN, "Client requested a non-existent file:" + chunk.requestedFilePath)
                ChunkServerFileData(
                    player.connection,
                    chunk.requestedFilePath,
                    -1,
                    -1,
                    ByteArray(0)
                ).enqueue(player.connection, false)
            }
        } else if (chunk is ChunkModuleDetailsRequest) {
            val matchingLoadedModule = server.moduleManager.modules.stream()
                .filter { module: ModuleManager.Module -> module.id == chunk.moduleID }
                .findAny()
            if (!matchingLoadedModule.isPresent) {
                Logger.log(LogType.WARN, "Client requested non-existent module " + chunk.moduleID)
                ChunkServerModuleDetails(
                    player.connection,
                    chunk.moduleID,
                    "",
                    "",
                    -1
                ).enqueue(player.connection, false)
            } else {
                val module = matchingLoadedModule.get()
                Logger.log(LogType.INFO, "Responding with file list for " + chunk.moduleID)
                module.resources.forEach { (_: String?, value: NamedResource) ->
                    ChunkServerModuleDetails(
                        player.connection,
                        matchingLoadedModule.get().id,
                        value.relativePath,
                        value.hash,
                        module.resources.size
                    ).enqueue(player.connection, false)
                }
            }
        } else if (chunk is ChunkServerRequest) {
            when (chunk.requestType) {
                ChunkServerRequest.STATE_REQUEST_NETSTRINGS -> {
                    for (id in server.netServer.netStringRegistry.registeredIds) {
                        ChunkNetString(
                            player.connection,
                            id,
                            server.netServer.netStringRegistry.intToString(id)
                        ).enqueue(player.connection, false)
                    }
                }

                ChunkServerRequest.STATE_REQUEST_SNAPSHOT -> {
                    if (player.state == Player.STATE_CONNECTING) {
                        server.luaSystem.libHook.call("NewPlayerConnected", player.repr)
                    }
                    player.state = Player.STATE_ACTIVE
                    server.luaSystem.libHook.call("PlayerConnected", player.repr)
                    server.updatePlayerInfo()
                    synchronized(server.world) {
                        ChunkGameInfo(
                            player.connection,
                            player.id,
                            player.dataEntityID,
                            server.world.curTime,
                            server.isEditing,
                            server.world.allLoadedEntities.size
                        ).enqueue(player.connection, false)
                        for (ent in server.world.allLoadedEntities) {
                            ChunkNewEntity(
                                player.connection,
                                0.0,
                                ent,
                                server.netServer.netStringRegistry,
                                -1,
                                false
                            ).enqueue(player.connection, false)
                        }
                        for (event in server.serverSoundDispatch.activeSounds.values) {
                            ChunkSoundEvent(player.connection, event, server.netServer.netStringRegistry).enqueue(
                                player.connection,
                                false
                            )
                        }
                    }
                }

                ChunkServerRequest.STATE_FINISH_CONNECTION -> {
                    ChunkServerConnectionResponse(
                        player.connection,
                        ChunkServerConnectionResponse.STATE_CONNECTION_FINISHED,
                        server.netServer.netStringRegistry.count(),
                        server.world.allLoadedEntities.size
                    ).enqueue(player.connection, true)
                }
            }
        } else {
            Logger.log(
                LogType.WARN,
                "Received unhandled DataChunk type from " + player.connection!!.remote.toString()
            )
        }
    }
}