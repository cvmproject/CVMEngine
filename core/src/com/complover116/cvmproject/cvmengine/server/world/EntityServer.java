package com.complover116.cvmproject.cvmengine.server.world;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.physics.box2d.Body;
import com.complover116.cvmproject.cvmengine.shared.Config;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.EntityState;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;

import java.util.LinkedList;

public class EntityServer extends Entity {
    LinkedList<Update> history;

    public Body physicsBody = null;

    void initPhysics() {
        if (repr.get("InitPhysics").isnil()) return;
        try {
            repr.get("InitPhysics").call(repr);
        } catch (LuaError e) {
            Logger.log(Logger.LogType.ERROR, "Error while initializing physics for "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }

    void onCollide(EntityServer otherEntity, float tangentVelocity) {
        if (repr.get("OnCollide").isnil()) return;
        try {
            repr.get("OnCollide").call(repr, otherEntity.repr, LuaValue.valueOf(tangentVelocity));
        } catch (LuaError e) {
            Logger.log(Logger.LogType.ERROR, "Error while running collision listener for "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void forceSyncPhysicsBody() {
        if (physicsBody != null) {
            physicsBody.setLinearVelocity(curState.vel);
            physicsBody.setAngularVelocity((float) Math.toRadians(curState.getAngleVel()));
            physicsBody.setTransform(curState.pos, (float) Math.toRadians(curState.getAngle()));
        }
    }

    public void drawServerDebug() {
        try {
            LuaValue func = repr.get("DrawServerDebug");
            if (!func.isnil()) {
                func.call(repr);
            }
        } catch (LuaError e) {
            Logger.log(Logger.LogType.ERROR, "Error while rendering "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }
}
