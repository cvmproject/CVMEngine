package com.complover116.cvmproject.cvmengine.server.world;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.complover116.cvmproject.cvmengine.client.Renderer;
import com.complover116.cvmproject.cvmengine.server.PlayerServer;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.Sector;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import com.complover116.cvmproject.cvmengine.shared.world.DataProvider;
import net.querz.nbt.tag.CompoundTag;

public class WorldServer extends World {
    protected com.badlogic.gdx.physics.box2d.World physicsWorld;
//    WorldScrubber scrubber = new WorldScrubber(this);

    public WorldServer(EntitySystemServer entitySystem, DataProvider dataProvider) {
        super(entitySystem, false, dataProvider);
        physicsWorld = new com.badlogic.gdx.physics.box2d.World(new Vector2(0, 0), true);
        physicsWorld.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                EntityServer ent1 = (EntityServer) contact.getFixtureA().getBody().getUserData();
                EntityServer ent2 = (EntityServer) contact.getFixtureB().getBody().getUserData();
                ent1.onCollide(ent2, contact.getTangentSpeed());
                ent2.onCollide(ent1, contact.getTangentSpeed());
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }

    public com.badlogic.gdx.physics.box2d.World getPhysicsWorld() {
        return physicsWorld;
    }

    @Override
    protected Sector createSectorFromData(CompoundTag data) {
        return new SectorServer(this, data);
    }

    @Override
    protected Sector createEmptySector(int sectorX, int sectorY) {
        return new SectorServer(this, sectorX, sectorY);
    }

    public PlayerServer getPlayer(int id) {
        if (players.containsKey(id)) {
            return (PlayerServer) players.get(id);
        }
        return null;
//        return PlayerServerKt.getMissingPlayer();
    }

    public synchronized void drawPhysicsDebug(Renderer renderer) {
        renderer.drawBox2DDebug(physicsWorld);
    }

    public synchronized void drawServerEntityDebug(Renderer renderer) {
        for (Entity ent : getAllActiveEntities()) {
            ((EntityServer) ent).drawServerDebug();
        }
    }

    @Override
    public void spawnEntity(Entity ent) {
        registerEntity(ent);
        ent.spawn();
    }

    @Override
    public void registerEntity(Entity ent, int id, boolean newlySpawned) {
        super.registerEntity(ent, id, newlySpawned);
        ((EntityServer) ent).initPhysics();
    }

    @Override
    public void registerEntityInSector(Entity ent, int id, Sector sector) {
        super.registerEntityInSector(ent, id, sector);
        ((EntityServer) ent).initPhysics();
    }

    @Override
    protected void updatePhysics(float deltaT) {
        super.updatePhysics(deltaT);
        physicsWorld.step(deltaT, 10, 6);
        Array<Body> bodies = new Array<>();
        physicsWorld.getBodies(bodies);
        bodies.forEach((body) -> {
            if (body.getType() == BodyDef.BodyType.KinematicBody) return;
            ((EntityServer) body.getUserData()).setPos(body.getPosition());
            ((EntityServer) body.getUserData()).setVel(body.getLinearVelocity());
            ((EntityServer) body.getUserData()).curState.setAngle((float) Math.toDegrees(body.getAngle()));
            ((EntityServer) body.getUserData()).curState.setAngleVel((float) Math.toDegrees(body.getAngularVelocity()));
        });
    }

    @Override
    public Entity removeEntity(int id) {
        EntityServer ent = (EntityServer) super.removeEntity(id);
        if (ent.physicsBody != null) {
            physicsWorld.destroyBody(ent.physicsBody);
            ent.physicsBody = null;
        }
        return ent;
    }
}
