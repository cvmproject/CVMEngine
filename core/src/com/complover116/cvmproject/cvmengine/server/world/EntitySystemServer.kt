package com.complover116.cvmproject.cvmengine.server.world

import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import com.complover116.cvmproject.cvmengine.shared.world.EntitySystem
import org.luaj.vm2.lib.TwoArgFunction
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.jse.CoerceLuaToJava
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.CircleShape
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.complover116.cvmproject.cvmengine.shared.world.Entity
import org.luaj.vm2.lib.ThreeArgFunction

class EntitySystemServer(system: LuaSystem?) : EntitySystem(system) {
    init {
        baseEntity["PhysicsInitBody"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                val ent = CoerceLuaToJava.coerce(arg1["ENT"], EntityServer::class.java) as EntityServer
                val type = arg2.checkjstring()
                val world = ent.parentSector?.world as WorldServer?
                if (world == null) {
                    error("Cannot init physics: too early, no world! Please, call this function in ENT:InitPhysics, not ENT:Init!")
                    return NIL
                }
                val bodyDef = BodyDef()
                if (type == "KINEMATIC") {
                    bodyDef.type = BodyDef.BodyType.KinematicBody
                } else if (type == "DYNAMIC") {
                    bodyDef.type = BodyDef.BodyType.DynamicBody
                } else {
                    error("Unknown physics body type $type")
                }
                bodyDef.position.x = ent.pos.x
                bodyDef.position.y = ent.pos.y
                ent.physicsBody = world.physicsWorld.createBody(bodyDef)
                ent.physicsBody.userData = ent
                return NIL
            }
        }
        baseEntity["PhysicsAddCollider"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                val ent = CoerceLuaToJava.coerce(arg1["ENT"], EntityServer::class.java) as EntityServer

                if (ent.physicsBody == null) {
                    error("Cannot add collider - physics not initialized!")
                    return NIL
                }
                val fixtureDef = FixtureDef()
                fixtureDef.density = if (arg2["density"].isnumber()) arg2["density"].checkdouble().toFloat() else 1f

                var x = 0f
                var y = 0f
                var angle = 0f
                if (arg2["x"].isnumber() && arg2["y"].isnumber()) {
                    x = arg2["x"].checkdouble().toFloat()
                    y = arg2["y"].checkdouble().toFloat()
                }
                if (arg2["angle"].isnumber()) {
                    angle = arg2["angle"].checkdouble().toFloat()
                }

                when (arg2["type"].checkjstring()) {
                    "Rect" -> {
                        val polygonShape = PolygonShape()
                        polygonShape.setAsBox(arg2["width"].checkdouble().toFloat()/2, arg2["height"].checkdouble().toFloat()/2, Vector2(x, y), angle)
                        fixtureDef.shape = polygonShape
                    }
                    "Circle" -> {
                        val circleShape = CircleShape()
                        circleShape.radius = arg2["radius"].checkdouble().toFloat()
                        circleShape.position = Vector2(x, y)
                        fixtureDef.shape = circleShape
                    }
                    else -> {
                        error("Unknown collider type ${arg2["type"].checkjstring()}")
                        return NIL
                    }
                }
                if (arg2["friction"].isnumber()) {
                    fixtureDef.friction = arg2["friction"].checkdouble().toFloat()
                }
                if (arg2["restitution"].isnumber()) {
                    fixtureDef.restitution = arg2["restitution"].checkdouble().toFloat()
                }
                if (arg2["collision_category"].isnumber()) {
                    fixtureDef.filter.categoryBits = arg2["collision_category"].checkint().toShort()
                }
                if (arg2["collision_mask"].isnumber()) {
                    fixtureDef.filter.maskBits = arg2["collision_mask"].checkint().toShort()
                }

                ent.physicsBody.createFixture(fixtureDef)
                return NIL
            }
        }
        baseEntity["PhysicsApplyForceCenter"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                val ent = CoerceLuaToJava.coerce(arg1["ENT"], EntityServer::class.java) as EntityServer

                if (ent.physicsBody == null) {
                    error("Cannot apply force - physics not initialized!")
                    return NIL
                }

                ent.physicsBody.applyForceToCenter(arg2["x"].checkdouble().toFloat(), arg2["y"].checkdouble().toFloat(), true)

                return NIL
            }
        }
        baseEntity["PhysicsApplyForce"] = object : ThreeArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue, arg3: LuaValue): LuaValue {
                val ent = CoerceLuaToJava.coerce(arg1["ENT"], EntityServer::class.java) as EntityServer

                if (ent.physicsBody == null) {
                    error("Cannot apply force - physics not initialized!")
                    return NIL
                }
                ent.physicsBody.applyForce(
                    arg2["x"].checkdouble().toFloat(), arg2["y"].checkdouble().toFloat(),
                    arg3["x"].checkdouble().toFloat(), arg3["y"].checkdouble().toFloat(),
                    true
                )

                return NIL
            }
        }
        baseEntity["PhysicsApplyAngForce"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                val ent = CoerceLuaToJava.coerce(arg1["ENT"], EntityServer::class.java) as EntityServer

                if (ent.physicsBody == null) {
                    error("Cannot apply force - physics not initialized!")
                    return NIL
                }

                ent.physicsBody.applyTorque(arg2.checkdouble().toFloat(), true)

                return NIL
            }
        }
        baseEntity["PhysicsApplyImpulseCenter"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                val ent = CoerceLuaToJava.coerce(arg1["ENT"], EntityServer::class.java) as EntityServer

                if (ent.physicsBody == null) {
                    error("Cannot apply impulse - physics not initialized!")
                    return NIL
                }

                ent.physicsBody.applyLinearImpulse(arg2["x"].checkdouble().toFloat(), arg2["y"].checkdouble().toFloat(), ent.pos.x, ent.pos.y, true)

                return NIL
            }
        }
    }

    override fun createEntity(type: String): Entity {
        val ent = EntityServer()
        return initEntity(type, ent)
    }
}
