package com.complover116.cvmproject.cvmengine.server.world;

import com.complover116.cvmproject.cvmengine.shared.world.Sector;
import net.querz.nbt.tag.CompoundTag;

public class SectorServer extends Sector {
    SectorServer(WorldServer world, int x, int y) {
        super(world, x, y);
    }

    SectorServer(WorldServer world, CompoundTag data) {
        super(world, data);
    }
}
