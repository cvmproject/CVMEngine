package com.complover116.cvmproject.cvmengine.server

import com.complover116.cvmproject.cvmengine.server.world.WorldServer
import com.complover116.cvmproject.cvmengine.shared.Player
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes
import com.complover116.cvmproject.cvmengine.shared.net.NetConnection
import com.complover116.cvmproject.cvmengine.shared.world.Entity
import com.complover116.cvmproject.cvmengine.shared.world.EntityState
import gnu.trove.map.hash.TIntObjectHashMap
import org.luaj.vm2.LuaTable

class PlayerServer(permaname: String, username: String?, dataEntityID: Int, luaTypes: LuaTypes, isBot: Boolean) :
    Player(permaname, username, dataEntityID, luaTypes, isBot) {
    @JvmField
    val requestedSectors = HashMap<Long, Long>()
    override fun getPing(): Float {
        return connection?.ping ?: 0f
    }

    @JvmField
    var connection: NetConnection? = null

    var aiInstance: LuaTable? = null

    @JvmField
    val lastSentEntityStates = TIntObjectHashMap<EntityState>()
    @JvmField
    val lastConfirmedReceivedEntityStates = TIntObjectHashMap<EntityState>()

    var lastProcessedKeystateId: Long = 0

    var requestedInterpolationDelay = 0.1f;

    fun getDeltaToSend(ent: Entity): EntityState? {
        synchronized(lastConfirmedReceivedEntityStates) {
            val lastSentState = lastSentEntityStates.get(ent.id)
            val lastConfirmedState = lastConfirmedReceivedEntityStates.get(ent.id)

            if (lastConfirmedState == null) {
                val stateToSend = ent.curState.copy()
                lastSentEntityStates.put(ent.id, stateToSend)
                return stateToSend
            }

            val stateToSend = EntityState()
            stateToSend.applyDelta(lastConfirmedState.getDeltaTo(ent.curState))
            stateToSend.applyDelta(lastSentState.getDeltaTo(ent.curState))

            if (stateToSend.empty) return null
            lastSentState.applyDelta(stateToSend)

            return stateToSend
        }
    }

    fun updateLastConfirmedEntityState(id: Int, state: EntityState) {
        synchronized(lastConfirmedReceivedEntityStates) {
            val lastConfirmedState = lastConfirmedReceivedEntityStates.get(id)
            if (lastConfirmedState == null) {
                lastConfirmedReceivedEntityStates.put(id, state)
            } else {
                lastConfirmedState.applyDelta(state)
            }
        }
    }
}

