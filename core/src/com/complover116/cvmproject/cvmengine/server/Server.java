package com.complover116.cvmproject.cvmengine.server;

import com.badlogic.gdx.files.FileHandle;
import com.complover116.cvmproject.cvmengine.server.lua.LuaSystemServer;
import com.complover116.cvmproject.cvmengine.server.net.NetServerHandler;
import com.complover116.cvmproject.cvmengine.server.world.EntityServer;
import com.complover116.cvmproject.cvmengine.server.world.EntitySystemServer;
import com.complover116.cvmproject.cvmengine.server.world.WorldServer;
import com.complover116.cvmproject.cvmengine.shared.*;
import com.complover116.cvmproject.cvmengine.shared.net.NetConnection;
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk;
import com.complover116.cvmproject.cvmengine.server.net.NetServer;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.EntityState;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import com.complover116.cvmproject.cvmengine.shared.world.DataProvider;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import java.io.IOException;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Objects;

public class Server implements Runnable {
    public NetServer netServer;
    public final NetServerHandler netServerHandler = new NetServerHandler(this);
    public int tickRate = 60;
    public WorldServer world;
    public ServerSoundDispatch serverSoundDispatch;
    public final ServerCommandProcessor commandProcessor;
    public LuaSystemServer luaSystem;
    public ModuleManager moduleManager;
    Thread serverThread;
    volatile private boolean shouldRun;
    volatile public boolean allowConnections = false;
    volatile public float realTPS = 0;
    public Utility.AveragedTimer tickTime = new Utility.AveragedTimer();
    public Utility.AveragedTimer idleTime = new Utility.AveragedTimer();
    volatile public float serverLoad = 0;
    volatile public float serverLoadLongTerm = 0;
    static final int MAX_TICKS_PER_CYCLE = 2;
    static final float MAX_LAG_BEFORE_GIVING_UP = 5;
    static final float PLAYER_INFO_INTERVAL = 2;
    long lastTick = 0;
    long lastSentPlayerInfo = 0;
    long firstUpdate;
    long ticksDone;
    private final int port;
    private boolean suspendWorldAfterLoad = false;
    private boolean isEditing = false;
    private DataProvider worldDataProvider;
    public DataProvider serverDataProvider;

    private ISteamAPIProxy steamAPI;

    public boolean isEditing() {
        return isEditing;
    }

    void resync() {
        firstUpdate = System.nanoTime();
        ticksDone = 0;
    }

    private final ModuleManager.ModuleManagerParams moduleManagerParams;
    public final LuaTable serverParams;
    public Server(int port, ModuleManager.ModuleManagerParams moduleManagerParams, LuaTable serverParams, ISteamAPIProxy steamAPI) throws IOException {
        this.moduleManagerParams = moduleManagerParams;
        this.port = port;
        if (serverParams == null) {
            this.serverParams = new LuaTable();
        } else {
            this.serverParams = serverParams;
        }
        this.steamAPI = steamAPI;
        netServer = new NetServer(this, port, steamAPI);
        serverThread = new Thread(this, "Server Thread");
        commandProcessor = new ServerCommandProcessor(this);
        worldDataProvider = new DataProvider(null, null, "world", null);
    }
    void stop() {
        netServer.stop();
        shouldRun = false;
    }
    public void start() {
        shouldRun = true;
        serverThread.start();
    }
    public void loadWorld(String worldPath) {
        worldDataProvider = new DataProvider(moduleManager.VFS.dataDir.child("worlds").child(worldPath), null, "world", null);
        isEditing = false;
        suspendWorldAfterLoad = false;
        world = new WorldServer((EntitySystemServer) luaSystem.entitySystem, worldDataProvider);
        worldDataProvider.setWorld(world);
        luaSystem.initServerLibs(this, true);
    }
    public void loadMap(String mapPath, boolean restart) {
        Logger.log(Logger.LogType.INFO, "Loading map "+mapPath);
        FileHandle map = moduleManager.VFS.dataDir.child("maps").child(mapPath);
        if (!map.exists()) {
            Logger.log(Logger.LogType.INFO, mapPath+" not found in data directory, looking in modules...");
            map = moduleManager.VFS.getDirectory("maps/"+mapPath).file;
        }
        if (map == null) {
            Logger.log(Logger.LogType.ERROR, "Cannot find map "+mapPath);
            worldDataProvider = new DataProvider(null, null, "world", null);
        }
        worldDataProvider = new DataProvider(null, new DataProvider(map, null, "world", null), "world", null);
        isEditing = false;
        suspendWorldAfterLoad = false;
        if (restart) {
            restart();
        } else {
            world = new WorldServer((EntitySystemServer) luaSystem.entitySystem, worldDataProvider);
            worldDataProvider.setWorld(world);
            luaSystem.initServerLibs(this, true);
        }
    }
    public void editMap(String worldPath) {
        FileHandle map = moduleManager.VFS.dataDir.child("maps").child(worldPath);
        if (!map.exists()) {
            map = moduleManager.VFS.getDirectory("maps/"+worldPath).file;
        }
        if (map == null) {
            map = moduleManager.VFS.dataDir.child("maps").child(worldPath);
        }
        worldDataProvider = new DataProvider(map, null, "world", null);
        isEditing = true;
        suspendWorldAfterLoad = true;
        restart();
    }
    public void restart() {
        new Thread(() -> {
            for (Player player : world.getAllConnectedPlayers()) {
                new NetDataChunk.ChunkServerConnectionResponse(((PlayerServer) player).connection, NetDataChunk.ChunkServerConnectionResponse.STATE_RELOADING, 0, 0).enqueue(((PlayerServer) player).connection, true);
            }
            try {
                Thread.sleep(1000);
                stopAndWait();
            } catch (InterruptedException ignored) {}
            shouldRun = true;
            world.getAllConnectedPlayers().forEach(
                    (it) -> disconnect(it.id)
            );
            try {
                netServer = new NetServer(Server.this, port, steamAPI);
            } catch (IOException e) {
                Logger.fatalError(e);
            }
            serverThread = new Thread(Server.this, "Server Thread");
            serverThread.start();
        }, "Server Restart Thread").start();
    }
	public void stopAndWait() throws InterruptedException {
        stop();
        netServer.join();
        Logger.log(Logger.LogType.INFO, "Waiting for Server Thread to stop...");
        serverThread.join();
	}

	private void tick() {
        // Process console commands
        commandProcessor.processQueue();
        // Process network messages
        luaSystem.libNetServer.processInboundQueue();
        // Update the world
        synchronized (world) {
            for (Player player : world.getAllActivePlayers()) {
                PlayerServer playerServer = (PlayerServer) player;
                synchronized (playerServer.requestedSectors) {
                    playerServer.requestedSectors.forEach((id, timeRequested) -> world.getSectorByID(id).heatUp());
                }
            }
            for (Player player : world.getAllPlayers()) {
                PlayerServer playerServer = (PlayerServer) player;
                if (player.isBot) {
                    try {
                        playerServer.getAiInstance().get("Tick").call(playerServer.getAiInstance(), playerServer.getRepr());
                    } catch (LuaError e) {
                        Logger.log(Logger.LogType.ERROR, "Error while ticking AI for "+playerServer.getUsername()+":"+e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
            world.update(1f / tickRate);

            // Call the think and tick hooks
            luaSystem.libHook.call("Tick");
            luaSystem.libHook.call("Think"); // TODO: Slow this down if can't keep up
        }
    }

    private void sendNetworkData() {
        for (World.CreatedEntity ent : world.getNewEntities()) {
            for (Player player : world.getAllActivePlayers()) {
                PlayerServer playerServer = (PlayerServer) player;
                new NetDataChunk.ChunkNewEntity(playerServer.connection, world.curTime, ent.entity, netServer.netStringRegistry, -1, ent.newlySpawned).enqueue(playerServer.connection, true);
                playerServer.lastSentEntityStates.put(ent.entity.id, ent.entity.curState);
//                    playerServer.lastConfirmedReceivedEntityStates.put(ent.entity.id, ent.entity.curState);
            }
        }

        // Notify the clients of the new world state
        for (Player player : world.getAllActivePlayers()) {
            PlayerServer playerServer = (PlayerServer) player;

                LinkedList<NetDataChunk.ChunkWorldUpdate.EntityUpdate> updates = new LinkedList<>();
                for (Entity ent : world.getAllActiveEntities()) {
                    EntityState deltaState = ((PlayerServer) player).getDeltaToSend(ent);
                    if (deltaState != null) {
                        NetDataChunk.ChunkWorldUpdate.EntityUpdate update = new NetDataChunk.ChunkWorldUpdate.EntityUpdate(ent.id, deltaState);
                        updates.add(update);
                        if (updates.size() > NetDataChunk.ChunkWorldUpdate.MAX_ENTITY_UPDATES) {
                            break;
                        }
                    }
                }
                do {
                    LinkedList<NetDataChunk.ChunkWorldUpdate.EntityUpdate> separatedUpdates = new LinkedList<>();
                    for (int i = 0; i < 100; i ++) {
                        if (updates.isEmpty()) {
                            break;
                        }
                        separatedUpdates.add(updates.pop());
                    }
                    new NetDataChunk.ChunkWorldUpdate(world, separatedUpdates, playerServer, netServer.netStringRegistry).enqueue(playerServer.connection, true);
                } while (!updates.isEmpty());

        }

        for (SoundEvent event : serverSoundDispatch.getQueuedSoundEvents()) {
            for (Player player : world.getAllActivePlayers()) {
                PlayerServer playerServer = (PlayerServer) player;
                new NetDataChunk.ChunkSoundEvent(playerServer.connection, event, this.netServer.netStringRegistry).enqueue(playerServer.connection, true);
            }
        }
        serverSoundDispatch.getQueuedSoundEvents().clear();

//            // Send the queued soundevents
//            for (Entity ent : world.getAllActiveEntities()) {
//                while (!ent.soundQueue.isEmpty()) {
//                    Entity.SoundEvent event = ent.soundQueue.remove();
//                    for (Player player : world.getAllActivePlayers()) {
//                        PlayerServer playerServer = (PlayerServer) player;
//                        new NetDataChunk.ChunkSoundEvent(playerServer.connection, event.time, this.netServer.netStringRegistry.stringToInt(event.soundToPlay), ent.id).enqueue(playerServer.connection);
//                    }
//                }
//            }

        LinkedList<Entity> deletedEntities = world.getDeletedEntities();
        // Destroy sounds attached to deleted entities
        for (Entity ent: deletedEntities) {
            for (SoundEvent playingSound : new LinkedList<>(serverSoundDispatch.getActiveSounds().values())) {
                if (playingSound.getTargetEntityID() != null && playingSound.getTargetEntityID() == ent.id) {
                    Logger.log(Logger.LogType.DEBUG, "Creating sound deletion event for entity "+ent.id);
                    SoundEvent deletionEvent = new SoundEvent(world.curTime, false, true);
                    deletionEvent.setPersistence(true);
                    deletionEvent.setPersistentID(playingSound.getPersistentID());
                    serverSoundDispatch.receiveSoundEvent(deletionEvent);
                }
            }
        }
        // Notify the clients of deleted entities
        for (Entity ent : deletedEntities) {
            for (Player player : world.getAllActivePlayers()) {
                PlayerServer playerServer = (PlayerServer) player;
                new NetDataChunk.ChunkEntityDeath(
                        playerServer.connection,
                        world.curTime,
                        ent.id
                ).enqueue(playerServer.connection, true);
            }
        }
    }

    @Override
    public void run() {
        Thread.currentThread().setUncaughtExceptionHandler(Logger.globalExceptionHandler);
        Logger.log(Logger.LogType.INFO, "Server thread started");
        luaSystem = new LuaSystemServer();
        moduleManager = new ModuleManager(luaSystem, moduleManagerParams);
        serverDataProvider = new DataProvider(moduleManager.VFS.dataDir.child("server"), null, "server-data", luaSystem);
        world = new WorldServer((EntitySystemServer) luaSystem.entitySystem, worldDataProvider);
        worldDataProvider.setWorld(world);
        serverSoundDispatch = new ServerSoundDispatch(this);
        if (suspendWorldAfterLoad) world.isSuspended = true;
        luaSystem.initServerLibs(this, false);
        if (isEditing) luaSystem.libHook.disableAll();
        while(!moduleManager.loadTick()){}
        luaSystem.entitySystem.setVFS(moduleManager.VFS);
        while(!luaSystem.entitySystem.loadTick()){}

        LuaValue map = serverParams.get("map");
        if (map.isstring()) {
            loadMap(map.checkjstring(), false);
        }

        LuaValue world = serverParams.get("world");
        if (world.isstring()) {
            loadWorld(world.checkjstring());
        }

        if (worldDataProvider.getEmpty() || worldDataProvider.getData("world") == null) {
            luaSystem.libHook.call("WorldCreated");
        }
        luaSystem.libHook.call("WorldStarted");

        netServer.start();
        for (String entName : luaSystem.entitySystem.getEntityTypes()) {
            netServer.addNetString(entName);
        }

        for (VirtualFS.NamedResource sound : moduleManager.VFS.listRecurse("sound")) {
            netServer.addNetString(sound.relativePath.substring(6).split("\\.")[0]);
        }

        netServer.addNetString("");
        Logger.log(Logger.LogType.INTERACT, "<LOADING_STATUS_UPDATE>--->WORLD_STARTED");
        lastTick = System.nanoTime();
        lastSentPlayerInfo = System.nanoTime();
        resync();
        while (shouldRun) {
            long projectedTicks = (int)((System.nanoTime() - firstUpdate)/1000000000f*tickRate);
            long ticksToDo = projectedTicks - ticksDone;
            netServerHandler.processPendingChunks();
            // Limit ticks per cycle to avoid freezing
            if (ticksToDo > MAX_TICKS_PER_CYCLE) ticksToDo = MAX_TICKS_PER_CYCLE;
            for (int tick = 0; tick < ticksToDo; tick++) {
                tickTime.start();
                tick();
                tickTime.end();
                ticksDone++;
            }
            if (ticksToDo > 0) {
                synchronized(this) {
                    sendNetworkData();
                    realTPS += (1000000000f / (System.nanoTime() - lastTick) * ticksToDo - realTPS) * 0.1f;
                    lastTick = System.nanoTime();
                    serverLoadLongTerm += (serverLoad - serverLoadLongTerm) * 0.01f;
                    serverLoad = tickTime.getAvg() / (1000f / tickRate) * 100f;
                }
            }

            if (System.nanoTime() - lastSentPlayerInfo > PLAYER_INFO_INTERVAL*1000000000) {
                lastSentPlayerInfo = System.nanoTime();
                updatePlayerInfo();
            }

            try {
                long nextTickIn = firstUpdate + (ticksDone+1)*(1000000000/tickRate) - System.nanoTime();
                if (nextTickIn > 0) {
                    // Time to spare, idle the CPU
                    idleTime.start();
                    Thread.sleep(nextTickIn/1000000);
                    idleTime.end();
                } else if (nextTickIn/1000000000f < -MAX_LAG_BEFORE_GIVING_UP) {
                    // Lagging too far behind, abandoning the idea of ever catching up
                    Logger.log(
                            Logger.LogType.WARN, String.format(Locale.US,
                                    "SERVER OVERLOADED: Running %.2f seconds behind, skipping %d ticks ahead",
                                    -nextTickIn/1000000000f,
                                    projectedTicks - ticksDone
                            )
                    );
                    resync();
                }
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    public boolean isRunning() {
        return serverThread.isAlive();
    }

    public void updatePlayerInfo() {
        for (Player player : world.getAllConnectedPlayers()) {
            PlayerServer playerServer = (PlayerServer) player;
            for (Player player2 : world.getAllPlayers()) {
                new NetDataChunk.ChunkPlayerInfo(player2.id, player2.getDataEntityID(), player2.getPing(), player2.getUsername(), player2.state, player2.isAdmin).enqueue(playerServer.connection, true);
            }
        }
    }

    public PlayerServer connect(NetConnection client, String permanentName, String username, LuaTable aiInstance) {
        if (client != null)
            for (Player player : world.getAllPlayers()) {
                PlayerServer playerServer = (PlayerServer) player;
                if (Objects.equals(playerServer.getPermanentName(), permanentName)) {
                    Logger.log(Logger.LogType.INFO, "Player "+permanentName+" is reconnecting");
                    playerServer.state = PlayerServer.STATE_RECONNECTING;
                    playerServer.connection = client;
                    // TODO: Update player username if changed
                    player.input.applyDelta(luaSystem.libInputServer.getDefaultInputState());
                    updatePlayerInfo();
                    luaSystem.libHook.call("PlayerConnecting", player.getRepr());
                    return playerServer;
                }
            }

        EntityServer playerDataEntity = (EntityServer) luaSystem.entitySystem.createEntity("cvm_logic_playerdata");
        world.registerEntity(playerDataEntity);

        PlayerServer player;
        if (client != null) {
            player = new PlayerServer(permanentName, username, playerDataEntity.id, luaSystem.luaTypes, false);
            player.state = PlayerServer.STATE_CONNECTING;
            player.connection = client;
        } else {
            player = new PlayerServer("AI"+world.getAllPlayers().size(), username, playerDataEntity.id, luaSystem.luaTypes, true);
            player.setAiInstance(aiInstance);
        }
        player.input.applyDelta(luaSystem.libInputServer.getDefaultInputState());
        world.registerPlayer(player);

        Logger.log(Logger.LogType.WARN, "Currently we have "+world.getAllConnectedPlayers().size()+" players");

        updatePlayerInfo();
        luaSystem.libHook.call("PlayerConnecting", player.getRepr());
        if (player.isBot) {
            luaSystem.libHook.call("PlayerConnected", player.getRepr());
            luaSystem.libHook.call("NewPlayerConnected", player.getRepr());
        }
        return player;
    }

    public void disconnect(int id) {
        world.getPlayer(id).state = Player.STATE_OFFLINE;
        world.getPlayer(id).connection = null;
        updatePlayerInfo();
        luaSystem.libHook.call("PlayerDisconnected", world.getPlayer(id).getRepr());
    }
}
