package com.complover116.cvmproject.cvmengine.server

import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.shared.CommandProcessor
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.world.Entity
import org.luaj.vm2.LuaError
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import java.util.*

class ServerCommandProcessor(val server: Server): CommandProcessor() {
    init {
        addCommand("sv_exit") {
            server.stop()
        }
        addCommand("sv_lua") {
            try {
                server.luaSystem.runGlobal((it.copyOfRange(1, it.size).joinToString(" ")), "CLI")
            } catch (e: LuaError) {
                Logger.log(Logger.LogType.ERROR, e.message)
            }
        }
        addCommand("sv_ent_create") {
            server.world.registerEntity(server.luaSystem.entitySystem.createEntity(it[1]))
        }
        addCommand("sv_ent_remove") {
            server.world.getEntityByID(it[1].toInt()).isDead = true
        }
        addCommand("sv_player_set_admin") {
            val ply = server.world.getPlayer(it[1].toInt())
            ply.isAdmin = if (it.size >= 3) it[2].toBoolean() else true
            server.updatePlayerInfo()
        }
        addCommand("sv_ent_move") {
            val ent = server.world.getEntityByID(it[1].toInt())
            ent.pos = Vector2(it[2].toFloat(), it[3].toFloat())
            ent.forceSyncPhysicsBody()
        }
        addCommand("sv_ent_set_vel") {
            val ent = server.world.getEntityByID(it[1].toInt())
            ent.vel = Vector2(it[2].toFloat(), it[3].toFloat())
            ent.forceSyncPhysicsBody()
        }
        addCommand("sv_netstring_dump") {
            server.netServer.netStringRegistry.registeredIds.forEach {
                Logger.log(Logger.LogType.INTERACT, "$it - ${server.netServer.netStringRegistry.intToString(it)}")
            }
        }
        addCommand("sv_timescale") {
            server.world.timeScale = it[1].toFloat()
        }
        addCommand("sv_tickrate") {
            server.tickRate = it[1].toInt()
            server.resync()
        }
        addCommand("sv_save") {
            server.world.save()
        }
        addCommand("sv_ent_types") {
            Logger.log(Logger.LogType.INTERACT, "<ENTITY_TYPE_LIST>--->")
            for (entType in server.luaSystem.entitySystem.entityTypes.keys) {
                Logger.log(Logger.LogType.INTERACT, "<ENTITY_TYPE_LIST_ELEMENT>--->${entType}")
            }
            Logger.log(Logger.LogType.INTERACT, "<ENTITY_TYPE_LIST_END>--->")
        }
        addCommand("sv_active_ent_list") {
            sendEntityList(server.world.allActiveEntities)
        }
        addCommand("sv_all_ent_list") {
            sendEntityList(server.world.allExistingEntities)
        }
        addCommand("sv_loaded_ent_list") {
            sendEntityList(server.world.allLoadedEntities)
        }
        addCommand("sv_reload") {
            server.restart()
        }
        addCommand("sv_load_world") {
            server.loadWorld(it[1])
        }
        addCommand("sv_load_map") {
            server.serverParams["map"] = LuaValue.NIL
            server.loadMap(it[1], true)
        }
        addCommand("sv_edit_map") {
            server.serverParams["map"] = LuaValue.NIL
            server.editMap(it[1])
        }
        addCommand("sv_suspend") {
            server.world.isSuspended = !server.world.isSuspended
        }
        addCommand("sv_ent_dump_state") {
            val entID = it[1].toInt()
            val ent = server.world.getEntityByID(entID)
//            Logger.log(Logger.LogType.INTERACT, "<ENTITY_STATE>--->${ent.id}")
            Logger.log(Logger.LogType.INTERACT, "<ENTITY_TYPE>--->${ent.id}:${ent.type}")
            Logger.log(Logger.LogType.INTERACT, "<ENTITY_POS>--->${ent.id}:${ent.curState.pos?.x}:${ent.curState.pos?.y}")
            Logger.log(Logger.LogType.INTERACT, "<ENTITY_VEL>--->${ent.id}:${ent.curState.vel?.x}:${ent.curState.vel?.y}")
//            Logger.log(Logger.LogType.INTERACT, "<ENTITY_ANGLE>--->${ent.id}:${ent.curState.angle}")
//            Logger.log(Logger.LogType.INTERACT, "<ENTITY_ANGLEVEL>--->${ent.id}:${ent.curState.angleVel}")
            ent.curState.dataTable.data.forEach { entry ->
                Logger.log(Logger.LogType.INTERACT, "<ENTITY_DATATABLE_ENTRY>--->${ent.id}:${entry.key}:${entry.value.type}:${entry.value.raw}")
            }
//            Logger.log(Logger.LogType.INTERACT, "<ENTITY_STATE_END>--->${ent.id}")
        }
        addCommand("sv_ents_change_type") {
            var renamedCount = 0
            server.world.allExistingEntities.forEach { entity ->
                if (entity.type == it[1]) {
                    entity.type = it[2]
                    renamedCount ++
                }
            }
            Logger.log(Logger.LogType.INTERACT, "<WORLD_ENTITY_TYPE_CHANGED>--->$renamedCount")
            if (renamedCount > 0) {
                server.world.save()
                server.restart()
            }
        }
    }

    fun sendEntityList(entities: LinkedList<Entity>) {
        Logger.log(Logger.LogType.INTERACT, "<ENTITY_LIST>--->${entities.size}")
        for (ent in entities) {
            Logger.log(Logger.LogType.INTERACT, "<ENTITY_LIST_ELEMENT>--->${ent.id}:${ent.type}")
        }
        Logger.log(Logger.LogType.INTERACT, "<ENTITY_LIST_END>--->")
    }
}
