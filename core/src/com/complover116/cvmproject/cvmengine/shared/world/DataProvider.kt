package com.complover116.cvmproject.cvmengine.shared.world

import com.badlogic.gdx.files.FileHandle
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import gnu.trove.set.hash.TIntHashSet
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.exception.ZipException
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.CompressionMethod
import net.querz.nbt.io.NBTDeserializer
import net.querz.nbt.io.NBTSerializer
import net.querz.nbt.io.NamedTag
import net.querz.nbt.tag.*
import org.luaj.vm2.LuaError
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.LuaValue.NIL
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.util.*

class DataProvider(val saveDir: FileHandle?, val parentProvider: DataProvider?, val dataFileName: String = "world", val luaSystem: LuaSystem? = null) {
    val empty: Boolean
        get() = (saveDir == null && parentProvider == null)

    var existingZipFile: ZipFile? = null

    var world: World? = null

    init {
        if (saveDir != null && saveDir.child("uncommitted").exists()) {
            Logger.log(Logger.LogType.WARN, "Discarding uncommitted changes for ${saveDir.path()}")
            saveDir.child("uncommitted").deleteDirectory()
        }
    }

    fun getData(dataName: String): CompoundTag? {
        if (saveDir != null) {
            if (!saveDir.exists()) {
                Logger.log(Logger.LogType.INFO, "Creating new data directory ${saveDir.path()}")
                saveDir.mkdirs()
            }
            var fileStream: DataInputStream?
            if (saveDir.child("uncommitted").child("$dataName.nbt").exists()) {
                try {
                    fileStream = DataInputStream(saveDir.child("$dataFileName-uncommitted").child("$dataName.nbt").read())
                } catch (e: IOException) {
                    throw Exception("Failed loading data $dataName: ${e.message}")
                }
            } else {
                try {
                    if (existingZipFile == null) {
                        existingZipFile = ZipFile(saveDir.child("$dataFileName.cvm.zip").file())
                    }
                    val header = existingZipFile!!.getFileHeader("$dataName.nbt")
                    fileStream = DataInputStream(existingZipFile!!.getInputStream(header))
                } catch (e: ZipException) {
                    fileStream = null
                }
            }
            if (fileStream != null) {
                val deserializer = NBTDeserializer()
                val data = deserializer.fromStream(fileStream).tag as CompoundTag
                fileStream.close()
                return data
            }
        }
        if (parentProvider != null) {
            return parentProvider.getData(dataName)
        }
        return null
    }

    fun writeData(dataName: String, data: CompoundTag): Boolean {
        if (saveDir == null) {
            return false
        }
        saveDir.child("$dataFileName-uncommitted").child("$dataName.nbt").parent().mkdirs()
        val fileStream = DataOutputStream(saveDir.child("$dataFileName-uncommitted").child("$dataName.nbt").write(false))
        try {
            val serializer = NBTSerializer()
            serializer.toStream(NamedTag("$dataName.nbt", data), fileStream)
            fileStream.close()
        } catch (e: IOException) {
            throw Exception("Failed saving world data $dataName: ${e.message}")
        }
        return true
    }

    fun commit() {
        if (saveDir == null) {
            Logger.log(Logger.LogType.WARN, "Attempted to commit changes, but no save directory is set!")
            return
        }
        existingZipFile?.close()
        existingZipFile = null
        Logger.log(Logger.LogType.INFO, "Committing changes to zip")
        if (saveDir.child("$dataFileName.cvm.zip").exists()) {
            Logger.log(Logger.LogType.DEBUG, "Copying existing zip...")
            saveDir.child("$dataFileName.cvm.zip").copyTo(saveDir.child("$dataFileName.cvm.zip.tmp"))
            saveDir.child("$dataFileName.cvm.zip").moveTo(saveDir.child("$dataFileName.cvm.zip.${saveDir.list().filter { it.nameWithoutExtension().startsWith(dataFileName) }.size-2}.bak"))
        }
        Logger.log(Logger.LogType.DEBUG, "Writing files to zip...")
        val tempZipFile = ZipFile(saveDir.child("$dataFileName.cvm.zip.tmp").file())
        saveDir.child("$dataFileName-uncommitted").file().listFiles().forEach {
            if (it.isDirectory) {
                val zipParameters = ZipParameters()
                zipParameters.compressionMethod = CompressionMethod.STORE
                zipParameters.rootFolderNameInZip = it.name
                tempZipFile.addFiles(it.listFiles().toMutableList(), zipParameters)
            } else {
                val zipParameters = ZipParameters()
                zipParameters.compressionMethod = CompressionMethod.STORE
                tempZipFile.addFile(it, zipParameters)
            }
        }
        tempZipFile.close()
        Logger.log(Logger.LogType.DEBUG, "Wiping uncommitted directory...")
        saveDir.child("$dataFileName-uncommitted").deleteDirectory()
        Logger.log(Logger.LogType.DEBUG, "Replacing original file...")
//        saveDir.child("world.cvm.zip").delete()
        saveDir.child("$dataFileName.cvm.zip.tmp").moveTo(saveDir.child("$dataFileName.cvm.zip"))
        Logger.log(Logger.LogType.INFO, "Changes committed")
    }

    fun loadWorld(world: World) {
        val worldData = getData("world")
        if (worldData == null) {
            Logger.log(Logger.LogType.INFO, "No world data found, assuming new world")
            return
        }
        world.lastID = worldData.getInt("LastID")
        world.curTime = worldData.getDouble("CurTime")
        worldData.getCompoundTag("EntityMap").forEach { entityID, entitySector ->
            world.entityMap.put(entityID.toInt(), (entitySector as LongTag).asLong())
        }
        if (worldData.containsKey("EntityTypeMap"))
            worldData.getCompoundTag("EntityTypeMap").forEach { entityType, entityList ->
                val entityListConverted = TIntHashSet()
                entityListConverted.addAll((entityList as IntArrayTag).value)
                world.entityTypeMap[entityType] = entityListConverted
            }
    }

    fun saveWorld(world: World) {
        if (saveDir == null ) {
            Logger.log(Logger.LogType.WARN, "Discarding world data: no save directory!")
        }
        val worldData = CompoundTag()
        worldData.putInt("LastID", world.lastID)
        worldData.putDouble("CurTime", world.curTime)
        val entityMap = CompoundTag()
        world.entityMap.forEach {
            entityMap.putLong(it.key.toString(), it.value)
        }
        worldData.put("EntityMap", entityMap)

        val entityTypeMap = CompoundTag()
        world.entityTypeMap.forEach {
            val thisTypeMap = IntArrayTag()
            thisTypeMap.value = it.value.toArray()
            entityTypeMap.put(it.key.toString(), thisTypeMap)
        }
        worldData.put("EntityTypeMap", entityTypeMap)
        writeData("world", worldData)

        LinkedList<Sector>(world.unsavedSectors).forEach {
            it.doFreeze()
            if (it.state == Sector.State.FROZEN) {
                val sectorData = it.save()
                writeData("sectors/sector_" + it.getX() + "_" + it.getY(), sectorData)
                if (it.state == Sector.State.FROZEN) {
                    it.state = Sector.State.SAVED
                    world.unsavedSectors.remove(it)
                }
            }
        }
        commit()
    }

    fun luaValueToNBT(value: LuaValue): CompoundTag {
        val tag = CompoundTag()
        when {
            value.isnil() -> {
                tag.put("Type", IntTag(0))
            }
            value.isnumber() -> {
                tag.put("Type", IntTag(1))
                tag.put("Value", DoubleTag(value.checkdouble()))
            }
            value.isboolean() -> {
                tag.put("Type", IntTag(2))
                tag.put("Value", IntTag(if (value.checkboolean()) 1 else 0))
            }
            value.isstring() -> {
                tag.put("Type", IntTag(3))
                tag.put("Value", StringTag(value.checkjstring()))
            }
            value.istable() -> {
                if (value.get("_cvm_type").isstring()) {
                    if(value.get("_cvm_type").checkjstring().equals("CVM_Vector")) {
                        tag.put("Type", IntTag(5))
                        tag.put("X", DoubleTag(value.get("x").checkdouble()))
                        tag.put("Y", DoubleTag(value.get("y").checkdouble()))
                        return tag
                    }
                    if(value.get("_cvm_type").checkjstring().equals("CVM_Entity")) {
                        tag.put("Type", IntTag(6))
                        tag.put("ID", IntTag(value.get("ENT").get("id").checkint()))
                        return tag
                    }
                }

                tag.put("Type", IntTag(4))
                val tableValueTag = CompoundTag()
                var counter = 1
                value.checktable().keys().forEach {
                    if (it.istable()) {
                        throw LuaError("Serializing tables with tables as keys is not supported")
                    }
                    val entryTag = CompoundTag()
                    entryTag.put("Key", luaValueToNBT(it))
                    entryTag.put("Value", luaValueToNBT(value[it]))
                    tableValueTag.put("$counter", entryTag)
                    counter++
                }
                tag.put("Value", tableValueTag)
            }
            value.isthread() || value.isuserdata() || value.isfunction() -> {
                throw LuaError("Cannot serialize lua object $value")
            }
        }
        return tag
    }

    fun NBTToLuaValue(tag: CompoundTag): LuaValue {
        when ((tag.get("Type") as IntTag).asInt()) {
            0 -> return NIL
            1 -> return LuaValue.valueOf((tag.get("Value") as DoubleTag).asDouble())
            2 -> return LuaValue.valueOf((tag.get("Value") as IntTag).asInt() == 1)
            3 -> return LuaValue.valueOf((tag.get("Value") as StringTag).value)
            4 -> {
                val table = LuaTable()
                val value = tag.get("Value") as CompoundTag
                value.values().forEach {
                    it as CompoundTag
                    val tableKey = NBTToLuaValue(it.get("Key") as CompoundTag)
                    val tableValue = NBTToLuaValue(it.get("Value") as CompoundTag)
                    table[tableKey] = tableValue
                }
                return table
            }
            5 -> {
                if (luaSystem == null) {
                    Logger.log(
                        Logger.LogType.ERROR,
                        "FAILED TO LOAD DATA - Vector cannot be initialized, this DataProvider does not have a LuaSystem!"
                    )
                    return NIL
                }

                return luaSystem.luaTypes.createVector(
                    (tag.get("X") as DoubleTag).asDouble(),
                    (tag.get("Y") as DoubleTag).asDouble()
                )
            }
            6 -> {
                if (world == null) {
                    Logger.log(
                        Logger.LogType.ERROR,
                        "FAILED TO LOAD DATA - Entity cannot be initialized, this DataProvider is not attached to a world!"
                    )
                    return NIL
                }
                return world!!.getEntityByID(tag.getInt("ID")).repr
            }
            else -> {
                Logger.log(Logger.LogType.ERROR, "DATA CORRUPTED - failed to load stored local data")
                return NIL
            }
        }
    }
    fun writeLuaValue(dataName: String, data: LuaValue): Boolean {
        val tag = luaValueToNBT(data)
        return writeData(dataName, tag)
    }

    fun readLuaValue(dataName: String): LuaValue {
        val tag = getData(dataName) ?: return NIL
        return NBTToLuaValue(tag)
    }
}
