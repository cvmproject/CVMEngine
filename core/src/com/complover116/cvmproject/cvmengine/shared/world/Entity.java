package com.complover116.cvmproject.cvmengine.shared.world;

import com.badlogic.gdx.math.Vector2;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import net.querz.nbt.tag.CompoundTag;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import java.util.PriorityQueue;

public abstract class Entity {
    public int id;
    public String type;
    public LuaTable repr;
    private boolean shouldSkipTick = false;
    public boolean isDead = false;

    protected boolean persistent = true;

    protected Sector parentSector;

    public EntityState curState = new EntityState(new Vector2(0, 0), new Vector2(0, 0), 0, 0);

    static final LuaString tick = LuaValue.valueOf("Tick");

    protected static class Update implements Comparable<Update> {
        public double time;
        public EntityState state;

        public Update(double time, EntityState state) {
            this.time = time;
            this.state = state;
        }

        @Override
        public int compareTo(Update o) {
            return Double.compare(time, o.time);
        }
    }

    /***
     * For physics-enabled bodies will force the new pos/vel onto the physics body
    ***/
    public void forceSyncPhysicsBody() {}

    // Entity functions
    void init() {
        try {
            repr.get("Init").call(repr);
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error while initializing "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void spawn() {
        if (repr.get("Spawn").isnil()) return;
        try {
            repr.get("Spawn").call(repr);
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error while spawning "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void setupDataTable() {
        LuaValue func = repr.get("SetupDataTable");
        if (func.isnil()) {
            return;
        }
        try {
            func.call(repr);
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error while setting up data tables for "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void tick(float deltaT, LuaSystem system) {
        if (shouldSkipTick) return;
        try {
            LuaValue tickFunc = repr.get(tick);
            if (tickFunc.isnil()) {
                shouldSkipTick = true;
            } else {
                tickFunc.call(repr, LuaValue.valueOf(deltaT));
            }
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error while updating "+type+":"+e.getMessage());
//            Logger.log(LogType.ERROR, system.debugLib.traceback(1));
            e.printStackTrace();
        }
        parentSector.getWorld().updateEntitySector(this);
    }

    public Vector2 getPos() {
        return curState.pos.cpy();
    }

    public void setPos(Vector2 newPos) {
        setPos(newPos.x, newPos.y);
    }

    public void setPos(float x, float y) {
        curState.pos = new Vector2(x, y);
        parentSector.getWorld().updateEntitySector(this);
    }

    public Vector2 getVel() {
        return curState.vel.cpy();
    }

    public void setVel(Vector2 newPos) {
        curState.vel = new Vector2(newPos);
    }

    public String getType() {
        return type;
    }

    protected boolean tryFreeze() {
        try {
            LuaValue tryFreeze = repr.get("Freeze");
            if (tryFreeze == LuaValue.NIL)
                return true;
            LuaValue out = tryFreeze.call(repr);
            if (out.isnil()) {
                return true;
            } else {
                return out.checkboolean();
            }
        } catch (LuaError e) {
            Logger.log(Logger.LogType.ERROR, "Error while attempting to freeze "+type+":"+e.getMessage());
            e.printStackTrace();
            return true;
        }
    }

    public Sector getParentSector() {
        return parentSector;
    }

    CompoundTag save(DataProvider dataProvider) {
        CompoundTag data = new CompoundTag();
        data.putString("Type", this.type);
        data.putInt("ID", this.id);

        CompoundTag pos = new CompoundTag();
        pos.putFloat("X", curState.pos.x);
        pos.putFloat("Y", curState.pos.y);
        pos.putFloat("Angle", curState.getAngle());

        data.put("Pos", pos);

        CompoundTag vel = new CompoundTag();
        vel.putFloat("X", curState.vel.x);
        vel.putFloat("Y", curState.vel.y);
        vel.putFloat("Angle", curState.getAngleVel());

        data.put("Vel", vel);

        data.put("DataTable", curState.getDataTable().save());
        CompoundTag customData = saveCustomData(dataProvider);
        if (customData != null) {
            data.put("CustomData", customData);
        }

        return data;
    }

    boolean load(CompoundTag data, DataProvider dataProvider) {
        CompoundTag pos = data.getCompoundTag("Pos");
        curState.pos.x = pos.getFloat("X");
        curState.pos.y = pos.getFloat("Y");
        curState.setAngle(pos.getFloat("Angle"));

        CompoundTag vel = data.getCompoundTag("Vel");
        curState.vel.x = vel.getFloat("X");
        curState.vel.y = vel.getFloat("Y");
        curState.setAngleVel(vel.getFloat("Angle"));

        curState.getDataTable().load(data.getCompoundTag("DataTable"));
        if (data.containsKey("CustomData")) {
            var customData = data.getCompoundTag("CustomData");
            loadCustomData(customData, dataProvider);
        }
        return true;
    }

    CompoundTag saveCustomData(DataProvider dataProvider) {
        LuaValue func = repr.get("SaveCustomData");
        if (func.isnil()) {
            return null;
        }
        try {
            LuaValue customData = func.call(repr);
            if (!customData.isnil()) {
                return dataProvider.luaValueToNBT(customData);
            }
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error while saving custom data for "+type+":"+e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    void loadCustomData(CompoundTag data, DataProvider dataProvider) {
        LuaValue func = repr.get("LoadCustomData");
        if (func.isnil()) {
            Logger.log(LogType.WARN, "Discarding custom data for "+type+": LoadCustomData is nil!");
            return;
        }
        try {
            func.call(repr, dataProvider.NBTToLuaValue(data));
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error while loading custom data for "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }
}