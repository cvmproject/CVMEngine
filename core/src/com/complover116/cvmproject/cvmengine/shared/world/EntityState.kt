package com.complover116.cvmproject.cvmengine.shared.world

import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes
import com.complover116.cvmproject.cvmengine.shared.net.NetStringRegistry
import org.luaj.vm2.LuaValue
import splitties.bitflags.hasFlag
import splitties.bitflags.withFlag
import java.nio.ByteBuffer

class EntityState {
    @JvmField
    var pos: Vector2?
    @JvmField
    var vel: Vector2?
    var angle = 0f
    private var hasAngle: Boolean
    var angleVel = 0f
    private var hasAngleVel: Boolean
    val dataTable: DataTable

    val empty: Boolean
        get() = !hasAngle && !hasAngleVel && pos == null && vel == null && dataTable.size == 0

    init {
        hasAngle = false
        hasAngleVel = false
        pos = null
        vel = null
    }

    internal constructor(pos: Vector2?, vel: Vector2?, angle: Float, angleVel: Float) {
        this.pos = pos
        this.vel = vel
        this.angle = angle
        this.angleVel = angleVel
        hasAngle = true
        hasAngleVel = true
        dataTable = DataTable()
    }

    internal constructor(dataTable: DataTable) {
        this.dataTable = dataTable
    }

    internal constructor() {
        dataTable = DataTable()
    }

    fun getDeltaTo(state2: EntityState): EntityState {
        val delta = EntityState(dataTable.getDeltaTo(state2.dataTable))
        if (Math.abs(angle - state2.angle) > ALLOWED_ERROR) {
            delta.angle = state2.angle
            delta.hasAngle = true
        }
        if (Math.abs(angleVel - state2.angleVel) > ALLOWED_ERROR) {
            delta.angleVel = state2.angleVel
            delta.hasAngleVel = true
        }

        if (pos!!.dst(state2.pos) > ALLOWED_ERROR) {
            delta.pos = state2.pos
        }
        if (vel!!.dst(state2.vel) > ALLOWED_ERROR) {
            delta.vel = state2.vel
        }
        return delta
    }

    fun applyDelta(delta: EntityState) {
        if (delta.hasAngle) {
            angle = delta.angle
            hasAngle = true
        }
        if (delta.hasAngleVel) {
            angleVel = delta.angleVel
            hasAngleVel = true
        }
        if (delta.pos != null) {
            pos = delta.pos!!.cpy()
        }
        if (delta.vel != null) {
            vel = delta.vel!!.cpy()
        }
        dataTable.applyDelta(delta.dataTable)
    }

    fun copy(): EntityState {
        val out = EntityState(dataTable.copy())
        out.angle = angle
        out.angleVel = angleVel
        out.hasAngle = hasAngle
        out.hasAngleVel = hasAngleVel
        if (pos != null) out.pos = pos!!.cpy()
        if (vel != null) out.vel = vel!!.cpy()

        return out
    }

    fun interpolate(delta: EntityState, interpValue: Float): EntityState {
        val result = copy()
        if (delta.hasAngle) {
            if (Math.abs(angle - delta.angle) > 180) {
                result.angle = delta.angle
            } else {
                result.angle = (delta.angle - angle) * interpValue + angle
            }
        }
        if (delta.hasAngleVel) {
            result.angleVel = (delta.angleVel - angleVel) * interpValue + angleVel
        }
        if (delta.pos != null) {
            if (delta.pos!!.dst2(result.pos) > result.vel!!.len2()/60) {
                result.pos!!.set(delta.pos)
            } else {
                result.pos!!.interpolate(delta.pos, interpValue, Interpolation.linear)
            }
        }
        if (delta.vel != null) {
            result.vel!!.interpolate(delta.vel, interpValue, Interpolation.linear)
        }
        return result
    }

    val size: Int
        get() {
            var size = 1 // The byte determining whether we have pos/vel/ang/angvel/netvars
            if (pos != null) {
                size += 4 + 4
            }
            if (vel != null) {
                size += 4 + 4
            }
            if (hasAngle) {
                size += 4
            }
            if (hasAngleVel) {
                size += 4
            }
            size += dataTable.size
            return size
        }

    fun encode(data: ByteBuffer, registry: NetStringRegistry) {
        var hasByte: Byte = 0
        if (pos != null) hasByte = hasByte.withFlag(HAS_POS)
        if (vel != null) hasByte = hasByte.withFlag(HAS_VEL)
        if (hasAngle) hasByte = hasByte.withFlag(HAS_ANGLE)
        if (hasAngleVel) hasByte = hasByte.withFlag(HAS_ANGLEVEL)
        if (dataTable.size > 0) hasByte = hasByte.withFlag(HAS_DATA_TABLE)
        data.put(hasByte)
        if (dataTable.size > 0) {
            dataTable.encode(data, registry)
        }
        if (pos != null) {
            data.putFloat(pos!!.x)
            data.putFloat(pos!!.y)
        }
        if (vel != null) {
            data.putFloat(vel!!.x)
            data.putFloat(vel!!.y)
        }
        if (hasAngle) {
            data.putFloat(angle)
        }
        if (hasAngleVel) {
            data.putFloat(angleVel)
        }
    }

    fun addDataTableEntry(name: String, type: String, initialValue: LuaValue, luaTypes: LuaTypes) {
        val newEntry = DataTableEntry.create(name, type, initialValue, luaTypes)
        dataTable.data[name] = newEntry
    }

    fun setDataTableEntry(name: String, value: LuaValue, luaTypes: LuaTypes) {
        if (!dataTable.data.containsKey(name)) {
            throw Exception("Attempted to set a nonexistent DataTable entry $name")
        }
        dataTable.data[name]!!.setFromLuaValue(value, luaTypes)
    }

    fun getDataTableEntry(name: String, world: World, luaTypes: LuaTypes): LuaValue {
        if (!dataTable.data.containsKey(name)) {
            throw Exception("Attempted to get a nonexistent DataTable entry $name")
        }
        return dataTable.data[name]!!.getAsLuaValue(world, luaTypes)
    }

    companion object {
        const val ALLOWED_ERROR = 0.01f
        const val HAS_POS: Byte = 1
        const val HAS_VEL: Byte = 2
        const val HAS_ANGLE: Byte = 4
        const val HAS_ANGLEVEL: Byte = 8
        const val HAS_DATA_TABLE: Byte = 16
        fun decode(data: ByteBuffer, registry: NetStringRegistry): EntityState? {
            var state: EntityState
            val hasByte = data.get()
            if (hasByte.hasFlag(HAS_DATA_TABLE)) {
                state = EntityState(DataTable.decode(data, registry) ?: return null)
            } else {
                state = EntityState()
            }
            if (hasByte.hasFlag(HAS_POS)) state.pos = Vector2(data.float, data.float)
            if (hasByte.hasFlag(HAS_VEL)) state.vel = Vector2(data.float, data.float)
            if (hasByte.hasFlag(HAS_ANGLE)) {
                state.angle = data.float
                state.hasAngle = true
            }
            if (hasByte.hasFlag(HAS_ANGLEVEL)) {
                state.angleVel = data.float
                state.hasAngleVel = true
            }
            return state
        }
    }
}