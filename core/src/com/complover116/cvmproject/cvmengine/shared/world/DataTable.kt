package com.complover116.cvmproject.cvmengine.shared.world

import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.net.NetStringRegistry
import net.querz.nbt.tag.CompoundTag
import java.nio.ByteBuffer

class DataTable() {
    var data = HashMap<String, DataTableEntry>()

    val size: Int
        get() {
            var size = 0
            if (data.size > 0) {
                size += 2
                for (entry in data.values) {
                    size += entry.totalSize
                }
            }
            return size
        }

    fun getDeltaTo(dataNew: DataTable): DataTable {
        val dataDelta = DataTable()
        for (entry in dataNew.data.values) {
            if (data[entry.name]?.valueHash != entry.valueHash)
                dataDelta.data[entry.name] = entry.copy()
        }
        return dataDelta
    }

    fun applyDelta(dataDelta: DataTable) {
        for (entry in dataDelta.data.values) {
            data[entry.name] = entry.copy()
        }
    }

    fun copy(): DataTable{
        val result = DataTable()
        for (key in data.keys) {
            result.data[key] = data[key]!!.copy()
        }
        return result
    }

    fun encode(buffer: ByteBuffer, registry: NetStringRegistry) {
        buffer.putShort(data.size.toShort())
        for (entry in data.values) {
            buffer.putInt(registry.stringToInt(entry.name))
            buffer.put(entry.type)
            entry.encode(buffer)
        }
    }

    fun save(): CompoundTag {
        val rootTag = CompoundTag()
        data.forEach {
            val entryData = it.value.save()
            rootTag.put(it.key, entryData)
        }
        return rootTag
    }

    fun load(savedData: CompoundTag) {
        savedData.forEach { name, entryData ->
            data[name] = DataTableEntry.load(name, entryData as CompoundTag)
        }
    }

    companion object {
        fun decode(buffer: ByteBuffer, registry: NetStringRegistry): DataTable? {
            val result = DataTable()
            val entryCount = buffer.short
            for (i in 0 until entryCount) {
                val nameID = buffer.int
                val name = registry.intToString(nameID) ?: null
                if (name == null) {
                    Logger.log(Logger.LogType.DEBUG, "" +
                            "Not decoding DataTable entry name: unknown netstring id $nameID")
                    return null
                }
                val entry = DataTableEntry.decode(name, buffer)
                result.data[name] = entry
            }
            return result
        }
    }
}
