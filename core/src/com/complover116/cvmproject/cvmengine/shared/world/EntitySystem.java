package com.complover116.cvmproject.cvmengine.shared.world;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.complover116.cvmproject.cvmengine.server.world.EntityServer;
import com.complover116.cvmproject.cvmengine.shared.ILoadable;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.VirtualFS;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;

import java.util.*;

public abstract class EntitySystem implements ILoadable {
    public Hashtable<String, LuaTable> entityTypes = new Hashtable<>();
    LuaSystem luaSystem;
    protected LuaTable baseEntity = new LuaTable();
    LuaTable playerBase = new LuaTable();

    private String loadStep = "Initializing base entity types";
    private String loadStepLow = "LOADING";
    private VirtualFS VFS;

    private static final LuaString ENT = LuaValue.valueOf("ENT");

    LinkedList<VirtualFS.NamedResource> entitiesToLoad;
    private int entitiesToLoadCount;

    public EntitySystem(LuaSystem system) {
        luaSystem = system;
    }

    void loadEntityType(VirtualFS.NamedResource entityType) {
        LuaTable entityTypeTable = null;
        if (entityType.file.isDirectory()) {
            List<FileHandle> filesToRun = Arrays.asList(entityType.file.list());
            filesToRun.sort(Comparator.comparing(FileHandle::name));
            for (FileHandle fileToRun : filesToRun) {
                if (entityTypeTable == null) {
                    entityTypeTable = luaSystem.runSBChunk(fileToRun.readString(), fileToRun.path(), entityType.module.id, "ENT", null);
                } else {
                    luaSystem.runSBChunk(fileToRun.readString(), fileToRun.path(), entityType.module.id, "ENT", entityTypeTable);
                }
            }
        } else {
            entityTypeTable = luaSystem.runSBChunk(entityType.file.readString(), entityType.file.path(), entityType.module.id, "ENT", null);
        }
        if (entityTypeTable == null) {
            Logger.log(LogType.ERROR, "Failed to load entity type "+entityType.file.nameWithoutExtension()+": not a single lua file is present!");
            return;
        }
        LuaValue baseType = entityTypeTable.get("BaseEntity");
        LuaTable parent;
        if (baseType.isnil()) {
            parent = baseEntity;
        } else {
            String parentType = baseType.checkjstring();
            parent = entityTypes.get(parentType);
            if (parent == null) {
                if (entitiesToLoad.stream().anyMatch((it) -> it.file.nameWithoutExtension().equals(parentType))) {
                    Logger.log(LogType.INFO, "Delaying loading of "+entityType.file.nameWithoutExtension()+": base type "+baseType.checkjstring()+" not yet loaded");
                    entitiesToLoad.add(entityType);
                } else {
                    Logger.log(LogType.ERROR, "Failed to load entity type "+entityType.file.nameWithoutExtension()+": base type "+baseType.checkjstring()+" not found!");
                }
                return;
            }
        }
        LuaTable metaTable = new LuaTable();
        metaTable.set("__index", parent);
        entityTypeTable.setmetatable(metaTable);
        entityTypeTable.set("BASE", entityTypeTable);
        entityTypes.put(entityType.file.nameWithoutExtension(), entityTypeTable);
    }

    @Override
    public String getLoadStep() {
        return loadStep;
    }

    @Override
    public String getLoadStepLow() {
        return loadStepLow;
    }

    @Override
    public String getLoadStepHigh() {
        return "LOADING ENTITY SYSTEM";
    }

    public void setVFS(VirtualFS VFS) {
        this.VFS = VFS;
    }

    @Override
    public boolean loadTick() {
        if (entitiesToLoad == null) {
            initializeBaseEntities();
            entitiesToLoad = VFS.list("lua/entities");
            entitiesToLoad.sort(Comparator.comparingInt(entry -> entry.module.priority));
            entitiesToLoadCount = entitiesToLoad.size();
        }
        loadStep = String.format(Locale.ENGLISH, "Loading entity types [%d/%d]", entitiesToLoadCount-entitiesToLoad.size(), entitiesToLoadCount);
        Logger.log(LogType.INFO, String.format(Locale.ENGLISH, "Loading entity type %s [%d/%d]", entitiesToLoad.get(0).file.nameWithoutExtension(), entitiesToLoadCount-entitiesToLoad.size(), entitiesToLoadCount));
        assert entitiesToLoad.get(0) != null;
        loadEntityType(entitiesToLoad.pollFirst());
        if (entitiesToLoad.isEmpty()) {
            Logger.log(LogType.INFO, "EntitySystem initialized");
            luaSystem.libHook.call("EntityTypesLoaded");
            Logger.log(Logger.LogType.INTERACT, "<LOADING_STATUS_UPDATE>--->ENTITY_TYPES_LOADED");
            return true;
        }
        loadStepLow = String.format("Loading entity type %s", entitiesToLoad.get(0).file.nameWithoutExtension());
        return false;
    }

    private void initializeBaseEntities() {
        Logger.log(LogType.INFO, "Loading base entity types..");
        baseEntity.set("Init", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                return NIL;
            }
        });
        baseEntity.set("GetID", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                LuaValue ent = arg.get(ENT);
                return ent.get("id");
            }
        });
        baseEntity.set("GetType", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                LuaValue ent = arg.get(ENT);
                return ent.get("type");
            }
        });
        baseEntity.set("GetPos", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                Vector2 pos = ent.curState.pos;
                return luaSystem.luaTypes.createVector(pos.x, pos.y);
            }
        });
        baseEntity.set("SetPos", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg, LuaValue arg2) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                Vector2 pos = ent.curState.pos;
                pos.x = (float)arg2.get("x").checkdouble();
                pos.y = (float)arg2.get("y").checkdouble();
                if (ent.parentSector != null)
                    ent.parentSector.world.updateEntitySector(ent);
                ent.forceSyncPhysicsBody();
                return NIL;
            }
        });
        baseEntity.set("GetAngle", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                return LuaValue.valueOf(ent.curState.getAngle());
            }
        });
        baseEntity.set("SetAngle", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg1.get(ENT), Entity.class);
                ent.curState.setAngle((float) arg2.checkdouble());
                ent.forceSyncPhysicsBody();
                return NIL;
            }
        });
        baseEntity.set("GetAngleVel", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                return LuaValue.valueOf(ent.curState.getAngleVel());
            }
        });
        baseEntity.set("SetAngleVel", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg1.get(ENT), Entity.class);
                ent.curState.setAngleVel((float) arg2.checkdouble());
                ent.forceSyncPhysicsBody();
                return NIL;
            }
        });
        baseEntity.set("GetVel", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                Vector2 vel = ent.curState.vel;
                return luaSystem.luaTypes.createVector(vel.x, vel.y);
            }
        });
        baseEntity.set("SetVel", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg, LuaValue arg2) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                Vector2 vel = ent.curState.vel;
                vel.x = (float)arg2.get("x").checkdouble();
                vel.y = (float)arg2.get("y").checkdouble();
                ent.forceSyncPhysicsBody();
                return NIL;
            }
        });
        baseEntity.set("SetPersistent", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue self, LuaValue arg) {
                Entity ent = (Entity)CoerceLuaToJava.coerce(self.get(ENT), Entity.class);
                ent.persistent = arg.checkboolean();
                return NIL;
            }
        });
        baseEntity.set("AddDataTableEntry", new VarArgFunction() {
            @Override
            public LuaValue invoke(Varargs args) {
                Entity ent = (Entity) CoerceLuaToJava.coerce(args.arg1().get(ENT), Entity.class);
                String entryName = args.arg(2).checkjstring();
                ent.curState.addDataTableEntry(entryName, args.arg(3).checkjstring(), args.arg(4), luaSystem.luaTypes);
                args.arg1().set("Set" + entryName, new TwoArgFunction() {
                    @Override
                    public LuaValue call(LuaValue arg1, LuaValue arg2) {
                        Entity ent = (Entity) CoerceLuaToJava.coerce(arg1.get(ENT), Entity.class);
                        ent.curState.setDataTableEntry(entryName, arg2, luaSystem.luaTypes);
                        return NIL;
                    }
                });
                args.arg1().set("Get" + entryName, new OneArgFunction() {
                    @Override
                    public LuaValue call(LuaValue arg1) {
                        Entity ent = (Entity) CoerceLuaToJava.coerce(arg1.get(ENT), Entity.class);
                        return ent.curState.getDataTableEntry(entryName, ent.parentSector.getWorld(), luaSystem.luaTypes);
                    }
                });
                return NIL;
            }
        });
//        baseEntity.set("EmitSound", new TwoArgFunction() {
//            @Override
//            public LuaValue call(LuaValue arg, LuaValue arg2) {
//                Entity ent = (Entity) CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
//                ent.enqueueSound(new Entity.SoundEvent(ent.getParentSector().getWorld().curTime, arg2.checkjstring()));
//                return NIL;
//            }
//        });
        baseEntity.set("Remove", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                Entity ent = (Entity) CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                if (ent instanceof EntityServer || ent.id < 0) {
                    return valueOf(ent.isDead = true);
                }
                return valueOf(false);
            }
        });
        baseEntity.set("IsDead", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                Entity ent = (Entity) CoerceLuaToJava.coerce(arg.get(ENT), Entity.class);
                return valueOf(ent.isDead);
            }
        });
        entityTypes.put("base_entity", baseEntity);
        LuaTable metaTable = new LuaTable();
        metaTable.set("__index", entityTypes.get("base_entity"));
        playerBase.setmetatable(metaTable);
        entityTypes.put("base_player", playerBase);
    }

    public abstract Entity createEntity(String type);

    protected Entity initEntity(String type, Entity ent) {
        String actualType = type;
        if (!entityTypes.containsKey(type)) {
            Logger.log(LogType.ERROR, "Attempted to create unknown entity type "+type);
            actualType = "base_entity";
        }
        LuaTable parent = entityTypes.get(actualType);
        ent.type = type;
        ent.repr = new LuaTable();
        LuaTable metaTable = new LuaTable();
        metaTable.set("__index", parent);
        ent.repr.setmetatable(metaTable);
        ent.repr.set(ENT, CoerceJavaToLua.coerce(ent));
        ent.repr.set("_cvm_type", "CVM_Entity");
        ent.setupDataTable();
        ent.init();
        return ent;
    }

    public Set<String> getEntityTypes() {
        return entityTypes.keySet();
    }
}
