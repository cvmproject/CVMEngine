package com.complover116.cvmproject.cvmengine.shared.world

import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes
import net.querz.nbt.tag.CompoundTag
import org.luaj.vm2.LuaValue
import java.nio.ByteBuffer
import java.nio.charset.Charset

abstract class DataTableEntry(val name: String) {
    protected abstract val encodedSize: Byte
    val totalSize: Byte
        get() = (encodedSize + 4 + 2).toByte()
    abstract val type: Byte

    abstract val valueHash: Int
    abstract val raw: Any

    abstract fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes)
    abstract fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue
    abstract fun copy(): DataTableEntry
    abstract fun encode(byteBuffer: ByteBuffer)
    abstract fun doSave(data: CompoundTag)
    abstract fun update(byteBuffer: ByteBuffer)

    fun save(): CompoundTag {
        val data = CompoundTag()
        data.putByte("Type", type)
        doSave(data)
        return data
    }

    companion object {
        const val TYPE_INT: Byte = 0
        const val TYPE_ENTITY: Byte = 1
        const val TYPE_PLAYER: Byte = 2
        const val TYPE_BOOL: Byte = 3
        const val TYPE_FLOAT: Byte = 4
        const val TYPE_VECTOR: Byte = 5
        const val TYPE_COLOR: Byte = 6
        const val TYPE_STRING: Byte = 7

        fun getTypeFromByte(byteType: Byte) = when(byteType) {
            TYPE_INT -> "Int"
            TYPE_FLOAT -> "Float"
            TYPE_BOOL -> "Bool"
            TYPE_ENTITY -> "Entity"
            TYPE_PLAYER -> "Player"
            TYPE_VECTOR -> "Vector"
            TYPE_COLOR -> "Color"
            TYPE_STRING -> "String"
            else -> "UNKNOWN"
        }

        fun decode(name:String, byteBuffer: ByteBuffer): DataTableEntry {
            val type = byteBuffer.get()
            return when (type) {
                TYPE_INT -> DataTableEntryInt(name, byteBuffer.getInt())
                TYPE_ENTITY -> DataTableEntryEntity(name, byteBuffer.getInt())
                TYPE_PLAYER -> DataTableEntryPlayer(name, byteBuffer.getInt())
                TYPE_BOOL -> DataTableEntryBool(name, byteBuffer.get() == 1.toByte())
                TYPE_FLOAT -> DataTableEntryFloat(name, byteBuffer.getFloat().toDouble())
                TYPE_VECTOR -> DataTableEntryVector(name, byteBuffer.getDouble(), byteBuffer.getDouble())
                TYPE_COLOR -> DataTableEntryColor(name, byteBuffer.getFloat(), byteBuffer.getFloat(), byteBuffer.getFloat(), byteBuffer.getFloat())
                TYPE_STRING -> {
                    val length = byteBuffer.get()
                    val bytes = ByteArray(length.toInt())
                    byteBuffer.get(bytes)
                    val value = bytes.decodeToString()
                    DataTableEntryString(name, value)
                }
                else -> throw Exception("Invalid DataTableEntry type $type")
            }
        }

        fun load(name: String, data: CompoundTag): DataTableEntry {
            val type = data.getByte("Type")
            return when (type) {
                TYPE_INT -> DataTableEntryInt(name, data.getInt("Value"))
                TYPE_ENTITY -> DataTableEntryEntity(name, data.getInt("Value"))
                TYPE_PLAYER -> DataTableEntryPlayer(name, data.getInt("Value"))
                TYPE_BOOL -> DataTableEntryBool(name, data.getBoolean("Value"))
                TYPE_FLOAT -> DataTableEntryFloat(name, data.getDouble("Value"))
                TYPE_VECTOR -> DataTableEntryVector(name, data.getDouble("ValueX"), data.getDouble("ValueY"))
                TYPE_COLOR -> DataTableEntryColor(name, data.getFloat("ValueR"), data.getFloat("ValueG"), data.getFloat("ValueB"), data.getFloat("ValueA"))
                TYPE_STRING -> DataTableEntryString(name, data.getString("Value"))
                else -> throw Exception("Invalid DataTableEntry type $type")
            }
        }

        fun create(name:String, type: String, initialValue: LuaValue, luaTypes: LuaTypes): DataTableEntry {
            return when (type) {
                "Int" -> DataTableEntryInt(name, initialValue.checkint())
                "Entity" -> {
                    val entry = DataTableEntryEntity(name, Integer.MIN_VALUE)
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                "Player" -> {
                    val entry = DataTableEntryPlayer(name, Integer.MIN_VALUE)
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                "Bool" -> {
                    val entry = DataTableEntryBool(name, false)
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                "Float" -> {
                    val entry = DataTableEntryFloat(name, 0.0)
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                "Vector" -> {
                    val entry = DataTableEntryVector(name, 0.0, 0.0)
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                "Color" -> {
                    val entry = DataTableEntryColor(name, 0.0f, 0.0f, 0.0f, 0.0f)
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                "String" -> {
                    val entry = DataTableEntryString(name, "")
                    if (!initialValue.isnil())
                        entry.setFromLuaValue(initialValue, luaTypes)
                    entry
                }
                else -> throw Exception("Invalid DataTableEntry type $type")
            }
        }
    }
}

open class DataTableEntryInt(name: String, var value: Int) : DataTableEntry(name) {
    override val encodedSize: Byte
        get() = 4
    override val type: Byte
        get() = TYPE_INT
    override val valueHash: Int
        get() = value
    override val raw: Any
        get() = value

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        value = luaValue.checkint()
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return LuaValue.valueOf(value)
    }

    override fun copy(): DataTableEntryInt {
        return DataTableEntryInt(name, value)
    }

    override fun encode(byteBuffer: ByteBuffer) {
        byteBuffer.putInt(value)
    }

    override fun doSave(data: CompoundTag) {
        data.putInt("Value", value)
    }

    override fun update(byteBuffer: ByteBuffer) {
        value = byteBuffer.getInt()
    }
}

open class DataTableEntryFloat(name: String, var value: Double) : DataTableEntry(name) {
    override val encodedSize: Byte
        get() = 4
    override val type: Byte
        get() = TYPE_FLOAT
    override val valueHash: Int
        get() = value.hashCode()
    override val raw: Any
        get() = value

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        if (luaValue.isboolean()) {
            value = if (luaValue.checkboolean()) 1.0 else 0.0
        } else {
            value = luaValue.checkdouble()
        }
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return LuaValue.valueOf(value)
    }

    override fun copy(): DataTableEntryFloat {
        return DataTableEntryFloat(name, value)
    }

    override fun encode(byteBuffer: ByteBuffer) {
        byteBuffer.putFloat(value.toFloat())
    }

    override fun doSave(data: CompoundTag) {
        data.putDouble("Value", value)
    }

    override fun update(byteBuffer: ByteBuffer) {
        value = byteBuffer.getFloat().toDouble()
    }
}

open class DataTableEntryBool(name: String, var value: Boolean) : DataTableEntry(name) {
    override val encodedSize: Byte
        get() = 1
    override val type: Byte
        get() = TYPE_BOOL
    override val valueHash: Int
        get() = if (value) 1 else 0
    override val raw: Any
        get() = value

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        value = luaValue.checkboolean()
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return LuaValue.valueOf(value)
    }

    override fun copy(): DataTableEntryBool {
        return DataTableEntryBool(name, value)
    }

    override fun encode(byteBuffer: ByteBuffer) {
        byteBuffer.put(if (value) 1 else 0)
    }

    override fun doSave(data: CompoundTag) {
        data.putBoolean("Value", value)
    }

    override fun update(byteBuffer: ByteBuffer) {
        value = byteBuffer.get() == 1.toByte()
    }
}

class DataTableEntryEntity(name: String, value: Int) : DataTableEntryInt(name, value) {
    override val type: Byte
        get() = TYPE_ENTITY

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        value = if (luaValue.isnil()) {
            -1000000000
        } else {
            luaValue.get("GetID").call(luaValue).checkint()
        }
    }

    override fun copy(): DataTableEntryInt {
        return DataTableEntryEntity(name, value)
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return world.getEntityByID(value)?.repr ?: LuaValue.NIL
    }
}

class DataTableEntryPlayer(name: String, value: Int) : DataTableEntryInt(name, value) {
    override val type: Byte
        get() = TYPE_PLAYER

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
//        if (luaValue.isnil()) {
//            Logger.log(Logger.LogType.WARN, "Attempting to set $name to nil")
//            return
//        }
        if (luaValue == LuaValue.NIL) {
            value = -1
        } else {
            value = luaValue.get("GetID").call(luaValue).checkint();
        }
    }

    override fun copy(): DataTableEntryInt {
        return DataTableEntryPlayer(name, value)
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        if (!world.hasPlayer(value)) {
            return LuaValue.NIL;
        }
        return world.getPlayerByID(value).repr
    }
}

open class DataTableEntryVector(name: String, var valueX: Double, var valueY: Double) : DataTableEntry(name) {
    override val encodedSize: Byte
        get() = 16
    override val type: Byte
        get() = TYPE_VECTOR
    override val valueHash: Int
        get() = valueX.hashCode() + 31*valueY.hashCode()
    override val raw: Any
        get() = "$valueX,$valueY"

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        valueX = luaValue.get("x").checkdouble()
        valueY = luaValue.get("y").checkdouble()
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return luaTypes.createVector(valueX, valueY)
    }

    override fun copy(): DataTableEntryVector {
        return DataTableEntryVector(name, valueX, valueY)
    }

    override fun encode(byteBuffer: ByteBuffer) {
        byteBuffer.putDouble(valueX)
        byteBuffer.putDouble(valueY)
    }

    override fun doSave(data: CompoundTag) {
        data.putDouble("ValueX", valueX)
        data.putDouble("ValueY", valueY)
    }

    override fun update(byteBuffer: ByteBuffer) {
        valueX = byteBuffer.getDouble()
        valueY = byteBuffer.getDouble()
    }
}

open class DataTableEntryColor(name: String, var valueR: Float, var valueG: Float, var valueB: Float, var valueA: Float) : DataTableEntry(name) {
    override val encodedSize: Byte
        get() = 16
    override val type: Byte
        get() = TYPE_COLOR
    override val valueHash: Int
        get() {
            var result = valueR.hashCode()
            result = 31*result + valueG.hashCode()
            result = 31*result + valueB.hashCode()
            result = 31*result + valueA.hashCode()
            return result
        }
    override val raw: Any
        get() = "$valueR,$valueG,$valueB,$valueA"

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        valueR = luaValue.get("r").checkdouble().toFloat()
        valueG = luaValue.get("g").checkdouble().toFloat()
        valueB = luaValue.get("b").checkdouble().toFloat()
        valueA = luaValue.get("a").checkdouble().toFloat()
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return luaTypes.createColor(valueR, valueG, valueB, valueA)
    }

    override fun copy(): DataTableEntryColor {
        return DataTableEntryColor(name, valueR, valueG, valueB, valueA)
    }

    override fun encode(byteBuffer: ByteBuffer) {
        byteBuffer.putFloat(valueR)
        byteBuffer.putFloat(valueG)
        byteBuffer.putFloat(valueB)
        byteBuffer.putFloat(valueA)
    }

    override fun doSave(data: CompoundTag) {
        data.putFloat("ValueR", valueR)
        data.putFloat("ValueG", valueG)
        data.putFloat("ValueB", valueB)
        data.putFloat("ValueA", valueA)
    }

    override fun update(byteBuffer: ByteBuffer) {
        valueR = byteBuffer.getFloat()
        valueG = byteBuffer.getFloat()
        valueB = byteBuffer.getFloat()
        valueA = byteBuffer.getFloat()
    }
}

open class DataTableEntryString(name: String, var value: String) : DataTableEntry(name) {
    override val encodedSize: Byte
        get() = (1+value.length).toByte()
    override val type: Byte
        get() = TYPE_STRING
    override val valueHash: Int
        get() {
            return value.hashCode()
        }
    override val raw: Any
        get() = value

    override fun setFromLuaValue(luaValue: LuaValue, luaTypes: LuaTypes) {
        value = luaValue.checkjstring()
    }

    override fun getAsLuaValue(world: World, luaTypes: LuaTypes): LuaValue {
        return LuaValue.valueOf(value)
    }

    override fun copy(): DataTableEntryString {
        return DataTableEntryString(name, value)
    }

    override fun encode(byteBuffer: ByteBuffer) {
        byteBuffer.put(value.length.toByte())
        byteBuffer.put(value.toByteArray(Charset.forName("ascii")))
    }

    override fun doSave(data: CompoundTag) {
        data.putString("Value", value)
    }

    override fun update(byteBuffer: ByteBuffer) {
        val length = byteBuffer.get()
        val bytes = ByteArray(length.toInt())
        byteBuffer.get(bytes)
        value = bytes.decodeToString()
    }
}
