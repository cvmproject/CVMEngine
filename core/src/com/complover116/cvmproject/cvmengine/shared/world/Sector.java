package com.complover116.cvmproject.cvmengine.shared.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.complover116.cvmproject.cvmengine.client.Renderer;
import com.complover116.cvmproject.cvmengine.client.world.SectorClient;
import net.querz.nbt.tag.CompoundTag;
import net.querz.nbt.tag.ListTag;

import java.util.HashMap;
import java.util.LinkedList;

public abstract class Sector {

    protected HashMap<Integer, Entity> entities = new HashMap<>();

    public static long posToID(int sectorX, int sectorY) {
        return (((long) sectorX) << 32) | (sectorY & 0xffffffffL);
    }

    public static int IDToX(long sectorID) {
        return (int) (sectorID >> 32);
    }

    public static int IDToY(long sectorID) {
        return (int) (sectorID);
    }

    public enum State {
        ACTIVE, FROZEN, SAVED, OFFLINE
    }

    State state = State.ACTIVE;
    static final int MAX_SECTOR_TEMP = 100;
    protected int temperature = 0;

    protected final int x, y;
    protected final long id;

    World world;
    public World getWorld() {
        return world;
    }

    protected Sector(World world, int x, int y) {
        this.x = x;
        this.y = y;
        this.id = posToID(x, y);
        this.world = world;
    }

    protected Sector(World world, CompoundTag data) {
        this(world, data.getInt("SectorX"), data.getInt("SectorY"));
        this.world = world;
        ListTag<CompoundTag> entitiesData = data.getListTag("Entities").asCompoundTagList();
//        Logger.log(Logger.LogType.INFO, String.format("Loading sector at %d:%d, %d entities", x, y, entitiesData.size()));
        for (int i = 0; i < entitiesData.size(); i ++) {
            CompoundTag entityData = entitiesData.get(i);
            int entId = entityData.getInt("ID");
            String entType = entityData.getString("Type");

//            Logger.log(Logger.LogType.INFO, String.format("Loading entity %d, type:%s", entId, entType));
            Entity ent = world.createEntity(entType);
            if (ent == null) {
//                Logger.log(Logger.LogType.ERROR, "Could not load entity "+entId);
                continue;
            }

            ent.load(entityData, world.dataProvider);
            world.registerEntityInSector(ent, entId, this);
        }

//        Logger.log(Logger.LogType.DEBUG, "Loaded sector at " + x + ":" + y);
    }

    public CompoundTag save() {
        CompoundTag sectorData = new CompoundTag();
        ListTag<CompoundTag> entitiesData = new ListTag<CompoundTag>(CompoundTag.class);
        for (Entity ent : entities.values()) {
            if (ent.persistent) {
                CompoundTag entityData = ent.save(world.dataProvider);
                entitiesData.add(entityData);
            }
        }
        sectorData.put("Entities", entitiesData);
        sectorData.putInt("SectorX", this.x);
        sectorData.putInt("SectorY", this.y);
        return sectorData;
    }

    public void tick(double curTime) {
        if (temperature > 0) {
            temperature--;
        } else {
            tryFreeze();
        }
    }

    public void drawDebug(Renderer renderer) {
        Color color;
        switch (state) {
            case ACTIVE:
                color = new Color(temperature/(float)MAX_SECTOR_TEMP, 1 - (float)temperature/(float)MAX_SECTOR_TEMP, 0, 1);
                break;
            case FROZEN:
                color = Color.CYAN;
                break;
            case SAVED:
                color = Color.BLUE;
                break;
            case OFFLINE:
            default:
                color = Color.GRAY;
        }
        int SECTOR_SIZE = getWorld().getSectorSize();
        renderer.setColor(color);
        renderer.setLineWidth(0.1f);
        renderer.drawHollowRect(SECTOR_SIZE*x, SECTOR_SIZE*y, SECTOR_SIZE, SECTOR_SIZE);
        for (Entity entity : this.entities.values()) {
            if (this instanceof SectorClient) {
                renderer.setColor(Color.YELLOW);
            } else {
                renderer.setColor(Color.CYAN);
            }
            renderer.drawRect(entity.curState.pos.x-0.1f, entity.curState.pos.y-0.1f, 0.2f, 0.2f);
            renderer.drawText(entity.type, "TitilliumWeb", 0.5f, entity.curState.pos.x, entity.curState.pos.y, color, false);
        }
    }

    Rectangle getBounds() {
        int SECTOR_SIZE = getWorld().getSectorSize();
        return new Rectangle(SECTOR_SIZE*x, SECTOR_SIZE*y, SECTOR_SIZE, SECTOR_SIZE);
    }

    void addEntity(Entity ent) {
        unFreeze();
        entities.put(ent.id, ent);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    Entity getEntity(int id) {
        unFreeze();
        return entities.get(id);
    }

    void removeEntity(int id) {
        unFreeze();
        entities.remove(id);
    }

    public void dumpEntities(LinkedList<Entity> activeEnts) {
        activeEnts.addAll(entities.values());
    }

    protected void bumpTemp(int amount) {
        if (temperature+amount < MAX_SECTOR_TEMP) {
            temperature += amount;
        } else {
            temperature = MAX_SECTOR_TEMP;
        }
    }

    protected void doFreeze() {
        if (state == State.ACTIVE) {
            // Logger.log(LogType.DEBUG, "Freezing sector at " + x + ":" + y);
            state = State.FROZEN;
            getWorld().activeSectors.remove(this);
//            synchronized (getWorld().sectors) {
//                if (this.entities.size() == 0) {
//                    getWorld().sectors.remove(this.id);
//                    getWorld().unsavedSectors.remove(this);
//                    state = State.OFFLINE;
//                }
//            }
        }
    }

    void unFreeze() {
        if (state == State.FROZEN || state == State.SAVED) {
            // Logger.log(LogType.DEBUG, "UnFreezing sector at " + x + ":" + y);
            state = State.ACTIVE;
            getWorld().activeSectors.add(this);
            getWorld().unsavedSectors.add(this);
            bumpTemp(100);
        } else {
            bumpTemp(100);
        }
    }

	public void heatUp() {
        unFreeze();
        bumpTemp(100);
	}

    boolean tryFreeze() {
        if (temperature > 0) {
            return false;
        }
        for (Entity ent : entities.values()) {
            if (!ent.tryFreeze()) {
                bumpTemp(100);
                return false;
            }
        }
        doFreeze();
        return true;
    }
}
