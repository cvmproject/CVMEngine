package com.complover116.cvmproject.cvmengine.shared.world;

import com.complover116.cvmproject.cvmengine.client.Renderer;
import com.complover116.cvmproject.cvmengine.server.world.WorldServer;
import com.complover116.cvmproject.cvmengine.shared.Config;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Player;
import gnu.trove.list.linked.TIntLinkedList;
import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;
import net.querz.nbt.tag.CompoundTag;

import java.util.*;

public abstract class World {
    final protected HashMap<Integer, Player> players = new HashMap<>();
    final protected TLongObjectHashMap<Sector> sectors = new TLongObjectHashMap<>();
    final HashMap<Integer, Long> entityMap = new HashMap<>();
    final HashMap<String, TIntHashSet> entityTypeMap = new HashMap<>();
    protected EntitySystem entitySystem;
    public static class CreatedEntity {
        public final Entity entity;
        public final boolean newlySpawned;
        CreatedEntity(Entity entity, boolean newlySpawned) {
            this.entity = entity;
            this.newlySpawned = newlySpawned;
        }
    }
    protected LinkedList<CreatedEntity> newEntities = new LinkedList<>();
    protected LinkedList<Entity> deletedEntities = new LinkedList<>();
    final protected LinkedList<Sector> activeSectors = new LinkedList<>();
    final protected LinkedList<Sector> unsavedSectors = new LinkedList<>();

    private volatile int activeSectorCount = 0;
    private volatile int frozenSectorCount = 0;
    private volatile int savedSectorCount = 0;
    protected int ticksSinceUpdatedSectorCounts = 0;
    protected static final int SECTOR_COUNT_UPDATE_EVERY = 10;
    protected LinkedList<Double> tickTimes = new LinkedList<>();

    protected int lastID = 0;
    protected int lastNonNetworkedID = 0;
    protected int lastPlayerID = 0;
    public float timeScale = 1;
    public boolean isSuspended = false;
    public DataProvider dataProvider;

    final int SECTOR_SIZE = 32;

    protected int lastTick = 0;

    public int getSectorSize() {return SECTOR_SIZE;}

    protected WorldScrubber worldScrubber;

    public final boolean isRemote;

    public World(EntitySystem entitySystem, boolean isRemote) {
        worldScrubber = new WorldScrubber(this);
        this.isRemote = isRemote;
        this.entitySystem = entitySystem;
    }

    public World(EntitySystem entitySystem, boolean isRemote, DataProvider provider) {
        this(entitySystem, isRemote);
        this.dataProvider = provider;
        if (dataProvider != null) {
            dataProvider.loadWorld(this);
        }
    }

    public void save() {
        if (dataProvider != null) {
            Logger.log(Logger.LogType.INFO, "Saving world");
            dataProvider.saveWorld(this);
        } else {
            Logger.log(Logger.LogType.WARN, "Attempted to save a world with no WorldProvider");
        }
    }

    protected void loadSector(long sectorID) {
        Sector sector;
        CompoundTag sectorData;
        if (!isRemote) {
            int sectorX = Sector.IDToX(sectorID);
            int sectorY = Sector.IDToY(sectorID);
            sectorData = dataProvider.getData("sectors/sector_" + sectorX + "_" + sectorY);
        } else {
            sectorData = null;
        }
        if (sectorData != null) {
            sector = createSectorFromData(sectorData);
        } else {
            sector = createEmptySector(Sector.IDToX(sectorID), Sector.IDToY(sectorID));
        }
        sectors.put(sectorID, sector);
        activeSectors.add(sector);
        unsavedSectors.add(sector);
    }

    protected abstract Sector createSectorFromData(CompoundTag data);

    protected abstract Sector createEmptySector(int sectorX, int sectorY);

    protected void updatePhysics(float deltaT) {};

    public LinkedList<Entity> update(float deltaT) {
        deltaT *= timeScale;
        if (!isSuspended) {
            curTime += deltaT;
            lastTick++;
            tickTimes.add(curTime);
        }

        LinkedList<Entity> activeEntities = new LinkedList<>();
        for (Sector sector : new LinkedList<Sector>(activeSectors)) {
            sector.dumpEntities(activeEntities);
            sector.tick(curTime);
        }

        if (!isSuspended)
            updatePhysics(deltaT);

        for (Entity ent : activeEntities) {
            if (!isSuspended)
                ent.tick(deltaT, entitySystem.luaSystem);
            if (ent.isDead) {
                if (ent.id > 0)
                    deletedEntities.add(ent);
                removeEntity(ent.id);
            }
        }
        activeEntities.removeIf((ent) -> ent.isDead);

        worldScrubber.scrubTick();

        ticksSinceUpdatedSectorCounts++;
        if (ticksSinceUpdatedSectorCounts >= SECTOR_COUNT_UPDATE_EVERY) {
            ticksSinceUpdatedSectorCounts = 0;
            updateSectorCounts();
        }

        tickTimes.removeIf(val -> (val < curTime - Config.MAX_LAGCOMP));
        return activeEntities;
    }

    public long getSectorIDByCoords(float x, float y) {
        return Sector.posToID(coordsToSectorPos(x), coordsToSectorPos(y));
    }

    public int coordsToSectorPos(float coord) {
        return coord >= 0 ? (int) (coord / SECTOR_SIZE) : (int) (coord / SECTOR_SIZE) - 1;
    }

    public Sector getSectorByID(long sectorID) {
        Sector sector = sectors.get(sectorID);
        if (sector == null) {
            loadSector(sectorID);
            return sectors.get(sectorID);
        }
        return sector;
    }

    public Sector getSectorByPos(int sectorX, int sectorY) {
        long sectorID = Sector.posToID(sectorX, sectorY);
        return getSectorByID(sectorID);
    }

    public double curTime = 0;

    public LinkedList<CreatedEntity> getNewEntities() {
        LinkedList<CreatedEntity> result = new LinkedList<>(newEntities);
        newEntities.clear();
        return result;
    }

    public LinkedList<Entity> getDeletedEntities() {
        LinkedList<Entity> result = new LinkedList<>(deletedEntities);
        deletedEntities.clear();
        return result;
    }

    public int registerPlayer(Player ply) {
        lastPlayerID++;
        registerPlayer(ply, lastPlayerID);
        return lastPlayerID;
    }

    public void registerPlayer(Player ply, int id) {
        ply.id = id;
        ply.parentWorld = this;
        players.put(id, ply);
    }

    public void registerEntity(Entity ent) {
        if (ent == null)
            return;
        lastID++;
        registerEntity(ent, lastID, true);
    }

    public void registerNonNetworkedEntity(Entity ent) {
        if (ent == null)
            return;
        lastNonNetworkedID--;
        registerEntity(ent, lastNonNetworkedID, true);
    }

    public void registerEntity(Entity ent, int id, boolean newlySpawned) {
        ent.id = id;
        newEntities.add(new CreatedEntity(ent, newlySpawned));
        updateEntitySector(ent);
        updateEntityTypeMap(ent);
    }

    private void updateEntityTypeMap(Entity ent) {
        TIntHashSet entityList = entityTypeMap.get(ent.type);
        if (entityList == null) {
            entityList = new TIntHashSet();
            entityTypeMap.put(ent.type, entityList);
        }
        if (ent.isDead) {
            entityList.remove(ent.id);
        } else {
            entityList.add(ent.id);
        }
    }

    public void registerEntityInSector(Entity ent, int id, Sector sector) {
        ent.id = id;
        entityMap.put(id, sector.id);
        ent.parentSector = sector;
        sector.addEntity(ent);
        newEntities.add(new CreatedEntity(ent, false));
    }

    public void spawnEntity(Entity ent) {
        registerEntity(ent);
        ent.spawn();
    }

    public boolean updateEntitySector(Entity ent) {
        if (entityMap.containsKey(ent.id)) {
            Sector oldSector = getSectorByID(entityMap.get(ent.id));
            if (oldSector.getBounds().contains(ent.curState.pos)) {
                return false;
            } else {
                oldSector.removeEntity(ent.id);
            }
        }
        // Determine sector for the entity, add to sector's entity list, register in
        // entitymap
        long newSectorID = getSectorIDByCoords(ent.curState.pos.x, ent.curState.pos.y);
        Sector newSector = getSectorByPos(Sector.IDToX(newSectorID), Sector.IDToY(newSectorID));
        newSector.addEntity(ent);
        ent.parentSector = newSector;
        entityMap.put(ent.id, newSectorID);
        return true;
    }

    public Entity removeEntity(int id) {
        if (entityMap.containsKey(id)) {
            Sector entitySector = getSectorByID(entityMap.get(id));
            Entity ent = entitySector.entities.get(id);
            entitySector.removeEntity(id);
            entityMap.remove(id);
            updateEntityTypeMap(ent);
            return ent;
        }
        return null;
    }

    public Entity getEntityByID(int id) {
        if (!entityMap.containsKey(id)) {
            return null;
        }
        Entity ent = getSectorByID(entityMap.get(id)).getEntity(id);
        if (ent == null) {
            entityMap.remove(id);
        }
        return ent;
    }

    public boolean doesEntityExist(int id) {
        return entityMap.containsKey(id);
    }

    public synchronized void drawDebug(Renderer renderer) {
        for (Sector sector : getAllLoadedSectors()) {
            sector.drawDebug(renderer);
        }
    }

    public LinkedList<Entity> getAllActiveEntities() {
        LinkedList<Entity> activeEnts = new LinkedList<>();
        for (Sector sector : activeSectors) {
            if (sector.state == Sector.State.ACTIVE)
                sector.dumpEntities(activeEnts);
        }
        return activeEnts;
    }

    public LinkedList<Entity> getAllLoadedEntities() {
        LinkedList<Entity> loadedEnts = new LinkedList<>();
        for (Sector sector : getAllLoadedSectors()) {
            sector.dumpEntities(loadedEnts);
        }
        return loadedEnts;
    }

    public LinkedList<Entity> getAllExistingEntities() {
        LinkedList<Entity> entities = new LinkedList<>();
        for (int entityID : new HashSet<Integer>(entityMap.keySet())) {
            Entity ent = getEntityByID(entityID);
            if (ent != null)
                entities.add(ent);
        }
        return entities;
    }

    public LinkedList<Entity> getEntitiesByType(String type) {
        TIntHashSet entitiesOfType = entityTypeMap.get(type);
        LinkedList<Entity> result = new LinkedList<>();
        TIntLinkedList brokenLinks = new TIntLinkedList();
        if (entitiesOfType == null) {
            return result;
        }
        entitiesOfType.forEach((entID) -> {
            Entity ent = getEntityByID(entID);
            if (ent != null) {
                result.add(ent);
            } else {
                brokenLinks.add(entID);
            }
            return true;
        });
        entitiesOfType.removeAll(brokenLinks);
        return result;
    }

    public void loadArea(float x, float y, float radius) {
        int maxreach = (int) (radius / SECTOR_SIZE) + 1;
        int cSectorX = coordsToSectorPos(x);
        int cSectorY = coordsToSectorPos(y);
        for (int i = cSectorX - maxreach; i < cSectorX + maxreach + 1; i++)
            for (int j = cSectorY - maxreach; j < cSectorY + maxreach + 1; j++) {
                getSectorByPos(i, j).heatUp();
            }
    }

    public LinkedList<Entity> getEntitiesWithinRadius(float x, float y, float radius) {
        LinkedList<Entity> ents = new LinkedList<>();
        int maxreach = (int) (radius * 2 / SECTOR_SIZE) + 1;
        int cSectorX = coordsToSectorPos(x);
        int cSectorY = coordsToSectorPos(y);
        for (int i = cSectorX - maxreach; i < cSectorX + maxreach + 1; i++)
            for (int j = cSectorY - maxreach; j < cSectorY + maxreach + 1; j++) {
                getSectorByPos(i, j).unFreeze();
                getSectorByPos(i, j).dumpEntities(ents);
            }
        ents.removeIf(ent -> ent.curState.pos.dst(x, y) > radius);
        return ents;
    }

    public int getActiveSectorCount() {
        return activeSectorCount;
    }

    public LinkedList<Sector> getActiveSectors() {
        return activeSectors;
    }

    public synchronized Sector[] getAllLoadedSectors() {
        Sector[] loadedSectors = new Sector[sectors.size()];
        sectors.values(loadedSectors);
        return loadedSectors;
    }

    public int getFrozenSectorCount() {
        return frozenSectorCount;
    }

    public int getSavedSectorCount() {
        return savedSectorCount;
    }

    public Entity createEntity(String type) {
        return entitySystem.createEntity(type);
    }

    public Player getPlayerByID(int id) {
        return players.get(id);
    }

    public boolean hasPlayer(int id) {
        return players.containsKey(id);
    }

    public LinkedList<Player> getAllConnectedPlayers() {
        LinkedList<Player> connectedPlayers = new LinkedList<>();
        for (Player ply : players.values()) {
            if (ply.state == Player.STATE_ACTIVE || ply.state == Player.STATE_CONNECTING || ply.state == Player.STATE_RECONNECTING) {
                connectedPlayers.add(ply);
            }
        }
        return connectedPlayers;
    }

    public LinkedList<Player> getAllActivePlayers() {
        LinkedList<Player> connectedPlayers = new LinkedList<>();
        for (Player ply : players.values()) {
            if (ply.state == Player.STATE_ACTIVE) {
                connectedPlayers.add(ply);
            }
        }
        return connectedPlayers;
    }

    public LinkedList<Player> getAll() {
        return new LinkedList<>(players.values());
    }


    public LinkedList<Player> getAllPlayers() {
        return new LinkedList<>(players.values());
    }

    public void updateSectorCounts() {
        activeSectorCount = activeSectors.size();
        frozenSectorCount = unsavedSectors.size();
        savedSectorCount = sectors.size();
    }

	public void forceFreeze(int sectorX, int sectorY) {
        getSectorByPos(sectorX, sectorY).doFreeze();
	}


    protected class WorldScrubber {
        World world;

        Iterator<Sector> unsavedSectorIterator;

        WorldScrubber(World world) {
            this.world = world;
        }

        public void scrubTick() {
            if (unsavedSectorIterator == null) {
                LinkedList<Sector> sectors = new LinkedList<Sector>(world.unsavedSectors);
                unsavedSectorIterator = sectors.iterator();
            }
            try {
                Sector sector = unsavedSectorIterator.next();
                if (sector.state == Sector.State.FROZEN) {
                    if (world instanceof WorldServer) {
                        dataProvider.writeData("sectors/sector_" + sector.getX() + "_" + sector.getY(), sector.save());
                    }
                    sector.state = Sector.State.SAVED;
                    unsavedSectors.remove(sector);
                }
            } catch (NoSuchElementException e) {
                unsavedSectorIterator = null;
            }
        }
    }
}
