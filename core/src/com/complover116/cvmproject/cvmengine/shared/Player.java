package com.complover116.cvmproject.cvmengine.shared;

import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes;
import com.complover116.cvmproject.cvmengine.shared.world.DataTable;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ThreeArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;

public abstract class Player {
    public World parentWorld;
    private String username;
    private String permanentName;

    public final boolean isBot;
    private int dataEntityID;
    public final DataTable input = new DataTable();
    private final LuaTable repr = new LuaTable();
    public int id = -1;

    public static final byte STATE_CONNECTING = 0;
    public static final byte STATE_RECONNECTING = -1;
    public static final byte STATE_ACTIVE = 1;
    public static final byte STATE_OFFLINE = 2;
    public static final byte STATE_MISSING = 3;
    public static final byte STATE_BOT = 4;

    public byte state = STATE_CONNECTING;

    public boolean isAdmin = false;

    public String getUsername() {
        return username;
    }

    public int getID() {
        return id;
    }

    public String getPermanentName() {
        return permanentName;
    }
    protected Player(String permanentName, String username, int dataEntityID, LuaTypes luaTypes, boolean isBot) {
        this.isBot = isBot;
        if (this.isBot) {
            state = STATE_BOT;
        }
        this.username = username;
        this.permanentName = permanentName;
        this.dataEntityID = dataEntityID;
        repr.set("GetState", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                switch (ply.state) {
                    case STATE_CONNECTING:
                    case STATE_RECONNECTING:
                        return LuaValue.valueOf("Connecting");
                    case STATE_ACTIVE:
                        return LuaValue.valueOf("Active");
                    case STATE_BOT:
                        return LuaValue.valueOf("Bot");
                    case STATE_OFFLINE:
                        return LuaValue.valueOf("Offline");
                    case STATE_MISSING:
                        return LuaValue.valueOf("Missing");
                    default:
                        return LuaValue.valueOf("ERROR");
                }
            }
        });
        repr.set("IsAdmin", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return LuaValue.valueOf(ply.isAdmin);
            }
        });
        repr.set("IsBot", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return LuaValue.valueOf(ply.isBot);
            }
        });
        repr.set("GetInput", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue self, LuaValue key) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                String inputKey = key.checkjstring();
                if (!ply.input.getData().containsKey(inputKey)) {
                    Logger.log(Logger.LogType.ERROR, "Attempted to get undefined input '" + inputKey + "'");
                    return NIL;
                }
                return ply.input.getData().get(inputKey).getAsLuaValue(parentWorld, luaTypes);
            }
        });
        repr.set("SetInput", new ThreeArgFunction() {
            @Override
            public LuaValue call(LuaValue self, LuaValue key, LuaValue newValue) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                String inputKey = key.checkjstring();
                if (!ply.input.getData().containsKey(inputKey)) {
                    Logger.log(Logger.LogType.ERROR, "Attempted to set undefined input '" + inputKey + "'");
                    return NIL;
                }
                ply.input.getData().get(inputKey).setFromLuaValue(newValue, luaTypes);
                return NIL;
            }
        });
        repr.set("GetPing", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return valueOf(ply.getPing());
            }
        });
        repr.set("GetUsername", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return valueOf(ply.getUsername());
            }
        });
        repr.set("GetPermanentName", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return valueOf(ply.getPermanentName());
            }
        });
        repr.set("GetID", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return valueOf(ply.getID());
            }
        });
        repr.set("GetDataEntity", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue self) {
                Player ply = (Player) CoerceLuaToJava.coerce(self.get("PLY"), Player.class);
                return ply.parentWorld.getEntityByID(ply.dataEntityID).repr;
            }
        });

        repr.set("PLY", CoerceJavaToLua.coerce(this));
    }
    public abstract float getPing();
    public int getDataEntityID() {
        return dataEntityID;
    }
    public LuaTable getRepr() {
        return repr;
    }
}
