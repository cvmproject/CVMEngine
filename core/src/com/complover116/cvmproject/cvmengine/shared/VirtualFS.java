package com.complover116.cvmproject.cvmengine.shared;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class VirtualFS {
    public final FileHandle dataDir;
    public final FileHandle cacheDir;

    ModuleManager manager;
    public final HashMap<String, NamedResource> fileCache = new HashMap<>();
    VirtualFS(ModuleManager manager) {
        this.manager = manager;
        // null means it's a dedicated server
        if (Gdx.app == null || Gdx.app.getType() == Application.ApplicationType.Desktop) {
            dataDir = Gdx.files.local("data");
        } else {
            dataDir = Gdx.files.external("data");
        }
        cacheDir = Gdx.files.local("cache");
    }

    public static class NamedResource {
        public final FileHandle file;
        public final String relativePath;
        public final ModuleManager.Module module;
        public final String hash;

        NamedResource(FileHandle file, String relativePath, ModuleManager.Module module, String hash) {
            this.file = file;
            this.relativePath = relativePath;
            this.module = module;
            this.hash = hash;
        }
    }

    static String relativePath(FileHandle parent, FileHandle child) {
        return parent.file().toPath().relativize(child.file().toPath()).toString().replace("\\", "/");
    }

    public LinkedList<NamedResource> list(String path) {
        Logger.log(LogType.DEBUG, "VFS:Listing contents of "+path);
        LinkedList<NamedResource> result = new LinkedList<>();
        for (ModuleManager.Module module : manager.modules) {
            FileHandle directory = module.moduleDir.child(path);
            if (directory.exists()) {
                FileHandle[] contents = directory.list();
                Logger.log(LogType.DEBUG, "VFS:Found " + contents.length + " items in module "+module.name);
                for (FileHandle item : contents) {
                    NamedResource resource = fileCache.get(relativePath(module.moduleDir, item));
                    if (resource == null) {
                        resource = new NamedResource(module.moduleDir.child(relativePath(module.moduleDir, item)), relativePath(module.moduleDir, item), module, "");
                    }
                    result.add(resource);
                }
            }
        }
        return result;
    }

    private LinkedList<NamedResource> collectListRecurse(FileHandle directory, ModuleManager.Module module) {
        FileHandle[] contents = directory.list();
        LinkedList<NamedResource> files = new LinkedList<>();
        for (FileHandle item : contents) {
            if (item.isDirectory()) {
                files.addAll(collectListRecurse(item, module));
            } else {
                String val = relativePath(module.moduleDir, item);
                files.add(fileCache.get(val));
            }
        }
        return files;
    }

    public LinkedList<NamedResource> listRecurse(String path) {
        Logger.log(LogType.DEBUG, String.format("VFS:Listing every file under %s or deeper", path));
        LinkedList<NamedResource> result = new LinkedList<>();
        for (ModuleManager.Module module : manager.modules) {
            FileHandle directory = module.moduleDir.child(path);
            if (!directory.exists()) continue;
            result.addAll(collectListRecurse(directory, module));
        }
        return result;
    }

    public NamedResource getDirectory(String path) {
        for (ModuleManager.Module module : manager.modules) {
            FileHandle directory = module.moduleDir.child(path);
            if (!directory.exists()) continue;
            return new NamedResource(directory, path, module, null);
        }
        return new NamedResource(null, path, null, null);
    }

    public NamedResource get(String path) {
        if(fileCache.containsKey(path)) {
            return fileCache.get(path);
        }
        Logger.log(LogType.ERROR, "VFS:File not found:"+path);
        return new NamedResource(null, path, null, null);
    }
}
