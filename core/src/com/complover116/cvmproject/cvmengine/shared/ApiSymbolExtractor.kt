package com.complover116.cvmproject.cvmengine.shared

import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import org.luaj.vm2.LuaTable
import java.io.File
import java.util.LinkedList

val librariesToDocument = arrayOf(
    "draw", "ents", "console", "hook",
    "input", "lualib", "net", "player",
    "sound", "cam", "client", "server",
    "physics"
)

class ApiSymbolExtractor(val luaSystemClient: LuaSystem, val luaSystemServer: LuaSystem) {
    class APISymbol(val type: String, var client: Boolean, var server: Boolean)



    val libraries = HashMap<String, HashMap<String, APISymbol>>()

    fun collectLibrary(libraryName: String, libraryTable: LuaTable, client: Boolean) {
        libraryTable.keys().forEach {key ->
            if (key.isstring()) {
                val type = libraryTable[key].typename()
                val library = libraries[libraryName]
                val isClient = client
                val isServer = !client
                if (!library!!.containsKey(key.checkjstring())) {
                    library[key.checkjstring()] = APISymbol(type, false, false)
                }
                library[key.checkjstring()]!!.client = library[key.checkjstring()]!!.client || isClient
                library[key.checkjstring()]!!.server = library[key.checkjstring()]!!.server || isServer
            }
        }
    }

    fun generate() {
        Logger.log(Logger.LogType.INFO, "Generating api reference")

        val outputDirectory = File("../../doc").resolve("extracted_symbols")
        outputDirectory.deleteRecursively()
        outputDirectory.mkdirs()

        librariesToDocument.forEach { libraryName ->
            libraries[libraryName] = HashMap()

            if (luaSystemClient.globals[libraryName].istable())
                collectLibrary(libraryName, luaSystemClient.globals[libraryName].checktable(), true)

            if (luaSystemServer.globals[libraryName].istable())
                collectLibrary(libraryName, luaSystemServer.globals[libraryName].checktable(), false)
        }

        libraries.forEach { libraryName, symbols ->
            outputDirectory.resolve("$libraryName.lua").outputStream().use { stream ->
                stream.write("""print("Symbol definitions for $libraryName ")""".toByteArray())
                stream.write("\n$libraryName = {}\n".toByteArray())
                symbols.forEach {
                    stream.write("""
                        $libraryName.${it.key} = {
                            type="${it.value.type}",
                            client=${it.value.client},
                            server=${it.value.server}
                        }
                    """.trimIndent().toByteArray())
                    stream.write("\n".toByteArray())
                }
            }
        }
    }
}
