package com.complover116.cvmproject.cvmengine.shared

class SteamAPIServiceDummy : ISteamAPIProxy {
    override val steamAvailable: Boolean
        get() = false

    override fun dispose() {}

    override fun setRichPresenceKey(key: String, value: String) {}

    override fun clearRichPresence() {}

    override fun runCallbacks() {}
    override fun enableSteamJoins() {}

    override fun disableSteamJoins() {}

    override fun initRelayNetworkAccess() {}

    override fun getRelayNetworkStatus() = false

    override fun sendMessageToUser(steamID: Long, data: ByteArray, channel: Int) {}

    override fun receiveMessage(channel: Int): Pair<Long, ByteArray>? = null
    override fun getLaunchCommandLine(): String? = null
}
