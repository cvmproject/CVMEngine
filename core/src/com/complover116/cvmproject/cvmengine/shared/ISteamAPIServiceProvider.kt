package com.complover116.cvmproject.cvmengine.shared

import com.complover116.cvmproject.cvmengine.client.CVMControl

interface ISteamAPIServiceProvider {
    fun initSteamService(engine: CVMControl): ISteamAPIProxy
}
