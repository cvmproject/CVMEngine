package com.complover116.cvmproject.cvmengine.shared

import com.badlogic.gdx.Gdx
import com.complover116.cvmproject.cvmengine.client.CVMControl
import java.util.*
import kotlin.math.min

object Config {
    /***
     * Lerp does NOT get adjusted by CVMEngine automatically
     */
    const val LERP_ADJ_MANUAL = 0

    /***
     * Lerp gets adjusted to RTT/2 + TickTime*2 + LERP_GRACE to make sure that interpolation failure never occurs
     */
    const val LERP_ADJ_AUTO_CAREFUL = 1

    /***
     * Lerp gets adjusted to RTT/2 + TickTime + LERP_GRACE to favor lower perceived lag,
     * but even a single dropped packet will result in interpolation failure
     */
    const val LERP_ADJ_AUTO_AGGRESSIVE = 2

    /***
     * Adjusting lerp leads to time warping and inconsistencies with lag compensation.
     * Lower values make this effect less perceivable. However, values that are too low will react slowly to changes in ping.
     */
    const val LERP_ADJ_MAX_PER_SECOND = 0.005f

    /***
     * How much should the lerp change with each tick
     */
    const val LERP_ADJ_STEP = 0.0001f

    /***
     * Since RTT is an average and packets arrive somewhat randomly,
     * a bit of grace time must be allowed for late packets not to cause interpolation failures.
     * This constant gets added to all auto adjusted LERP values
     */
    const val LERP_GRACE = 0.004;

    @JvmField
    var MAX_LAGCOMP = 0.3f
    var lerpAdjusmentMode = LERP_ADJ_AUTO_CAREFUL
    var clientInterpolationDelay = 0.2f;
    var lastLerpAdjustment = System.nanoTime()

    val engineVersion: String
    get() {
        val properties = Properties()
        properties.load(Gdx.files.internal("version.properties").read())
        return properties["version"].toString()
    }

    fun autoAdjustLerp(engine: CVMControl) {
        var lerpTarget = 0f
        when (lerpAdjusmentMode) {
            LERP_ADJ_MANUAL -> return
            LERP_ADJ_AUTO_CAREFUL -> {
                lerpTarget = (engine.netClient.connection.ping/2000 + 1f/60f + LERP_GRACE).toFloat()
            }
            LERP_ADJ_AUTO_AGGRESSIVE -> {
                lerpTarget = (engine.netClient.connection.ping/2000 + 1f/120f + LERP_GRACE).toFloat()
            }
        }
        if ((System.nanoTime() - lastLerpAdjustment) > LERP_ADJ_STEP/LERP_ADJ_MAX_PER_SECOND*1000000000) {
            lastLerpAdjustment = System.nanoTime()
            val lerpAdjustment = min(lerpTarget - clientInterpolationDelay, LERP_ADJ_STEP)
            engine.setLerp(clientInterpolationDelay + lerpAdjustment)
        }
    }
}
