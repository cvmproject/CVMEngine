package com.complover116.cvmproject.cvmengine.shared;

public interface ILoadable {
    String getLoadStep();
    String getLoadStepLow();
    String getLoadStepHigh();
    boolean loadTick();

//    @SuppressWarnings("StatementWithEmptyBody")
//    public boolean loadCompletely() {
//        while (!loadTick());
//        return true;
//    }
}
