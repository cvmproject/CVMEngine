package com.complover116.cvmproject.cvmengine.shared;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.ast.Str;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

public class ModuleManager implements ILoadable {
    public static class ModuleManagerParams {
        public ModuleManagerParams(String[] moduleDirs, boolean ignoreDefaultDirs, List<FileHandle> additionalModules, List<String> stringAdditionalModules) {
            this.moduleSourceDirs = moduleDirs;
            this.ignoreDefaultDirs = ignoreDefaultDirs;
            this.additionalModules = additionalModules;
            this.stringAdditionalModules = stringAdditionalModules;
        }

        public ModuleManagerParams() {
            System.out.println( "WARNING: Using blank ModuleManager params, this should not happen in normal operation!");
            this.moduleSourceDirs = new String[0];
            this.ignoreDefaultDirs = false;
            this.additionalModules = new LinkedList<>();
            this.stringAdditionalModules = new LinkedList<>();
        }
        private final String[] moduleSourceDirs;
        private final boolean ignoreDefaultDirs;
        private final List<FileHandle> additionalModules;
        private final List<String> stringAdditionalModules;
    }

    private final ModuleManagerParams moduleManagerParams;
    LuaSystem luaSystem;
    public VirtualFS VFS;
    public final ArrayList<Module> modules = new ArrayList<>();

    private static class AutorunFileToExec {
        FileHandle file;
        String sandboxName;

        AutorunFileToExec(FileHandle file, String sandboxName) {
            this.file = file;
            this.sandboxName = sandboxName;
        }
    }

    private LinkedList<AutorunFileToExec> filesToExec;
    private int filesToExecCount;
    private LinkedList<Module> modulesToCalculateHashFor;
    private int modulesToCalculateHashForCount;
    private String loadStatusMed = "Loading module list [0/3]";
    private String loadStatusLow;

    public class Module {
        public final String name;
        public final String id;
        public final String author;
        public final FileHandle moduleDir;
        public final int priority;
        public String hash;
        public HashMap<String, VirtualFS.NamedResource> resources = new HashMap<>();
        private LinkedList<AutorunFileToExec> moduleFilesToExec = new LinkedList<>();
        Module(String name, String author, FileHandle moduleDir, int priority) {
            this.name = name;
            this.author = author;
            this.moduleDir = moduleDir;
            this.priority = priority;
            this.id = moduleDir.name();
        }
        private void addAllLuaFilesRecurse(FileHandle directory, String sandboxName) {
            for (FileHandle item : directory.list()) {
                if (item.isDirectory()) {
                    addAllLuaFilesRecurse(item, sandboxName);
                } else {
                    moduleFilesToExec.add(new AutorunFileToExec(item, sandboxName));
                }
            }
        }

        private void walkRecurse(FileHandle directory, MessageDigest moduleDigest) throws NoSuchAlgorithmException {
            FileHandle[] contents = directory.list();
            for (FileHandle item : contents) {
                if (item.isDirectory()) {
                    walkRecurse(item, moduleDigest);
                } else {
                    MessageDigest digest = MessageDigest.getInstance("SHA-256");
                    String relativePath = VirtualFS.relativePath(moduleDir, item);
                    Logger.log(LogType.DEBUG, "Adding hash for "+relativePath);
                    digest.update(item.readBytes());
                    byte[] fileHash = digest.digest();

                    resources.put(relativePath, new VirtualFS.NamedResource(item, relativePath, this, new String(Base64.getEncoder().encode(fileHash))));
                    moduleDigest.update(relativePath.getBytes(StandardCharsets.UTF_8));
                    moduleDigest.update(fileHash);
                }
            }
        }
        
        void loadAllFiles() {
            try {
                MessageDigest moduleDigest = MessageDigest.getInstance("SHA-256");
                Logger.log(LogType.INFO, "Loading file list and calculating hash for "+name);
                walkRecurse(moduleDir, moduleDigest);
                hash = new String(Base64.getEncoder().encode(moduleDigest.digest()));
                Logger.log(LogType.INFO, "Calculated hash for "+name+": "+hash);
                resources.forEach((key, value) -> {
                    if (VFS.fileCache.containsKey(key)) {
                        Logger.log(LogType.WARN, "File exists in multiple modules: "+key+" exists in "+this.name+" and "+VFS.fileCache.get(key).module.name);
                    }
                    VFS.fileCache.put(key, value);
                });
            } catch (NoSuchAlgorithmException e) {
                Logger.fatalError( "Failed to load SHA-256 hashing algorithm");
            }
        }

        void addAllLuaFiles() {
            FileHandle autorun_shared = moduleDir.child("lua/autorun/shared");
            if (autorun_shared.exists()) {
                addAllLuaFilesRecurse(autorun_shared, moduleDir.name());
            }
            moduleFilesToExec.sort(Comparator.comparing(autorunFileToExec -> autorunFileToExec.file.path()));
            filesToExec.addAll(moduleFilesToExec);
            moduleFilesToExec.clear();
            if (luaSystem.isClient()) {
                FileHandle autorun_client = moduleDir.child("lua/autorun/client");
                if (autorun_client.exists()) {
                    addAllLuaFilesRecurse(autorun_client, moduleDir.name());
                }
            }
            if (luaSystem.isServer()) {
                FileHandle autorun_server = moduleDir.child("lua/autorun/server");
                if (autorun_server.exists()) {
                    addAllLuaFilesRecurse(autorun_server, moduleDir.name());
                }
            }
            moduleFilesToExec.sort(Comparator.comparing(autorunFileToExec -> autorunFileToExec.file.path()));
            filesToExec.addAll(moduleFilesToExec);
        }
    }
    public ModuleManager(LuaSystem system, ModuleManagerParams params) {
        luaSystem = system;
        this.moduleManagerParams = params;
        VFS = new VirtualFS(this);
    }

    @Override
    public String getLoadStep() {
        return loadStatusMed;
    }

    @Override
    public String getLoadStepLow() {
        return loadStatusLow;
    }

    @Override
    public String getLoadStepHigh() {
        return "LOADING MODULES";
    }

    public boolean loadTick() {
        if (filesToExec == null) {
            loadModules();
            return false;
        } if (!modulesToCalculateHashFor.isEmpty()) {
            Module moduleToCalculateHashFor = modulesToCalculateHashFor.pollFirst();
            moduleToCalculateHashFor.loadAllFiles();
            loadStatusMed = "Loading module files [1/3]";
            loadStatusLow = String.format(Locale.ENGLISH,"Loaded files for %s [%d/%d]", moduleToCalculateHashFor.name, modulesToCalculateHashForCount-modulesToCalculateHashFor.size(), modulesToCalculateHashForCount);
            filesToExecCount = filesToExec.size();
            return false;
        } else {
            AutorunFileToExec item = filesToExec.pollFirst();
            assert item != null;
            Logger.log(LogType.DEBUG, String.format("Executing %s", item.file.path()));
            luaSystem.runSandboxed(item.file.readString(), item.file.path(), item.sandboxName);
            if (item.sandboxName.equals("CVM_ENGINE_BASE"))
            if (filesToExec.isEmpty() || !filesToExec.get(0).sandboxName.equals("CVM_ENGINE_BASE")) {
                Logger.log(LogType.INFO, "Loading lua types");
                luaSystem.initLuaTypes();
            }
        }
        if (filesToExec.isEmpty()) {
            luaSystem.entitySystem.setVFS(VFS);
            luaSystem.libHook.call("ModulesLoaded");
            Logger.log(LogType.INFO, "Loaded all modules");
            Logger.log(Logger.LogType.INTERACT, "<LOADING_STATUS_UPDATE>--->MODULES_LOADED");
            return true;
        }
        loadStatusMed = "Executing autorun files [2/3]";
        loadStatusLow = String.format(Locale.ENGLISH, "Executing autorun files for %s [%d/%d]", filesToExec.getFirst().sandboxName, filesToExecCount - filesToExec.size(), filesToExecCount);
        return false;
    }

    private void loadSingleModule(FileHandle moduleDir) {
        try {
            LuaValue moduleData = luaSystem.runSBChunk(moduleDir.child("module.lua").readString(), moduleDir.name() + "/module.lua", moduleDir.name(), "MODULE", null);
            Module module = new Module(moduleData.get("name").checkjstring(),
                    moduleData.get("author").checkjstring(), moduleDir, moduleData.get("priority").checkint());
            modules.add(module);
            Logger.log(Logger.LogType.INFO, "Found module " + module.name
                    + " (" + moduleDir.name() + ")");
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, e.getMessage());
            Logger.log(LogType.ERROR, "Invalid module.lua file for " + moduleDir.name()
                    + ", NOT LOADING");
        }
    }

    private void loadModules() {
        LinkedList<FileHandle> moduleDirs = new LinkedList<>();
        if (!moduleManagerParams.ignoreDefaultDirs) {
            if (Gdx.app == null || Gdx.app.getType() == Application.ApplicationType.Desktop) {
                moduleDirs.add(Gdx.files.local("modules"));
            } else {
                moduleDirs.add(Gdx.files.internal("modules"));
                moduleDirs.add(Gdx.files.external("modules"));
                Gdx.files.external("modules").mkdirs();
                Gdx.files.external("data").mkdirs();
            }
        }
        for (String moduleDirStr : moduleManagerParams.moduleSourceDirs) {
            Logger.log(LogType.INFO, String.format("Adding %s to module search paths", moduleDirStr));
            moduleDirs.add(Gdx.files.local(moduleDirStr));
        }
        FileHandle[] moduleSourceDirs = new FileHandle[moduleDirs.size()];
        moduleSourceDirs = moduleDirs.toArray(moduleSourceDirs);
        for (FileHandle modulesDir : moduleSourceDirs) {
            for (FileHandle moduleDir : modulesDir.list()) {
                if (modules.stream().anyMatch(module -> module.moduleDir.name().equals(moduleDir.name()))) {
                    Logger.log(Logger.LogType.WARN, "Ignoring duplicate module "+moduleDir.name());
                    continue;
                }
                if (!moduleDir.child("module.lua").exists()) {
                    Logger.log(Logger.LogType.ERROR, "Module " + moduleDir.name() + " does not have a module.lua!");
                    continue;
                }
                loadSingleModule(moduleDir);
            }
        }
        Logger.log(LogType.INFO, "Found " + modules.size() + " modules in search directories");
        Logger.log(LogType.INFO, "Adding "+moduleManagerParams.additionalModules.size()+" additional modules");
        moduleManagerParams.stringAdditionalModules.forEach((it) -> {
            moduleManagerParams.additionalModules.add(Gdx.files.local(it));
        });
        moduleManagerParams.additionalModules.forEach(this::loadSingleModule);
        Logger.log(LogType.INFO, "Initialized " + modules.size() + " modules");
        if (modules.stream().noneMatch(module -> module.moduleDir.name().equals("CVM_ENGINE_BASE"))) {
            Logger.log(LogType.ERROR, "FATAL ERROR: CAN NOT FIND CVM_ENGINE_BASE MODULE");
            System.exit(-1);
        }
        modules.sort(Comparator.comparingInt(o -> o.priority));
        filesToExec = new LinkedList<>();
        modulesToCalculateHashFor = new LinkedList<>();
        for(Module module : modules) {
            module.addAllLuaFiles();
            modulesToCalculateHashFor.add(module);
        }
        modulesToCalculateHashForCount = modulesToCalculateHashFor.size();
    }
}
