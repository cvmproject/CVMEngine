package com.complover116.cvmproject.cvmengine.shared

interface SoundEventReceiver {
    fun receiveSoundEvent(soundEvent: SoundEvent)
}