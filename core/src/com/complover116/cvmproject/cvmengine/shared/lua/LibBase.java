package com.complover116.cvmproject.cvmengine.shared.lua;

import com.complover116.cvmproject.cvmengine.shared.world.World;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ZeroArgFunction;

public class LibBase extends LuaLib {
    World world;
    public LibBase(LuaSystem system, World world) {
        super(system);
        this.world = world;
    }
    @Override
    protected String setUp(LuaTable lib) {
//        lib.set("Vector", new TwoArgFunction() {
//            @Override
//            public LuaValue call(LuaValue x, LuaValue y) {
//                return CoerceJavaToLua.coerce(
//                    new Vector2((float)x.checkdouble(), (float)y.checkdouble())
//                );
//			}
//        });
        system.globals.set("CurTime", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(world.curTime);
			}
        });
        system.globals.set("IsFirstTimePredicted", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(system.isFirstTimePredicted);
            }
        });
        system.globals.set("IsRemote", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(world.isRemote);
            }
        });
        return "base";
    }
}