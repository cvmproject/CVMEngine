package com.complover116.cvmproject.cvmengine.shared.lua;

import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import org.luaj.vm2.*;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ThreeArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import java.util.*;

public class LibHook extends LuaLib {
    private boolean isDisabled = false;
    class Hook {
        String uniqueName;
        LuaFunction func;
        boolean isDead = false;
        Hook(String name, LuaFunction func) {
            this.uniqueName = name;
            this.func = func;
        }
        boolean call(Varargs args) {
            try {
                system.callFunctionSafe(func, args);
                return true;
            } catch (Exception e) {
                Logger.log(LogType.ERROR, "Hook "+uniqueName+" failed:"+e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
    }
    class Event {
        private final LinkedList<Hook> hooks = new LinkedList<>();
        private final LinkedList<Hook> newHooks = new LinkedList<>();
        private boolean isDisabled = false;
        void call(Varargs args) {
            newHooks.forEach((hook) -> {
                hooks.removeIf((hook2) -> hook2.uniqueName.equals(hook.uniqueName));
                hooks.add(hook);
            });
            newHooks.clear();

            if (!isDisabled && !LibHook.this.isDisabled)
                for (Iterator<Hook> hook_iter = hooks.iterator(); hook_iter.hasNext();) {
                    Hook hook = hook_iter.next();
                    if (hook.isDead) {
                        hook_iter.remove();
                    } else {
                        hook.call(args);
                    }
                }
        }
        void add(Hook hook) {
            newHooks.add(hook);
        }
    }
    private final HashMap<String, Event> events = new HashMap<>();
    private final HashMap<String, Hook> hooks = new HashMap<>();
    public boolean call(String name, Varargs args) {
        if (events.containsKey(name)) {
            events.get(name).call(args);
            return true;
        } else {
            system.error("Attempted to call an unknown event "+name);
            return false;
        }
    }
    public boolean call(String name) {
        return call(name, LuaValue.NONE);
    }
    boolean add(String name, String id, LuaValue val) {
        if (events.containsKey(name)) {
            LuaFunction func = val.checkfunction();
            Hook hook = new Hook(id, func);
            events.get(name).add(hook);
            hooks.put(id, hook);
            return true;
        } else {
            system.error("Attempted to add a hook to an unknown event "+name);
            return false;
        }
    }
    void disableEvent(String name) {
        if (this.events.containsKey(name)) {
            this.events.get(name).isDisabled = true;
        }
    }
    void enableEvent(String name) {
        if (this.events.containsKey(name)) {
            this.events.get(name).isDisabled = true;
        }
    }
    public void disableAll() {
        isDisabled = true;
    }
    public void enableAll() {
        isDisabled = false;
    }
    boolean define(String name) {
        if (this.events.containsKey(name)) {
            system.error("Attempted to define an already defined event "+name);
        }
        this.events.put(name, new Event());
        return true;
    }
    boolean remove(String id) {
        if (hooks.containsKey(id)) {
            hooks.get(id).isDead = true;
            return true;
        } else {
            return false;
        }
    }
    LibHook(LuaSystem system) {
        super(system);
    }
    @Override
    protected String setUp(LuaTable lib) {
        LibHook libj = this;
        lib.set("Add", new ThreeArgFunction() {

			@Override
			public LuaValue call(LuaValue arg1, LuaValue arg2, LuaValue arg3) {
                String name = arg1.checkjstring();
                String id = arg2.checkjstring();
                return valueOf(libj.add(name, id, arg3));
			}
            
        });
        lib.set("CallEvent", new VarArgFunction() {

			@Override
			public Varargs invoke(Varargs args) {
                String name = args.arg(1).checkjstring();
                return valueOf(libj.call(name, args.subargs(2)));
			}
            
        });
        lib.set("Remove", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg1) {
                String id = arg1.checkjstring();
                return valueOf(libj.remove(id));
			}
            
        });
        lib.set("DefineEvent", new OneArgFunction() {

			@Override
			public LuaValue call(LuaValue arg1) {
                String name = arg1.checkjstring();
                Logger.log(LogType.DEBUG, "LibHook: Defined event "+name);
                return valueOf(libj.define(name));
			}
            
        });
        return "hook";
    }
}
