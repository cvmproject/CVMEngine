package com.complover116.cvmproject.cvmengine.shared.lua;

import org.luaj.vm2.LuaTable;

public abstract class LuaLib {
    protected LuaSystem system;
    protected LuaLib(LuaSystem system) {
        this.system = system;
        LuaTable lib = new LuaTable();
        system.globals.set(setUp(lib), lib);
    }
    protected abstract String setUp(LuaTable table);
}