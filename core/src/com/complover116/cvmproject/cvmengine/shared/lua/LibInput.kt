package com.complover116.cvmproject.cvmengine.shared.lua

import com.badlogic.gdx.Input
import com.complover116.cvmproject.cvmengine.shared.world.DataTable
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry.Companion.create
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.ThreeArgFunction
import java.util.InputMismatchException

open class LibInput constructor(system: LuaSystem) : LuaLib(system) {
    open fun makeSureStringIsRegistered(s: String) {}

    companion object {
        val keysToSend = arrayOf(
            Input.Keys.LEFT,
            Input.Keys.RIGHT,
            Input.Keys.UP,
            Input.Keys.DOWN,
            Input.Keys.INSERT,
            Input.Keys.TAB,
            Input.Keys.A,
            Input.Keys.B,
            Input.Keys.C,
            Input.Keys.D,
            Input.Keys.E,
            Input.Keys.F,
            Input.Keys.G,
            Input.Keys.H,
            Input.Keys.I,
            Input.Keys.J,
            Input.Keys.K,
            Input.Keys.L,
            Input.Keys.M,
            Input.Keys.N,
            Input.Keys.O,
            Input.Keys.P,
            Input.Keys.Q,
            Input.Keys.R,
            Input.Keys.S,
            Input.Keys.T,
            Input.Keys.U,
            Input.Keys.V,
            Input.Keys.W,
            Input.Keys.X,
            Input.Keys.Y,
            Input.Keys.Z,
            Input.Keys.SPACE,
            Input.Keys.CAPS_LOCK
        )

        val keyNames = arrayOf(
            "KEY_LEFT",
            "KEY_RIGHT",
            "KEY_UP",
            "KEY_DOWN",
            "KEY_INSERT",
            "KEY_TAB",
            "KEY_A",
            "KEY_B",
            "KEY_C",
            "KEY_D",
            "KEY_E",
            "KEY_F",
            "KEY_G",
            "KEY_H",
            "KEY_I",
            "KEY_J",
            "KEY_K",
            "KEY_L",
            "KEY_M",
            "KEY_N",
            "KEY_O",
            "KEY_P",
            "KEY_Q",
            "KEY_R",
            "KEY_S",
            "KEY_T",
            "KEY_U",
            "KEY_V",
            "KEY_W",
            "KEY_X",
            "KEY_Y",
            "KEY_Z",
            "KEY_SPACE",
            "KEY_CAPS_LOCK"
        )
    }

    val defaultInputState = DataTable()
    override fun setUp(lib: LuaTable): String {
        lib["Define"] = object : ThreeArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue, arg3: LuaValue): LuaValue {
                val newEntry = create(arg1.checkjstring(), arg2.checkjstring(), arg3, system.luaTypes)
                this@LibInput.defaultInputState.data[arg1.checkjstring()] = newEntry
                makeSureStringIsRegistered(arg1.checkjstring())
                return NIL
            }
        }
        (keyNames zip keysToSend).forEach {
            lib[it.first] = it.second
        }
        return "input"
    }
}
