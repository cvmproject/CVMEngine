package com.complover116.cvmproject.cvmengine.shared.lua;

import com.complover116.cvmproject.cvmengine.server.world.EntityServer;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;

public class LibEnts extends LuaLib {
    protected World world;
    public LibEnts(LuaSystem system, World world) {
        super(system);
        this.world = world;
    }
    @Override
    protected String setUp(LuaTable lib) {
        lib.set("Create", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue type) {
                return system.entitySystem.createEntity(type.checkjstring()).repr;
            }
        });
        lib.set("Spawn", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue ent) {
                world.spawnEntity((EntityServer) CoerceLuaToJava.coerce(ent.get("ENT"), Entity.class));
                return NIL;
            }
        });
        lib.set("GetAllActive", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                LuaTable result = new LuaTable();
                for (Entity ent : world.getAllActiveEntities()) {
                    result.set(ent.id, ent.repr);
                }
                return result;
            }
        });
        lib.set("GetInRadius", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue vec, LuaValue argrad) {
                float x = (float)vec.get("x").checkdouble();
                float y = (float)vec.get("y").checkdouble();
                float radius = (float) argrad.checkdouble();
                LuaTable result = new LuaTable();
                int i = 1;
                for (Entity ent : world.getEntitiesWithinRadius(x, y, radius)) {
                    result.set(i, ent.repr);
                    i++;
                }
                return result;
            }
        });
        lib.set("GetByType", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1) {
                String type = arg1.checkjstring();
                LuaTable result = new LuaTable();
                int counter = 1;
                for (Entity ent : world.getEntitiesByType(type)) {
                    result.set(counter, ent.repr);
                    counter++;
                }
                return result;
            }
        });
        lib.set("GetByID", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1) {
                int id = arg1.checkint();
                Entity ent = world.getEntityByID(id);
                if (ent != null) {
                    return ent.repr;
                }
                // Logger.log(LogType.WARN, "Attempted to get entity by unknown id "+id);
                return NIL;
            }
        });
        lib.set("LoadArea", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue vec, LuaValue argrad) {
                float x = (float)vec.get("x").checkdouble();
                float y = (float)vec.get("y").checkdouble();
                float radius = (float) argrad.checkdouble();
                world.loadArea(x, y, radius);
                return NIL;
            }
        });
        return "ents";
    }
}
