package com.complover116.cvmproject.cvmengine.shared.lua

import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.server.world.WorldServer
import com.complover116.cvmproject.cvmengine.shared.SoundEvent
import com.complover116.cvmproject.cvmengine.shared.SoundEventReceiver
import com.complover116.cvmproject.cvmengine.shared.world.World
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.TwoArgFunction

class LibSound(luaSystem: LuaSystem, val world: World?, val receiver: SoundEventReceiver): LuaLib(luaSystem) {
    var lastSoundID = 0
    override fun setUp(table: LuaTable): String {
//        table["PlayMusic"] = object : TwoArgFunction() {
//            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
//                engine.resourceManager.getMusic(arg1.checkjstring()).volume = clamp(arg2.checkdouble(), 0.0, 1.0).toFloat()
//                engine.resourceManager.getMusic(arg1.checkjstring()).play()
//                engine.resourceManager.getMusic(arg1.checkjstring()).isLooping = true
//                return NIL
//            }
//        }
//        table["GetMusicPos"] = object : OneArgFunction() {
//            override fun call(arg1: LuaValue): LuaValue {
//                return LuaValue.valueOf(engine.resourceManager.getMusic(arg1.checkjstring()).position.toDouble())
//            }
//        }
//        table["StopMusic"] = object : OneArgFunction() {
//            override fun call(arg1: LuaValue): LuaValue {
//                engine.resourceManager.getMusic(arg1.checkjstring()).stop()
//                return NIL
//            }
//        }
        table["PlaySound"] = object : TwoArgFunction() {
            override fun call(arg: LuaValue, arg2: LuaValue): LuaValue {
                createSoundEvent(arg.checkjstring(), arg2, isNew=true, persistent=false)
                return NIL
            }
        }
        table["CreateSound"] = object : TwoArgFunction() {
            override fun call(arg: LuaValue, arg2: LuaValue): LuaValue {
                return LuaValue.valueOf(createSoundEvent(arg.checkjstring(), arg2, isNew=true, persistent=true)!!)
            }
        }
        table["UpdateSound"] = object : TwoArgFunction() {
            override fun call(arg: LuaValue, arg2: LuaValue): LuaValue {
                return LuaValue.valueOf(createSoundEvent(null, arg2, isNew=false, persistent=true, persistentID = arg.checkint())!!)
            }
        }
        table["StopSound"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return LuaValue.valueOf(createSoundEvent(null, LuaTable(), persistent=true, isNew = false, persistentID = arg.checkint(), deleting = true)!!)
            }
        }
        return "sound"
    }

    fun createSoundEvent(soundName: String?, args: LuaValue, isNew: Boolean, persistent: Boolean, persistentID: Int? = null, deleting: Boolean = false): Int? {
        val time = if (args["time"].isnumber()) args["time"].checkdouble() else world!!.curTime
        val soundEvent = SoundEvent(time, isNew, deleting)
        if (persistent) soundEvent.persistence = true
        if (isNew) {
            soundEvent.soundName = soundName
        }
        if (args["volume"].isnumber()) {
            soundEvent.soundVolume = args["volume"].checkdouble().toFloat()
        }
        if (args["loudness"].isnumber()) {
            soundEvent.soundLoudness = args["loudness"].checkdouble().toFloat()
        }
        if (args["pitch"].isnumber()) {
            soundEvent.soundPitch = args["pitch"].checkdouble().toFloat()
        }
        if (args["pos"].istable()) {
            soundEvent.soundPos = Vector2(args["pos"]["x"].checkdouble().toFloat(), args["pos"]["y"].checkdouble().toFloat())
        }
        if (args["entity_id"].isnumber()) {
            soundEvent.targetEntityID = args["entity_id"].checkint()
        }
        if (isNew && persistent) {
            if (world is WorldServer)
                soundEvent.persistentID = ++lastSoundID
            else
                soundEvent.persistentID = --lastSoundID // Client persistent sounds have negative ids to avoid collisions
            soundEvent.persistence = true
        } else if (persistentID != null) {
            soundEvent.persistentID = persistentID
        }
        // And off it goes! On the server, this will be later processed and broadcast to relevant clients.
        // On the client, it will be sent directly to the local SoundManager
        receiver.receiveSoundEvent(soundEvent)
        return soundEvent.persistentID
    }
}
