package com.complover116.cvmproject.cvmengine.shared.lua;

import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;

public class LuaTypes {
    LuaFunction luaCreateVector;
    LuaFunction luaCreateColor;

    LuaTypes(LuaValue env) {
        luaCreateVector = env.get("Vector").checkfunction();
        luaCreateColor = env.get("Color").checkfunction();
    }

    public LuaValue createVector(double x, double y) {
        return luaCreateVector.call(LuaValue.valueOf(x), LuaValue.valueOf(y));
    }

    public LuaValue createColor(float r, float g, float b, float a) {
        return luaCreateColor.invoke(LuaValue.varargsOf(new LuaValue[]{
                LuaValue.valueOf(r),
                LuaValue.valueOf(g),
                LuaValue.valueOf(b),
                LuaValue.valueOf(a)
        })).arg1();
    }
}