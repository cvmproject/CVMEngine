package com.complover116.cvmproject.cvmengine.shared.lua;

import com.complover116.cvmproject.cvmengine.shared.CommandProcessor;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;

public class LibConsole extends LuaLib {
    private CommandProcessor processor;
    public LibConsole(LuaSystem system, CommandProcessor processor) {
        super(system);
        this.processor = processor;
    }
    boolean addCommand(String name, LuaFunction callback) {
        processor.addCommand(name, callback, system);
        return true;
    }
    boolean delCommand(String name) {

        return true;
    }
    public void runCommand(String name) {
        processor.process(name);
    }
    @Override
    protected String setUp(LuaTable lib) {
        LibConsole libj = this;

        lib.set("AddCommand", new TwoArgFunction() {
			@Override
			public LuaValue call(LuaValue arg1, LuaValue arg2) {
                String name = arg1.checkjstring();
                LuaFunction callback = arg2.checkfunction();
                return valueOf(libj.addCommand(name, callback));
			}

        });

        lib.set("RunCommand", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1) {
                String name = arg1.checkjstring();
                libj.runCommand(name);
                return NIL;
            }
        });

        lib.set("RemoveCommand", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg1) {
                String name = arg1.checkjstring();
                return valueOf(libj.delCommand(name));
			}

        });

        return "console";
    }
}
