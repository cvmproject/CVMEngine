package com.complover116.cvmproject.cvmengine.shared.lua;

import com.badlogic.gdx.Gdx;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.world.EntitySystem;
import org.luaj.vm2.*;
import org.luaj.vm2.compiler.LuaC;
import org.luaj.vm2.lib.*;
import org.luaj.vm2.lib.jse.JseBaseLib;
import org.luaj.vm2.lib.jse.JseMathLib;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.luaj.vm2.lib.jse.JseStringLib;

public abstract class LuaSystem {
    public Globals globals;
    public LibHook libHook;
    public LibConsole libConsole;
    public LibLuaLib libLuaLib;
    public LibBase libBase;
    public LibEnts libEnts;
    protected LibPlayer libPlayer;
    
    public LuaTypes luaTypes;

    public LibSound libSound;


    public EntitySystem entitySystem;
    public abstract boolean isServer();
    public abstract boolean isClient();
//    public final DebugLib debugLib;

    LuaFunction sandboxFunc;

    public boolean isFirstTimePredicted = true;

    protected LuaSystem() {
        Logger.log(Logger.LogType.INFO, "Lua System initializing");
        globals = new Globals();
        globals.load(new JseBaseLib());
        globals.load(new PackageLib());
        globals.load(new JseStringLib());
        globals.load(new JseMathLib());
        globals.load(new TableLib());
        globals.load(new CoroutineLib());
        globals.load(new DebugLib());
        LoadState.install(globals);
        LuaC.install(globals);
        globals.set("SERVER", LuaValue.valueOf(isServer()));
        globals.set("CLIENT", LuaValue.valueOf(isClient()));
        globals.set("print", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg1) {
                print(arg1.toString());
                return NIL;
			} 
        });
        libHook = new LibHook(this);
        libLuaLib = new LibLuaLib(this);
//        tileSystem = new TileSystem(this);

        LuaValue sandboxScript = globals.load(Gdx.files.internal("lua/sandboxing.lua").readString(), "CVM Sandboxing Script");
        sandboxScript.call();
        sandboxFunc = globals.get("CVM_RunInSandbox").checkfunction();
        Logger.log(Logger.LogType.INFO, "Base Lua System initialized");
    }

    public void initLuaTypes() {
        luaTypes = new LuaTypes(globals);
    }
    LuaTable makeSandbox(Globals globals, String name) {
        LuaTable sandboxGlobals = new LuaTable();
        LuaTable metaTable = new LuaTable();
        metaTable.set("__index", globals);
        sandboxGlobals.setmetatable(metaTable);
//        if (name != null) {
//            globals.set(name, sandboxGlobals);
//        }
        return sandboxGlobals;
    }
    void error(String message) {
        print("ERROR:" + message);
    }
    void print(String message) {
        if (isClient()) {
            Logger.log(Logger.LogType.LUA_CL, message);
        }
        if (isServer()) {
            Logger.log(Logger.LogType.LUA_SV, message);
        }
    }
    public boolean callFunctionSafe(LuaFunction func, Varargs args) {
        try {
            func.invoke(args);
            return true;
        } catch (LuaError e) {
            Logger.log(LogType.ERROR, "Error calling lua function:"+e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
    public void runSandboxed(String script, String name, String sandboxName) {
        LuaValue error = sandboxFunc.call(LuaValue.valueOf(sandboxName), LuaValue.valueOf(script), LuaValue.valueOf(name));
        if (error.isstring()) {
            throw new LuaError("Failed to load game code: "+error.checkjstring());
        }
    }
    public LuaTable runSBChunk(String script, String name, String sandboxName, String tableName, LuaTable existingTable) throws LuaError {
        Logger.log(LogType.DEBUG, String.format("Running LUA script %s", name));
        LuaTable resultTable;
        if (existingTable == null) {
            resultTable = new LuaTable();
        } else {
            resultTable = existingTable;
        }

        LuaValue sandbox = globals.get(sandboxName);
        if (sandbox.isnil()) {
            sandbox = new LuaTable();
            globals.set(sandboxName, sandbox);
            LuaTable metatable = new LuaTable();
            metatable.set("__index", globals);
            sandbox.setmetatable(metatable);
        }

        sandbox.set(tableName, resultTable);
        runSandboxed(script, name, sandboxName);
        sandbox.set(tableName, LuaValue.NIL);
        return resultTable;
    }

    public void runGlobal(String script, String name) {
        LuaValue fileChunk = globals.load(script, name);
        fileChunk.call();
    }
}
