package com.complover116.cvmproject.cvmengine.shared.lua;

import com.complover116.cvmproject.cvmengine.shared.Player;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

public class LibPlayer extends LuaLib {
    World world;
    public LibPlayer(LuaSystem system, World world) {
        super(system);
        this.world = world;
    }

    @Override
    protected String setUp(LuaTable lib) {
        lib.set("GetAllActive", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                LuaTable result = new LuaTable();
                int i = 1;
                for (Player ply : world.getAllActivePlayers()) {
                    result.set(i, ply.getRepr());
                    i++;
                }
                return result;
            }
        });
        lib.set("GetAll", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                LuaTable result = new LuaTable();
                int i = 1;
                for (Player ply : world.getAllPlayers()) {
                    result.set(i, ply.getRepr());
                    i++;
                }
                return result;
            }
        });
        lib.set("GetByID", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue id) {
                Player player = world.getPlayerByID(id.checkint());
                if (player == null){
                    return LuaValue.NIL;
                }
                return player.getRepr();
            }
        });
        lib.set("STATE_CONNECTING", Player.STATE_CONNECTING);
        lib.set("STATE_ACTIVE", Player.STATE_ACTIVE);
        lib.set("STATE_OFFLINE", Player.STATE_OFFLINE);
        lib.set("STATE_MISSING", Player.STATE_MISSING);
        return "player";
    }
}
