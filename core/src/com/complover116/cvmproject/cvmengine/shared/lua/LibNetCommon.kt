package com.complover116.cvmproject.cvmengine.shared.lua

import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType
import com.complover116.cvmproject.cvmengine.shared.net.NetStringRegistry
import com.complover116.cvmproject.cvmengine.shared.world.DataTable
import com.complover116.cvmproject.cvmengine.shared.world.DataTableEntry
import com.complover116.cvmproject.cvmengine.shared.world.World
import org.luaj.vm2.LuaError
import org.luaj.vm2.LuaFunction
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.TwoArgFunction

abstract class LibNetCommon protected constructor(system: LuaSystem) : LuaLib(system) {
    class NetMessage(val name: String?, val senderPlayerID: Int, val data: DataTable) {}
    var outgoingMessage: NetMessage? = null
    var incomingMessage: NetMessage? = null

    var dispatch = HashMap<String, LuaFunction>()

    override fun setUp(lib: LuaTable): String {
        lib["GetNetworkString"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val i = arg.checkint()
                return valueOf(getNetStringRegistry().intToString(i))
            }
        }
        lib["GetNetworkStringId"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val string = arg.checkjstring()
                return valueOf(getNetStringRegistry().stringToInt(string))
            }
        }

        lib["Receive"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                dispatch[arg1.checkjstring()] = arg2.checkfunction()
                return NIL
            }
        }

        lib["Write"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                if (outgoingMessage == null) {
                    LuaValue.error("Tried to write data before starting a message!")
                    return NIL
                }
                val dataPos = outgoingMessage!!.data.data.size + 1
                outgoingMessage!!.data.data[dataPos.toString()] = DataTableEntry.create(dataPos.toString(), arg1.checkjstring(), arg2, system.luaTypes)
                return NIL
            }
        }

        lib["Read"] = object : OneArgFunction() {
            override fun call(arg1: LuaValue): LuaValue {
                if (incomingMessage == null) {
                    LuaValue.error("Tried to read data when not receiving a message!")
                    return NIL
                }
                val dataTableEntry = incomingMessage!!.data.data[arg1.checkint().toString()]
                if (dataTableEntry == null) {
                    LuaValue.error("Attempted to read beyond message end!")
                    return NIL
                }
                return dataTableEntry.getAsLuaValue(getWorld(), system.luaTypes)
            }
        }
        return "net"
    }

    protected abstract fun getWorld(): World

    protected abstract fun getNetStringRegistry(): NetStringRegistry

    protected abstract fun getPlayerReprByID(playerID: Int): LuaValue

    fun handle(message: NetMessage): Boolean {
        incomingMessage = message
        if (dispatch.containsKey(message.name)) {
            try {
                val result = dispatch[message.name]!!.call(getPlayerReprByID(message.senderPlayerID))
                return result.isnil() || result.checkboolean()
            } catch (e: LuaError) {
                Logger.log(LogType.ERROR, "Hook for message type ${message.name} failed:" + e.message)
                e.printStackTrace()
            }
        } else {
            Logger.log(Logger.LogType.ERROR, "Ignoring unknown NetMessage type ${message.name}")
        }
        incomingMessage = null
        return true
    }
}
