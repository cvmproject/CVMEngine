package com.complover116.cvmproject.cvmengine.shared.lua;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.TwoArgFunction;

public class LibLuaLib extends LuaLib {
    LibLuaLib(LuaSystem system) {
        super(system);
    }
    @Override
    protected String setUp(LuaTable lib) {
        lib.set("SetGlobal", new TwoArgFunction() {
			@Override
			public LuaValue call(LuaValue arg1, LuaValue arg2) {
                String name = arg1.checkjstring();
                system.globals.set(name, arg2);
                return valueOf(true);
			}
            
        });
        return "lua";
    }
}
