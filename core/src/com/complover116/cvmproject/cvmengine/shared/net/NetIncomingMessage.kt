package com.complover116.cvmproject.cvmengine.shared.net

import java.nio.ByteBuffer

data class NetIncomingMessage(val data: ByteBuffer, val remote: NetRemoteDestination)
