package com.complover116.cvmproject.cvmengine.shared.net;

public class NetConstants {
    public static int MAX_PACKET_SIZE = 8192;

    /***
     * Isn't actually the MINIMAL packet size, rather the smallest guaranteed okay packet size
     */
    public static int MIN_PACKET_SIZE = 256;
    public static final float PACKET_RATE = 120f;
    public static final long PACKET_LOST_TIMEOUT = 500000000;
    public static final long TIMEOUT_HARD = 5000000000L;
    public static final long TIMEOUT_WARN = 1000000000L;
    public static final int PROTOCOL_ID = 228481337;
    public static final int INPUT_REPEAT_COUNT = 5;
    public static final int MAX_TILE_UPDATES_PER_TICK = 1000;
    public static final long SECTOR_REQUEST_TIMEOUT = 1000000000;
}
