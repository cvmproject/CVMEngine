package com.complover116.cvmproject.cvmengine.shared.net

import com.complover116.cvmproject.cvmengine.shared.ISteamAPIProxy
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.nio.ByteBuffer
import java.nio.channels.DatagramChannel

sealed class NetRemoteDestination {

    companion object {
        fun getUDPSimple(address: String, port: Int): SocketAddressDestination {
            return SocketAddressDestination(InetSocketAddress(address, port), null)
        }
    }

    abstract fun send(data: ByteBuffer)

    data class SteamIdDestination(val id: Long, val steamService: ISteamAPIProxy, val channel: Int) : NetRemoteDestination() {
        override fun send(data: ByteBuffer) {
            steamService.sendMessageToUser(id, data.array(), channel)
        }

        override fun equals(other: Any?): Boolean
                = this === other || (other is SteamIdDestination && id == other.id)

        override fun hashCode(): Int {
            return id.hashCode()
        }

        override fun toString(): String {
            return "STEAM:$id"
        }
    }

    data class SocketAddressDestination(val address: InetSocketAddress, var datagramChannel : DatagramChannel?) : NetRemoteDestination() {
        override fun send(data: ByteBuffer) {
            datagramChannel!!.send(data, address)
        }

        override fun equals(other: Any?): Boolean
                = this === other || (other is SocketAddressDestination && address == other.address)

        override fun hashCode(): Int {
            return address.hashCode()
        }

        override fun toString(): String {
            return address.toString()
        }
    }


}
