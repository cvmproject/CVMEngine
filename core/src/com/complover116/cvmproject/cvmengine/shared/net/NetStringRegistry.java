package com.complover116.cvmproject.cvmengine.shared.net;

import com.complover116.cvmproject.cvmengine.server.net.NetServer;
import com.complover116.cvmproject.cvmengine.shared.Logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class NetStringRegistry {
    final NetServer netServer;
    public NetStringRegistry(NetServer netServer) {
        this.netServer = netServer;
    }

    private volatile HashMap<Integer, String> registry = new HashMap<>();
    private volatile HashMap<String, Integer> registry_reverse = new HashMap<>();
    public int register(String string) {
        int id = registry.size();
        return update(string, id);
    }
    public int update(String string, int id) {
        synchronized(registry) {
            registry.put(id, string);
            registry_reverse.put(string, id);
        }
        return id;
    }
    public int stringToInt(String string) {
        synchronized (registry) {
            if (registry_reverse.containsKey(string)) {
//                Logger.log(Logger.LogType.WARN, "NSR ACCESSED "+string + " " + registry_reverse.get(string));
                return registry_reverse.get(string);
            } else {
                if (netServer != null) {
                    Logger.log(Logger.LogType.WARN, "Automatically registered netstring '"+string+"', consider pre-registering it with net.AddNetworkString(\""+string+"\")");
                    return netServer.addNetString(string);
                } else {
                    return -1;
                }
            }
        }
    }
    public String intToString(int i) {
        synchronized (registry) {
            return registry.get(i);
        }
    }
    public Set<Integer> getRegisteredIds() {
        synchronized (registry) {
            return new HashSet<Integer>(registry.keySet());
        }
    }

    public int count() {
        return registry.size();
    }
}