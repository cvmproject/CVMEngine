package com.complover116.cvmproject.cvmengine.shared.net;

import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import kotlin.Pair;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

public class NetConnection {
    

    public NetRemoteDestination remote;
    int localSequence = 0;
    int remoteSequence = 0;
    private volatile float ping = 0;
    long lastReceived = 0;
    private volatile float dropRate = 0;
    long dropBits1 = 0;
    long dropBits2 = 0;
    final HashMap<Integer, NetPacket> sentPackets = new HashMap<>();
    ArrayList<Integer> receivedPackets = new ArrayList<>();
    int recv_size = 0;
    final ConcurrentLinkedQueue<NetDataChunk> queue = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<NetDataChunk> low_priority_queue = new ConcurrentLinkedQueue<>();
    private float avgPacketOut = 0;
    private float avgPacketIn = 0;
    private boolean alive = true;
    public NetConnection(NetRemoteDestination remote) {
        this.remote = remote;
        lastReceived = System.nanoTime();
    }
    int booleansToInt(boolean[] arr) {
        int n = 0;
        for (boolean b : arr)
            n = (n << 1) | (b ? 1 : 0);
        return n;
    }
    boolean[] intToBooleans(int arg) {
        boolean out[] = new boolean[32];
        for (int i = 0; i < 32; i ++) {
            out[31-i] = ((1 << i) & arg) != 0;
        }
        return out;
    }
    public float getPing() {
        return ping;
    }
    public float getDropRate() {
        return dropRate;
    }
    public float getAvgPacketOut() {
        return avgPacketOut*NetConstants.PACKET_RATE;
    }
    public float getAvgPacketIn() {
        return avgPacketIn*NetConstants.PACKET_RATE;
    }
    public long getLastReceived() { return lastReceived; }
    int sendPacket(NetPacket packet) {
        boolean[] lastReceived = new boolean[32];
        for (int i = 0; i < 32; i ++)
            lastReceived[i] = false;
        for (int i = 0; i < receivedPackets.size(); i ++) {
            if (receivedPackets.get(i) <= remoteSequence - 32) {
                receivedPackets.remove(i);
                i--;
                continue;
            }
            lastReceived[remoteSequence - receivedPackets.get(i)] = true;
        }
        packet.sentTime = System.nanoTime();
        synchronized (sentPackets) {
            sentPackets.put(localSequence, packet);
        }
        ByteBuffer data = packet.toBytes(localSequence, remoteSequence, booleansToInt(lastReceived));
        remote.send(data);
        localSequence ++;
        return data.limit();
    }
    public void forceAckReceival() {
        synchronized (sentPackets) {
            sentPackets.clear();
        }
//        for (Map.Entry<Integer, NetPacket> entry : new HashSet<>(sentPackets.entrySet())) {
//            if (entry.getValue().getClass() == type) {
//                sentPackets.remove(entry.getKey());
//            }
//        }
    }
    void ackReceival(int id) {
        synchronized (sentPackets) {
            if (!sentPackets.containsKey(id)) return;
            sentPackets.get(id).onReceivedByRemote();
            dropBits2 = dropBits2 << 1;
            dropBits2 = dropBits2 | ((dropBits1 & (1 << 63)) >>> 63);
            dropBits1 = dropBits1 << 1;
            dropRate = (Long.bitCount(dropBits1) + Long.bitCount(dropBits2)) / 128f;
            ping += ((System.nanoTime() - sentPackets.get(id).sentTime) / 1000000f - ping) / 100;
            // Logger.log(LogType.DEBUG, "Conf "+id+"@"+socket.getLocalPort()+": "+Math.round(ping)+" ping");
            sentPackets.remove(id);
        }
    }
    public NetPacket receivePacket(ByteBuffer buffer, NetStringRegistry registry) throws IOException {
        lastReceived = System.nanoTime();
        recv_size += buffer.limit();
        NetPacket packet_in = new NetPacket(buffer, registry);
        packet_in.onReceive();
        int remote_seq = packet_in.localSequence;
        // Logger.log(LogType.DEBUG, "Recv "+remote_seq+"@"+socket.getLocalPort()+": "+Math.round(ping)+" ping");
        if (remote_seq > remoteSequence) {
            remoteSequence = remote_seq;
        }
        receivedPackets.add(remote_seq);
        int ack = packet_in.remoteSequence;
        // ackReceival(ack);
        boolean[] packedAcks = intToBooleans(packet_in.lastReceived);
        for (int i = 0; i < 32; i ++) {
            if (packedAcks[i]) {
                ackReceival(ack - i);
            }
        }
        return packet_in;
    }

    public boolean isAlive() { return alive; }

    public boolean update() throws IOException {
        if (System.nanoTime() - lastReceived > NetConstants.TIMEOUT_HARD) {
            Logger.log(LogType.WARN, "Connection to " + remote.toString() + " timed out");
            // socket.close();
            alive = false;
            return false;
        }
        synchronized (sentPackets) {
            for (int id : new HashSet<Integer>(sentPackets.keySet())) {
                if (System.nanoTime() - sentPackets.get(id).sentTime > NetConstants.PACKET_LOST_TIMEOUT) {
                    // Logger.log(LogType.WARN, "Packet with id "+id+" lost by "+remoteAddress);
                    sentPackets.get(id).onLostByRemote();
                    dropBits2 = dropBits2 << 1;
                    dropBits2 = dropBits2 | ((dropBits1 & (1 << 63)) >>> 63);
                    dropBits1 = dropBits1 << 1;
                    dropBits1 = dropBits1 | 1;
                    dropRate = (Long.bitCount(dropBits1) + Long.bitCount(dropBits2)) / 128f;
                    sentPackets.remove(id);
                }
            }
        }
        NetPacket packet = new NetPacket();
        synchronized (queue) {
            boolean packetHasSpace = true;
            while (packetHasSpace) {
                NetDataChunk chunk = queue.peek();
                if (chunk instanceof NetDataChunk.NetDataChunkImportant && sentPackets.size() > 15) {
                    packetHasSpace = false;
                    break;
                }
                if (chunk == null) break;
                if (!packet.addDataChunk(chunk)) {
                    packetHasSpace = false;
                } else {
                    queue.poll();
                }
            }

            while (packetHasSpace && !low_priority_queue.isEmpty()) {
                NetDataChunk chunk = low_priority_queue.peek();
                if (!packet.addDataChunk(chunk)) {
                    break;
                } else {
                    low_priority_queue.poll();
                }
                if (packet.size > NetConstants.MIN_PACKET_SIZE) {
                    break;
                }
            }
        }
        int sent_size = sendPacket(packet);
        avgPacketOut += (sent_size - avgPacketOut)/NetConstants.PACKET_RATE*2;
        avgPacketIn += (recv_size - avgPacketIn)/NetConstants.PACKET_RATE*2;
        recv_size = 0;
        return true;
    }
}