package com.complover116.cvmproject.cvmengine.shared.net;

import com.complover116.cvmproject.cvmengine.client.PlayerClient;
import com.complover116.cvmproject.cvmengine.server.PlayerServer;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.SoundEvent;
import com.complover116.cvmproject.cvmengine.shared.lua.LibNetCommon;
import com.complover116.cvmproject.cvmengine.shared.world.DataTable;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.EntityState;
import com.complover116.cvmproject.cvmengine.shared.world.World;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

public abstract class NetDataChunk {
    public void onReceivedByRemote() {};
    public void onLostByRemote() {};
    abstract void onReceive();
    abstract byte[] encode();
    abstract void decode(byte[] data);
    private byte[] data;
    byte[] getData() {
        if (data == null) {
            this.data = encode();
        }
        return data;
    };
    abstract byte getID();
    public boolean enqueue(NetConnection connection, boolean highPriority) {
        synchronized(connection.queue) {
            if (highPriority)
                connection.queue.add(this);
            else
                connection.low_priority_queue.add(this);
        }
        return true;
    }

    public static final byte CHUNK_TERMINAL_COMMAND = 0;
    public static final byte CHUNK_ENTITY_NEW = 1;
    public static final byte CHUNK_ENTITY_UPDATE = 2;
    public static final byte CHUNK_ENTITY_DEATH = 3;
    public static final byte CHUNK_NETSTRING = 4;
    public static final byte CHUNK_WORLD_UPDATE = 5;
    public static final byte CHUNK_GAME_INFO = 6;
    public static final byte CHUNK_PLAYER_INPUT = 7;
    public static final byte CHUNK_PLAYER_INFO = 8;
    public static final byte CHUNK_TILE_UPDATE = 9;
    public static final byte CHUNK_SECTOR_REQUEST = 10;
    public static final byte CHUNK_PLAYER_OPTIONS = 11;
    public static final byte CHUNK_SOUND_EVENT = 12;
    public static final byte CHUNK_SERVER_REQUEST = 13;
    public static final byte CHUNK_SERVER_CONNECTION_RESPONSE = 14;
    public static final byte CHUNK_SERVER_MODULE_INFO = 15;
    public static final byte CHUNK_MODULE_DETAILS_REQUEST = 16;
    public static final byte CHUNK_SERVER_MODULE_DETAILS = 17;
    public static final byte CHUNK_FILE_REQUEST = 18;
    public static final byte CHUNK_SERVER_FILE_DATA = 19;
    public static final byte CHUNK_NET_MESSAGE = 20;

    public static final byte CHUNK_CONNECTION_REQUEST = 21;


    public abstract static class NetDataChunkImportant extends NetDataChunk {
        NetConnection target;

        boolean isHighPriority;
        byte[] rawReceivedData;
        NetDataChunkImportant(NetConnection target) {
            this.target = target;
        }
        NetDataChunkImportant() {}
        public void reDecode() {
            decode(rawReceivedData);
        }

        @Override
        public boolean enqueue(NetConnection connection, boolean highPriority) {
            super.enqueue(connection, highPriority);
            this.target = connection;
            this.isHighPriority = highPriority;
            return true;
        }

        @Override
        public void onLostByRemote() {
            if (target == null) {
                Logger.log(LogType.ERROR, "Attempted to resend a chunk with no destination");
                return;
            }
            Logger.log(LogType.DEBUG, "Important chunk (type "+this.getID()+") missed by remote, requeueing");
            enqueue(target, isHighPriority);
        }

        @Override
        public void onReceivedByRemote() {
            // Logger.log(LogType.DEBUG, "Important chunk (type "+this.getID()+") received by remote successfully");
        }
    }

    public static class ChunkNewEntity extends NetDataChunkImportant {
        public double creationTime;
        public int type;
        public int id;
        public EntityState initialState;
        public NetStringRegistry registry;
        /**
         * True if the entity was just spawned, false if the entity already existed in the world but wasn't loaded
         */
        public boolean newlySpawned;
        // I thought about making it a separate chunk in case it's a predicted creation, but it's just 4 bytes, who cares
        // Entity updates send 8 bytes every tick just for the position
        // It's simply set to -1 if it's not a predicted creation
        public int creationID;
        public ChunkNewEntity(NetConnection target, double creationTime, Entity ent, NetStringRegistry registry, int creationID, boolean newlySpawned) {
            super(target);
            this.id = ent.id;
            this.type = registry.stringToInt(ent.type);
            this.creationTime = creationTime;
            this.creationID = creationID;
            this.newlySpawned = newlySpawned;
            this.initialState = ent.curState.copy();
            this.registry = registry;
        }

        public ChunkNewEntity(byte data[], NetStringRegistry registry) {
            this.registry = registry;
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(4+4+8+4+1+initialState.getSize());
            dataBuf.putInt(id);
            dataBuf.putInt(type);
            dataBuf.putDouble(creationTime);
            dataBuf.putInt(creationID);
            dataBuf.put(newlySpawned ? (byte) 1 : (byte) 0);
            initialState.encode(dataBuf, registry);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            id = dataBuf.getInt();
            type = dataBuf.getInt();
            creationTime = dataBuf.getDouble();
            creationID = dataBuf.getInt();
            newlySpawned = dataBuf.get() == 1;
            initialState = EntityState.Companion.decode(dataBuf, registry);
        }

        @Override
        byte getID() {
            return CHUNK_ENTITY_NEW;
        }
    }

    public static class ChunkSoundEvent extends NetDataChunkImportant {
        public SoundEvent event;
        private NetStringRegistry registry;
        public ChunkSoundEvent(NetConnection target, SoundEvent event, NetStringRegistry registry) {
            super(target);
            this.event = event;
            this.registry = registry;
        }

        public ChunkSoundEvent(byte data[], NetStringRegistry registry) {
            this.registry = registry;
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            return event.encode(registry);
        }

        @Override
        void decode(byte[] data) {
            event = SoundEvent.decode(data, registry);
        }

        @Override
        byte getID() {
            return CHUNK_SOUND_EVENT;
        }
    }

    public static class ChunkEntityDeath extends NetDataChunkImportant {
        public double removalTime;
        public int id;

        public ChunkEntityDeath(NetConnection target, double removalTime, int id) {
            super(target);
            this.id = id;
            this.removalTime = removalTime;
        }

        public ChunkEntityDeath(byte data[]) {
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(12);
            dataBuf.putInt(id);
            dataBuf.putDouble(removalTime);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            id = dataBuf.getInt();
            removalTime = dataBuf.getDouble();
        }

        @Override
        byte getID() {
            return CHUNK_ENTITY_DEATH;
        }
    }

    public static class ChunkGameInfo extends NetDataChunkImportant {
        public int playerID;
        public int playerDataEntityID;
        public double curTime;
        public boolean isEditing;

        public int activeEntityCount;
        public ChunkGameInfo(NetConnection target, int playerID, int playerDataEntityID, double curTime, boolean isEditing, int activeEntityCount) {
            super(target);
            this.playerID = playerID;
            this.playerDataEntityID = playerDataEntityID;
            this.curTime = curTime;
            this.isEditing = isEditing;
            this.activeEntityCount = activeEntityCount;
        }

        public ChunkGameInfo(byte data[]) {
            decode(data);
        }

        @Override
        void onReceive() {
            Logger.log(LogType.INFO, "Received gameinfo: player id:"+playerID+", time:"+curTime);
        }

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(17+4);
            dataBuf.putInt(playerID);
            dataBuf.putInt(playerDataEntityID);
            dataBuf.putDouble(curTime);
            dataBuf.put(isEditing ? (byte)1: 0);
            dataBuf.putInt(activeEntityCount);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            playerID = dataBuf.getInt();
            playerDataEntityID = dataBuf.getInt();
            curTime = dataBuf.getDouble();
            isEditing = dataBuf.get() == 1;
            activeEntityCount = dataBuf.getInt();
        }

        @Override
        byte getID() {
            return CHUNK_GAME_INFO;
        }
    }

    public static class ChunkPlayerInput extends NetDataChunk {
        public DataTable playerInput;
        public int playerID;
        private final NetStringRegistry registry;
        public ChunkPlayerInput(PlayerClient player, NetStringRegistry registry) {
            playerInput = player.input.copy();
            this.playerID = player.id;
            this.registry = registry;
        }

        public ChunkPlayerInput(byte data[], NetStringRegistry registry) {
            this.registry = registry;
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(8+4+playerInput.getSize());
            playerInput.encode(dataBuf, registry);
            dataBuf.putInt(playerID);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            playerInput = DataTable.Companion.decode(dataBuf, registry);
            playerID = dataBuf.getInt();
        }

        @Override
        byte getID() {
            return CHUNK_PLAYER_INPUT;
        }
    }

    public static class ChunkSectorRequest extends NetDataChunk {
        public int sectorX;
        public int sectorY;
        public boolean shouldTransmit;

        public ChunkSectorRequest(int sectorX, int sectorY, boolean shouldTransmit) {
            this.sectorX = sectorX;
            this.sectorY = sectorY;
            this.shouldTransmit = shouldTransmit;
        }

        public ChunkSectorRequest(byte[] data) {
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(9);
            dataBuf.putInt(sectorX);
            dataBuf.putInt(sectorY);
            dataBuf.put((byte) (shouldTransmit ? 1 : 0));
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            sectorX = dataBuf.getInt();
            sectorY = dataBuf.getInt();
            shouldTransmit = dataBuf.get() != 0;
        }

        @Override
        byte getID() {
            return CHUNK_SECTOR_REQUEST;
        }
    }

    public static class ChunkWorldUpdate extends NetDataChunk {
        public static final int MAX_ENTITY_UPDATES = 50;
        public double time;
        public PlayerServer player;
        public NetStringRegistry registry;
        public LinkedList<EntityUpdate> entityUpdates = new LinkedList<>();
        public long lastProcessedKeyState;
        public static class EntityUpdate {
            public int id;
            public EntityState deltaState;
            public EntityUpdate(int id, EntityState deltaState) {
                this.id = id;
                this.deltaState = deltaState;
            }

            int getSize() {
                int size = 4; // Entity ID
                size += deltaState.getSize();
                return size;
            }
        }
        public ChunkWorldUpdate(World world, LinkedList<EntityUpdate> updates, PlayerServer player, NetStringRegistry registry) {
            this.player = player;
            this.time = world.curTime;
            this.entityUpdates = updates;
            this.registry = registry;
            this.lastProcessedKeyState = player.getLastProcessedKeystateId();
        }

        public ChunkWorldUpdate(byte data[], NetStringRegistry registry) {
            this.registry = registry;
            decode(data);
        }

        @Override
        public void onReceivedByRemote() {
            synchronized (player) {
                for (EntityUpdate update : entityUpdates) {
                    player.updateLastConfirmedEntityState(update.id, update.deltaState);
                }
            }
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            int size = 8+4+8;
            for (EntityUpdate update : entityUpdates) {
                size += update.getSize();
//                size += 2; // Update size
            }
            ByteBuffer dataBuf = ByteBuffer.allocate(size);
            dataBuf.putDouble(time);
            dataBuf.putInt(entityUpdates.size());
            for (EntityUpdate update : entityUpdates) {
                dataBuf.putInt(update.id);
                update.deltaState.encode(dataBuf, registry);
            }
            dataBuf.putLong(lastProcessedKeyState);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            time = dataBuf.getDouble();
            int updateCount = dataBuf.getInt();
            for (int i = 0; i < updateCount; i ++) {
                int entID = dataBuf.getInt();
                EntityState deltaState = EntityState.Companion.decode(dataBuf, registry);
                EntityUpdate update = new EntityUpdate(
                        entID,
                        deltaState
                );
                entityUpdates.add(update);
            }
            lastProcessedKeyState = dataBuf.getLong();
        }

        @Override
        byte getID() {
            return CHUNK_WORLD_UPDATE;
        }
    }

    public static class ChunkPlayerInfo extends NetDataChunkImportant {
        public int id;
        public int dataEntityID;
        public float ping;
        public String username;
        public byte state;
        public boolean isAdmin;

        public ChunkPlayerInfo(int id, int dataEntityID, float ping, String username, byte state, boolean isAdmin) {
            this.id = id;
            this.dataEntityID = dataEntityID;
            this.ping = ping;
            this.username = username;
            this.state = state;
            this.isAdmin = isAdmin;
        }

        public ChunkPlayerInfo(byte data[]) {
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(4 + 4 + 4 + 4 + username.length() + 1 + 1);
            dataBuf.putInt(id);
            dataBuf.putInt(dataEntityID);
            dataBuf.putFloat(ping);
            dataBuf.putInt(username.length());
            byte[] typeBytes = username.getBytes();
            dataBuf.put(typeBytes);
            dataBuf.put(state);
            dataBuf.put((byte) (isAdmin ? 1 : 0));
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            id = dataBuf.getInt();
            dataEntityID = dataBuf.getInt();
            ping = dataBuf.getFloat();
            int usernameLength = dataBuf.getInt();
            byte[] usernameBytes = new byte[usernameLength];
            dataBuf.get(usernameBytes);
            username = new String(usernameBytes);
            state = dataBuf.get();
            isAdmin = dataBuf.get() == 1;
        }

        @Override
        byte getID() { return CHUNK_PLAYER_INFO; }
    }

    /***
     * Player options that can change during a single connection
     */
    public static class ChunkPlayerOptions extends NetDataChunkImportant {
        public float requestedInterpolationDelay;

        public ChunkPlayerOptions(float requestedInterpolationDelay) {
            this.requestedInterpolationDelay = requestedInterpolationDelay;
        }

        public ChunkPlayerOptions(byte[] data) {
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(4);
            dataBuf.putFloat(requestedInterpolationDelay);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            this.requestedInterpolationDelay = dataBuf.getFloat();
        }

        @Override
        byte getID() { return CHUNK_PLAYER_OPTIONS; }
    }

    public static class ChunkNetString extends NetDataChunkImportant {
        public String string;
        public int id;
        public ChunkNetString(byte data[]) {
            decode(data);
        }
        public ChunkNetString(NetConnection target, int id, String type) {
            super(target);
            this.id = id;
            this.string = type;
        }

        @Override
        byte getID() {
            return CHUNK_NETSTRING;
        }
        @Override
        void onReceive() {}

        @Override
        public void onReceivedByRemote() {
//            Logger.log(LogType.INFO, "Client confirmed receival of '" + this.string + "'");
        }

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(4 + 4 + string.length());
            dataBuf.putInt(id);
            dataBuf.putInt(string.length());
            byte typeBytes[] = string.getBytes();
            dataBuf.put(typeBytes);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            id = dataBuf.getInt();
            int typeLength = dataBuf.getInt();
            byte typeBytes[] = new byte[typeLength];
            dataBuf.get(typeBytes);
            string = new String(typeBytes);
        }
    }

    public static class ChunkConnectionRequest extends NetDataChunkImportant {
        public String permanentName;
        public String username;
        public int id;
        public ChunkConnectionRequest(byte data[]) {
            decode(data);
        }
        public ChunkConnectionRequest(NetConnection target, String permanentName, String username) {
            super(target);
            this.permanentName = permanentName;
            this.username = username;
        }

        @Override
        byte getID() {
            return CHUNK_CONNECTION_REQUEST;
        }
        @Override
        void onReceive() {}

        @Override
        public void onReceivedByRemote() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(4 + permanentName.length() + 4 + username.length());
            dataBuf.putInt(permanentName.length());
            byte[] permanentNameBytes = permanentName.getBytes();
            dataBuf.put(permanentNameBytes);
            dataBuf.putInt(username.length());
            byte[] usernameBytes = username.getBytes();
            dataBuf.put(usernameBytes);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            int permanentNameLength = dataBuf.getInt();
            byte[] permanentNameBytes = new byte[permanentNameLength];
            dataBuf.get(permanentNameBytes);
            permanentName = new String(permanentNameBytes);

            int usernameLength = dataBuf.getInt();
            byte[] usernameBytes = new byte[usernameLength];
            dataBuf.get(usernameBytes);
            username = new String(usernameBytes);
        }
    }
    public static class ChunkServerRequest extends NetDataChunkImportant {
        public static final byte STATE_INFO = 1; // Server will NOT add the client to the player list
//        public static final byte STATE_BEGIN_CONNECTION = 2; // Server adds the client to the player list and opens a persistent connection
        public static final byte STATE_REQUEST_NETSTRINGS = 3; // Server sends all existing netstrings and begins sending any new ones
        public static final byte STATE_REQUEST_SNAPSHOT = 4; // Server sends all existing entities and begins sending any new ones
        public static final byte STATE_FINISH_CONNECTION = 5; // Server marks the player as connected, runs onConnect hooks and begins sending world updates

        public byte requestType;
        public ChunkServerRequest(byte data[]) {
            decode(data);
        }
        public ChunkServerRequest(NetConnection target, byte requestType) {
            super(target);
            this.requestType = requestType;
        }

        @Override
        byte getID() {
            return CHUNK_SERVER_REQUEST;
        }
        @Override
        void onReceive() {}

        @Override
        public void onReceivedByRemote() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(1);
            dataBuf.put(requestType);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            requestType = dataBuf.get();
        }
    }

    public static class ChunkServerModuleInfo extends NetDataChunk {
        public static class ModuleInfo {
            public final String moduleID;
            public final String moduleHash;
            public ModuleInfo(String moduleID, String moduleHash) {
                this.moduleID = moduleID;
                this.moduleHash = moduleHash;
            }
        }
        public List<ModuleInfo> moduleInfos;
        @Override
        void onReceive() {}

        public ChunkServerModuleInfo(byte data[]) {
            decode(data);
        }

        public ChunkServerModuleInfo(List<ModuleInfo> moduleInfos) {
            this.moduleInfos = moduleInfos;
        }

        @Override
        byte[] encode() {
            int size = 4;
            for (ModuleInfo moduleInfo : moduleInfos) {
                size += 8;
                size += moduleInfo.moduleHash.length();
                size += moduleInfo.moduleID.length();
            }
            ByteBuffer dataBuf = ByteBuffer.allocate(size);
            dataBuf.putInt(moduleInfos.size());
            for (ModuleInfo moduleInfo : moduleInfos) {
                dataBuf.putInt(moduleInfo.moduleID.length());
                dataBuf.put(moduleInfo.moduleID.getBytes());
                dataBuf.putInt(moduleInfo.moduleHash.length());
                dataBuf.put(moduleInfo.moduleHash.getBytes());
            }
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            int moduleCount = dataBuf.getInt();
            moduleInfos = new LinkedList<>();
            for (int i = 0; i < moduleCount; i ++) {
                byte[] moduleName = new byte[dataBuf.getInt()];
                dataBuf.get(moduleName);
                byte[] moduleHash = new byte[dataBuf.getInt()];
                dataBuf.get(moduleHash);
                moduleInfos.add(new ModuleInfo(new String(moduleName), new String(moduleHash)));
            }
        }

        @Override
        byte getID() {
            return CHUNK_SERVER_MODULE_INFO;
        }
    }

    public static class ChunkServerConnectionResponse extends NetDataChunkImportant {
        public static final byte STATE_CONNECTION_BEGUN = 1; // Server has added the client to the player list and opens a persistent connection
        public static final byte STATE_CONNECTION_FINISHED = 2; // Server marked the player as connected, the client can switch to RunningScreen now

        public static final byte STATE_RELOADING = 3; // Server is restarting (changing world, for example) and wants the client to reconnect

        public int netStringCount;
        public int activeEntityCount;
        public byte responseType;
        public ChunkServerConnectionResponse(byte data[]) {
            decode(data);
        }
        public ChunkServerConnectionResponse(NetConnection target, byte responseType, int netStringCount, int activeEntityCount) {
            super(target);
            this.responseType = responseType;
            this.netStringCount = netStringCount;
            this.activeEntityCount = activeEntityCount;
        }

        @Override
        byte getID() {
            return CHUNK_SERVER_CONNECTION_RESPONSE;
        }
        @Override
        void onReceive() {}

        @Override
        public void onReceivedByRemote() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(1 + 4 + 4);
            dataBuf.put(responseType);
            dataBuf.putInt(netStringCount);
            dataBuf.putInt(activeEntityCount);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            responseType = dataBuf.get();
            netStringCount = dataBuf.getInt();
            activeEntityCount = dataBuf.getInt();
        }
    }

    public static class ChunkModuleDetailsRequest extends NetDataChunkImportant {
        public String moduleID;
        @Override
        void onReceive() {}

        public ChunkModuleDetailsRequest(byte data[]) {
            decode(data);
        }

        public ChunkModuleDetailsRequest(NetConnection connection, String moduleID) {
            super(connection);
            this.moduleID = moduleID;
        }

        @Override
        byte[] encode() {
            int size = 4 + moduleID.length();
            ByteBuffer dataBuf = ByteBuffer.allocate(size);
            dataBuf.putInt(moduleID.length());
            dataBuf.put(moduleID.getBytes());
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            byte[] moduleNameBytes = new byte[dataBuf.getInt()];
            dataBuf.get(moduleNameBytes);
            moduleID = new String(moduleNameBytes);
        }

        @Override
        byte getID() {
            return CHUNK_MODULE_DETAILS_REQUEST;
        }
    }

    public static class ChunkServerModuleDetails extends NetDataChunkImportant {
        public String moduleName;
        public String filePath;
        public String fileHash;
        public int fileCount;
        @Override
        void onReceive() {}

        public ChunkServerModuleDetails(byte data[]) {
            decode(data);
        }

        public ChunkServerModuleDetails(NetConnection connection, String moduleName, String filePath, String fileHash, int fileCount) {
            super(connection);
            this.moduleName = moduleName;
            this.filePath = filePath;
            this.fileHash = fileHash;
            this.fileCount = fileCount;
        }

        @Override
        byte[] encode() {
            int size = 16 + moduleName.length() + filePath.length() + fileHash.length();
            ByteBuffer dataBuf = ByteBuffer.allocate(size);
            dataBuf.putInt(moduleName.length());
            dataBuf.put(moduleName.getBytes());
            dataBuf.putInt(filePath.length());
            dataBuf.put(filePath.getBytes());
            dataBuf.putInt(fileHash.length());
            dataBuf.put(fileHash.getBytes());
            dataBuf.putInt(fileCount);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            byte[] moduleNameBytes = new byte[dataBuf.getInt()];
            dataBuf.get(moduleNameBytes);
            moduleName = new String(moduleNameBytes);
            byte[] filePathBytes = new byte[dataBuf.getInt()];
            dataBuf.get(filePathBytes);
            filePath = new String(filePathBytes);
            byte[] fileHashBytes = new byte[dataBuf.getInt()];
            dataBuf.get(fileHashBytes);
            fileHash = new String(fileHashBytes);
            fileCount = dataBuf.getInt();
        }

        @Override
        byte getID() {
            return CHUNK_SERVER_MODULE_DETAILS;
        }
    }

    public static class ChunkFileRequest extends NetDataChunkImportant {
        public String requestedFilePath;
        public String targetModule;
        @Override
        void onReceive() {}

        public ChunkFileRequest(byte data[]) {
            decode(data);
        }

        public ChunkFileRequest(NetConnection connection, String requestedFilePath, String targetModule) {
            super(connection);
            this.requestedFilePath = requestedFilePath;
            this.targetModule = targetModule;
        }

        @Override
        byte[] encode() {
            int size = 8 + requestedFilePath.length() + targetModule.length();
            ByteBuffer dataBuf = ByteBuffer.allocate(size);
            dataBuf.putInt(requestedFilePath.length());
            dataBuf.put(requestedFilePath.getBytes());
            dataBuf.putInt(targetModule.length());
            dataBuf.put(targetModule.getBytes());
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            byte[] requestedFilePathBytes = new byte[dataBuf.getInt()];
            dataBuf.get(requestedFilePathBytes);
            requestedFilePath = new String(requestedFilePathBytes);
            byte[] targetModuleBytes = new byte[dataBuf.getInt()];
            dataBuf.get(targetModuleBytes);
            targetModule = new String(targetModuleBytes);
        }

        @Override
        byte getID() {
            return CHUNK_FILE_REQUEST;
        }
    }

    public static class ChunkServerFileData extends NetDataChunkImportant {
        public String filePath;
        public int partID;
        public int totalParts;
        public byte[] filePartData;
        @Override
        void onReceive() {}

        public ChunkServerFileData(byte data[]) {
            decode(data);
        }

        public ChunkServerFileData(NetConnection connection, String filePath, int partID, int totalParts, byte[] filePartData) {
            super(connection);
            this.filePath = filePath;
            this.partID = partID;
            this.totalParts = totalParts;
            this.filePartData = filePartData;
        }

        @Override
        byte[] encode() {
            int size = 16 + filePath.length() + filePartData.length;
            ByteBuffer dataBuf = ByteBuffer.allocate(size);
            dataBuf.putInt(filePath.length());
            dataBuf.put(filePath.getBytes());
            dataBuf.putInt(partID);
            dataBuf.putInt(totalParts);
            dataBuf.putInt(filePartData.length);
            dataBuf.put(filePartData);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            byte[] requestedFilePathBytes = new byte[dataBuf.getInt()];
            dataBuf.get(requestedFilePathBytes);
            filePath = new String(requestedFilePathBytes);
            partID = dataBuf.getInt();
            totalParts = dataBuf.getInt();
            filePartData = new byte[dataBuf.getInt()];
            dataBuf.get(filePartData);
        }

        @Override
        byte getID() {
            return CHUNK_SERVER_FILE_DATA;
        }
    }

    public static class ChunkNetMessage extends NetDataChunkImportant {
        public LibNetCommon.NetMessage message;
        public NetStringRegistry registry;
        public ChunkNetMessage(NetConnection target, LibNetCommon.NetMessage message, NetStringRegistry registry) {
            super(target);
            this.registry = registry;
            this.message = message;
        }

        public ChunkNetMessage(byte data[], NetStringRegistry registry) {
            this.registry = registry;
            decode(data);
        }

        @Override
        void onReceive() {}

        @Override
        byte[] encode() {
            ByteBuffer dataBuf = ByteBuffer.allocate(8+4+4+message.getData().getSize());
            dataBuf.putInt(registry.stringToInt(message.getName()));
            dataBuf.putInt(message.getSenderPlayerID());
            message.getData().encode(dataBuf, registry);
            return dataBuf.array();
        }

        @Override
        void decode(byte[] data) {
            ByteBuffer dataBuf = ByteBuffer.wrap(data);
            String name = registry.intToString(dataBuf.getInt());
            int senderPlayerID = dataBuf.getInt();
            DataTable dataTable = DataTable.Companion.decode(dataBuf, registry);
            if (dataTable == null) {
                message = null;
            } else {
                message = new LibNetCommon.NetMessage(name, senderPlayerID, dataTable);
            }
        }

        @Override
        byte getID() {
            return CHUNK_NET_MESSAGE;
        }
    }
}
