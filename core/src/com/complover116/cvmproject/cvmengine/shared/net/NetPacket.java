package com.complover116.cvmproject.cvmengine.shared.net;

import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class NetPacket {
    long sentTime;
    public ArrayList<NetDataChunk> dataChunks = new ArrayList<>();
    int size = 18; // HEADER
    int localSequence;
    int remoteSequence;
    int lastReceived;
    void onReceivedByRemote() {
        for (NetDataChunk chunk : dataChunks) 
            chunk.onReceivedByRemote();
    }
    void onLostByRemote() {
        for (NetDataChunk chunk : dataChunks)
            chunk.onLostByRemote();
    }
    public boolean addDataChunk(NetDataChunk chunk) {
        if (chunk.getData().length + 3 + size > NetConstants.MAX_PACKET_SIZE)
            return false;
        dataChunks.add(chunk);
        size += 3 + chunk.getData().length;
        return true;
    }
    public ByteBuffer toBytes(int localSequence, int remoteSequence, int lastReceived) {
        ByteBuffer buffer_out = ByteBuffer.allocate(size);
        buffer_out.putInt(NetConstants.PROTOCOL_ID);
        buffer_out.putInt(localSequence);
        buffer_out.putInt(remoteSequence);
        buffer_out.putInt(lastReceived);
        buffer_out.putShort((short)dataChunks.size());
        for (NetDataChunk chunk : dataChunks) {
            buffer_out.put(chunk.getID());
            buffer_out.putShort((short)chunk.getData().length);
            buffer_out.put(chunk.getData());
        }
        buffer_out.flip();
        return buffer_out;
    }
    public NetPacket() {}
    public NetPacket(ByteBuffer buffer_in, NetStringRegistry registry) {
        buffer_in.getInt(); // protocol id
        localSequence = buffer_in.getInt();
        remoteSequence = buffer_in.getInt();
        lastReceived = buffer_in.getInt();
        short chunkCount = buffer_in.getShort();
        for (short i = 0; i < chunkCount; i ++) {
            byte chunkType = buffer_in.get();
            short chunkLength = buffer_in.getShort();
            byte[] chunkData = new byte[chunkLength];
            try {
                buffer_in.get(chunkData);
            } catch (BufferUnderflowException e) {
                Logger.log(LogType.ERROR, "BUFFER UNDERFLOW DETECTED");
                Logger.log(LogType.ERROR, "Expected "+chunkCount+" chunks, underflowed on chunk "+i+" of type "+chunkType);
                Logger.log(LogType.ERROR, "Was supposed to be of length "+chunkLength);
                Logger.log(LogType.ERROR, "Packet dump: " + buffer_in);
                Logger.fatalError(e);
            }
            NetDataChunk newChunk;
            switch (chunkType) {
                case NetDataChunk.CHUNK_NETSTRING:
                    newChunk = new NetDataChunk.ChunkNetString(chunkData);
                    break;
                case NetDataChunk.CHUNK_ENTITY_NEW:
                    newChunk = new NetDataChunk.ChunkNewEntity(chunkData, registry);
                    break;
                case NetDataChunk.CHUNK_ENTITY_DEATH:
                    newChunk = new NetDataChunk.ChunkEntityDeath(chunkData);
                    break;
                case NetDataChunk.CHUNK_WORLD_UPDATE:
                    newChunk = new NetDataChunk.ChunkWorldUpdate(chunkData, registry);
                    break;
                case NetDataChunk.CHUNK_GAME_INFO:
                    newChunk = new NetDataChunk.ChunkGameInfo(chunkData);
                    break;
                case NetDataChunk.CHUNK_PLAYER_INPUT:
                    newChunk = new NetDataChunk.ChunkPlayerInput(chunkData, registry);
                    break;
                case NetDataChunk.CHUNK_PLAYER_INFO:
                    newChunk = new NetDataChunk.ChunkPlayerInfo(chunkData);
                    break;
                case NetDataChunk.CHUNK_SECTOR_REQUEST:
                    newChunk = new NetDataChunk.ChunkSectorRequest(chunkData);
                    break;
                case NetDataChunk.CHUNK_PLAYER_OPTIONS:
                    newChunk = new NetDataChunk.ChunkPlayerOptions(chunkData);
                    break;
                case NetDataChunk.CHUNK_SOUND_EVENT:
                    newChunk = new NetDataChunk.ChunkSoundEvent(chunkData, registry);
                    break;
                case NetDataChunk.CHUNK_SERVER_REQUEST:
                    newChunk = new NetDataChunk.ChunkServerRequest(chunkData);
                    break;
                case NetDataChunk.CHUNK_SERVER_CONNECTION_RESPONSE:
                    newChunk = new NetDataChunk.ChunkServerConnectionResponse(chunkData);
                    break;
                case NetDataChunk.CHUNK_SERVER_MODULE_INFO:
                    newChunk = new NetDataChunk.ChunkServerModuleInfo(chunkData);
                    break;
                case NetDataChunk.CHUNK_MODULE_DETAILS_REQUEST:
                    newChunk = new NetDataChunk.ChunkModuleDetailsRequest(chunkData);
                    break;
                case NetDataChunk.CHUNK_SERVER_MODULE_DETAILS:
                    newChunk = new NetDataChunk.ChunkServerModuleDetails(chunkData);
                    break;
                case NetDataChunk.CHUNK_FILE_REQUEST:
                    newChunk = new NetDataChunk.ChunkFileRequest(chunkData);
                    break;
                case NetDataChunk.CHUNK_SERVER_FILE_DATA:
                    newChunk = new NetDataChunk.ChunkServerFileData(chunkData);
                    break;
                case NetDataChunk.CHUNK_NET_MESSAGE:
                    newChunk = new NetDataChunk.ChunkNetMessage(chunkData, registry);
                    break;
                case NetDataChunk.CHUNK_CONNECTION_REQUEST:
                    newChunk = new NetDataChunk.ChunkConnectionRequest(chunkData);
                    break;

                default:
                    Logger.log(LogType.ERROR, "Received unknown chunk type "+chunkType);
                    newChunk = null;
            }
            if (newChunk != null) {
                if (newChunk instanceof NetDataChunk.NetDataChunkImportant) {
                    ((NetDataChunk.NetDataChunkImportant) newChunk).rawReceivedData = chunkData;
                }
                dataChunks.add(newChunk);
            }
        }
    }

    void onReceive() {
        for (NetDataChunk chunk : dataChunks) {
            chunk.onReceive();
        }
    }
}
