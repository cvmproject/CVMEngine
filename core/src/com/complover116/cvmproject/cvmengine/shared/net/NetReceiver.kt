package com.complover116.cvmproject.cvmengine.shared.net
import com.complover116.cvmproject.cvmengine.shared.ISteamAPIProxy
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.DatagramChannel

class NetReceiver(port: Int? = null, val receiverSteam: ISteamAPIProxy? = null, val steamChannel: Int = SERVER_CHANNEL) {
    companion object {
        const val CLIENT_CHANNEL = 1
        const val SERVER_CHANNEL = 2
    }
    val receiverUDP: DatagramChannel = DatagramChannel.open().apply {
        if (port != null) bind(InetSocketAddress(port))
        else bind(null)
        configureBlocking(false)
    }

    private val buffer = ByteBuffer.allocate(NetConstants.MAX_PACKET_SIZE)

    private fun receiveUDPMessage(): NetIncomingMessage? {
        buffer.clear()
        val senderAddress = receiverUDP.receive(buffer)
        buffer.flip()
        if (senderAddress != null) {
            val destination = NetRemoteDestination.SocketAddressDestination(senderAddress as InetSocketAddress, receiverUDP)
            return NetIncomingMessage(buffer, destination)
        }
        return null
    }

    private fun receiveSteamMessage(): NetIncomingMessage? {
        receiverSteam ?: return null
        val steamMessage = receiverSteam.receiveMessage(steamChannel)
        if (steamMessage != null) {
            return NetIncomingMessage(ByteBuffer.wrap(steamMessage.second), NetRemoteDestination.SteamIdDestination(steamMessage.first, receiverSteam, if (steamChannel == CLIENT_CHANNEL) SERVER_CHANNEL else CLIENT_CHANNEL))
        }
        return null
    }

    fun receive(): NetIncomingMessage? {
        val steamMessage = receiveSteamMessage()
        if (steamMessage != null) return steamMessage
        val udpMessage = receiveUDPMessage()
        if (udpMessage != null) return udpMessage
        return null
    }

    fun dispose() {
        receiverUDP.close()
    }
}
