package com.complover116.cvmproject.cvmengine.shared

import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.shared.net.NetStringRegistry
import splitties.bitflags.hasFlag
import splitties.bitflags.withFlag
import java.nio.ByteBuffer


const val FLAG_IS_NEW_SOUND =     0b00000001.toByte()
const val FLAG_IS_PERSISTENT =    0b00000010.toByte()
const val FLAG_HAS_POS =          0b00000100.toByte()
const val FLAG_HAS_VOLUME =       0b00001000.toByte()
const val FLAG_HAS_PITCH =        0b00010000.toByte()
const val FLAG_IS_DELETING =      0b00100000.toByte()
const val FLAG_HAS_LOUDNESS =     0b01000000.toByte()
class SoundEvent(val time: Double, val isNewSound: Boolean, val isDeleting: Boolean): Comparable<SoundEvent> {

    var soundPos: Vector2? = null
    var soundVolume: Float? = null
    var soundLoudness: Float? = null
    var soundPitch: Float? = null

    // New sound portion
    var soundName: String? = null
    var persistence: Boolean = false
    var targetEntityID: Int? = null

    var persistentID: Int? = null

    override fun compareTo(other: SoundEvent): Int {
        return time.compareTo(other.time)
    }

    fun encode(netStringRegistry: NetStringRegistry): ByteArray {
        val data = ByteBuffer.allocate(
            8 // Time
            + 1 // Flag byte
            + if (isNewSound) 4 + 4 else {0}
            + if (soundPos != null) 4 + 4 else {0}
            + if (soundVolume != null) 4 else {0}
            + if (soundLoudness != null) 4 else {0}
            + if (soundPitch != null) 4 else {0}
            + if (persistence) 4 else {0}
        )
        data.putDouble(time)
        var flagByte = 0.toByte()
        if (isNewSound) flagByte = 0.toByte().withFlag(FLAG_IS_NEW_SOUND)
        if (persistence) flagByte = flagByte.withFlag(FLAG_IS_PERSISTENT)
        if (soundPos != null) flagByte = flagByte.withFlag(FLAG_HAS_POS)
        if (soundVolume != null) flagByte = flagByte.withFlag(FLAG_HAS_VOLUME)
        if (soundLoudness != null) flagByte = flagByte.withFlag(FLAG_HAS_LOUDNESS)
        if (soundPitch != null) flagByte = flagByte.withFlag(FLAG_HAS_PITCH)
        if (isDeleting) flagByte = flagByte.withFlag(FLAG_IS_DELETING)
        data.put(flagByte)
        if (isNewSound) {
            data.putInt(netStringRegistry.stringToInt(soundName))
            data.putInt(if (targetEntityID != null) targetEntityID!! else -1)
        }
        if (soundPos != null) {
            data.putFloat(soundPos!!.x)
            data.putFloat(soundPos!!.y)
        }
        if (soundVolume != null) {
            data.putFloat(soundVolume!!)
        }
        if (soundLoudness != null) {
            data.putFloat(soundLoudness!!)
        }
        if (soundPitch != null) {
            data.putFloat(soundPitch!!)
        }
        if (persistence) {
            data.putInt(persistentID!!)
        }
        return data.array()
    }

    fun assimilate(soundEvent: SoundEvent) {
        this.soundPos = soundEvent.soundPos?: this.soundPos
        this.soundName = soundEvent.soundName?: this.soundName
        this.soundPitch = soundEvent.soundPitch?: this.soundPitch
        this.targetEntityID = soundEvent.targetEntityID?: this.targetEntityID
        this.soundVolume = soundEvent.soundVolume?: this.soundVolume
    }

    companion object {
        @JvmStatic
        fun decode(data: ByteArray, netStringRegistry: NetStringRegistry): SoundEvent {
            val buf = ByteBuffer.wrap(data)
            val time = buf.getDouble()
            val flagByte = buf.get()
            val isNewSound = flagByte.hasFlag(FLAG_IS_NEW_SOUND)
            val isPersistent = flagByte.hasFlag(FLAG_IS_PERSISTENT)
            val isDeleting = flagByte.hasFlag(FLAG_IS_DELETING)
            val soundEvent = SoundEvent(time, isNewSound, isDeleting)
            soundEvent.persistence = isPersistent
            if (isNewSound) {
                soundEvent.soundName = netStringRegistry.intToString(buf.getInt())
                soundEvent.targetEntityID = buf.getInt()
            }

            if (flagByte.hasFlag(FLAG_HAS_POS)) {
                soundEvent.soundPos = Vector2(buf.getFloat(), buf.getFloat())
            }
            if (flagByte.hasFlag(FLAG_HAS_VOLUME)) {
                soundEvent.soundVolume = buf.getFloat()
            }
            if (flagByte.hasFlag(FLAG_HAS_LOUDNESS)) {
                soundEvent.soundLoudness = buf.getFloat()
            }
            if (flagByte.hasFlag(FLAG_HAS_PITCH)) {
                soundEvent.soundPitch = buf.getFloat()
            }
            if (isPersistent) {
                soundEvent.persistentID = buf.getInt()
            }
            return soundEvent
        }
    }
}
