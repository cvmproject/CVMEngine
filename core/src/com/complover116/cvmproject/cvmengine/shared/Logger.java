package com.complover116.cvmproject.cvmengine.shared;

import com.badlogic.gdx.Gdx;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Logger implements Runnable {
    private static final int OUT_FLUSH_INTERVAL = 100;
    private static final int DISK_FLUSH_MULTIPLIER = 10;
    private static final Logger instance = new Logger();
    public static final Handler globalExceptionHandler = new Handler();
    private final BufferedWriter logFile;
    private final BufferedWriter systemOut;
    private final Thread syncThread;
    volatile private ILogOutput logOut;

    boolean useGDXLogging = true;

    public static void disableGDXLogging() {
        instance.useGDXLogging = false;
    }

    static class Handler implements Thread.UncaughtExceptionHandler {
        public void uncaughtException(Thread t, Throwable e) {
            Logger.log(LogType.ERROR, String.format("FATAL ENGINE ERROR IN THREAD %s: %s\n", t.getName(), e.toString()));
            for (StackTraceElement element : e.getStackTrace()) {
                Logger.log(LogType.ERROR, element.toString());
            }
            instance.syncThread.interrupt();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            Gdx.app.exit();
        }
    }

    @Override
    public void run() {
        int flushCount = 0;
        while (!Thread.interrupted()) {
            try {
                try {
                    flushCount++;
                    if (flushCount >= DISK_FLUSH_MULTIPLIER) {
                        logFile.flush();
                        flushCount = 0;
                    }
                    systemOut.flush();
                    Thread.sleep(OUT_FLUSH_INTERVAL);
                } catch (InterruptedException interrupted) {
                    log(LogType.WARN, "Logger disk thread was interrupted and is shutting down");
                    logFile.flush();
                    systemOut.flush();
                    break;
                }
            } catch (IOException e) {
                System.err.println("FATAL ERROR: Could not write to log file!");
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }
    private Logger() {
        System.out.println("Logging system initializing...");
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HH-mm")
                .format(new Timestamp(System.currentTimeMillis()));
        logFile = new BufferedWriter(Gdx.files.local("log/log-"+timeStamp+".log").writer(false));
        systemOut = new BufferedWriter(new OutputStreamWriter(System.out));
        syncThread = new Thread(this, "Logger sync thread");
        syncThread.start();
    }
    public static void hook(ILogOutput out) {
        instance.logOut = out;
    }
    public static void exit() {
        instance.syncThread.interrupt();
        if (instance.logOut != null) {
            instance.logOut.shutdown();
        }
        try {
            instance.syncThread.join();
            if (instance.useGDXLogging)
                Gdx.app.log("CVMENGINE", "Logging thread has shut down");
            else
                System.out.println("Logging thread has shut down");
        } catch (InterruptedException e) {
            if (instance.useGDXLogging)
                Gdx.app.error("CVMENGINE", "Interrupted logging thread while shutting down!");
            else
                System.err.println("Interrupted logging thread while shutting down!");
        }
    }

    public static void fatalError(String message) {
        globalExceptionHandler.uncaughtException(Thread.currentThread(), new Exception(message));
    }
    public static void fatalError(Exception e) {
        globalExceptionHandler.uncaughtException(Thread.currentThread(), e);
    }
    public static synchronized void log(LogType type, String message) {

        try {
            boolean writeToFile = true;
            boolean writeToOut = true;
            boolean writeToConsole = true;
            String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                    .format(new Timestamp(System.currentTimeMillis()));
            String strType = "DEFAULT";
            switch (type) {
                case INFO:
                    strType = "INFO";
                    break;
                case WARN:
                    strType = "WARN";
                    break;
                case ERROR:
                    strType = "ERROR";
                    break;
                case DEBUG:
                    strType = "DEBUG";
//                    writeToFile = false;
                    writeToConsole = false;
                    break;
                case LUA_CL:
                    strType = "CLIENT";
                    break;
                case LUA_SV:
                    strType = "SERVER";
                    break;
                case INTERACT:
                    strType = "INTERACT";
                default:
                    writeToFile = false;
                    writeToOut = false;
            }
            String logLine = "[" + timeStamp + " - " + Thread.currentThread().getName() + "/" + strType + "] " + message;
            if (writeToOut) {
                switch (type) {
                    case ERROR:
                        if (instance.useGDXLogging)
                            Gdx.app.error("CVMENGINE", logLine);
                        else
                            System.err.println(logLine);
                        break;
                    case DEBUG:
                        if (instance.useGDXLogging)
                            Gdx.app.debug("CVMENGINE", logLine);
                        else
                            System.out.println(logLine);
                        break;
                    default:
                        if (instance.useGDXLogging)
                            Gdx.app.log("CVMENGINE", logLine);
                        else
                            System.out.println(logLine);
                }
            }
            if (writeToFile) instance.logFile.write(logLine + "\n");
            if (writeToConsole && instance.logOut != null)
                if(type == LogType.INTERACT)
                    instance.logOut.log(message, type);
                else
                    instance.logOut.log(Thread.currentThread().getName() + ":" + message, type);
        } catch (IOException e) {
            // TODO GUI Crash
            System.err.println("FATAL ERROR: Could not open log file for writing!");
            e.printStackTrace();
            System.exit(-1);
        }
    }
    public enum LogType {
        INFO, WARN, ERROR, DEBUG, LUA_CL, LUA_SV, INTERACT
    }
}
