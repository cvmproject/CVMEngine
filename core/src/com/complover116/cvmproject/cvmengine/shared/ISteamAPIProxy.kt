package com.complover116.cvmproject.cvmengine.shared

interface ISteamAPIProxy {
    abstract val steamAvailable: Boolean

    fun dispose()

    fun setRichPresenceKey(key: String, value: String)
    fun clearRichPresence()

    fun runCallbacks()

    fun enableSteamJoins()
    fun disableSteamJoins()
    fun initRelayNetworkAccess()
    fun getRelayNetworkStatus(): Boolean

    fun sendMessageToUser(steamID: Long, data: ByteArray, channel: Int)
    fun receiveMessage(channel: Int): Pair<Long, ByteArray>?
    fun getLaunchCommandLine(): String?
}
