package com.complover116.cvmproject.cvmengine.shared

import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import org.luaj.vm2.LuaFunction
import org.luaj.vm2.LuaValue
import java.util.concurrent.ConcurrentLinkedQueue

open class CommandProcessor {
    class ConsoleCommand(val command: String, val func: (Array<String>) -> Unit)

    protected val commands: HashMap<String, ConsoleCommand> = HashMap()
    private val inboundCommandQueue: ConcurrentLinkedQueue<String> = ConcurrentLinkedQueue()

    init {
        addCommand("command_list") {
            Logger.log(Logger.LogType.INTERACT, "<COMMAND_LIST_CLEAR>--->")
            listCommands().forEach {
                Logger.log(Logger.LogType.INTERACT, "<COMMAND_LIST_ADD>--->$it")
            }
        }
        addCommand("<---keepalive") {
            Logger.log(Logger.LogType.INTERACT, "<KEEPALIVE>--->")
        }
    }

    fun processQueue() {
        while (!inboundCommandQueue.isEmpty()) {
            val command = inboundCommandQueue.poll()
            val inputSplit = command.split(" ").toTypedArray()
            try {
                doProcess(inputSplit)
            } catch (e: Exception) {
                Logger.log(Logger.LogType.ERROR, "Exception while running $command:\n${e.stackTraceToString()}")
            }
        }
    }

    open fun process(input: String) {
        inboundCommandQueue.add(input)
    }

    open fun doProcess(inputSplit: Array<String>) {
        if (!commands.containsKey(inputSplit.first())) {
            Logger.log(Logger.LogType.ERROR, "Attempted to run unknown command ${inputSplit[0]}")
            return
        }
        commands[inputSplit.first()]!!.func(inputSplit)
    }

    open fun listCommands(): Array<String> {
        return commands.keys.toTypedArray()
    }

    fun addCommand(command: String, func: (Array<String>) -> Unit) {
        commands[command] = ConsoleCommand(command, func)
    }

    fun addCommand(command: String, func: LuaFunction, luaSystem: LuaSystem) {
        addCommand(command) {
            luaSystem.callFunctionSafe(func, LuaValue.varargsOf(it.copyOfRange(1, it.size).map { LuaValue.valueOf(it) }.toTypedArray()))
        }
    }
}
