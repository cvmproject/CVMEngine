package com.complover116.cvmproject.cvmengine.shared

import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.util.concurrent.ConcurrentLinkedQueue

class DevConsoleConnector(port: Int, val commandProcessor: CommandProcessor): ILogOutput {
    private val logQueue: ConcurrentLinkedQueue<String> = ConcurrentLinkedQueue()
    private var clientSocket: Socket? = null
    private var serverSocket: ServerSocket? = null
    private val lock: Object = Object()
    private val threadInbound: Thread = Thread({
        synchronized(lock) {
            try {
                lock.wait()
            } catch (e: InterruptedException) { return@Thread }
        }
        println("Starting DevConsole inbound thread")

        try {
            val reader = clientSocket!!.getInputStream().bufferedReader()
            while (!Thread.interrupted()) {
                while (!reader.ready())
                    try {
                        Thread.sleep(100)
                    } catch (e: InterruptedException) {
                        throw InterruptedException()
                    }
                if (Thread.interrupted()) {
                    break
                }
                while (reader.ready()) {
                    val command = reader.readLine()
                    if (!command.startsWith("<---"))
                        Logger.log(Logger.LogType.INTERACT, "---> $command")
                    commandProcessor.process(command)
                }
            }
        } catch (e: InterruptedException) {}
        println("Shutting down DevConsole inbound thread")
    }, "DevConsole inbound thread")


    private val threadOutgoing: Thread = Thread({
        println("Starting DevConsole output thread")
        serverSocket = ServerSocket(port)
        synchronized(lock) {
            try {
                clientSocket = serverSocket!!.accept()
            } catch (e: SocketException) {
                Logger.log(Logger.LogType.ERROR, e.message)
                return@Thread
            }
            lock.notify()
        }
        serverSocket!!.close()
        serverSocket = null
        val writer = clientSocket!!.getOutputStream().bufferedWriter()
        while (!Thread.interrupted()) {
            val message = logQueue.poll()
            if (message == null) {
                writer.flush()
                try {
                    Thread.sleep(100)
                } catch (e: InterruptedException) {
                    break
                }
                continue
            } else {
                writer.write(message)
                writer.newLine()
//                if (message.contains("<ENGINE_SHUTDOWN>--->")) break
            }
        }
        println("Shutting down DevConsole output thread")
        clientSocket!!.close()
    },"DevConsole outgoing thread")

    init {
        threadOutgoing.start()
        threadInbound.start()
    }
    override fun log(message: String, type: Logger.LogType) {
        logQueue.add("[${type}]$message")
    }

    override fun shutdown() {
        Logger.log(Logger.LogType.INFO, "Waiting for log queue to empty out")
        while (clientSocket != null && logQueue.isNotEmpty()) {
            Thread.sleep(100)
        }
        Logger.log(Logger.LogType.INFO, "Log queue empty")
        threadInbound.interrupt()
        Thread.sleep(200)
        threadOutgoing.interrupt()
        if (serverSocket != null) {
            serverSocket!!.close()
        }
        threadInbound.join()
        threadOutgoing.join()
    }
}
