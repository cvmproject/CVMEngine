package com.complover116.cvmproject.cvmengine.shared

class Utility {
    class AveragedTimer {
        private var time = 0f
        private var timeStarted: Long = 0

        val avg: Float
            get() = ((time * 100f).toInt()) / 100f

        fun start() {
            timeStarted = System.nanoTime()
        }

        fun end() {
            time += ((System.nanoTime() - timeStarted) / 1000000f - time) * 0.1f
        }
    }
}