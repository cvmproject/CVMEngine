package com.complover116.cvmproject.cvmengine.shared

import com.complover116.cvmproject.cvmengine.shared.Logger.LogType

interface ILogOutput {
    fun log(message: String, type: LogType)
    fun shutdown()
}
