package com.complover116.cvmproject.cvmengine.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.complover116.cvmproject.cvmengine.client.ui.UIInputProcessor;
import com.complover116.cvmproject.cvmengine.client.ui.UISystem;
import com.complover116.cvmproject.cvmengine.client.ui.UIText;
import com.complover116.cvmproject.cvmengine.shared.Config;
import com.complover116.cvmproject.cvmengine.shared.ILoadable;
import com.complover116.cvmproject.cvmengine.shared.Logger;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class LoadingScreen implements Screen {

    CVMControl engine;

    UIText poweredByText;
    UIText loadStepHigh;
    UIText loadStepMid;
    UIText loadStepLow;
    UISystem uiSystem;

    float dim = 0;
    final Pattern progressPattern = Pattern.compile(".*\\[([0-9]+)/([0-9]+)]");

    LinkedList<ILoadable> toLoad = new LinkedList<>();
    boolean fadeDisabled = true;

    LoadingScreen(CVMControl engine) {
        this.engine = engine;
        uiSystem = new UISystem();
        poweredByText = new UIText(0, 380, Config.INSTANCE.getEngineVersion(), Color.GREEN, "data-control", 1280, 100);
        loadStepHigh = new UIText(0, 140, "LOADING", Color.GREEN, "data-control", 1280, 60);
        loadStepMid = new UIText(0, 100, "LOADING", Color.GREEN, "data-control", 1280, 40);
        loadStepLow = new UIText(0, 60, "LOADING", Color.GREEN, "data-control", 1280, 35);
        uiSystem.add(poweredByText);
        uiSystem.add(loadStepHigh);
        uiSystem.add(loadStepMid);
        uiSystem.add(loadStepLow);
        Gdx.input.setInputProcessor(new UIInputProcessor(engine, uiSystem));
    }

    @Override
    public void show() {
        engine.debugOverlay.drawServerEntityDebug = false;
    }

    @Override
    public void render(float delta) {
        uiSystem.think(
                Gdx.graphics.getDeltaTime(),
                engine.renderer.uiViewport.getWorldWidth(),
                engine.renderer.uiViewport.getWorldHeight()
        );
        Gdx.gl.glClearColor(0, 0, 0, 1);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        engine.renderer.prepareForUI();
        engine.renderer.drawSprite("cvm_splash_screen", 0, 0, 1280, 1024, 0, 1280/2f, 1024/2f, 1f);
        engine.renderer.setColor(1f, 1f, 1f,0.5f);


        Matcher matcherLow = progressPattern.matcher(loadStepLow.getText());
        float progressLow = 0;
        if (matcherLow.matches()) {
            engine.renderer.drawHollowRect(100, 60, 1080, 35);
            progressLow = Float.parseFloat(matcherLow.group(1))/Float.parseFloat(matcherLow.group(2));
            engine.renderer.drawRect(100, 60, 1080*progressLow, 35);
        }

        Matcher matcherMed = progressPattern.matcher(loadStepMid.getText());
        float progressMed = 0;
        if (matcherMed.matches()) {
            engine.renderer.drawHollowRect(100, 100, 1080, 40);
            progressMed = Float.parseFloat(matcherMed.group(1))/Float.parseFloat(matcherMed.group(2)) + 1/Float.parseFloat(matcherMed.group(2))*progressLow;
            engine.renderer.drawRect(100, 100, 1080*progressMed, 40);
        }


//        float progressHigh = (toLoadCount-toLoad.size())/(float)toLoadCount;
//        engine.renderer.drawHollowRect(100, 140, 1080, 60);
//        engine.renderer.drawRect(100, 140, 1080*progressHigh + 1080/(float)toLoadCount*progressMed, 60);

        uiSystem.render(engine);
        if (toLoad.isEmpty()) {
            dim += delta * 100;
            engine.renderer.setColor(0, 0, 0, Math.max(0, Math.min((dim - 50) / 50, 1)));
            engine.renderer.drawRect(0, 0,
                    engine.renderer.uiViewport.getWorldWidth(), engine.renderer.uiViewport.getWorldHeight());
            if (dim > 100 || fadeDisabled) {
                Logger.log(Logger.LogType.INTERACT, "<LOADING_STATUS_UPDATE>--->GAME_START");
                engine.setScreen(engine.runningScreen);
                engine.luaSystem.libHook.call("LoadFinished");
            }
            loadStepHigh.setText("Loading finished, launching game...");
            loadStepMid.setText("");
            loadStepLow.setText("");
        } else {
            loadStepHigh.setText(toLoad.get(0).getLoadStepHigh());
            loadStepMid.setText(toLoad.get(0).getLoadStep());
            if (toLoad.get(0).getLoadStepLow() != null) {
                loadStepLow.setText(toLoad.get(0).getLoadStepLow());
            }

            ILoadable loading = toLoad.get(0);
            long deadline = System.nanoTime() + 1000000000/120;
            while (System.nanoTime() < deadline) {
                if (loading.loadTick()) {
                    toLoad.remove(loading);
                    break;
                }
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
