package com.complover116.cvmproject.cvmengine.client;

import com.complover116.cvmproject.cvmengine.shared.Player;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes;

public class PlayerClient extends Player {

    public PlayerClient(String permaname, String username, int dataEntityID, LuaTypes luaTypes, boolean isBot) {
        super(permaname, username, dataEntityID, luaTypes, isBot);
    }

    private float ping = 0;

    @Override
    public float getPing() {
        return ping;
    }

    public void setPing(float ping) {
        this.ping = ping;
    }
}
