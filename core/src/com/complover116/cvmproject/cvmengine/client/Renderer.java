package com.complover116.cvmproject.cvmengine.client;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import space.earlygrey.shapedrawer.ShapeDrawer;


public class Renderer {
    CVMControl engine;
    public PolygonSpriteBatch spriteBatch;
    public ShapeDrawer shapeDrawer;
    public ExtendViewport uiViewport;
    public ExtendViewport worldViewport;
    private Box2DDebugRenderer box2DDebugRenderer;

    private ShaderProgram lastShader = null;
    final ShaderManager shaderManager = new ShaderManager();


//  With this, UI programmers don't have to think about scaling to HDPI screens
    static final float UI_HEIGHT = 1024;
    static final float UI_WIDTH = 1280;
    float camWidth = 20f;
    float camHeight = 15;
    int screenWidth = 0;
    int screenHeight = 0;

    Renderer(CVMControl engine) {
        this.engine = engine;
        spriteBatch = new PolygonSpriteBatch();
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.drawPixel(0, 0);
        Texture texture = new Texture(pixmap);
        pixmap.dispose();
        TextureRegion region = new TextureRegion(texture, 0, 0, 1, 1);
        shapeDrawer = new ShapeDrawer(spriteBatch, region);
        uiViewport = new ExtendViewport(UI_WIDTH, UI_HEIGHT);
        worldViewport = new ExtendViewport(camWidth, camHeight);
        box2DDebugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);
    }
    void dispose() {
        spriteBatch.dispose();
    }
    void resizeWindow(int width, int height) {
        uiViewport.update(width, height);
        uiViewport.getCamera().position.x = 1280/2.0f;
        uiViewport.getCamera().position.y = 1024/2.0f;
        worldViewport.update(width, height);
        screenWidth = width;
        screenHeight = height;
    }

    public void prepareForWorld() {
        worldViewport.apply();
        spriteBatch.setProjectionMatrix(worldViewport.getCamera().combined);
        shapeDrawer.update();
    }
    void prepareForUI() {
        uiViewport.apply();
        spriteBatch.setProjectionMatrix(uiViewport.getCamera().combined);
        shapeDrawer.update();
    }
    public void resetMatrix() {
        spriteBatch.setProjectionMatrix(spriteBatch.getProjectionMatrix().idt());
        shapeDrawer.update();
    }
    public float getUILeft() {
        return -(uiViewport.getWorldWidth() - UI_WIDTH)/2;
    }
    public float getUIRight() {
        return 1280+(uiViewport.getWorldWidth() - UI_WIDTH)/2;
    }
    public float getUIBottom() {
        return -(uiViewport.getWorldHeight() - UI_HEIGHT)/2;
    }
    public float getUITop() {
        return 1024+(uiViewport.getWorldHeight() - UI_HEIGHT)/2;
    }
    public void setColor(float r, float g, float b, float a) {
        shapeDrawer.setColor(r, g, b, a);
    }
    public void setColor(float r, float g, float b) {
        setColor(r, g, b, 1f);
    }
    public void setColor(Color color) {
        shapeDrawer.setColor(color);
    }
    public void drawRect(float x, float y, float width, float height) {
        shapeDrawer.filledRectangle(x, y, width, height);
    }
    public void drawHollowRect(float x, float y, float width, float height) {
//        shapeDrawer.setDefaultLineWidth(0.15f);
        shapeDrawer.rectangle(x, y, width, height);
    }
    public void setLineWidth(float width) {
        shapeDrawer.setDefaultLineWidth(width);
    }
    public void drawCircle(float x, float y, float radius) {
        shapeDrawer.filledCircle(x, y, radius);
    }
    public void drawText(String text, String font, float size, float x, float y, Color color, boolean centered) {
        drawText(text, font, size, x, y, color.r, color.g, color.b, color.a, centered);
    }
    public void drawText(String text, String font, float size, float x, float y, float r, float g, float b, float a, boolean centered) {
        BitmapFont bitmapFont = engine.resourceManager.getBitmapFont(font, size, false);
        bitmapFont.setColor(r, g, b, a);
        if (centered) {
            GlyphLayout layout = new GlyphLayout(bitmapFont, text);
            x -= layout.width/2;
            y += layout.height/2;
        }
        bitmapFont.draw(spriteBatch, text, x, y);
    }
    public void drawLine(float x1, float y1, float x2, float y2, float width) {
        shapeDrawer.line(x1, y1, x2, y2, width);
    }
	public void setCamPos(float x, float y) {
        worldViewport.getCamera().position.x = x;
        worldViewport.getCamera().position.y = y;
    }
    public float getCamX() {
        return worldViewport.getCamera().position.x;
    }
    public float getCamY() {
        return worldViewport.getCamera().position.y;
    }
    public float getCamWidth() { return worldViewport.getMinWorldWidth(); }
    public float getCamHeight() { return worldViewport.getMinWorldHeight(); }
    public float getActualCamWidth() { return worldViewport.getWorldWidth(); }
    public float getActualCamHeight() { return worldViewport.getWorldHeight(); }
    public void zoomCamera(float coefficient) {
        worldViewport.setMinWorldWidth(worldViewport.getMinWorldWidth()*coefficient);
        worldViewport.setMinWorldHeight(worldViewport.getMinWorldHeight()*coefficient);
        resizeWindow(screenWidth, screenHeight);
    }
    public void setCamViewSize(float width, float height) {
        worldViewport.setMinWorldHeight(height);
        worldViewport.setMinWorldWidth(width);
        resizeWindow(screenWidth, screenHeight);
    }
	public void drawSprite(String assetName, float x, float y, float width, float height, float rotation, float originX, float originY, float alpha) {
        TextureRegion texture = engine.resourceManager.getTexture(assetName);
        if (texture == null) {
            Logger.log(Logger.LogType.ERROR, String.format("Cannot draw %s, asset not found", assetName));
        } else {
            if (alpha != 1f) {
                spriteBatch.setColor(1f, 1f, 1f, alpha);
            }
            spriteBatch.draw(texture, x, y, originX, originY, width, height, 1, 1, rotation);
            if (alpha != 1f) {
                spriteBatch.setColor(1f, 1f, 1f, 1f);
            }
        }
    }
    public void drawTexture(Texture region, float x, float y, float width, float height) {
        spriteBatch.draw(region, x, y, width, height);
    }

    public void setShader(String shaderName) {
        ShaderProgram shader = engine.resourceManager.getShader(shaderName);
        spriteBatch.setShader(shader);
        shader.bind();
        if (engine.world != null) {
            shader.setUniformf("u_time", (float) engine.world.curTime);
        }
        lastShader = shader;
    }

    public void setShaderUniform(String uniformName, float value) {
        if (lastShader != null) {
            lastShader.setUniformf(uniformName, value);
        }
    }
    public void setShaderUniform(String uniformName, float valueX, float valueY) {
        if (lastShader != null) {
            lastShader.bind();
            lastShader.setUniform2fv(uniformName, new float[] {valueX, valueY}, 0, 2);
        }
    }

    public void resetShader() {
        spriteBatch.setShader(null);
    }

    public void drawBox2DDebug(World world) {
        spriteBatch.end();
        box2DDebugRenderer.render(world, worldViewport.getCamera().combined);
        spriteBatch.begin();
    }

}
