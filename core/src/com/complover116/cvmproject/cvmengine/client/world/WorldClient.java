package com.complover116.cvmproject.cvmengine.client.world;

import com.badlogic.gdx.files.FileHandle;
import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.Sector;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import com.complover116.cvmproject.cvmengine.shared.world.DataProvider;
import net.querz.nbt.tag.CompoundTag;

import java.util.Comparator;
import java.util.LinkedList;

public class WorldClient extends World {

    public double offset = 0;
    public double lastReceivedTime = 0;
    public int localPlayerID = -1;
    public EntityClient localPlayerDataEntity;
    final CVMControl engine;

    public final LinkedList<EntityClient> selectedEntities = new LinkedList<>();

    public WorldClient(CVMControl engine, boolean isRemote) {
        super(engine.luaSystem.entitySystem, isRemote, new DataProvider(null, null, "world", null));
        this.engine = engine;
    }

    public WorldClient(CVMControl engine, FileHandle saveDir) {
        super(engine.luaSystem.entitySystem, false, new DataProvider(saveDir, null, "world", null));
        this.engine = engine;
    }

    public void draw() {
        LinkedList<Entity> activeEntities = new LinkedList<>();
        for (Sector sector : new LinkedList<>(activeSectors)) {
            sector.dumpEntities(activeEntities);
            sector.tick(curTime);
        }

        LinkedList<EntityClient> activeEntityClients = new LinkedList<>();

        activeEntities.forEach((ent) -> {
            activeEntityClients.add((EntityClient) ent);
        });

        activeEntityClients.sort(Comparator.comparingInt(e -> e.drawLayer));

        activeEntityClients.forEach(EntityClient::draw);

        for (EntityClient ent : selectedEntities) {
            if (!ent.isDead) {
                ent.drawDebug();
            }
        }
    }

    @Override
    public LinkedList<Entity> update(float deltaT) {
        if (curTime > lastReceivedTime+1) {
            return new LinkedList<>();
        }
        double deltaMod = offset * 0.1;
        if (-deltaMod > deltaT) {
            deltaMod = -deltaT;
        }
        deltaT += deltaMod;
        offset -= deltaMod;
        LinkedList<Entity> activeEntities = super.update(deltaT);

        for (Entity ent : activeEntities) {
            EntityClient entClient = (EntityClient) ent;
            if (entClient.id > 0) {
                entClient.clientsideTick(curTime, engine);
            }
        }
        return activeEntities;
    }

    @Override
    protected Sector createSectorFromData(CompoundTag data) {
        return new SectorClient(this, data);
    }

    @Override
    protected Sector createEmptySector(int sectorX, int sectorY) {
        return new SectorClient(this, sectorX, sectorY);
    }

    @Override
    public void loadArea(float x, float y, float radius) {
        super.loadArea(x, y, radius);
    }
}
