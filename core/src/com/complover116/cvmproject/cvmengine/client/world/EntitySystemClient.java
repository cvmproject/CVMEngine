package com.complover116.cvmproject.cvmengine.client.world;

import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import com.complover116.cvmproject.cvmengine.shared.world.EntitySystem;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;

public class EntitySystemClient extends EntitySystem {

    public EntitySystemClient(LuaSystem system) {
        super(system);
        baseEntity.set("SetPredicted", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2) {
                Logger.log(Logger.LogType.WARN, "Prediction is disabled in the engine, ignoring call to SetPredicted");
                return NIL;
//                EntityClient ent = (EntityClient) CoerceLuaToJava.coerce(arg1.get("ENT"), EntityClient.class);
//                ent.predictionEnabled = arg2.checkboolean();
//                return NIL;
            }
        });
        baseEntity.set("SetDrawLayer", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2) {
                EntityClient ent = (EntityClient) CoerceLuaToJava.coerce(arg1.get("ENT"), EntityClient.class);
                ent.drawLayer = arg2.checkint();
                return NIL;
            }
        });
        baseEntity.set("Draw", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                // The base entity is invisible
                return NIL;
            }
        });
        baseEntity.set("Remove", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                EntityClient ent = (EntityClient) CoerceLuaToJava.coerce(arg.get("ENT"), EntityClient.class);
                if (ent.id < 0) {
                    ent.isDead = true;
                } else {
                    Logger.log(Logger.LogType.WARN, "Attempted removal of non-clientside entity "+ent.id);
                }
                return NIL;
            }
        });
    }

    public EntityClient createEntity(String type) {
        EntityClient ent = new EntityClient();
        return (EntityClient) initEntity(type, ent);
    }
}
