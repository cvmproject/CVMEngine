package com.complover116.cvmproject.cvmengine.client.world;

import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.EntityState;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import java.util.LinkedList;
import java.util.PriorityQueue;

public class EntityClient extends Entity {
    PriorityQueue<Update> updates = new PriorityQueue<>();
    Update lastUpdate;

    public EntityState lastReceivedState;

    int drawLayer = 0;

    public void update(double time, EntityState delta) {
        if (lastReceivedState == null) {
            lastReceivedState = delta;
        } else {
            lastReceivedState.applyDelta(delta);
        }
        updates.add(new Update(time, lastReceivedState.copy()));
    }

    public void clientsideTick(double curTime, CVMControl engine) {
        interpolate(curTime);
        parentSector.getWorld().updateEntitySector(this);
    }

    void interpolate(double curTime) {
        while (updates.peek() != null && curTime > updates.peek().time) {
            lastUpdate = updates.remove();
        }

        if (lastUpdate != null)
            if (updates.peek() == null) {
                // There are no new messages from the server, the entity will attempt to extrapolate its position
                this.curState = lastUpdate.state.copy();
//                this.curState.pos.mulAdd(curState.vel, (float) (curTime - lastUpdate.time));
            } else {
                // Perfect condition: we have insight into the future from the server, interpolating
                this.curState = lastUpdate.state.interpolate(
                        updates.peek().state,
                        (float) ((curTime - lastUpdate.time) / (updates.peek().time - lastUpdate.time))
                );
            }
    }

    public void draw() {
        try {
            repr.get("Draw").call(repr);
        } catch (LuaError e) {
            Logger.log(Logger.LogType.ERROR, "Error while rendering "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void drawDebug() {
        try {
            LuaValue func = repr.get("DrawDebug");
            if (!func.isnil()) {
                func.call(repr);
            }
        } catch (LuaError e) {
            Logger.log(Logger.LogType.ERROR, "Error while rendering "+type+":"+e.getMessage());
            e.printStackTrace();
        }
    }
}
