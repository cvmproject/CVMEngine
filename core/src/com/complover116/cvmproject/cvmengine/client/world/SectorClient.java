package com.complover116.cvmproject.cvmengine.client.world;

import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.Sector;
import com.complover116.cvmproject.cvmengine.shared.world.World;
import net.querz.nbt.tag.CompoundTag;

public class SectorClient extends Sector {

    protected SectorClient(World world, int x, int y) {
        super(world, x, y);
    }

    protected SectorClient(WorldClient world, CompoundTag data) {
        super(world, data);
    }

    public void draw(CVMControl engine) {

    }
}
