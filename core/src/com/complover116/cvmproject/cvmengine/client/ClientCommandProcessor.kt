package com.complover116.cvmproject.cvmengine.client

import com.badlogic.gdx.Gdx
import com.complover116.cvmproject.cvmengine.client.world.EntityClient
import com.complover116.cvmproject.cvmengine.server.Server
import com.complover116.cvmproject.cvmengine.shared.ApiSymbolExtractor
import com.complover116.cvmproject.cvmengine.shared.CommandProcessor
import com.complover116.cvmproject.cvmengine.shared.Config
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.net.NetReceiver
import com.complover116.cvmproject.cvmengine.shared.net.NetRemoteDestination
import org.luaj.vm2.LuaError
import java.net.InetSocketAddress
import java.net.SocketException
import kotlin.system.exitProcess

class ClientCommandProcessor(val engine: CVMControl): CommandProcessor() {
    init {
        addCommand("vfs_search") {
            for (item in engine.moduleManager.VFS.list(it[1])) {
                Logger.log(Logger.LogType.INTERACT, item.file.path())
            }
        }
        addCommand("vfs_exists") {
            val resource = engine.moduleManager.VFS.get(it[1])
            Logger.log(Logger.LogType.INTERACT, "${it[1]}: ${resource.file != null}")
        }
        addCommand("vfs_dump_all") {
            for (item in engine.moduleManager.VFS.fileCache.entries.sortedBy { it.component1() }) {
                Logger.log(Logger.LogType.INTERACT, "${item.component1()} in ${item.component2().module.name}:${item.component2().file.path()}")
            }
        }
        addCommand("cl_ent_list") {
            for (ent in engine.world.allActiveEntities) {
                Logger.log(Logger.LogType.INTERACT, "ID " + ent.id + ":" + ent.type)
            }
        }
        addCommand("cl_player_list") {
            for (ply in engine.world.allConnectedPlayers) {
                Logger.log(Logger.LogType.INTERACT, ply.id.toString() + " (" + ply.permanentName + "):" + ply.username)
            }
        }
        addCommand("steam_init_relay_network") {
            engine.steamAPI.initRelayNetworkAccess()
        }
        addCommand("steam_get_relay_network_status") {
            engine.steamAPI.getRelayNetworkStatus()
        }
        addCommand("exit") {
            Gdx.app.exit()
        }
        addCommand("localhost") {
            if (engine.world != null) {
                Logger.log(Logger.LogType.INTERACT, "Client world already loaded")
            }
            if (engine.listenServer != null && engine.listenServer.isRunning) {
                Logger.log(Logger.LogType.INTERACT, "Server already running")
                return@addCommand
            }
            engine.localhost(20480, null)
        }
        addCommand("cl_lua") {
            try {
                engine.luaSystem.runGlobal((it.copyOfRange(1, it.size).joinToString(" ")), "CLI")
            } catch (e: LuaError) {
                Logger.log(Logger.LogType.ERROR, e.message)
            }
        }
//        TODO: Implement console variables
        addCommand("cl_lerp") {
            if (it.size > 1) {
                engine.setLerp(it[1].toFloat())
            } else Logger.log(
                Logger.LogType.INTERACT, "Lerp:" + Config.clientInterpolationDelay
            )
        }

        addCommand("debug_pointer_snap") {
            engine.debugPointer.gridSnap = it[1].toInt()
            if (engine.debugPointer.gridSnap == 0) {
                engine.debugPointer.gridSnap = null
            }
        }

        addCommand("connect_udp") {
            try {
                engine.connect(NetRemoteDestination.SocketAddressDestination(InetSocketAddress( it[1], it[2].toInt()), null))
            } catch (e: NumberFormatException) {
                Logger.log(Logger.LogType.ERROR, it[2] + " is not a number")
            } catch (e: SocketException) {
                Logger.log(Logger.LogType.ERROR, "Failed: SocketException - ${e.message}")
            }
        }

        addCommand("connect_steam") {
            val targetSteamID = it[1].toLong()
            if (!engine.steamAPI.steamAvailable) {
                Logger.log(Logger.LogType.ERROR, "Steam API is not available")
                return@addCommand
            }
            engine.connect(NetRemoteDestination.SteamIdDestination(targetSteamID, engine.steamAPI, NetReceiver.SERVER_CHANNEL))
        }

        addCommand("cvm_extract_api_symbols") {
            engine.listenServer = Server(25565, engine.moduleManagerParams, null, null)
            engine.listenServer.start()
            while (!engine.listenServer.netServer.isReady) {
                Thread.sleep(100)
            }
            ApiSymbolExtractor(engine.luaSystem, engine.listenServer.luaSystem).generate()
            Gdx.app.exit()
        }
        addCommand("disconnect") {
            engine.disconnect(false, null)
        }
        addCommand("debug_pointer") {
            engine.debugPointer.enabled = true
            engine.debugPointer.text = it.joinToString(" ")
        }
        addCommand("host") {
            var port = 25565
            if (it.size >= 2) {
                try {
                    port = it[1].toInt()
                } catch (e: NumberFormatException) {
                    Logger.log(Logger.LogType.ERROR, it[1] + " is not a number")
                }
            }
            try {
                engine.listenServer = Server(port, engine.moduleManagerParams, null, null)
                engine.listenServer.start()
                while (!engine.listenServer.netServer.isReady) {
                    Thread.sleep(100)
                }
            } catch (e: SocketException) {
                Logger.log(Logger.LogType.ERROR, "Failed: SocketException - ${e.message}")
            }
        }

        addCommand("cl_ent_select") {
            val entID = it[1].toInt()
            val ent = engine.world.getEntityByID(entID)
            engine.world.selectedEntities.add(ent as EntityClient)
        }

        addCommand("cl_ent_deselect") {
            engine.world.selectedEntities.clear()
        }
    }

    override fun doProcess(inputSplit: Array<String>) {
        if (inputSplit[0].startsWith("sv_")) {
            if (engine.listenServer != null) {
                engine.listenServer.commandProcessor.process(inputSplit.joinToString(" "))
            } else {
                Logger.log(Logger.LogType.INTERACT, "Cannot run command $inputSplit, listen server not running")
            }
        } else {
            super.doProcess(inputSplit)
        }
    }

    override fun listCommands(): Array<String> {
        val commands = super.listCommands()
        if (engine.listenServer != null) {
            return commands + engine.listenServer.commandProcessor.listCommands()
        }
        return commands
    }
}
