package com.complover116.cvmproject.cvmengine.client.ui;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.complover116.cvmproject.cvmengine.client.CVMControl;

public abstract class UIElement {
    Rectangle boundingBox = new Rectangle(0, 0, 0, 0);
    abstract void draw(CVMControl engine, Vector2 offset);
    abstract void think(float deltaT);
    boolean onMouseDown(float x, float y, int button) { return false; }
    boolean onKeyTyped(char character) { return false; }
    boolean onKeyDown(int keycode) { return false; }
    boolean hidden = false;
    void onDrag(float x, float y) {}
    UIElement(float x, float y) {
        setPos(x, y);
    }
    public void setPos(float x, float y) {
        boundingBox.x = x;
        boundingBox.y = y;
    }


}
