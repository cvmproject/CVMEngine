package com.complover116.cvmproject.cvmengine.client.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.complover116.cvmproject.cvmengine.client.CVMControl;

public class UIText extends UIElement {
    String text;
    Color color;
    private String fontId;
    public UIText(float x, float y, String text, Color color, String fontId, float width, float height) {
        super(x, y);
        this.text = text;
        this.color = color;
        this.fontId = fontId;
        this.boundingBox.width = width;
        this.boundingBox.height = height;
    }

    public void setText(String text) {
        assert text != null;
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    @Override
    void draw(CVMControl engine, Vector2 offset) {
        float x = offset.x + boundingBox.x;
        float y = offset.y + boundingBox.y;
        engine.renderer.drawText(text, fontId, (int)boundingBox.height, x+boundingBox.width/2, y + this.boundingBox.height/2, this.color, true);
    }

    @Override
    void think(float deltaT) {}
}
