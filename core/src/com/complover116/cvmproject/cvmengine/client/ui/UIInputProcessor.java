package com.complover116.cvmproject.cvmengine.client.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.client.lua.LibDraw;
import com.complover116.cvmproject.cvmengine.shared.Logger;

public class UIInputProcessor implements InputProcessor {
    private UISystem system;
    private CVMControl engine;

    public UIInputProcessor(CVMControl engine, UISystem system) {
        this.system = system;
        this.engine = engine;
    }

    Vector3 screenCoords = new Vector3();
    Vector3 uiCoords = new Vector3();

    Vector2 worldDragPos = new Vector2();
    boolean isDraggingWorld = false;

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.GRAVE) {
            return true;
        }
        if (keycode == Input.Keys.F3) {
            engine.debugOverlay.hidden = !engine.debugOverlay.hidden;
        }
        if (keycode == Input.Keys.F4) {
            engine.debugOverlay.drawSectors= !engine.debugOverlay.drawSectors;
        }
        if (keycode == Input.Keys.F7) {
            engine.debugOverlay.drawPhysics= !engine.debugOverlay.drawPhysics;
        }
        if (keycode == Input.Keys.F8) {
            engine.debugOverlay.drawServerEntityDebug= !engine.debugOverlay.drawServerEntityDebug;
            if (engine.debugOverlay.drawServerEntityDebug) {
                if (engine.listenServer.luaSystem.globals.get("draw").isnil()) {
                    Logger.log(Logger.LogType.WARN, "WARNING: Injecting the draw library into server LuaSystem to allow DrawServerDebug");
                    new LibDraw(engine.listenServer.luaSystem, engine.renderer);
                }
            }
        }
        if (keycode == Input.Keys.F5) {
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) && engine.listenServer != null) {
                engine.listenServer.restart();
            }
            if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
                engine.disconnect(true, null);
            }
        }
        if (keycode == Input.Keys.F2) {
            engine.debugOverlay.drawSectorsLocal= !engine.debugOverlay.drawSectorsLocal;
        }
        if (keycode == Input.Keys.F6 && engine.listenServer != null && engine.listenServer.world != null) {
            engine.listenServer.world.isSuspended = !engine.listenServer.world.isSuspended;
        }
        if (keycode == Input.Keys.F11) {
            if (Gdx.graphics.isFullscreen()) {
                Gdx.graphics.setWindowedMode(800, 480);
            } else {
                Graphics.DisplayMode mode = Gdx.graphics.getDisplayMode();
                Gdx.graphics.setFullscreenMode(mode);
                Gdx.graphics.setVSync(true);
            }
        }
        if (keycode == Input.Keys.F9) {
            engine.resourceManager.stopAllMusic();
        }
        for (UIElement element : system.elements) {
            if (element.hidden)
                continue;
            element.onKeyDown(keycode);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character)
    {
        if (character == '`') {
            return true;
        }
        for (UIElement element : system.elements) {
            if (element.hidden)
                continue;
            element.onKeyTyped(character);
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (engine.isEditing) {
            if (button == Input.Buttons.RIGHT) {
                worldDragPos.x = Gdx.input.getX();
                worldDragPos.y = Gdx.input.getY();
                isDraggingWorld = true;
            }
        }
        uiCoords.set(engine.renderer.uiViewport.unproject(screenCoords.set(screenX, screenY, 0)));
        for (UIElement element : system.elements) {
            if (element.hidden)
                continue;
            if (element.boundingBox.contains(uiCoords.x, uiCoords.y)) {
                element.onMouseDown(uiCoords.x, uiCoords.y, button); //TODO: Account for what is on top
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (engine.isEditing) {
            if (button == Input.Buttons.RIGHT) {
                isDraggingWorld = false;
            }
        }
        return false;
    }

    @Override
    public boolean touchCancelled(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        uiCoords.set(engine.renderer.uiViewport.unproject(screenCoords.set(screenX, screenY, 0)));
        for (UIElement element : system.elements) {
            if (element.hidden)
                continue;
            element.onDrag(uiCoords.x, uiCoords.y);
        }
        if (engine.isEditing && isDraggingWorld) {
            float xToMove = worldDragPos.x - screenX;
            float yToMove = worldDragPos.y - screenY;

            worldDragPos.x = Gdx.input.getX();
            worldDragPos.y = Gdx.input.getY();

            float multiplier = engine.renderer.worldViewport.getWorldHeight() / engine.renderer.worldViewport.getScreenHeight();
            engine.renderer.setCamPos(engine.renderer.getCamX() + xToMove*multiplier, engine.renderer.getCamY() - yToMove*multiplier);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {

        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (engine.isEditing)
            engine.renderer.zoomCamera(1 - amountY/20);
        return false;
    }
}
