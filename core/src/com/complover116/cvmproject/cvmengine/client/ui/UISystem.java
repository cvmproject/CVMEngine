package com.complover116.cvmproject.cvmengine.client.ui;

import com.badlogic.gdx.math.Vector2;
import com.complover116.cvmproject.cvmengine.client.CVMControl;

import java.util.ArrayList;

public class UISystem {
    ArrayList<UIElement> elements = new ArrayList<>();
    public void render(CVMControl engine) {
        for (UIElement element : elements) {
            if (element.hidden)
                continue;
            element.draw(engine, Vector2.Zero);
        }
    }
    public void think(float deltaT, float displayWidth, float displayHeight) {
        for (UIElement element : elements) {
            element.think(deltaT);
        }
    }
    public void add(UIElement element) {
        elements.add(element);
    }
}
