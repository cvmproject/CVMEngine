package com.complover116.cvmproject.cvmengine.client.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.world.Entity
import kotlin.math.roundToInt

class UIDebugPointer: UIElement(0f, 0f) {
    var enabled = false
    var text: String? = null
    var draggedEntity: Entity? = null
    var dragStartPos: Vector2 = Vector2(0F, 0F)

    var gridSnap: Int? = null

    override fun draw(engine: CVMControl, offset: Vector2?) {
        if (enabled || engine.isEditing) {
            engine.renderer.setColor(1f, 1f, 1f)
            val worldPos = engine.renderer.worldViewport.unproject(Vector2(
                Gdx.input.getX(0).toFloat(),
                Gdx.input.getY(0).toFloat()
            ))

            if (gridSnap != null) {
                val flipX = worldPos.x < 0
                val flipY = worldPos.x < 0

                if (flipX) worldPos.x = -worldPos.x
                if (flipY) worldPos.y = -worldPos.y

                worldPos.x = ((worldPos.x.roundToInt() + gridSnap!!/2)/ gridSnap!! * gridSnap!!).toFloat()
                worldPos.y = ((worldPos.y.roundToInt() + gridSnap!!/2)/ gridSnap!! * gridSnap!!).toFloat()

                if (flipX) worldPos.x = -worldPos.x
                if (flipY) worldPos.y = -worldPos.y
            }

            val restoredScreenPos = engine.renderer.worldViewport.project(worldPos.cpy())

            val uiPos = engine.renderer.uiViewport.unproject(restoredScreenPos.cpy())

            engine.renderer.drawLine(
                uiPos.x, engine.renderer.uiTop,
                uiPos.x, engine.renderer.uiBottom,
                2f
            )
            engine.renderer.drawLine(engine.renderer.uiLeft, engine.renderer.uiViewport.worldHeight-uiPos.y,
                engine.renderer.uiRight, engine.renderer.uiViewport.worldHeight-uiPos.y,
                2f
            )

            engine.renderer.drawText("worldX:${worldPos.x.roundToInt()} uiX:${uiPos.x.roundToInt()}", "data-control", 20f, uiPos.x + 2, engine.renderer.uiTop-30, 1f, 1f, 1f, 1f, false)
            engine.renderer.drawText("worldY:${worldPos.y.roundToInt()} uiY:${uiPos.y.roundToInt()}", "data-control", 20f, engine.renderer.uiLeft + 5, engine.renderer.uiViewport.worldHeight-uiPos.y + 23, 1f, 1f, 1f, 1f, false)

            if (text != null) {
                engine.renderer.drawText(
                    text,
                    "data-control",
                    30f,
                    engine.renderer.uiViewport.worldWidth / 2 - 400,
                    30f,
                    1f,
                    1f,
                    1f,
                    1f,
                    false
                )
                if (Gdx.input.isTouched(0) && Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                    Logger.log(Logger.LogType.INTERACT, "<POINTER_CLICK>--->${worldPos.x}:${worldPos.y}")
                    enabled = false
                    text = null
                }
            } else {
                if (!engine.isEditing) return
                if (engine.listenServer == null) return
                if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
                    if (draggedEntity == null) {
                        val scaledRadius =
                            engine.renderer.worldViewport.worldHeight / 60
                        val nearbyEntities =
                            engine.listenServer.world.getEntitiesWithinRadius(worldPos.x, worldPos.y, scaledRadius)
                        Logger.log(Logger.LogType.INTERACT, "<ENTITY_SELECTION_CLEAR>--->")
                        var closestEntity: Entity? = null
                        nearbyEntities.forEach {
                            if (closestEntity == null || it.pos.dst(worldPos) < closestEntity!!.pos.dst(worldPos)) {
                                closestEntity = it
                            }
                            Logger.log(Logger.LogType.INTERACT, "<ENTITY_SELECTION_ADD>--->${it.id}")
                        }
                        dragStartPos = uiPos
                        draggedEntity = closestEntity
                    }
                }
                if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                    if (draggedEntity != null && uiPos.dst(dragStartPos) > 10) {
                        dragStartPos.x = -1000f
                        dragStartPos.y = -1000f
                        draggedEntity!!.curState.pos?.set(worldPos)
                    }
                } else {
                    if (draggedEntity != null)
                        draggedEntity = null
                }
            }
        }
    }

    override fun think(deltaT: Float) {

    }

}