package com.complover116.cvmproject.cvmengine.client.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.server.PlayerServer
import com.complover116.cvmproject.cvmengine.shared.Config

class UIDebugOverlay : UIElement(0f, 0f) {
    fun toHuman(num: Int): String {
        return if (num < 1000) {
            Integer.toString(num)
        } else if (num < 1000000) {
            String.format("%.2fK", num / 1000f)
        } else {
            String.format("%.2fM", num / 1000000f)
        }
    }

    var FPS = 0f
    var lastDrawn: Long
    @JvmField
    var drawSectors = false
    @JvmField
    var drawPhysics = false
    @JvmField
    var drawServerEntityDebug = false
    @JvmField
    var drawSectorsLocal = false
    public override fun draw(engine: CVMControl, offset: Vector2) {
        // float x = offset.x + boundingBox.x;
        // float y = offset.y + boundingBox.y;
        FPS += (1000000000f / (System.nanoTime() - lastDrawn) - FPS) * 0.1f
        lastDrawn = System.nanoTime()
        val displayLeft = engine.renderer.uiLeft
        val displayRight = engine.renderer.uiRight
        val displayBottom = engine.renderer.uiBottom
        val displayTop = engine.renderer.uiTop
        if (engine.world != null) {
            val curTimeRounded = Math.round(engine.world.curTime * 100.0) / 100.0
            val timeOffset = Math.round(engine.world.offset * 100.0) / 100.0

            engine.renderer.drawText("CurTime:$curTimeRounded", "TitilliumWeb", 20f, displayLeft, displayTop - 20, Color.WHITE, false)
            var desyncColor = Color.GREEN
            if (Math.abs(timeOffset) > 0.03) {
                desyncColor =
                    if (Math.abs(timeOffset) > 0.1) if (Math.abs(timeOffset) > 0.5) Color.RED else Color.ORANGE else Color.YELLOW
            }
            engine.renderer.drawText("Desync: ${(timeOffset*1000).toInt()} ms", "TitilliumWeb", 20f, displayLeft+370f, displayTop - 20, desyncColor, false)
        }
        val lerp = Config.clientInterpolationDelay

        var lerpColor = Color.GREEN
        if (lerp > 0.02) {
            lerpColor = if (lerp > 0.05) if (lerp > 0.1) Color.RED else Color.ORANGE else Color.YELLOW
        }
        engine.renderer.drawText("LERP: ${(lerp*1000).toInt()} ms", "TitilliumWeb", 20f, displayLeft+370f, displayTop - 90, lerpColor, false)

        engine.renderer.drawText("FPS:${Math.round(FPS)}", "TitilliumWeb", 20f, displayLeft+200f, displayTop - 20, Color.WHITE, false)
        engine.renderer.drawText(
            "Net:${engine.netTime.avg}ms",
            "TitilliumWeb",
            20f,
            displayLeft,
            displayTop - 40,
            Color.WHITE,
            false
        )
        engine.renderer.drawText(
            "Update:${engine.updateTime.avg}ms",
            "TitilliumWeb",
            20f,
            displayLeft + 100f,
            displayTop - 40,
            Color.WHITE,
            false
        )
        engine.renderer.drawText(
            "Draw:${engine.drawTime.avg}ms",
            "TitilliumWeb",
            20f,
            displayLeft + 240f,
            displayTop - 40,
            Color.WHITE,
            false
        )
        val clientLoadColor: Color
        if (engine.runningScreen.clientLoadLongTerm > 100) {
            clientLoadColor = Color.RED
            engine.renderer.drawText("OVERLOAD", "TitilliumWeb", 15f, 470f, displayTop - 55, clientLoadColor, false)
        } else if (engine.runningScreen.clientLoadLongTerm > 100) {
            clientLoadColor = Color.ORANGE
        } else if (engine.runningScreen.clientLoadLongTerm > 90) {
            clientLoadColor = Color.YELLOW
        } else {
            clientLoadColor = Color.GREEN
        }
        engine.renderer.drawText(
            String.format(
                "Instant Load: %.1f%%",
                engine.runningScreen.clientLoad
            ), "TitilliumWeb", 15f, displayLeft + 370f, displayTop - 55, clientLoadColor, false
        )
        engine.renderer.drawText(
            String.format(
                "Average Load: %.1f%%",
                engine.runningScreen.clientLoadLongTerm
            ), "TitilliumWeb", 15f, displayLeft + 370f, displayTop - 70, clientLoadColor, false
        )
        if (engine.world != null) {
            val worldPos = engine.renderer.worldViewport.unproject(Vector2(Gdx.input.getX(0).toFloat(), Gdx.input.getY(0).toFloat()));
            engine.renderer.drawText(
                "World pointer pos X:${worldPos.x} Y:${worldPos.y}", "TitilliumWeb", 15f, displayLeft + 500f, displayTop - 70, clientLoadColor, false
            )
        }
        if (engine.netClient != null && engine.netClient.ready) {
            engine.renderer.drawText(
                "Ping: " + engine.netClient.connection.ping.toInt() + "ms",
                "TitilliumWeb",
                20f,
                displayLeft + 0f,
                displayTop - 60,
                Color.WHITE,
                false
            ) // PING
            engine.renderer.drawText(
                "Dropping " + (engine.netClient.connection.dropRate * 100).toInt() + "%",
                "TitilliumWeb",
                20f,
                displayLeft + 120f,
                displayTop - 60,
                Color.WHITE,
                false
            ) // PACKET LOSS
            engine.renderer.drawText(
                "In " + toHuman(engine.netClient.connection.avgPacketIn.toInt()) + "B/s",
                "TitilliumWeb",
                20f,
                displayLeft + 0f,
                displayTop - 80,
                Color.WHITE,
                false
            ) // Avg in
            engine.renderer.drawText(
                "Out " + toHuman(engine.netClient.connection.avgPacketOut.toInt()) + "B/s",
                "TitilliumWeb",
                20f,
                displayLeft + 150f,
                displayTop - 80,
                Color.WHITE,
                false
            ) // Avg out
        }
        if (engine.soundManager != null) {
            engine.renderer.drawText(
                "Sounds [Active/Total]: " + engine.soundManager.getActiveSoundCount() + "/" + engine.soundManager.getSoundCount(),
                "TitilliumWeb",
                20f,
                displayLeft + 0f,
                displayTop - 100,
                Color.WHITE,
                false
            )
        }
        if (engine.listenServer != null && engine.listenServer.netServer.isReady) {
            engine.renderer.setColor(0.0f, 0.0f, 0.0f, 0.7f)
            engine.renderer.drawRect(displayRight - 300, displayBottom, 300f, 100f)
            val curTimeServer = Math.round(engine.listenServer.world.curTime * 100.0) / 100.0
            engine.renderer.drawText(
                String.format(
                    "TPS: %.2f (%d)",
                    engine.listenServer.realTPS,
                    engine.listenServer.tickRate
                ), "TitilliumWeb", 15f, displayRight - 250, 60f, Color.WHITE, false
            )
            val loadColor: Color
            if (engine.listenServer.serverLoadLongTerm > 100) {
                loadColor = Color.RED
                engine.renderer.drawText("OVERLOAD", "TitilliumWeb", 15f, displayRight - 250, displayBottom + 105f, loadColor, false)
            } else if (engine.listenServer.serverLoad > 100) {
                loadColor = Color.ORANGE
            } else if (engine.listenServer.serverLoadLongTerm > 90) {
                loadColor = Color.YELLOW
            } else {
                loadColor = Color.GREEN
            }
            engine.renderer.drawText(
                String.format(
                    "Instant Load: %.1f%% (%.2f ms/tick)",
                    engine.listenServer.serverLoad,
                    engine.listenServer.tickTime.avg
                ), "TitilliumWeb", 15f, displayRight - 250, displayBottom + 75f, loadColor, false
            )
            engine.renderer.drawText(
                String.format(
                    "Average Load: %.1f%% (%.2f ms idle)",
                    engine.listenServer.serverLoadLongTerm,
                    engine.listenServer.idleTime.avg
                ), "TitilliumWeb", 15f, displayRight - 250, displayBottom + 90f, loadColor, false
            )
            engine.renderer.drawText("CurTime:$curTimeServer", "TitilliumWeb", 15f, displayRight - 250, displayBottom + 15f, Color.WHITE, false)
            engine.renderer.drawText(
                "Sectors:          /         /",
                "TitilliumWeb",
                15f,
                displayRight - 300,
                displayBottom + 45f,
                Color.WHITE,
                false
            )
            engine.renderer.drawText(
                "" + engine.listenServer.world.activeSectorCount,
                "TitilliumWeb",
                15f,
                displayRight - 245,
                displayBottom + 45f,
                Color.GREEN,
                false
            )
            engine.renderer.drawText(
                "" + engine.listenServer.world.frozenSectorCount,
                "TitilliumWeb",
                15f,
                displayRight - 210,
                displayBottom + 45f,
                Color.YELLOW,
                false
            )
            engine.renderer.drawText(
                "" + engine.listenServer.world.savedSectorCount,
                "TitilliumWeb",
                15f,
                displayRight - 175,
                displayBottom + 45f,
                Color.CYAN,
                false
            )
            val players = engine.listenServer.world.allConnectedPlayers
            for (i in players.indices) {
                val connection = (players[i] as PlayerServer).connection
                engine.renderer.drawText(
                    "Connection " + i + " - " + connection!!.remote.toString(),
                    "TitilliumWeb",
                    15f,
                    displayRight - 250,
                    displayTop - 15 - i * 90,
                    Color.LIME,
                    false
                )
                engine.renderer.drawText(
                    "Ping: " + connection.ping.toInt() + "ms",
                    "TitilliumWeb",
                    15f,
                    displayRight - 250,
                    displayTop - 30 - i * 90,
                    Color.WHITE,
                    false
                ) // PING
                engine.renderer.drawText(
                    "Dropping " + (connection.dropRate * 100).toInt() + "%",
                    "TitilliumWeb",
                    15f,
                    displayRight - 130,
                    displayTop - 30 - i * 90,
                    Color.WHITE,
                    false
                ) // PACKET LOSS
                engine.renderer.drawText(
                    "In " + toHuman(connection.avgPacketIn.toInt()) + "B/s",
                    "TitilliumWeb",
                    15f,
                    displayRight - 250,
                    displayTop - 45 - i * 90,
                    Color.WHITE,
                    false
                ) // Avg in
                engine.renderer.drawText(
                    "Out " + toHuman(connection.avgPacketOut.toInt()) + "B/s",
                    "TitilliumWeb",
                    15f,
                    displayRight - 150,
                    displayTop - 45 - i * 90,
                    Color.WHITE,
                    false
                ) // Avg out
            }
        }
    }

    public override fun think(deltaT: Float) {}

    init {
        lastDrawn = System.nanoTime()
        hidden = true
    }
}