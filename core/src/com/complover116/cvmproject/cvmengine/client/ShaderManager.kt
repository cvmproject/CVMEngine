package com.complover116.cvmproject.cvmengine.client

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.complover116.cvmproject.cvmengine.shared.Logger

class ShaderManager {
    private val fboMap = HashMap<Int, FrameBuffer>()
    private var fboCounter = 0

    fun createFBO(width: Int, height: Int): Int {
        val fbo = FrameBuffer(Pixmap.Format.RGBA8888, width, height, false)
        fboCounter++
        fboMap[fboCounter] = fbo
        return fboCounter
    }

    fun dispose() {
        Logger.log(Logger.LogType.INFO, "ShaderManager: Dumping all FBOs")
        fboMap.values.forEach {
            it.dispose()
        }
        fboMap.clear()
        fboCounter = 0
    }
}
