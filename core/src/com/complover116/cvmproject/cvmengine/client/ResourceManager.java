package com.complover116.cvmproject.cvmengine.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.complover116.cvmproject.cvmengine.shared.ILoadable;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.VirtualFS;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

public class ResourceManager implements ILoadable {

    private static final String baseVertexShader = "#version 120\nattribute vec4 a_position;\n" +
            "attribute vec4 a_color;\n" +
            "attribute vec2 a_texCoord0;\n" +
            "\n" +
            "uniform mat4 u_projTrans;\n" +
            "\n" +
            "varying vec4 v_color;\n" +
            "varying vec2 v_texCoords;\n" +
            "\n" +
            "void main()\n" +
            "{\n" +
            "    v_color = a_color;\n" +
            "    v_color.a = v_color.a * (256.0/255.0);\n" +
            "    v_texCoords = a_texCoord0 + 0;\n" +
            "    gl_Position =  u_projTrans * a_position;\n" +
            "}";
    private final HashMap<String, BitmapFont> generatedFonts = new HashMap<String, BitmapFont>();
    private final HashMap<String, FreeTypeFontGenerator> fonts = new HashMap<String, FreeTypeFontGenerator>();
    private final HashMap<String, TextureRegion> textures = new HashMap<String, TextureRegion>();
    private final HashMap<String, ShaderProgram> shaders = new HashMap<>();
    private final HashMap<String, Sound> sounds = new HashMap<String, Sound>();
    private final HashMap<String, Music> music = new HashMap<>();

    VirtualFS VFS;

    private String loadStep = "Building resource lists";
    private String loadStepLow = null;
    private LinkedList<VirtualFS.NamedResource> texturesToLoad;
    private int texturesToLoadCount;
    private LinkedList<VirtualFS.NamedResource> fontsToLoad;
    private int fontsToLoadCount;
    private LinkedList<VirtualFS.NamedResource> soundsToLoad;
    private int soundsToLoadCount;
    private LinkedList<VirtualFS.NamedResource> musicToLoad;
    private int musicToLoadCount;
    private LinkedList<VirtualFS.NamedResource> shadersToLoad;
    private int shadersToLoadCount;

    ResourceManager(VirtualFS VFS) {
        ShaderProgram.pedantic = false;
        this.VFS = VFS;
        fonts.put("data-control", new FreeTypeFontGenerator(Gdx.files.internal("fonts/data-control.ttf")));
        fonts.put("TitilliumWeb", new FreeTypeFontGenerator(Gdx.files.internal("fonts/TitilliumWeb.ttf")));
        textures.put("cvm_splash_screen", new TextureRegion(new Texture(Gdx.files.internal("img/cvm_splash_screen.png"))));
    }

    public BitmapFont getBitmapFont(String fontId, float fontSize, boolean forceNewInstance) {
        String generatedFontId = fontId + "_" +
                fontSize;
        if (!generatedFonts.containsKey(generatedFontId) || forceNewInstance) {
            if (!fonts.containsKey(fontId)) {
                Logger.log(Logger.LogType.ERROR, "Failed to load font "+fontId);
                generatedFonts.put(generatedFontId, new BitmapFont());
                Logger.log(Logger.LogType.WARN, "Failed generating font " + generatedFontId + ": "
                        + fontId + " is not loaded, falling back to default");
                return generatedFonts.get(generatedFontId);
            }
            final int MINIMAL_FONT_SIZE = 30;

            BitmapFont newFont;
            if (fontSize < MINIMAL_FONT_SIZE) {

                newFont = getBitmapFont(fontId, MINIMAL_FONT_SIZE, true);
                newFont.getData().setScale((float)fontSize/MINIMAL_FONT_SIZE);
                newFont.setUseIntegerPositions(false);
            } else {
                FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
                parameter.size = (int)fontSize;
                parameter.color = Color.WHITE;
                newFont = fonts.get(fontId).generateFont(parameter);
                newFont.getRegion().getTexture()
                        .setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
            if (!forceNewInstance) {
                generatedFonts.put(generatedFontId, newFont);
            } else {
                return newFont;
            }
        }
        return generatedFonts.get(generatedFontId);
    }

    private boolean loadFont(VirtualFS.NamedResource target) {
        String fontId = target.relativePath.substring(6).split("\\.")[0];
        Logger.log(Logger.LogType.INFO, "Loading font "+fontId);
        FileHandle font_file = target.file;
        try {
            fonts.put(fontId, new FreeTypeFontGenerator(font_file));
        } catch (Exception e) {
            Logger.log(Logger.LogType.ERROR, "Failed to load font "+fontId+": "+e.getMessage());
            return false;
        }
        return false;
    }

    private boolean loadTexture(VirtualFS.NamedResource target) {
        String name = target.relativePath.substring(4).split("\\.")[0];
        Logger.log(Logger.LogType.INFO, String.format("Loading texture %s", name));
        if (target.file == null) {
            Logger.log(Logger.LogType.ERROR, "Failed to load texture "+name+": file not found");
            return false;
        }
        try {
            TextureRegion tex = new TextureRegion(new Texture(target.file));
            textures.put(name, tex);
        } catch (Exception e) {
            Logger.log(Logger.LogType.ERROR, "Failed to load texture "+name+": "+e.getMessage());
            return false;
        }
        Logger.log(Logger.LogType.DEBUG, "Loaded texture "+name);
        return true;
    }

    private boolean loadSound(VirtualFS.NamedResource target) {
        String name = target.relativePath.substring(6).split("\\.")[0];
        Logger.log(Logger.LogType.INFO, String.format("Loading sound %s", name));
        if (target.file == null) {
            Logger.log(Logger.LogType.ERROR, "Failed to load sound "+name+": file not found");
            return false;
        }
        try {
            Sound sound = Gdx.audio.newSound(target.file);
            sounds.put(name, sound);
        } catch (Exception e) {
            Logger.log(Logger.LogType.ERROR, "Failed to load sound "+name+": "+e.getMessage());
            return false;
        }
        Logger.log(Logger.LogType.DEBUG, "Loaded sound "+name);
        return true;
    }

    private boolean loadMusic(VirtualFS.NamedResource target) {
        String name = target.relativePath.substring(6).split("\\.")[0];
        Logger.log(Logger.LogType.INFO, String.format("Loading music %s", name));
        if (target.file == null) {
            Logger.log(Logger.LogType.ERROR, "Failed to load music "+name+": file not found");
            return false;
        }
        try {
            Music sound = Gdx.audio.newMusic(target.file);
            music.put(name, sound);
        } catch (Exception e) {
            Logger.log(Logger.LogType.ERROR, "Failed to load music "+name+": "+e.getMessage());
            return false;
        }
        Logger.log(Logger.LogType.DEBUG, "Loaded music "+name);
        return true;
    }

    private boolean loadShader(VirtualFS.NamedResource target) {
        String name = target.relativePath.substring(8).split("\\.")[0];
        Logger.log(Logger.LogType.INFO, String.format("Loading shader %s", name));
        if (target.file == null) {
            Logger.log(Logger.LogType.ERROR, "Failed to load shader "+name+": file not found");
            return false;
        }
        try {
            ShaderProgram shader = new ShaderProgram(baseVertexShader, target.file.readString());

            if (!shader.isCompiled()) {
//                Logger.log(Logger.LogType.ERROR, "Failed to compile shader "+name+": "+shader.getLog());
                Logger.fatalError("Failed to compile shader "+name+": "+shader.getLog());
                return false;
            }
            shaders.put(name, shader);
        } catch (Exception e) {
            Logger.log(Logger.LogType.ERROR, "Failed to load shader "+name+": "+e.getMessage());
            return false;
        }
        Logger.log(Logger.LogType.DEBUG, "Loaded shader "+name);
        return true;
    }

    TextureRegion getTexture(String name) {
        if (!textures.containsKey(name)) {
            Logger.log(Logger.LogType.WARN, "Texture " + name + " was not ready, force loading on main thread");
            boolean loadedOk = loadTexture(VFS.get("img/"+name));
            assert loadedOk;
        }
        return textures.get(name);
    }

    ShaderProgram getShader(String name) {
        if (!shaders.containsKey(name)) {
            Logger.log(Logger.LogType.WARN, "Shader " + name + " not ready, the null shader will be used instead");
            return null;
        }
        return shaders.get(name);
    }

    public Sound getSound(String name) {
        if (!sounds.containsKey(name)) {
            Logger.log(Logger.LogType.WARN, "Sound " + name + " was not ready, force loading on main thread");
            boolean loadedOk = loadSound(VFS.get("sound/"+name+".ogg"));
            assert loadedOk;
        }
        return sounds.get(name);
    }

    public Music getMusic(String name) {
        if (!music.containsKey(name)) {
            Logger.log(Logger.LogType.WARN, "Music " + name + " was not ready, force loading on main thread");
            boolean loadedOk = loadSound(VFS.get("music/"+name+".ogg"));
            assert loadedOk;
        }
        return music.get(name);
    }

    @Override
    public String getLoadStep() {
        return loadStep;
    }

    @Override
    public String getLoadStepLow() {
        return loadStepLow;
    }

    @Override
    public String getLoadStepHigh() {
        return "Loading resources";
    }

    @Override
    public boolean loadTick() {
        if (texturesToLoad == null) {
            texturesToLoad = VFS.listRecurse("img");
            texturesToLoadCount = texturesToLoad.size();
            fontsToLoad = VFS.listRecurse("fonts");
            fontsToLoadCount = fontsToLoad.size();
            soundsToLoad = VFS.listRecurse("sound");
            soundsToLoadCount = soundsToLoad.size();
            musicToLoad = VFS.listRecurse("music");
            musicToLoadCount = musicToLoad.size();
            shadersToLoad = VFS.listRecurse("shaders");
            shadersToLoadCount = shadersToLoad.size();
        } else {
            if (!texturesToLoad.isEmpty()) {
                loadTexture(texturesToLoad.pollFirst());
            } else if (!fontsToLoad.isEmpty()) {
                loadFont(fontsToLoad.get(0));
            } else if (!soundsToLoad.isEmpty()) {
                loadSound(soundsToLoad.pollFirst());
            } else if (!musicToLoad.isEmpty()) {
                loadMusic(musicToLoad.pollFirst());
            } else if (!shadersToLoad.isEmpty()) {
                loadShader(shadersToLoad.pollFirst());
            }
        }
        if (!texturesToLoad.isEmpty()) {
            loadStep = "Loading textures [0/5]";
            loadStepLow = String.format(Locale.ENGLISH, "Loading %s [%d/%d]", texturesToLoad.get(0).relativePath, texturesToLoadCount-texturesToLoad.size(), texturesToLoadCount);
        } else if (!fontsToLoad.isEmpty()) {
            loadStep = "Loading fonts [1/5]";
            loadStepLow = String.format(Locale.ENGLISH, "Loading %s [%d/%d]", fontsToLoad.get(0).relativePath, fontsToLoadCount-fontsToLoad.size(), fontsToLoadCount);
        } else if (!soundsToLoad.isEmpty()) {
            loadStep = "Loading sounds [2/5]";
            loadStepLow = String.format(Locale.ENGLISH, "Loading %s [%d/%d]", soundsToLoad.get(0).relativePath, soundsToLoadCount-soundsToLoad.size(), soundsToLoadCount);
        } else if (!musicToLoad.isEmpty()) {
            loadStep = "Loading music [3/5]";
            loadStepLow = String.format(Locale.ENGLISH, "Loading %s [%d/%d]", musicToLoad.get(0).relativePath, musicToLoadCount-musicToLoad.size(), musicToLoadCount);
        } else if (!shadersToLoad.isEmpty()) {
            loadStep = "Loading shaders [4/5]";
            loadStepLow = String.format(Locale.ENGLISH, "Loading %s [%d/%d]", shadersToLoad.get(0).relativePath, shadersToLoadCount-shadersToLoad.size(), shadersToLoadCount);
        } else {
            Logger.log(Logger.LogType.INTERACT, "<LOADING_STATUS_UPDATE>--->RESOURCES_LOADED");
            return true;
        }
        return false;
    }

    public void dispose() {
        textures.values().forEach((texture) -> {
            texture.getTexture().dispose();
        });
        sounds.values().forEach(Sound::dispose);
        stopAllMusic();
        music.values().forEach(Music::dispose);
        fonts.values().forEach(FreeTypeFontGenerator::dispose);
        generatedFonts.values().forEach(BitmapFont::dispose);
        shaders.values().forEach(ShaderProgram::dispose);
    }

    public void stopAllMusic() {
        music.values().forEach(Music::stop);
    }
}
