package com.complover116.cvmproject.cvmengine.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.complover116.cvmproject.cvmengine.client.ui.UIInputProcessor;
import com.complover116.cvmproject.cvmengine.client.ui.UISystem;
import com.complover116.cvmproject.cvmengine.shared.Config;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Utility;
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk;
import com.complover116.cvmproject.cvmengine.shared.world.Sector;
import org.luaj.vm2.LuaValue;


public class RunningScreen implements Screen {

    CVMControl engine;
    long lastUpdate = System.nanoTime();
    UISystem uiSystem;

    float refreshRate;

    volatile public float clientLoad = 0;
    volatile public float clientLoadLongTerm = 0;

    private final Utility.AveragedTimer updateTime = new Utility.AveragedTimer();

    float dim = 50;
    long lastSentSectorRequest = System.nanoTime();
    // World currentWord;
    RunningScreen(CVMControl engine) {
        this.engine = engine;
        uiSystem = new UISystem();
        uiSystem.add(engine.debugOverlay);
        uiSystem.add(engine.debugPointer);
        Gdx.input.setInputProcessor(new UIInputProcessor(engine, uiSystem));
        refreshRate = Gdx.graphics.getDisplayMode().refreshRate;
    }
    @Override
    public void show() {
        dim = 50f;
    }

    @Override
    public void render(float delta) {
        updateTime.start();
        // Process console commands
        engine.commandProcessor.processQueue();
        // NETWORK IN
        engine.netTime.start();

        if (engine.world != null && engine.netClient != null && engine.netClient.connection != null) {
            if (!engine.netClient.connection.isAlive()) {
                engine.errorAndReset("Connection to server was lost - server stopped responding");
            }
            // Process the inbound queue
            synchronized(engine.inboundQueue) {
                engine.inboundQueue.removeIf(netDataChunk -> engine.netClient.netClientHandler.handle(netDataChunk, engine));
                if (engine.inboundQueue.size() > 100) {
                    Logger.log(Logger.LogType.WARN, "Inbound network queue overloaded, "+engine.inboundQueue.size()+" chunks still in queue!");
                }
            }
            // LERP Adjustment
            Config.INSTANCE.autoAdjustLerp(engine);
            // Send hot sector info
            if (lastSentSectorRequest < System.nanoTime() - 500000000) {
                lastSentSectorRequest = System.nanoTime();
                for (Sector sector : engine.world.getActiveSectors()) {
                    new NetDataChunk.ChunkSectorRequest(sector.getX(), sector.getY(), true).enqueue(engine.netClient.connection, true);
                }
            }
        }

        engine.netTime.end();

        engine.updateTime.start();
        // UI UPDATE
        uiSystem.think(
                Gdx.graphics.getDeltaTime(),
                engine.renderer.uiViewport.getWorldWidth(),
                engine.renderer.uiViewport.getWorldHeight()
        );
        // PLAYER INPUT
        if (engine.world != null && engine.world.hasPlayer(engine.world.localPlayerID) && !engine.isEditing) {
            PlayerClient localPlayer = ((PlayerClient) engine.world.getPlayerByID(engine.world.localPlayerID));
            engine.luaSystem.libInputClient.updateInputs(localPlayer);
            new NetDataChunk.ChunkPlayerInput(localPlayer, engine.netClient.netStringRegistry).enqueue(engine.netClient.connection, true);
        }
        if (engine.world != null) {
            synchronized (engine.world) {
                // WORLD UPDATE
                engine.world.update(delta);
                lastUpdate = System.nanoTime();
                engine.luaSystem.libHook.call("Think"); // TODO: Slow this down if can't keep up
                engine.luaSystem.libHook.call("Tick", LuaValue.valueOf(delta));

                engine.soundManager.update(engine.world.curTime);
            }
        }

        engine.luaSystem.libHook.call("UITick", LuaValue.valueOf(delta));
        engine.luaSystem.libHook.call("UIThink");
        engine.updateTime.end();
        // RENDERING
        engine.drawTime.start();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
        if (engine.world != null) {
            synchronized (engine.world) {
                engine.luaSystem.libHook.call("CameraSetup");
                engine.renderer.prepareForWorld();
                // Keep sectors that we are looking at drawn
                engine.world.loadArea(
                        engine.renderer.worldViewport.getCamera().position.x,
                        engine.renderer.worldViewport.getCamera().position.y,
                        Math.max(engine.renderer.worldViewport.getWorldHeight(), engine.renderer.worldViewport.getWorldWidth())/2f
                );

                // World rendering
                engine.world.draw();

                if (engine.debugOverlay.drawSectorsLocal) {
                    engine.world.drawDebug(engine.renderer);
                }

                if (engine.listenServer != null && engine.listenServer.netServer.isReady()) {
                    if (engine.debugOverlay.drawSectors) {
                        engine.listenServer.world.drawDebug(engine.renderer);
                    }
                    if (engine.debugOverlay.drawPhysics) {
                        engine.listenServer.world.drawPhysicsDebug(engine.renderer);
                    }
                    if (engine.debugOverlay.drawServerEntityDebug) {
                        engine.listenServer.world.drawServerEntityDebug(engine.renderer);
                    }
                }
            }
        }
        // Lua HUD rendering
        engine.renderer.prepareForUI();
        engine.luaSystem.libHook.call("UIDraw", LuaValue.valueOf(delta));
        uiSystem.render(engine);
        if (dim > 0) {
            dim -= delta*50;
            engine.renderer.setColor(0, 0, 0, Math.max(0, Math.min(dim/50, 1)));
            engine.renderer.drawRect(engine.renderer.getUILeft(), engine.renderer.getUIBottom(),
                    engine.renderer.uiViewport.getWorldWidth(), engine.renderer.uiViewport.getWorldHeight());
        }

        engine.drawTime.end();
        updateTime.end();
        clientLoadLongTerm += (clientLoad - clientLoadLongTerm)*0.01f;
        clientLoad = updateTime.getAvg()/(1000f/refreshRate)*100f;
    }

    @Override
    public void resize(int width, int height) {
        
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        lastUpdate = System.nanoTime() - 10000000;
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
