package com.complover116.cvmproject.cvmengine.client.net;

import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.shared.Config;
import com.complover116.cvmproject.cvmengine.shared.ILoadable;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.net.*;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class NetClient implements Runnable, ILoadable {
    public NetConnection connection;
    public long lastReceivedActiveEntity = 0;
    volatile boolean shouldRun = false;
    public volatile boolean ready = false;

    NetRemoteDestination remote;
    NetReceiver receiver;
    CVMControl engine;
    Thread netClientThread;
    public NetStringRegistry netStringRegistry = new NetStringRegistry(null);
    public final NetClientHandler netClientHandler = new NetClientHandler(this);
    public ModuleDownloader moduleDownloader;

    public int expectedNetStringCount = -1;
    public int expectedActiveEntityCount = -1;
    public int receivedActiveEntitiesDuringHandshake = 0;
    public enum ConnectionState {
        STATE_PURGING, STATE_UNINITIALIZED, STATE_AWAITING_STEAM, STATE_MULTICAST_SCANNING, STATE_RETRIEVING_INFO, STATE_BEGINNING, STATE_AWAITING_CONFIRMATION, STATE_REQUESTING_NETSTRINGS, STATE_RECEIVING_NETSTRINGS, STATE_RECEIVING_SNAPSHOT, STATE_RECEIVING_LOCAL_PLAYER, STATE_FINALIZING, STATE_CONNECTED
    }

    public ConnectionState connectionState = ConnectionState.STATE_PURGING;

    public NetClient(NetRemoteDestination destination, CVMControl engine) throws IOException {
        if (destination instanceof NetRemoteDestination.SteamIdDestination) {
            receiver = new NetReceiver(null, engine.steamAPI, NetReceiver.CLIENT_CHANNEL);
        } else {
            receiver = new NetReceiver(null, null, NetReceiver.CLIENT_CHANNEL);
        }

        if (destination instanceof NetRemoteDestination.SocketAddressDestination) {
            NetRemoteDestination.SocketAddressDestination socketAddressDestination = (NetRemoteDestination.SocketAddressDestination) destination;
            if (socketAddressDestination.getDatagramChannel() == null) {
                socketAddressDestination.setDatagramChannel(receiver.getReceiverUDP());
            }
        }
        remote = destination;
        this.engine = engine;
    }
    public void start() {
        shouldRun = true;
        netClientThread = new Thread(this, "Client Network Thread");
        netClientThread.setDaemon(true);
        netClientThread.start();
    }
    public void stopAndWait() {
        shouldRun = false;
        ready = false;
        Logger.log(Logger.LogType.INFO, "Waiting for NetClient to stop...");
        try {
            netClientThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
	@Override
	public void run() {
        Thread.currentThread().setUncaughtExceptionHandler(Logger.globalExceptionHandler);
        Logger.log(LogType.INFO, "Client network thread started");

        connection = new NetConnection(remote);

        float packetTime = 1/NetConstants.PACKET_RATE;
        float time = 0;
        long ctime = System.nanoTime();
        ready = true;
		while (shouldRun) {
            time += (System.nanoTime() - ctime)/1000000000f;
            ctime = System.nanoTime();
            try {
                if (time > packetTime) {
                    time = 0;
                    if (!connection.update()) {
                        //TODO: call OnDisconnect on client
                        shouldRun = false;
                    }
                }

                NetIncomingMessage message = receiver.receive();
                if (message == null) {
                    Thread.sleep(0, 100000);
                    continue;
                }

                NetPacket packet_in_processed = connection.receivePacket(message.getData(), netStringRegistry);
                synchronized(engine.inboundQueue) {
                    boolean dropUnimportant = false;
                    if (engine.isPaused) dropUnimportant = true;
                    if (System.nanoTime() > engine.lastUpdate + 1000000000) dropUnimportant = true;
                    boolean finalDropUnimportant = dropUnimportant;
                    packet_in_processed.dataChunks.forEach((it) -> {
                        if (!finalDropUnimportant || it instanceof NetDataChunk.NetDataChunkImportant) {
                            engine.inboundQueue.add(it);
                        }
                    });
                }
            } catch (SocketTimeoutException ee) {
                //pass
            } catch (IOException e) {
                Logger.log(LogType.ERROR, "Failed receiving data from client socket");
                break;
            } catch (IllegalArgumentException e) {
                Logger.log(LogType.ERROR, "Unknown host:" + connection.remote.toString());
                shouldRun = false;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        ready = false;
        receiver.dispose();
        if (remote instanceof NetRemoteDestination.SocketAddressDestination) {
            NetRemoteDestination.SocketAddressDestination socketAddressDestination = (NetRemoteDestination.SocketAddressDestination) remote;
            socketAddressDestination.setDatagramChannel(null);
        }
        Logger.log(LogType.INFO, "Client thread stopped");
	}

    @Override
    public String getLoadStep() {
        switch (connectionState) {
            case STATE_PURGING:
                return "Purging old server messages...";
            case STATE_UNINITIALIZED:
                return "Starting Client Network Thread...";
            case STATE_AWAITING_STEAM:
                return "Initializing Steam networking...";
            case STATE_RETRIEVING_INFO:
                if (System.nanoTime() < connection.getLastReceived() + NetConstants.TIMEOUT_WARN) {
                    return "Requesting server info...";
                } else {
                    return "Requesting server info... (no response for "+(System.nanoTime() - connection.getLastReceived())/1000000000+"s)";
                }
            case STATE_BEGINNING:
                return "Requesting connection to server...";
            case STATE_AWAITING_CONFIRMATION:
                return "Awaiting server response...";
            case STATE_REQUESTING_NETSTRINGS:
                return "Requesting NetStrings...";
            case STATE_RECEIVING_NETSTRINGS:
                return "Receiving NetStrings...";
            case STATE_RECEIVING_SNAPSHOT:
                return "Receiving initial world snapshot...";
            case STATE_RECEIVING_LOCAL_PLAYER:
                return "Receiving local player data...";
            case STATE_FINALIZING:
                return "Finalizing...";
            case STATE_CONNECTED:
                return "Connected!";
        }
        return "ERROR";
    }

    @Override
    public String getLoadStepLow() {
        switch (connectionState) {
            case STATE_RECEIVING_NETSTRINGS:
                return "Receiving network strings: [" + netStringRegistry.count() + "/" + expectedNetStringCount + "]";
            case STATE_RECEIVING_SNAPSHOT:
                return "Receiving entities: [" + receivedActiveEntitiesDuringHandshake + "/" + expectedActiveEntityCount + "]";
        }
        return "";
    }

    @Override
    public String getLoadStepHigh() {
        return "Connecting to "+remote;
    }

    @Override
    public boolean loadTick() {
        synchronized(engine.inboundQueue) {
            engine.inboundQueue.removeIf(netDataChunk -> netClientHandler.handle(netDataChunk, engine));
        }

        if (connectionState != ConnectionState.STATE_UNINITIALIZED && connectionState != ConnectionState.STATE_PURGING && connectionState != ConnectionState.STATE_AWAITING_STEAM && !shouldRun) {
            engine.errorAndReset("No response from server");
        }

        switch (connectionState) {
            case STATE_PURGING:
                NetIncomingMessage message = receiver.receive();
                if (message == null) {
                    if (remote instanceof NetRemoteDestination.SteamIdDestination) {
                        connectionState = ConnectionState.STATE_AWAITING_STEAM;
                        engine.steamAPI.initRelayNetworkAccess();
                    } else {
                        connectionState = ConnectionState.STATE_UNINITIALIZED;
                    }
                }
                break;
            case STATE_AWAITING_STEAM:
                if (engine.steamAPI.getRelayNetworkStatus()) {
                    connectionState = ConnectionState.STATE_UNINITIALIZED;
                }
                break;
            case STATE_UNINITIALIZED:
                if (!shouldRun)
                    start();
                if (ready) {
                    connectionState = ConnectionState.STATE_RETRIEVING_INFO;
                    new NetDataChunk.ChunkServerRequest(connection, NetDataChunk.ChunkServerRequest.STATE_INFO).enqueue(connection, false);
                }
                break;
            case STATE_RETRIEVING_INFO:
                break;
            case STATE_BEGINNING:
                LuaValue identity = engine.clientDataProvider.readLuaValue("identity");
                new NetDataChunk.ChunkConnectionRequest(connection, identity.get("permanentName").checkjstring(), identity.get("username").checkjstring()).enqueue(connection, false);
                connectionState = ConnectionState.STATE_AWAITING_CONFIRMATION;
                break;
            case STATE_REQUESTING_NETSTRINGS:
                new NetDataChunk.ChunkServerRequest(connection, NetDataChunk.ChunkServerRequest.STATE_REQUEST_NETSTRINGS).enqueue(connection, false);
                connectionState = ConnectionState.STATE_RECEIVING_NETSTRINGS;
                break;
            case STATE_AWAITING_CONFIRMATION:
                break;
            case STATE_RECEIVING_NETSTRINGS:
                break;
            case STATE_RECEIVING_SNAPSHOT:
                if (System.nanoTime() - lastReceivedActiveEntity > 1000000000) {
                    connectionState = ConnectionState.STATE_RECEIVING_LOCAL_PLAYER;
                }
                break;
            case STATE_RECEIVING_LOCAL_PLAYER:
                if (engine.world.hasPlayer(engine.world.localPlayerID)) {
                    connectionState = NetClient.ConnectionState.STATE_FINALIZING;
                    new NetDataChunk.ChunkServerRequest(connection, NetDataChunk.ChunkServerRequest.STATE_FINISH_CONNECTION).enqueue(connection, false);
                }
                break;
            case STATE_FINALIZING:
                break;
            case STATE_CONNECTED:
                new NetDataChunk.ChunkPlayerOptions(Config.INSTANCE.getClientInterpolationDelay()).enqueue(connection, false);
                engine.luaSystem.libHook.call("Connected");
                Logger.log(LogType.WARN, "CONNECTED");
                return true;
        }
        return false;
    }
}
