package com.complover116.cvmproject.cvmengine.client.net

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.ILoadable
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.ModuleManager
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk
import java.security.MessageDigest
import java.util.*

class ModuleDownloader(val engine: CVMControl, val modulesToTransfer: MutableList<NetDataChunk.ChunkServerModuleInfo.ModuleInfo>, val modulesToKeep: MutableList<ModuleManager.Module>): ILoadable {
    private val downloadsPath: FileHandle = engine.moduleManager.VFS.cacheDir.child("downloads")

    var currentModuleFileCount = -1;
    private val currentModuleFileList = LinkedList<String>();

    private val transferredModules = LinkedList<NetDataChunk.ChunkServerModuleInfo.ModuleInfo>()

    private val fileData = HashMap<Int, ByteArray>()
    private var currentFileChunkCount = -1

    enum class TransferState {
        REQUESTING_FILE_LIST, RECEIVING_FILE_LIST, REQUESTING_FILE, RECEIVING_FILE
    }

    private var transferState: TransferState = TransferState.REQUESTING_FILE_LIST

    override fun getLoadStep(): String {
        return when(transferState) {
            TransferState.REQUESTING_FILE_LIST -> if (modulesToTransfer.isEmpty()) "Finalizing..." else "Requesting file list for ${modulesToTransfer[0].moduleID}"
            TransferState.RECEIVING_FILE_LIST -> "Receiving file list for ${modulesToTransfer[0].moduleID}"
            TransferState.REQUESTING_FILE, TransferState.RECEIVING_FILE -> "Receiving files for ${modulesToTransfer[0].moduleID} [${currentModuleFileCount - currentModuleFileList.size}/$currentModuleFileCount]"
        }
    }

    override fun getLoadStepLow(): String {
        return when(transferState) {
            TransferState.REQUESTING_FILE_LIST -> ""
            TransferState.RECEIVING_FILE_LIST -> "Receiving file list [${currentModuleFileList.size}/$currentModuleFileCount]"
            TransferState.REQUESTING_FILE -> "Requesting ${currentModuleFileList[0]}"
            TransferState.RECEIVING_FILE -> "Transferring ${currentModuleFileList[0]} [${fileData.size}/$currentFileChunkCount]"
        }
    }

    override fun getLoadStepHigh(): String {
        return "Transferring module data"
    }

    override fun loadTick(): Boolean {
        synchronized(engine.inboundQueue) {
            engine.inboundQueue.removeIf { netDataChunk: NetDataChunk? ->
                engine.netClient.netClientHandler.handle(
                    netDataChunk,
                    engine
                )
            }
        }

        if (!engine.netClient.shouldRun) {
            engine.errorAndReset("Server stopped responding during transfer")
        }

        when(transferState) {
            TransferState.REQUESTING_FILE_LIST -> {
                if (modulesToTransfer.isEmpty()) {
                    val allModules = LinkedList<FileHandle>()
                    allModules.addAll(modulesToKeep.map { module -> module.moduleDir }.toList())
                    allModules.addAll(transferredModules.map { moduleInfo -> downloadsPath.child(moduleInfo.moduleID) })
                    val newParams = ModuleManager.ModuleManagerParams(emptyArray(), true, allModules, LinkedList())
                    engine.queueFullReload(false, newParams)
                    engine.netClient.connectionState = NetClient.ConnectionState.STATE_REQUESTING_NETSTRINGS
                    return true
                }
                Logger.log(Logger.LogType.INFO, "Requesting details for ${modulesToTransfer[0].moduleID}")
                NetDataChunk.ChunkModuleDetailsRequest(engine.netClient.connection, modulesToTransfer[0].moduleID).enqueue(engine.netClient.connection, false)
                currentModuleFileCount = -1;
                transferState = TransferState.RECEIVING_FILE_LIST
            }
            TransferState.REQUESTING_FILE -> {
                if (currentModuleFileList.isEmpty()) {
                    transferState = TransferState.REQUESTING_FILE_LIST
                    transferredModules.add(modulesToTransfer.removeAt(0))
                    return false
                }
                Logger.log(Logger.LogType.INFO, "Requesting file ${currentModuleFileList[0]}")
                NetDataChunk.ChunkFileRequest(engine.netClient.connection, currentModuleFileList[0], modulesToTransfer[0].moduleID).enqueue(engine.netClient.connection, false)
                transferState = TransferState.RECEIVING_FILE
            }
            TransferState.RECEIVING_FILE_LIST -> {}
            TransferState.RECEIVING_FILE -> {}
        }
        return false
    }

    fun onReceiveModuleDetails(chunkServerModuleDetails: NetDataChunk.ChunkServerModuleDetails) {
        if (transferState != TransferState.RECEIVING_FILE_LIST) return
        if (chunkServerModuleDetails.moduleName == modulesToTransfer[0].moduleID) {
            if (chunkServerModuleDetails.fileCount == -1) engine.errorAndReset("Server refused to transfer file list for "+modulesToTransfer[0].moduleID)
            if (currentModuleFileCount == -1)
                currentModuleFileCount = chunkServerModuleDetails.fileCount

            val destination = downloadsPath.child(modulesToTransfer[0].moduleID).child(chunkServerModuleDetails.filePath)
            var shouldDownload = true
            if (destination.exists()) {
                val digest = MessageDigest.getInstance("SHA-256")
                val hash = String(Base64.getEncoder().encode(digest.digest(destination.readBytes())))
                if (hash == chunkServerModuleDetails.fileHash) {
                    shouldDownload = false
                }
            }

            if (shouldDownload) {
                currentModuleFileList.add(chunkServerModuleDetails.filePath)
            } else {
                currentModuleFileCount--
            }
            if (currentModuleFileList.size == currentModuleFileCount) {
                transferState = TransferState.REQUESTING_FILE
                currentFileChunkCount = -1
                fileData.clear()
            }
        }
    }

    fun onReceiveFileData(chunkServerFileData: NetDataChunk.ChunkServerFileData) {
        if (transferState != TransferState.RECEIVING_FILE) return
        if (chunkServerFileData.filePath == currentModuleFileList[0]) {
            if (chunkServerFileData.filePath.contains("..")) {
                engine.errorAndReset("Server attempted to trick us into writing a file where we shouldn't!")
            }
            if (chunkServerFileData.totalParts == -1) engine.errorAndReset("Server refused to transfer file ${chunkServerFileData.filePath}")
            currentFileChunkCount = chunkServerFileData.totalParts
            fileData[chunkServerFileData.partID] = chunkServerFileData.filePartData
            if (fileData.size == currentFileChunkCount) {
                val target = downloadsPath.child(modulesToTransfer[0].moduleID).child(chunkServerFileData.filePath)
                target.parent().mkdirs()
                val outputStream = target.write(false)
                fileData.entries.stream().sorted{ entry1, entry2 ->
                    return@sorted entry1.key - entry2.key
                }.forEach {
                    outputStream.write(it.value)
                }
                outputStream.flush()
                outputStream.close()

                currentModuleFileList.removeAt(0)
                fileData.clear()
                currentFileChunkCount = -1
                transferState = TransferState.REQUESTING_FILE
            }
        }
    }
}