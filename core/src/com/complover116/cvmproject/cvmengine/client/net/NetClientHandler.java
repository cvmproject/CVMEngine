package com.complover116.cvmproject.cvmengine.client.net;

import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.client.PlayerClient;
import com.complover116.cvmproject.cvmengine.client.world.EntityClient;
import com.complover116.cvmproject.cvmengine.client.world.WorldClient;
import com.complover116.cvmproject.cvmengine.shared.Config;
import com.complover116.cvmproject.cvmengine.shared.Logger;
import com.complover116.cvmproject.cvmengine.shared.Logger.LogType;
import com.complover116.cvmproject.cvmengine.shared.ModuleManager;
import com.complover116.cvmproject.cvmengine.shared.Player;
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import com.complover116.cvmproject.cvmengine.shared.world.EntityState;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class NetClientHandler {
    private final HashMap<Integer, List<UnappliedUpdate>> unappliedEntityUpdates = new HashMap<>();

    private static class UnappliedUpdate {
        double time;
        EntityState state;
        private UnappliedUpdate(double time, EntityState state) {
            this.time = time;
            this.state = state;
        }
    }
    private final NetClient netClient;
    public NetClientHandler(NetClient netClient) {
        this.netClient = netClient;
    }
    private interface Handler {
        boolean handle(NetDataChunk chunk, CVMControl engine);
    }
    private final HashMap<Class, Handler> dispatch = new HashMap<>();
    {
        dispatch.put(NetDataChunk.ChunkNetString.class, new Handler() {
			@Override
			public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkNetString chunkNetString = (NetDataChunk.ChunkNetString) chunk;
//                Logger.log(LogType.DEBUG, "Received string '"+ chunkNetString.string + "' with id "+chunkNetString.id);
                engine.netClient.netStringRegistry.update(chunkNetString.string, chunkNetString.id);
                if (netClient.connectionState == NetClient.ConnectionState.STATE_RECEIVING_NETSTRINGS) {
                    if (netClient.netStringRegistry.count() >= netClient.expectedNetStringCount) {
                        netClient.connectionState = NetClient.ConnectionState.STATE_RECEIVING_SNAPSHOT;
                        netClient.lastReceivedActiveEntity = System.nanoTime();
                        new NetDataChunk.ChunkServerRequest(netClient.connection, NetDataChunk.ChunkServerRequest.STATE_REQUEST_SNAPSHOT).enqueue(netClient.connection, false);
                    }
                }
                return true;
			}
        });
        dispatch.put(NetDataChunk.ChunkNewEntity.class, new Handler() {
			@Override
			public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkNewEntity chunkNewEntity = (NetDataChunk.ChunkNewEntity) chunk;
                String type = engine.netClient.netStringRegistry.intToString(chunkNewEntity.type);
                if (type == null) {
                    return false;
                }
                if (engine.world.doesEntityExist(chunkNewEntity.id)) {
                    Logger.log(LogType.WARN, "Ignoring duplicate command to create "+chunkNewEntity.type +" id "+chunkNewEntity.id);
                    return true;
                }
                if (chunkNewEntity.initialState == null) {
//                    Logger.log(LogType.WARN, "Delaying clientside creation of entity "+chunkNewEntity.id+" of type "+type+": initial state has not arrived");
                    return false;
                }
                if (engine.world.curTime < chunkNewEntity.creationTime) return false;
                EntityClient newEntity = null;
                if (newEntity == null) {
//                    Logger.log(LogType.DEBUG, "Creating new "+chunkNewEntity.type +" id "+chunkNewEntity.id);
                    newEntity = (EntityClient) engine.luaSystem.entitySystem.createEntity(type);
                    engine.world.registerEntity(newEntity, chunkNewEntity.id, chunkNewEntity.newlySpawned);
                }
                newEntity.curState.applyDelta(chunkNewEntity.initialState);
                newEntity.lastReceivedState = newEntity.curState;
                if (unappliedEntityUpdates.containsKey(chunkNewEntity.id)) {
//                    Logger.log(LogType.DEBUG, "Applying unapplied update to fresh entity "+chunkNewEntity.id);
                    for (UnappliedUpdate update : unappliedEntityUpdates.get(chunkNewEntity.id)) {
                        newEntity.update(update.time, update.state);
                    }
                    newEntity.clientsideTick(engine.world.curTime, engine);
                    unappliedEntityUpdates.remove(chunkNewEntity.id);
                }
                if (chunkNewEntity.newlySpawned) newEntity.spawn();
                if (netClient.connectionState == NetClient.ConnectionState.STATE_RECEIVING_SNAPSHOT) {
                    netClient.receivedActiveEntitiesDuringHandshake ++;
                    netClient.lastReceivedActiveEntity = System.nanoTime();
                    if (netClient.receivedActiveEntitiesDuringHandshake >= netClient.expectedActiveEntityCount) {
                        netClient.connectionState = NetClient.ConnectionState.STATE_RECEIVING_LOCAL_PLAYER;
                    }
                }

                return true;
			}
        });
        dispatch.put(NetDataChunk.ChunkEntityDeath.class, new Handler() {
			@Override
			public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkEntityDeath chunkEntityDeath = (NetDataChunk.ChunkEntityDeath) chunk;
                if (engine.world.curTime < chunkEntityDeath.removalTime) return false;
                if (engine.world.doesEntityExist(chunkEntityDeath.id)) { 
//                    Logger.log(LogType.DEBUG, "Deleting entity "+chunkEntityDeath.id);
                    engine.world.getEntityByID(chunkEntityDeath.id).isDead = true;
                } else {
                    Logger.log(LogType.WARN, "Delaying deletion of entity "+chunkEntityDeath.id+": not yet created");
                    return false;
                }
                return true;
			}
        });
        dispatch.put(NetDataChunk.ChunkNetMessage.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkNetMessage chunkNetMessage = (NetDataChunk.ChunkNetMessage) chunk;
                if (chunkNetMessage.message == null || chunkNetMessage.message.getName() == null) return false;

                return engine.luaSystem.libNetClient.handle(chunkNetMessage.message);
            }
        });
        dispatch.put(NetDataChunk.ChunkWorldUpdate.class, new Handler() {
			@Override
			public boolean handle(NetDataChunk chunk, CVMControl engine) {
			    if (!engine.netClient.ready || engine.world == null) return true;
//                if (engine.world == null) return true;
                NetDataChunk.ChunkWorldUpdate chunkWorldUpdate = (NetDataChunk.ChunkWorldUpdate) chunk;
                engine.world.offset = (chunkWorldUpdate.time) - engine.world.curTime - Config.INSTANCE.getClientInterpolationDelay();
                engine.world.lastReceivedTime = Math.max(chunkWorldUpdate.time, engine.world.lastReceivedTime);
//                if (chunkWorldUpdate.time < engine.world.curTime) return true;
                for (NetDataChunk.ChunkWorldUpdate.EntityUpdate update : chunkWorldUpdate.entityUpdates) {
                    if (update.deltaState == null) {
                        continue;
                    }
                    if (!engine.world.doesEntityExist(update.id)) {
//                        Logger.log(LogType.DEBUG, "Received update for unknown entity "+update.id+", will apply when entity info arrives");
                        if (!unappliedEntityUpdates.containsKey(update.id)) {
                            unappliedEntityUpdates.put(update.id, new LinkedList<>());
                        }
                        unappliedEntityUpdates.get(update.id).add(new UnappliedUpdate(chunkWorldUpdate.time, update.deltaState));
                        continue;
                    }

                    EntityClient entityClient = ((EntityClient) engine.world.getEntityByID(update.id));
                    entityClient.update(chunkWorldUpdate.time, update.deltaState);
                }
                if (chunkWorldUpdate.lastProcessedKeyState > engine.lastProcessedKeyStateByRemote && chunkWorldUpdate.lastProcessedKeyState < Long.MAX_VALUE - 1000)
                    engine.lastProcessedKeyStateByRemote = chunkWorldUpdate.lastProcessedKeyState;
                return true;
			}
        });
        dispatch.put(NetDataChunk.ChunkGameInfo.class, new Handler() {
			@Override
			public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkGameInfo chunkGameInfo = (NetDataChunk.ChunkGameInfo) chunk;
                if (!engine.world.doesEntityExist(chunkGameInfo.playerDataEntityID)) {
                    return false;
                }
                engine.world.curTime = chunkGameInfo.curTime;
                engine.world.localPlayerID = chunkGameInfo.playerID;
                engine.world.localPlayerDataEntity = (EntityClient) engine.world.getEntityByID(chunkGameInfo.playerDataEntityID);
                if (chunkGameInfo.isEditing) {
                    engine.luaSystem.libHook.disableAll();
                    engine.isEditing = true;
                    engine.debugPointer.setEnabled(true);
                    engine.world.isSuspended = true;
                    engine.world.curTime = 10;
                } else {
                    engine.luaSystem.libHook.enableAll();
                    engine.isEditing = false;
                    engine.debugPointer.setEnabled(false);
                    engine.world.isSuspended = false;
                }
                netClient.expectedActiveEntityCount = chunkGameInfo.activeEntityCount;
                return true;
			}
        });
        dispatch.put(NetDataChunk.ChunkPlayerInput.class, new Handler() {
                @Override
                public boolean handle(NetDataChunk chunk, CVMControl engine) {
                    return true;
//                    NetDataChunk.ChunkPlayerInput chunkPlayerInput = (NetDataChunk.ChunkPlayerInput) chunk;
//                    PlayerClient ply = (PlayerClient) engine.world.getPlayerByID(chunkPlayerInput.playerID);
//                    if (ply != null)
//                        ply.input.applyDelta(chunkPlayerInput.playerInput);
//                    return true;
                }
            }
        );
        dispatch.put(NetDataChunk.ChunkPlayerInfo.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkPlayerInfo chunkGamePlayerInfo = (NetDataChunk.ChunkPlayerInfo) chunk;
                if (engine.world == null || !engine.world.doesEntityExist(chunkGamePlayerInfo.dataEntityID)) {
                    return false;
                }
                if (!engine.world.hasPlayer(chunkGamePlayerInfo.id)) {
                    PlayerClient playerClient = new PlayerClient(""+chunkGamePlayerInfo.id, chunkGamePlayerInfo.username, chunkGamePlayerInfo.dataEntityID, engine.luaSystem.luaTypes, chunkGamePlayerInfo.state == Player.STATE_BOT);
                    playerClient.input.applyDelta(engine.luaSystem.libInputClient.getDefaultInputState());
                    engine.world.registerPlayer(playerClient, chunkGamePlayerInfo.id);
                    Logger.log(LogType.INFO, "Creating player "+chunkGamePlayerInfo.id+":"+chunkGamePlayerInfo.username);
                }
                PlayerClient ply = (PlayerClient) engine.world.getPlayerByID(chunkGamePlayerInfo.id);
                ply.setPing(chunkGamePlayerInfo.ping);
                ply.state = chunkGamePlayerInfo.state;
                ply.isAdmin = chunkGamePlayerInfo.isAdmin;
//                Logger.log(LogType.INFO, "Updating player "+chunkGamePlayerInfo.id+" with state "+ply.state);
                return true;
            }
        });
        dispatch.put(NetDataChunk.ChunkServerConnectionResponse.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkServerConnectionResponse chunkServerConnectionResponse = (NetDataChunk.ChunkServerConnectionResponse) chunk;
                netClient.expectedActiveEntityCount = chunkServerConnectionResponse.activeEntityCount;
                netClient.expectedNetStringCount = chunkServerConnectionResponse.netStringCount;
                if (chunkServerConnectionResponse.responseType == NetDataChunk.ChunkServerConnectionResponse.STATE_CONNECTION_BEGUN && engine.netClient.connectionState == NetClient.ConnectionState.STATE_AWAITING_CONFIRMATION) {
                    engine.world = new WorldClient(engine, true);
                    engine.luaSystem.initClientLibs(engine, true);
                    if (netClient.moduleDownloader != null) {
                        engine.downloadModules();
                    } else {
                        engine.queueFullReload(false, null);
                        netClient.connectionState = NetClient.ConnectionState.STATE_REQUESTING_NETSTRINGS;
                    }
                }
                if (chunkServerConnectionResponse.responseType == NetDataChunk.ChunkServerConnectionResponse.STATE_CONNECTION_FINISHED) {
                    netClient.connectionState = NetClient.ConnectionState.STATE_CONNECTED;
                }
                if (chunkServerConnectionResponse.responseType == NetDataChunk.ChunkServerConnectionResponse.STATE_RELOADING) {
                    engine.disconnect(true, null);
                }
                return true;
            }
        });
        dispatch.put(NetDataChunk.ChunkServerModuleInfo.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                if (netClient.connectionState != NetClient.ConnectionState.STATE_RETRIEVING_INFO) return true;
                NetDataChunk.ChunkServerModuleInfo chunkServerModuleInfo = (NetDataChunk.ChunkServerModuleInfo) chunk;
                netClient.connection.forceAckReceival();
                List<ModuleManager.Module> modulesToKeep = new LinkedList<>();
                List<NetDataChunk.ChunkServerModuleInfo.ModuleInfo> modulesToDownload = new LinkedList<>();
                for (NetDataChunk.ChunkServerModuleInfo.ModuleInfo moduleInfo : chunkServerModuleInfo.moduleInfos) {
                    Optional<ModuleManager.Module> matchingLoadedModule = engine.moduleManager.modules.stream().filter(module -> module.id.equals(moduleInfo.moduleID)).findAny();
                    if (matchingLoadedModule.isEmpty()) {
                        modulesToDownload.add(moduleInfo);
                        Logger.log(LogType.WARN, "Required module is not loaded: "+moduleInfo.moduleID);
                        continue;
                    }
                    if (!matchingLoadedModule.get().hash.equals(moduleInfo.moduleHash)) {
                        modulesToDownload.add(moduleInfo);
                        Logger.log(LogType.WARN, "Module hash mismatch for "+moduleInfo.moduleID +" - OURS:"+matchingLoadedModule.get().hash + " THEIRS:"+moduleInfo.moduleHash);
                        continue;
                    }

                    Logger.log(LogType.INFO, "Module "+moduleInfo.moduleID + " matches local module");
                    modulesToKeep.add(matchingLoadedModule.get());
                }
                netClient.connectionState = NetClient.ConnectionState.STATE_BEGINNING;
                if (!modulesToDownload.isEmpty()) {
                    Logger.log(LogType.WARN, modulesToDownload.size()+" modules need downloading");
                    netClient.moduleDownloader = new ModuleDownloader(engine, modulesToDownload, modulesToKeep);
                } else {
                    Logger.log(LogType.INFO, "All "+chunkServerModuleInfo.moduleInfos.size()+" required modules match, continuing");
//                    engine.errorAndReset("No response from server");
                }
                return true;
            }
        });
        dispatch.put(NetDataChunk.ChunkServerModuleDetails.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkServerModuleDetails chunkServerModuleDetails = (NetDataChunk.ChunkServerModuleDetails) chunk;
                netClient.moduleDownloader.onReceiveModuleDetails(chunkServerModuleDetails);
                return true;
            }
        });

        dispatch.put(NetDataChunk.ChunkSoundEvent.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkSoundEvent chunkSoundEvent = (NetDataChunk.ChunkSoundEvent) chunk;

                // Delay new sound event if the NetStringRegistry hasn't received the sound name yet
                if (chunkSoundEvent.event.isNewSound() && chunkSoundEvent.event.getSoundName() == null) {
                    return false;
                }
                engine.soundManager.receiveSoundEvent(chunkSoundEvent.event);
                return true;
            }
        });
        dispatch.put(NetDataChunk.ChunkServerFileData.class, new Handler() {
            @Override
            public boolean handle(NetDataChunk chunk, CVMControl engine) {
                NetDataChunk.ChunkServerFileData chunkServerFileData = (NetDataChunk.ChunkServerFileData) chunk;
                netClient.moduleDownloader.onReceiveFileData(chunkServerFileData);
                return true;
            }
        });
    }
    public boolean handle(NetDataChunk chunk, CVMControl engine) {
        Handler h = dispatch.get(chunk.getClass());
        if (h == null) {
            Logger.log(LogType.WARN, "Unhandled NetDataChunk type: "+chunk.getClass());
            return true;
        } else {
            boolean handled = h.handle(chunk, engine);
            if (!handled && chunk instanceof NetDataChunk.NetDataChunkImportant) {
                ((NetDataChunk.NetDataChunkImportant) chunk).reDecode();
            }
            return handled;
        }
    }
}
