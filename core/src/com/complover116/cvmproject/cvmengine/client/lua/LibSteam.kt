package com.complover116.cvmproject.cvmengine.client.lua

import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.lua.LuaLib
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.TwoArgFunction
import org.luaj.vm2.lib.ZeroArgFunction

class LibSteam(system: LuaSystem, val engine: CVMControl) : LuaLib(system) {
    override fun setUp(table: LuaTable): String {
        table["SetRichPresence"] = object: TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                engine.steamAPI.setRichPresenceKey(arg1.checkjstring(), arg2.checkjstring())
                return NIL
            }
        }
        table["EnableSteamJoins"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                engine.steamAPI.enableSteamJoins()
                return NIL
            }
        }
        table["DisableSteamJoins"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                engine.steamAPI.disableSteamJoins()
                return NIL
            }
        }
        return "steam"
    }
}
