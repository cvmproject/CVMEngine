package com.complover116.cvmproject.cvmengine.client.lua

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.controllers.Controllers
import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.client.PlayerClient
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.lua.LibInput
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.ThreeArgFunction
import org.luaj.vm2.lib.TwoArgFunction
import java.util.*

class LibInputClient(system: LuaSystem, var engine: CVMControl) : LibInput(system) {
    val bindings = LinkedList<InputBinding>()

    fun getPointerPos(arg: LuaValue, isWorld: Boolean): LuaValue {
        var pos: Vector2 = if (arg.isint()) {
            val pointer = arg.checkint()
            Vector2(
                Gdx.input.getX(pointer).toFloat(),
                Gdx.input.getY(pointer).toFloat()
            )
        } else {
            Vector2(
                Gdx.input.x.toFloat(),
                Gdx.input.y.toFloat()
            )
        }
        pos = if (isWorld) {
            engine.renderer.worldViewport.unproject(pos)
        } else {
            engine.renderer.uiViewport.unproject(pos)
        }
        if (pos.x.isNaN() || pos.y.isNaN()) return system.luaTypes.createVector(0.0, 0.0)
        return system.luaTypes.createVector(
            pos.x.toDouble(), pos.y.toDouble()
        )
    }

    override fun setUp(lib: LuaTable): String {
        super.setUp(lib)
        lib["IsButtonDown"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return valueOf(Gdx.input.isButtonPressed(arg.checkint()))
            }
        }
        lib["WasButtonPressed"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return valueOf(Gdx.input.isButtonJustPressed(arg.checkint()))
            }
        }
        lib["UIPointerPos"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return getPointerPos(arg, false)
            }
        }
        lib["WorldPointerPos"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return getPointerPos(arg, true)
            }
        }
        lib["IsTouched"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return if (arg.isint()) {
                    valueOf(Gdx.input.isTouched(arg.checkint()))
                } else {
                    valueOf(Gdx.input.isTouched)
                }
            }
        }
        lib["IsLMBPressed"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return valueOf(Gdx.input.isButtonPressed(Buttons.LEFT))
            }
        }
        lib["IsMMBPressed"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return valueOf(Gdx.input.isButtonPressed(Buttons.MIDDLE))
            }
        }
        lib["IsRMBPressed"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                return valueOf(Gdx.input.isButtonPressed(Buttons.RIGHT))
            }
        }
        lib["IsControllerButtonDown"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val controller = Controllers.getCurrent()
                return when (arg.checkjstring()) {
                    "A" -> valueOf(controller.getButton(controller.mapping.buttonA))
                    "B" -> valueOf(controller.getButton(controller.mapping.buttonB))
                    "X" -> valueOf(controller.getButton(controller.mapping.buttonX))
                    "Y" -> valueOf(controller.getButton(controller.mapping.buttonY))
                    "Down" -> valueOf(controller.getButton(controller.mapping.buttonDpadDown))
                    "Up" -> valueOf(controller.getButton(controller.mapping.buttonDpadUp))
                    "Left" -> valueOf(controller.getButton(controller.mapping.buttonDpadLeft))
                    "Right" -> valueOf(controller.getButton(controller.mapping.buttonDpadRight))
                    "L1" -> valueOf(controller.getButton(controller.mapping.buttonL1))
                    "L2" -> valueOf(controller.getButton(controller.mapping.buttonL2))
                    "R1" -> valueOf(controller.getButton(controller.mapping.buttonR1))
                    "R2" -> valueOf(controller.getButton(controller.mapping.buttonR2))
                    "Back" -> valueOf(controller.getButton(controller.mapping.buttonBack))
                    "Start" -> valueOf(controller.getButton(controller.mapping.buttonStart))
                    "LeftStick" -> valueOf(controller.getButton(controller.mapping.buttonLeftStick))
                    "RightStick" -> valueOf(controller.getButton(controller.mapping.buttonRightStick))
                    else -> error("Invalid button ${arg.checkjstring()}")
                }
            }
        }
        lib["GetControllerAxis"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val controller = Controllers.getCurrent() ?: return LuaValue.valueOf(0)
                return when (arg.checkjstring()) {
                    "LeftX" -> valueOf(controller.getAxis(controller.mapping.axisLeftX).toDouble())
                    "LeftY" -> valueOf(controller.getAxis(controller.mapping.axisLeftY).toDouble())
                    "RightX" -> valueOf(controller.getAxis(controller.mapping.axisRightX).toDouble())
                    "RightY" -> valueOf(controller.getAxis(controller.mapping.axisRightY).toDouble())
                    else -> error("Invalid axis ${arg.checkjstring()}")
                }
            }
        }
        lib["ControllerHaptic"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                if (Controllers.getCurrent() != null) {
                    Controllers.getCurrent().startVibration(arg1.checkint(), arg2.checkdouble().toFloat())
                }
                return NIL
            }
        }
        lib["Bind"] = object : ThreeArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue, arg3: LuaValue): LuaValue {
                val input = arg1.checkjstring()
                val bindingType = arg2.checkjstring()
                if (!defaultInputState.data.containsKey(input)) {
                    Logger.log(Logger.LogType.ERROR, "Attempted to bind to undefined input '$input'")
                    return NIL
                }
                val newBinding = when(bindingType) {
                    "Key" -> InputBindingKey(input, arg3.checkint(), system.luaTypes)
                    "ControllerButton" -> InputBindingControllerButton(input, arg3.checkint(), system.luaTypes)
                    else -> {
                        Logger.log(Logger.LogType.ERROR, "Attempted to create unknown binding type '$bindingType'")
                        return NIL
                    }
                }
                bindings.add(newBinding)
                return NIL
            }
        }
        return "input"
    }

    fun updateInputs(player: PlayerClient) {
        bindings.forEach { it.updateBoundInput(player) }
    }
}

abstract class InputBinding(val input: String, val luaTypes: LuaTypes) {
    var lastState: LuaValue? = null
    fun updateBoundInput(playerClient: PlayerClient) {
        val newValue = processBinding()
        if (newValue != lastState) {
            playerClient.input.data[input]!!.setFromLuaValue(newValue, luaTypes)
            lastState = newValue
        }
    }
    abstract fun processBinding(): LuaValue
}

class InputBindingKey(input: String, val key: Int, luaTypes: LuaTypes) : InputBinding(input, luaTypes) {
    override fun processBinding(): LuaValue {
        return LuaValue.valueOf(Gdx.input.isKeyPressed(key))
    }
}

class InputBindingControllerButton(input: String, val button: Int, luaTypes: LuaTypes) : InputBinding(input, luaTypes){
    override fun processBinding(): LuaValue {
        return if (Controllers.getCurrent() == null) {
            LuaValue.valueOf(false)
        } else {
            LuaValue.valueOf(Controllers.getCurrent().getButton(button))
        }
    }
}
