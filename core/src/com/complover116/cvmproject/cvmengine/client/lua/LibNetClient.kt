package com.complover116.cvmproject.cvmengine.client.lua

import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.lua.LibNetCommon
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk
import com.complover116.cvmproject.cvmengine.shared.net.NetStringRegistry
import com.complover116.cvmproject.cvmengine.shared.world.DataTable
import com.complover116.cvmproject.cvmengine.shared.world.World
import org.luaj.vm2.LuaTable
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.ZeroArgFunction

internal class LibNetClient(system: LuaSystem, var engine: CVMControl) : LibNetCommon(system) {
    override fun setUp(lib: LuaTable): String {
        super.setUp(lib)
        lib["GetNetworkString"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val i = arg.checkint()
                return valueOf(engine.netClient.netStringRegistry.intToString(i))
            }
        }
        lib["GetNetworkStringId"] = object : OneArgFunction() {
            override fun call(arg: LuaValue): LuaValue {
                val string = arg.checkjstring()
                return valueOf(engine.netClient.netStringRegistry.stringToInt(string))
            }
        }

        lib["Start"] = object : OneArgFunction() {
            override fun call(name: LuaValue): LuaValue {
                if (outgoingMessage != null) {
                    LuaValue.error("Tried to start a new network message before the old one got sent!")
                    return NIL
                }
                outgoingMessage = NetMessage(name.checkjstring(), engine.world.localPlayerID, DataTable())
                return NIL
            }
        }
        lib["SendToServer"] = object : ZeroArgFunction() {
            override fun call(): LuaValue {
                if (outgoingMessage == null) {
                    LuaValue.error("Tried to send a message before starting one!")
                    return NIL
                }
                NetDataChunk.ChunkNetMessage(engine.netClient.connection, outgoingMessage, engine.netClient.netStringRegistry).enqueue(engine.netClient.connection, true)
                outgoingMessage = null
                return NIL
            }
        }
        return "net"
    }

    override fun getWorld(): World = engine.world

    override fun getNetStringRegistry(): NetStringRegistry {
        return engine.netClient.netStringRegistry
    }

    override fun getPlayerReprByID(playerID: Int): LuaValue {
        if (engine.world.hasPlayer(playerID))
            return engine.world.getPlayerByID(playerID).repr
        else
            return LuaValue.NIL
    }
}
