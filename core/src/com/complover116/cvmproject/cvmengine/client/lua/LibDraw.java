package com.complover116.cvmproject.cvmengine.client.lua;

import com.badlogic.gdx.math.Vector2;
import com.complover116.cvmproject.cvmengine.client.Renderer;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaLib;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

public class LibDraw extends LuaLib {
    Renderer renderer;
    public LibDraw(LuaSystem system, Renderer renderer) {
        super(system);
        this.renderer = renderer;
    }
    @Override
    protected String setUp(LuaTable lib) {
        lib.set("SetColor", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                LuaTable color = args.arg(1).checktable();
                renderer.setColor(
                    (float) color.get("r").checkdouble()/255f,
                    (float) color.get("g").checkdouble()/255f,
                    (float) color.get("b").checkdouble()/255f,
                    (float) color.get("a").checkdouble()/255f
                );
                return NIL;
            }
        });

        lib.set("Rect", new VarArgFunction() {
			@Override
			public Varargs invoke(Varargs args) {
                float x = (float)args.arg(1).checkdouble();
                float y = (float)args.arg(2).checkdouble();
                float width = (float)args.arg(3).checkdouble();
                float height = (float)args.arg(4).checkdouble();
                renderer.drawRect(x, y, width, height);
                return NIL;
			}
        });

        lib.set("Line", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                float x1 = (float)args.arg(1).checkdouble();
                float y1 = (float)args.arg(2).checkdouble();
                float x2 = (float)args.arg(3).checkdouble();
                float y2 = (float)args.arg(4).checkdouble();
                float width = (float)args.arg(5).checkdouble();
                renderer.drawLine(x1, y1, x2, y2, width);
                return NIL;
            }
        });

        lib.set("Circle", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                float x = (float)args.arg(1).checkdouble();
                float y = (float)args.arg(2).checkdouble();
                float radius = (float)args.arg(3).checkdouble();
                renderer.drawCircle(x, y, radius);
                return NIL;
            }
        });

        lib.set("Sprite", new VarArgFunction() {
			@Override
			public Varargs invoke(Varargs args) {
                float x = (float)args.arg(2).checkdouble();
                float y = (float)args.arg(3).checkdouble();
                String assetName = args.arg(1).checkjstring();
                float width = (float)args.arg(4).checkdouble();
                float height = (float)args.arg(5).checkdouble();
                float rotation = 0;
                if (args.arg(6).isnumber()) {
                    rotation = (float) args.arg(6).checkdouble();
                }
                float originX = width/2;
                float originY = height/2;
                if (!args.arg(7).isnil()) {
                    originX = (float)args.arg(7).checkdouble();
                }
                if (!args.arg(8).isnil()) {
                    originY = (float)args.arg(8).checkdouble();
                }
                float alpha = 1f;
                if (!args.arg(9).isnil()) {
                    alpha = (float)args.arg(9).checkdouble();
                }
                renderer.drawSprite(assetName, x, y, width, height, rotation, originX, originY, alpha);
                return NIL;
			}
        });

        lib.set("Text", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                String text = args.checkjstring(1);
                String font = args.checkjstring(2);
                float size = (float)args.checkdouble(3);
                float x = (float)args.checkdouble(4);
                float y = (float)args.checkdouble(5);
                float r = (float)args.checkdouble(6);
                float g = (float)args.checkdouble(7);
                float b = (float)args.checkdouble(8);
                float a = (float)args.checkdouble(9);
                boolean centered = true;
                if (!args.isnil(10) && !args.checkboolean(10)) {
                    centered = false;
                }
                renderer.drawText(text, font, size, x, y, r, g, b, a, centered);
                return NIL;
            }
        });

        lib.set("UIScreenWidth", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(renderer.uiViewport.getWorldWidth());
            }
        });

        lib.set("UIScreenHeight", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(renderer.uiViewport.getWorldHeight());
            }
        });
        lib.set("UIScreenLeft", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(renderer.getUILeft());
            }
        });
        lib.set("UIScreenBottom", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(renderer.getUIBottom());
            }
        });
        lib.set("UIScreenRight", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(renderer.getUILeft() + renderer.uiViewport.getWorldWidth());
            }
        });
        lib.set("UIScreenTop", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return valueOf(renderer.getUIBottom() + renderer.uiViewport.getWorldHeight());
            }
        });
        lib.set("ProjectWorldToScreen", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1) {
                Vector2 vec = renderer.worldViewport.project(new Vector2((float)arg1.get("x").checkdouble(), (float)arg1.get("y").checkdouble()));
                return system.luaTypes.createVector(vec.x, vec.y);
            }
        });
        lib.set("ProjectUIToScreen", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1) {
                Vector2 vec = renderer.uiViewport.project(new Vector2((float)arg1.get("x").checkdouble(), (float)arg1.get("y").checkdouble()));
                return system.luaTypes.createVector(vec.x, vec.y);
            }
        });
        lib.set("SetShader", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1) {
                renderer.setShader(arg1.checkjstring());
                return NIL;
            }
        });
        lib.set("SetShaderUniform", new VarArgFunction() {
            @Override
            public LuaValue invoke(Varargs arg) {
                if (arg.isnil(3)) {
                    if (arg.isnumber(2)) {
                        renderer.setShaderUniform(arg.checkjstring(1), (float) arg.checkdouble(2));
                    } else {
                        renderer.setShaderUniform(arg.checkjstring(1), (float) arg.checktable(2).get("x").checkdouble(), (float) arg.checktable(2).get("y").checkdouble());
                    }
                } else {
                    renderer.setShaderUniform(arg.checkjstring(1), (float) arg.checkdouble(2), (float) arg.checkdouble(3));
                }
                return NIL;
            }
        });
        lib.set("ResetShader", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                renderer.resetShader();
                return NIL;
            }
        });
        return "draw";
    }
}
