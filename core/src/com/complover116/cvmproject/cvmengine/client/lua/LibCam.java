package com.complover116.cvmproject.cvmengine.client.lua;

import com.complover116.cvmproject.cvmengine.client.Renderer;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaLib;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaTypes;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

class LibCam extends LuaLib {
    Renderer renderer;
    LibCam(LuaSystem system, Renderer renderer) {
        super(system);
        this.renderer = renderer;
    }
    @Override
    protected String setUp(LuaTable lib) {
        lib.set("SetPos", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue vec) {
                renderer.setCamPos((float)vec.get("x").checkdouble(), (float)vec.get("y").checkdouble());
                return NIL;
			}
        });
        lib.set("SetViewSize", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue x, LuaValue y) {
                renderer.setCamViewSize((float)x.checkdouble(), (float)y.checkdouble());
                return NIL;
			}
        });
        lib.set("GetViewSize", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return system.luaTypes.createVector(renderer.getCamWidth(), renderer.getCamHeight());
            }
        });
        lib.set("GetActualViewSize", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return system.luaTypes.createVector(renderer.getActualCamWidth(), renderer.getActualCamHeight());
            }
        });
        lib.set("GetPos", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return system.luaTypes.createVector(renderer.getCamX(), renderer.getCamY());
            }
        });
        return "cam";
    }
}
