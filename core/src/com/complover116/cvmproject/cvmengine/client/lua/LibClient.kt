package com.complover116.cvmproject.cvmengine.client.lua

import com.badlogic.gdx.Gdx
import com.complover116.cvmproject.cvmengine.client.CVMControl
import com.complover116.cvmproject.cvmengine.shared.lua.LuaLib
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem
import com.complover116.cvmproject.cvmengine.shared.net.NetRemoteDestination
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.OneArgFunction
import org.luaj.vm2.lib.TwoArgFunction
import org.luaj.vm2.lib.ZeroArgFunction

class LibClient(system: LuaSystem, val engine: CVMControl) : LuaLib(system) {
    override fun setUp(table: LuaTable): String {
        table["StartListenServer"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                engine.localhost(arg1.checkint(), arg2.checktable())
                return NIL
            }
        }
        table["ConnectUDP"] = object : TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                engine.connect(NetRemoteDestination.getUDPSimple(arg1.checkjstring(), arg2.checkint()))
                return NIL
            }
        }
        table["IsConnected"] = object : ZeroArgFunction() {
            override fun call(): LuaValue {
                return valueOf(engine.netClient != null && engine.netClient.ready)
            }
        }
        table["StopListenServer"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                engine.listenServer.stopAndWait()
                return NIL
            }
        }
        table["Disconnect"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                engine.disconnect(false, null)
                return NIL
            }
        }
        table["Exit"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                Gdx.app.exit()
                return NIL
            }
        }
        table["WriteClientData"] = object: TwoArgFunction() {
            override fun call(arg1: LuaValue, arg2: LuaValue): LuaValue {
                return LuaValue.valueOf(engine.clientDataProvider.writeLuaValue(arg1.checkjstring(), arg2))
            }
        }
        table["CommitClientData"] = object: ZeroArgFunction() {
            override fun call(): LuaValue {
                engine.clientDataProvider.commit()
                return NIL
            }
        }
        table["ReadClientData"] = object : OneArgFunction() {
            override fun call(arg1: LuaValue): LuaValue {
                return engine.clientDataProvider.readLuaValue(arg1.checkjstring())
            }
        }
        return "client"
    }
}
