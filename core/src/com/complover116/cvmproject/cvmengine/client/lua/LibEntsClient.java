package com.complover116.cvmproject.cvmengine.client.lua;

import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.client.world.EntityClient;
import com.complover116.cvmproject.cvmengine.shared.lua.LibEnts;
import com.complover116.cvmproject.cvmengine.shared.world.Entity;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;

public class LibEntsClient extends LibEnts {
    CVMControl engine;
    LibEntsClient(LuaSystemClient system, CVMControl engine) {
        super(system, engine.world);
        this.engine = engine;
    }

    @Override
    protected String setUp(LuaTable lib) {
        super.setUp(lib);
        lib.set("SpawnClientside", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue ent) {
                engine.world.registerNonNetworkedEntity((EntityClient) CoerceLuaToJava.coerce(ent.get("ENT"), Entity.class));
                return NIL;
            }
        });
        return "ents";
    }
}
