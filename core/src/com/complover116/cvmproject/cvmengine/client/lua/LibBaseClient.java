package com.complover116.cvmproject.cvmengine.client.lua;

import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.shared.lua.LibBase;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ZeroArgFunction;


class LibBaseClient extends LibBase {
    CVMControl engine;
    LibBaseClient(LuaSystem system, CVMControl engine) {
        super(system, engine.world);
        this.engine = engine;
    }
    @Override
    protected String setUp(LuaTable lib) {
        super.setUp(lib);
        lib.set("LocalPlayer", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                if (engine.world == null) return NIL;
                while (!engine.world.hasPlayer(engine.world.localPlayerID)) {
                    Thread.yield();
                }
                return engine.world.getPlayerByID(engine.world.localPlayerID).getRepr();
			}
        });
        return "base";
    }
}
