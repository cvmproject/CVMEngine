package com.complover116.cvmproject.cvmengine.client.lua;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.complover116.cvmproject.cvmengine.client.CVMControl;
import com.complover116.cvmproject.cvmengine.client.world.EntitySystemClient;
import com.complover116.cvmproject.cvmengine.shared.lua.LibConsole;
import com.complover116.cvmproject.cvmengine.shared.lua.LibPlayer;
import com.complover116.cvmproject.cvmengine.shared.lua.LibSound;
import com.complover116.cvmproject.cvmengine.shared.lua.LuaSystem;
import org.luaj.vm2.LuaValue;

public class LuaSystemClient extends LuaSystem {

    public LibDraw libDraw;
    public LibCam libCam;
    public LibNetClient libNetClient;
    public LibInputClient libInputClient;
    public LibClient libClient;

    public LibSteam libSteam;

    @Override
    public boolean isServer() {
        return false;
    }

    @Override
    public boolean isClient() {
        return true;
    }

    public LuaSystemClient() {
        super();
        if (Gdx.app != null) {
            globals.set("ANDROID", LuaValue.valueOf(Gdx.app.getType() == Application.ApplicationType.Android));
            globals.set("PC", LuaValue.valueOf(Gdx.app.getType() == Application.ApplicationType.Desktop));
        }
        entitySystem = new EntitySystemClient(this);
    }

    public void initClientLibs(CVMControl engine, boolean worldOnly) {
        if (!worldOnly) {
            libDraw = new LibDraw(this, engine.renderer);
            libInputClient = new LibInputClient(this, engine);
            libNetClient = new LibNetClient(this, engine);
            libCam = new LibCam(this, engine.renderer);
            libConsole = new LibConsole(this, engine.commandProcessor);
            libClient = new LibClient(this, engine);
            libSteam = new LibSteam(this, engine);
        }
        libBase = new LibBaseClient(this, engine);
        libEnts = new LibEntsClient(this, engine);
        libPlayer = new LibPlayer(this, engine.world);
        libSound = new LibSound(this, engine.world, engine.soundManager);
    }
}
