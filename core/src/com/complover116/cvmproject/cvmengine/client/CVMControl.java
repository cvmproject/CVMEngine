package com.complover116.cvmproject.cvmengine.client;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.complover116.cvmproject.cvmengine.client.lua.LuaSystemClient;
import com.complover116.cvmproject.cvmengine.client.net.NetClient;
import com.complover116.cvmproject.cvmengine.client.ui.UIDebugOverlay;
import com.complover116.cvmproject.cvmengine.client.ui.UIDebugPointer;
import com.complover116.cvmproject.cvmengine.client.world.WorldClient;
import com.complover116.cvmproject.cvmengine.server.Server;
import com.complover116.cvmproject.cvmengine.shared.*;
import com.complover116.cvmproject.cvmengine.shared.net.NetDataChunk;
import com.complover116.cvmproject.cvmengine.shared.net.NetReceiver;
import com.complover116.cvmproject.cvmengine.shared.net.NetRemoteDestination;
import com.complover116.cvmproject.cvmengine.shared.world.DataProvider;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import java.io.IOException;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.UUID;

public class CVMControl extends Game {

    public Renderer renderer;

	public ISteamAPIProxy steamAPI;

	private ISteamAPIServiceProvider steamAPIServiceProvider;
    public ResourceManager resourceManager;
	public SoundManager soundManager;
	public CommandProcessor commandProcessor;
	public ModuleManager moduleManager;
	public LuaSystemClient luaSystem;
	public NetClient netClient;
	public Server listenServer;
	public UIDebugOverlay debugOverlay;
	public UIDebugPointer debugPointer = new UIDebugPointer();
	public WorldClient world;
	public DataProvider clientDataProvider;
	private DevConsoleConnector devConsoleConnector;
	public final LinkedList<NetDataChunk> inboundQueue = new LinkedList<>();
	
	public final Utility.AveragedTimer netTime = new Utility.AveragedTimer();
	public final Utility.AveragedTimer updateTime = new Utility.AveragedTimer();
	public final Utility.AveragedTimer drawTime = new Utility.AveragedTimer();

	public long lastProcessedKeyStateByRemote = 0;

	public RunningScreen runningScreen;
	public LoadingScreen loadingScreen;

	public boolean isEditing = false;
	public boolean isPaused = false;

	public long lastUpdate = System.nanoTime();

	private final boolean startInFullscreen;

	final ModuleManager.ModuleManagerParams moduleManagerParams;
	public CVMControl(ModuleManager.ModuleManagerParams moduleManagerParams, boolean startInFullscreen, ISteamAPIServiceProvider steamAPIServiceProvider) {
		this.steamAPIServiceProvider = steamAPIServiceProvider;
		this.moduleManagerParams = moduleManagerParams;
		this.startInFullscreen = startInFullscreen;
		commandProcessor = new ClientCommandProcessor(this);
	}
	@Override
	public void create () {
		Graphics.DisplayMode mode = Gdx.graphics.getDisplayMode();
		Gdx.graphics.setForegroundFPS(mode.refreshRate);
		if (startInFullscreen) {
			Gdx.graphics.setFullscreenMode(mode);
			Gdx.graphics.setVSync(false);
		}
		devConsoleConnector = new DevConsoleConnector(20483, commandProcessor);
		Logger.hook(devConsoleConnector);
		Logger.log(Logger.LogType.INTERACT, "<LOADING_STATUS_UPDATE>--->ENGINE_INIT");
		Thread.currentThread().setName("Client Thread");
		Thread.currentThread().setUncaughtExceptionHandler(Logger.globalExceptionHandler);
		Logger.log(Logger.LogType.INFO, "Starting CVMEngine "+Config.INSTANCE.getEngineVersion());
		Gdx.graphics.setTitle("CVMEngine "+Config.INSTANCE.getEngineVersion());
		Logger.log(Logger.LogType.INFO, "Game directory: "+Gdx.files.getLocalStoragePath());
		debugOverlay = new UIDebugOverlay();
		renderer = new Renderer(this);
		loadingScreen = new LoadingScreen(this);
		runningScreen = new RunningScreen(this);
		steamAPI = steamAPIServiceProvider.initSteamService(this);
		String cmdLine = steamAPI.getLaunchCommandLine();
		if (cmdLine == null || cmdLine.isEmpty()) {
			queueFullReload(true, null);
			showLoadingScreen();
		} else {
			queueFullReload(false, null);
			connect(new NetRemoteDestination.SteamIdDestination(Long.parseLong(cmdLine), steamAPI, NetReceiver.SERVER_CHANNEL));
		}
	}

	public void downloadModules() {
		loadingScreen.toLoad.add(0, netClient.moduleDownloader);
	}

	public void queueFullReload(boolean callGameStart, ModuleManager.ModuleManagerParams replacementParams) {
		luaSystem = new LuaSystemClient();
		if (soundManager != null) {
			// TODO: Shutdown sound manager, culling all running sounds
		}
		if (replacementParams == null)
			replacementParams = moduleManagerParams;
		moduleManager = new ModuleManager(luaSystem, replacementParams);
		if (resourceManager != null)
			resourceManager.dispose();
		clientDataProvider = new DataProvider(moduleManager.VFS.dataDir, null, "client-data", luaSystem);
		if (clientDataProvider.readLuaValue("identity").isnil()) {
			LuaTable table = new LuaTable();
			table.set("permanentName", LuaValue.valueOf(UUID.randomUUID().toString()));
			table.set("username", LuaValue.valueOf("Anon"));
			clientDataProvider.writeLuaValue("identity", table);
			clientDataProvider.commit();
		}
		resourceManager = new ResourceManager(moduleManager.VFS);
		soundManager = new SoundManager(this);
		luaSystem.initClientLibs(this, false);
		if (callGameStart) {
			loadingScreen.toLoad.add(new ILoadable() {
				private int framesWaited = 0;
				private final int framesToWait = 10;

				@Override
				public String getLoadStep() {
					return String.format(Locale.ENGLISH, "Initializing [%d/%d]", framesWaited, framesToWait);
				}

				@Override
				public String getLoadStepLow() {
					return "WAITING";
				}

				@Override
				public String getLoadStepHigh() {
					return "Starting game";
				}

				@Override
				public boolean loadTick() {
					framesWaited++;
					if (framesWaited >= framesToWait) {
						luaSystem.libHook.call("GameStart");
						return true;
					}
					return false;
				}
			});
		}
		loadingScreen.toLoad.add(0, resourceManager);
		loadingScreen.toLoad.add(0, luaSystem.entitySystem);
		loadingScreen.toLoad.add(0, moduleManager);
	}
	public void showLoadingScreen() {
		this.setScreen(loadingScreen);
	}

	@Override
	public void render () {
		lastUpdate = System.nanoTime();
		steamAPI.runCallbacks();
		renderer.spriteBatch.begin();
		super.render();
		renderer.spriteBatch.end();
	}

	public void errorAndReset(String error) {
		loadingScreen.toLoad.clear();
		loadingScreen.toLoad.add(new ILoadable() {
			final long startTime = System.nanoTime();

			@Override
			public String getLoadStep() {
				return error;
			}

			@Override
			public String getLoadStepLow() {
				return "Returning to main menu...";
			}

			@Override
			public String getLoadStepHigh() {
				return "Connection FAILED";
			}

			@Override
			public boolean loadTick() {
				return System.nanoTime() > startTime + 2000000000;
			}
		});
		disconnect(false, null);
	}

	@Override
	public void dispose () {
		super.dispose();
		renderer.dispose();
		Logger.log(Logger.LogType.ERROR, "ENGINE SHUTTING DOWN");
		Logger.log(Logger.LogType.INTERACT, "<ENGINE_SHUTDOWN>--->");
		if (netClient != null) {
			netClient.stopAndWait();
		}
		if(listenServer != null)
			try {
				listenServer.stopAndWait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		steamAPI.dispose();
		Logger.exit();
	}

	@Override
	public void resume() {
		super.resume();
		Logger.log(Logger.LogType.INFO, "Resuming main thread");
		if (resourceManager != null)
			resourceManager.dispose();
		isPaused = false;
		resourceManager = new ResourceManager(moduleManager.VFS);
		loadingScreen.toLoad.add(resourceManager);
		showLoadingScreen();
	}

	@Override
	public void pause() {
		super.pause();
		Logger.log(Logger.LogType.INFO, "Suspending main thread");
		isPaused = true;
	}

	@Override
    public void resize(int width, int height) {
		super.resize(width, height);
	    renderer.resizeWindow(width, height);
    }

    public boolean connect(NetRemoteDestination destination) {
		renderer.setCamPos(0, 0);
		if (world != null) {
			Logger.log(Logger.LogType.WARN, "Attempted to connect while a world is already active, disconnecting");
			disconnect(true, destination);
			return true;
		}
		CVMControl engine = this;
		try {
			engine.inboundQueue.clear();
			netClient = new NetClient(destination, engine);
		} catch (IOException e) {
			Logger.log(Logger.LogType.ERROR, "Unable to connect to "+destination.toString()+" - "+e.getMessage());
		}
		isEditing = false;
		loadingScreen.toLoad.add(netClient);
		showLoadingScreen();
		return true;
	}

	public boolean localhost(int port, LuaTable serverParams) {
		isEditing = false;
		try {
			listenServer = new Server(port, moduleManagerParams, serverParams, steamAPI);
		} catch (IOException e) {
			Logger.log(Logger.LogType.ERROR, "Unable to host on port "+port+" - "+e.getMessage());
			return false;
		}
		listenServer.start();
		loadingScreen.toLoad.add(new ILoadable() {
			int frameSkip = 2000;
			@Override
			public String getLoadStep() {
				return String.format(Locale.ENGLISH, "Stabilizing tickrate: %.2f", listenServer.realTPS);
			}

			@Override
			public String getLoadStepLow() {
				return "";
			}

			@Override
			public String getLoadStepHigh() {
				return "Starting local game server...";
			}

			@Override
			public boolean loadTick() {
				frameSkip--;
				return listenServer.netServer.isReady() && frameSkip < 0;
			}
		});
		return connect(NetRemoteDestination.Companion.getUDPSimple("localhost", port));
	}

	public boolean disconnect(boolean reconnect, NetRemoteDestination reconnectDestination) {
		isEditing = false;
		resourceManager.stopAllMusic();
		NetRemoteDestination lastDestination = null;
		if (netClient != null && netClient.connection != null) {
			lastDestination = netClient.connection.remote;
			loadingScreen.toLoad.add(new ILoadable() {
				@Override
				public String getLoadStep() {
					return "";
				}

				@Override
				public String getLoadStepLow() {
					return "";
				}

				@Override
				public String getLoadStepHigh() {
					return "Disconnecting...";
				}

				@Override
				public boolean loadTick() {
					netClient.stopAndWait();
					world = null;
					netClient = null;
					inboundQueue.clear();
					return true;
				}
			});

		}

		if (!reconnect || lastDestination == null) {
			queueFullReload(true, null);
		} else {
			if (reconnectDestination == null) {
				reconnectDestination = lastDestination;
			}
			NetRemoteDestination finalReconnectDestination = reconnectDestination;
			loadingScreen.toLoad.add(new ILoadable() {
				final long startTime = System.nanoTime();
				@Override
				public String getLoadStep() {
					return "";
				}

				@Override
				public String getLoadStepLow() {
					return "";
				}

				@Override
				public String getLoadStepHigh() {
					return "Server is reloading...";
				}

				@Override
				public boolean loadTick() {
					if (System.nanoTime() > startTime + 2000000000) {
						CVMControl.this.connect(finalReconnectDestination);
						return true;
					}
					return false;
				}
			});
		}
		showLoadingScreen();
		return true;
	}

	public void setLerp(float lerp) {
		Config.INSTANCE.setClientInterpolationDelay(lerp);
		if (netClient != null && netClient.ready) {
			new NetDataChunk.ChunkPlayerOptions(Config.INSTANCE.getClientInterpolationDelay()).enqueue(netClient.connection, true);
		}
	}
}
