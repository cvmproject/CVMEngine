package com.complover116.cvmproject.cvmengine.client

import com.badlogic.gdx.math.Vector2
import com.complover116.cvmproject.cvmengine.client.world.EntityClient
import com.complover116.cvmproject.cvmengine.shared.Logger
import com.complover116.cvmproject.cvmengine.shared.SoundEvent
import com.complover116.cvmproject.cvmengine.shared.SoundEventReceiver
import java.util.LinkedList
import java.util.PriorityQueue

const val MAX_EVENT_HOLD_TIME = 10 // How long should the SoundManager keep hold of a soundEvent that it can't apply yet
const val SILENT_SOUND_CULL_DELAY = 2 // If a sound is silent for more than this time, the libGDX sound will be terminated to free up slots
const val SOUND_TIMEOUT_CULL_DELAY = 3 // Non-persistent sounds get culled after this delay
const val PERSISTENT_SOUND_TIMEOUT_CULL_DELAY = 10 // If a sound received no events for more than this time, the sound is considered stuck and will be culled

class SoundManager(private val engine: CVMControl) : SoundEventReceiver {

    init {
        Logger.log(Logger.LogType.INFO, "SoundManager initialized!")
    }
    private val eventQueue = PriorityQueue<SoundEvent>()
    private val unprocessedEventList = LinkedList<SoundEvent>()

    override fun receiveSoundEvent(soundEvent: SoundEvent) {
        eventQueue.add(soundEvent)
    }

    class CVMSound(val timeCreated: Double, val soundName: String, val targetEntityID: Int?, var position: Vector2?, val persistent: Boolean, val persistentID: Int? = null) {
        var volume = 1f
        var pitch = 1f
        var loudness = 1f
        var soundID: Long? = null
        var targetEntity: EntityClient? = null // This will get mapped during a tick
        var lastHadVolume = -6.0

        fun update(engine: CVMControl, curTime: Double): Boolean {

            val gdxSound = engine.resourceManager.getSound(soundName)
            val listenerPos = Vector2(engine.renderer.camX, engine.renderer.camY)
            if (gdxSound == null) {
                Logger.log(Logger.LogType.ERROR, "SoundManager: Failed creating gdx sound, sound file not found: $soundName")
                return true
            }

            if (targetEntity == null) {
                if (targetEntityID != null) {
                    val ent = engine.world.getEntityByID(targetEntityID)
                    if (ent != null) {
//                        Logger.log(Logger.LogType.DEBUG, "SoundManager: Attaching sound $persistentID:$soundName to entity $targetEntityID:${ent.type}")
                        targetEntity = ent as EntityClient
                    }
                }
            }
            if (targetEntity != null) {
                position = targetEntity!!.pos
            }
            val hearingDistance = engine.renderer.actualCamWidth/2*loudness;
            val finalVolume = if (position == null) {
                volume
            } else {
                val distance = listenerPos.dst(position)
                if (distance < hearingDistance) {
                    volume
                } else {
                    Math.max(0f, volume - (distance-hearingDistance) / hearingDistance)
                }
            }

            if (finalVolume > 0) {
                lastHadVolume = curTime
            }

            if (persistent && curTime > lastHadVolume + SILENT_SOUND_CULL_DELAY) {
                if (soundID != null) {
//                    Logger.log(Logger.LogType.DEBUG, "SoundManager: Culling GDX sound for $persistentID:$soundName")
                    gdxSound.stop(soundID!!)
                    soundID = null
                }
            } else if (soundID == null) {
                if (persistent && soundName.endsWith("_loop")) {
//                    Logger.log(Logger.LogType.DEBUG, "SoundManager: Spawning GDX sound for $persistentID:$soundName")
                    soundID = gdxSound.loop(finalVolume, pitch, 0f)
                } else if (curTime < lastHadVolume + SILENT_SOUND_CULL_DELAY) {
                    soundID = gdxSound.play(finalVolume, pitch, 0f)
                }
            }

            if (soundID != null) {
                if (position != null) {
                    val horizontalDistance = position!!.x - listenerPos.x
                    var pan = horizontalDistance / 100
                    if (pan > 1) pan = 1f
                    if (pan < -1) pan = -1f
                    gdxSound.setPan(soundID!!, pan, finalVolume)
                } else {
                    gdxSound.setVolume(soundID!!, finalVolume)
                }
                gdxSound.setPitch(soundID!!, pitch)
            }
            if (!persistent && curTime > timeCreated + SOUND_TIMEOUT_CULL_DELAY) {
                if (soundID != null) gdxSound.stop(soundID!!)
                return true
            }
            return false
        }

        fun destroy(engine: CVMControl) {
            val gdxSound = engine.resourceManager.getSound(soundName)
            if (gdxSound == null) {
                Logger.log(Logger.LogType.ERROR, "SoundManager: Failed destroying gdx sound, sound file not found: $soundName")
            }
            if (soundID != null)
                gdxSound.stop(soundID!!)
        }
    }

    private val soundList: LinkedList<CVMSound> = LinkedList()

    fun update(curTime: Double) {
        // STEP 1: Apply pending sound events
        while (!eventQueue.isEmpty() && eventQueue.peek().time <= curTime) {
            val event = eventQueue.poll()
            if (event.isNewSound && !event.isDeleting) {
                val sound = CVMSound(curTime, event.soundName!!, event.targetEntityID, event.soundPos, event.persistence, event.persistentID)
                if (event.soundVolume != null) {
                    sound.volume = event.soundVolume!!
                }
                if (event.soundPitch != null) {
                    sound.pitch = event.soundPitch!!
                }
                if (event.soundLoudness != null) {
                    sound.loudness = event.soundLoudness!!
                }
                soundList.add(sound)
            } else {
                val existingSound = soundList.find { it.persistentID == event.persistentID }
                if (existingSound == null) {
                    if (event.time < curTime - MAX_EVENT_HOLD_TIME) {
                        Logger.log(Logger.LogType.WARN, "SoundManager: Event timed out in queue for sound ID ${event.persistentID}")
                        continue
                    }
                    Logger.log(Logger.LogType.DEBUG, "SoundManager delaying SoundEvent, no sound with id ${event.persistentID} is playing")
                    unprocessedEventList.add(event)
                    continue
                }
                if (event.isDeleting) {
                    existingSound.destroy(engine)
                    soundList.remove(existingSound)
                }
                existingSound.volume = event.soundVolume?: existingSound.volume
                existingSound.pitch = event.soundPitch?: existingSound.pitch
                existingSound.position = event.soundPos?: existingSound.position
            }
        }
        eventQueue.addAll(unprocessedEventList)
        unprocessedEventList.clear()
        // STEP 2: Tick each sound
        soundList.removeIf {
            it.update(engine, curTime)
        }
    }

    fun getSoundCount(): Int {
        return soundList.size
    }
    fun getActiveSoundCount(): Int {
        return soundList.filter { it.soundID != null }.size
    }

    fun getSoundQueueLength(): Int {
        return eventQueue.size
    }
}
